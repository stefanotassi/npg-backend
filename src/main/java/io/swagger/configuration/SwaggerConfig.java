package io.swagger.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;


@Configuration
@EnableSwagger2 //Loads the spring beans required by the framework
@PropertySource("classpath:swagger.properties")
@javax.annotation.Generated(value = "class io.swagger.codegen.languages.SpringMVCServerCodegen", date = "2016-04-11T10:21:21.241Z")
public class SwaggerConfig {

    @SuppressWarnings("deprecation")
	@Bean
    ApiInfo apiInfo() {
        ApiInfo apiInfo = new ApiInfo(
        "Digital Personalization Guide API",
        "NewModis DPG",
        "0.1.0",
        "",
        "",
        "",
        "" );
        return apiInfo;
    }

    @Bean
    public Docket customImplementation(){
        return new Docket(DocumentationType.SWAGGER_2).apiInfo(apiInfo());
    }
    /*
    @Bean(name = "dozerBean")
    public DozerBeanMapperFactoryBean configDozer() throws java.io.IOException {
        DozerBeanMapperFactoryBean mapper = new DozerBeanMapperFactoryBean();
        org.springframework.core.io.Resource[] resources = new PathMatchingResourcePatternResolver().getResources("classpath*:META-INF/dozer/dozer-bean-mappings.xml");
        mapper.setMappingFiles(resources);
        return mapper;
    }
    */
}
