package io.swagger.api;


public class FileDTO {

   private String id;

   public String getFilename() {
	return filename;
}

public void setFilename(String filename) {
	this.filename = filename;
}

public String getContentType() {
	return contentType;
}

public void setContentType(String contentType) {
	this.contentType = contentType;
}

public Integer getContentLength() {
	return contentLength;
}

public void setContentLength(Integer contentLength) {
	this.contentLength = contentLength;
}

public String getHash() {
	return hash;
}

public void setHash(String hash) {
	this.hash = hash;
}

public String getUrlCdn() {
	return urlCdn;
}

public void setUrlCdn(String urlCdn) {
	this.urlCdn = urlCdn;
}

public String getUrlCdnThumbnail() {
	return urlCdnThumbnail;
}

public void setUrlCdnThumbnail(String urlCdnThumbnail) {
	this.urlCdnThumbnail = urlCdnThumbnail;
}

public String getUrlS3() {
	return urlS3;
}

public void setUrlS3(String urlS3) {
	this.urlS3 = urlS3;
}

public String getId() {
	return id;
}

public void setId(String id) {
	this.id = id;
}

private String filename;

   private String contentType;

   private Integer contentLength;

   private String hash;

   private String urlCdn;

   private String urlCdnThumbnail;

   private String urlS3;

}
