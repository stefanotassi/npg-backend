package io.swagger.api.utils;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

public class UnixTimestampDeserializer extends JsonDeserializer<Date> {

	 public UnixTimestampDeserializer() {
	  }



	@Override
	public Date deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JsonProcessingException {
	    final JsonToken token = p.getCurrentToken();
	    if (token == JsonToken.VALUE_STRING) {
	      final String str = p.getText().trim();
	      return toDate(Long.parseLong(str));
	    } else if (token == JsonToken.VALUE_NUMBER_INT) {
	      return toDate(p.getLongValue());
	    }
	    throw ctxt.wrongTokenException(p, JsonToken.VALUE_STRING, "Expected a string or numeric value");
	}
	  private static Date toDate(long secondsSinceEpoch) {
			Calendar c = new GregorianCalendar(TimeZone.getTimeZone("UTC"));
		  	if (secondsSinceEpoch* 1000<4099680000000L && secondsSinceEpoch* 1000>-2207520000000L){
		  		//return new Date(secondsSinceEpoch * 1000);
		  		// c.setTimeInMillis(secondsSinceEpoch * 1000 +23*60*60*1000);	// Domenico 06.02.2018; ho testato con 50 date	  		
		  		c.setTimeInMillis(secondsSinceEpoch +23*60*60*1000);		  		
		  	}else{ 
		  		//return new Date(secondsSinceEpoch);
		  		c.setTimeInMillis(secondsSinceEpoch);
		  	}
		  	return c.getTime();
		  }
}