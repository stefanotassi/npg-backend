package io.swagger.api.utils;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;
import java.util.Date;

public class UnixTimestampSerializer extends  JsonSerializer<Date>{ 
		    @Override
	    public void serialize(Date date, JsonGenerator generator, SerializerProvider provider)
	            throws IOException, JsonProcessingException {
//	        String formattedDate = dateFormat.format(date);
	        generator.writeNumber(date.getTime());
	    }
}