package io.swagger.api.utils;

import java.util.Collection;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

import io.swagger.api.ValidationException;

public class Authorizer {
	public static void authorizeOnly(String role) throws ValidationException {
		if (!isUserInRole(role)){
			throw new ValidationException(-1000,"UNAUTHORIZED");
		}
	}
	public static boolean  isUserInRole(String role) {
        return isGranted(role);
    }
	private static boolean isGranted(String role) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        if ((auth == null) || (auth.getPrincipal() == null)) {
            return false;
        }

        Collection<? extends GrantedAuthority> authorities = auth.getAuthorities();

        if (authorities == null) {
            return false;
        }

        //This is the loop which do actual search
        for (GrantedAuthority grantedAuthority : authorities) {
            if (role.equals(grantedAuthority.getAuthority())) {
                return true;
            }
        }

        return false;
    }	
}
