package io.swagger.api;

public class ValidationException extends ApiException{

	private static final long serialVersionUID = -3615513165681590290L;
	public ValidationException(int code, String message){
		super(code,message);
	}
}
