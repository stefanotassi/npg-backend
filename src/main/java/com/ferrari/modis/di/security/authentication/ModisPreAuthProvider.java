package com.ferrari.modis.di.security.authentication;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationProvider;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;

import com.ferrari.modis.portal.ws.ProfileXML;
import com.ferrari.modis.security.authentication.ModisAuthenticationException;
import com.ferrari.modis.security.service.ProfilerService;


public class ModisPreAuthProvider  extends PreAuthenticatedAuthenticationProvider {
	//private static final Log logger = LogFactory.getLog(ModisPreAuthProvider.class);
	private static final Logger logger = LoggerFactory.getLogger(ModisPreAuthProvider.class);
	
	@Autowired
	ProfilerService profilerService;

	public ProfilerService getProfilerService() {
		return profilerService;
	}

	public void setProfilerService(ProfilerService profilerService) {
		this.profilerService = profilerService;
	}

	private String ambiente;
	
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		try{
			
//			logger.warn("Ambiente {}",this.ambiente);
			if (!supports(authentication.getClass())) {
				return null;
			}
	
			
			if ( "dev-spindox".equals(ambiente)) {
				// se dev --> impostare USER nell'header col valore del ruolo da testare
				String uname = authentication.getPrincipal().toString();
				String role;
				role = uname.toUpperCase();

				PreAuthenticatedAuthenticationToken result2 = new PreAuthenticatedAuthenticationToken(
						authentication.getPrincipal(), authentication.getCredentials(), 
						AuthorityUtils.createAuthorityList(role));
				result2.setDetails(authentication.getDetails());
				List<String> accRole = Arrays.asList("HQ-FULL","HQ-READ","AM-ABM","DEALER");
				//if (!("HQ-FULL".equals(role)) && !("HQ-READ".equals(role)) && !("AM-ABM".equals(role)) && !("DEALER".equals(role))) {
				if ( !accRole.contains(role)) {
					System.err.println("User ROLE not valid for User=["+uname+"] Role=["+role+"]");
					logger.debug("User ROLE not valid for User=["+uname+"] Role=["+role+"]");;
					throw new BadCredentialsException("User ROLE not valid");
				}

				return result2;
				
			}
			
			
			if (logger.isDebugEnabled()) {
				logger.warn("PreAuthenticated authentication request: " + authentication);
			}
	
			if (authentication.getPrincipal() == null) {
				logger.warn("No pre-authenticated principal found in request.");
				throw new BadCredentialsException("No pre-authenticated principal found in request.");
	
			}
			if (authentication.getCredentials() == null) {
				logger.warn("No pre-authenticated credentials found in request.");
				throw new BadCredentialsException("No pre-authenticated credentials found in request.");
			}
			String username=authentication.getPrincipal().toString();
			ProfileXML profile=profilerService.getUserProfile(username);
//			logger.warn("Profiel {}",profile);
		  	Collection<? extends GrantedAuthority> authorities = ModisAuthorityUtils.createAuthorities(profile,profilerService.getApplication());
	    	if (authorities.size()==0){
	    		logger.warn("No authorities");
				throw new BadCredentialsException(String.format("Application %s not authorized for user %s.",profilerService.getApplication(),username));
	    	}
	  		PreAuthenticatedAuthenticationToken result = new PreAuthenticatedAuthenticationToken(authentication.getPrincipal(), authentication.getCredentials(), authorities);
			result.setDetails(authentication.getDetails());
//			infoService.saveProfile(username, profile);
//			WebContextHolder context=WebContextHolder.getInstance();
//			HttpSession session = context.getSession(false);
//			if (session!=null){ 
//				session.invalidate();
//			}
//			logger.warn("Result {}",result);
			return result;
		}catch(Exception e){
			logger.error("exxret ",e);
			throw new ModisAuthenticationException(e.getMessage(),e);
		}
	}

	public String getAmbiente() {
		return ambiente;
	}

	public void setAmbiente(String ambiente) {
		this.ambiente = ambiente;
	}


}