package com.ferrari.modis.di.security.authentication;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;

import com.ferrari.modis.portal.ws.ApplicationXML;
import com.ferrari.modis.portal.ws.ProfileXML;
import com.ferrari.modis.portal.ws.RoleXML;


public class ModisAuthorityUtils {
	  private static final List<GrantedAuthority> ADMIN_ROLES = AuthorityUtils.createAuthorityList("ROLE_ADMIN", "ROLE_USER");
	  private static final List<GrantedAuthority> USER_ROLES = AuthorityUtils.createAuthorityList("ROLE_USER");

	    public static Collection<? extends GrantedAuthority> createAuthorities(ProfileXML profile,String application) {
	    	List<String> roles=new ArrayList<String>();
	    	if (profile.getUserProfile().getApplicationList()!=null)
	    	for (ApplicationXML app: profile.getUserProfile().getApplicationList().getApplication()){
	    		if (app.getName().equals(application)){
	    			for (RoleXML role:app.getRoleList().getRole()){
	    				roles.add(role.getId());
	    			}
	    			break;
	    		}
	    	}
	    	SimpleDateFormat dt = new SimpleDateFormat("yyyy-mm-dd-hh-mm-ss");
	    	roles.add(String.format("#$%s_%s",profile.getUserInfo().getUsername(),dt.format(new Date())));
	    	return AuthorityUtils.createAuthorityList(roles.toArray(new String[roles.size()]));
	    }
	    public static Collection<? extends GrantedAuthority> createAuthorities(String username) {
	        if (username.startsWith("admin")) {
	            return ADMIN_ROLES;
	        }
	        return USER_ROLES;
	    }
	    public static Collection<? extends GrantedAuthority> createAuthorities(ProfileXML profile) {
	        if (profile.getUserInfo().getUsername().startsWith("admin")) {
	            return ADMIN_ROLES;
	        }
	        return USER_ROLES;
	    }

	    private ModisAuthorityUtils() {
	    }
}
