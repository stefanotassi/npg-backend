package com.ferrari.modis.di.security.authentication;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.ferrari.modis.security.service.ProfilerService;

public class ModisPreAuthUserDetailService implements UserDetailsService{

	
	private static final Logger log = LoggerFactory.getLogger(ModisPreAuthUserDetailService.class);
	
	@Autowired
	ProfilerService profilerService;
	public UserDetails loadUserByUsername(String username)
	        throws UsernameNotFoundException
    {
//		ProfileXML profile=profilerService.getUserProfileByApplication(username);
//		List<String> groups=new ArrayList<String>();
//		for (ApplicationXML app:profile.getUserProfile().getApplicationList().getApplication()){
//			for (RoleXML role:app.getRoleList().getRole()){
//				groups.add(role.getId());
//			}
//		}
		log.warn("username recieved :: " + username);
//		UserDetails user = new User(username, "password",AuthorityUtils.createAuthorityList(groups.toArray(new String[groups.size()])));
//		return user;
		
		log.warn("*** assegnazione ruolo in ambiente DEV ****");
		UserDetails user = new User(username, "password",AuthorityUtils.createAuthorityList("HQ-FULL"));
		return user;
    }
}
