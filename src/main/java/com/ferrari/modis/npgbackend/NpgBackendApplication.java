package com.ferrari.modis.npgbackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.WebApplicationInitializer;

import com.ferrari.npg.media.client.MediaClientManual;

@SpringBootApplication
@EnableJpaRepositories(basePackages = {"com.ferrari.modis.dpg.repository"})
@ComponentScan(basePackages= {
		"com.ferrari.modis.dpg.repository",
		"com.ferrari.modis.dpg",
		"com.ferrari.modis.security",
		"com.ferrari.modis.npgbackend",
		"com.ferrari.npg.client",
		"io.swagger.configuration",
		"com.ferrari.modis.di.security.authentication"
},basePackageClasses = {MediaClientManual.class})
@EntityScan(basePackages = {
		"com.ferrari.modis.dpg.model",
})
public class NpgBackendApplication extends SpringBootServletInitializer implements WebApplicationInitializer {

	public static void main(String[] args) {
		SpringApplication.run(NpgBackendApplication.class, args);
		
		//RequestHeaderAuthenticationFilter
		//FilterRegistrationBean
		//FilterChainProxy
		//SecurityContextPersistenceFilter 
		//SecurityContextPersistenceFilter 
	}

}
