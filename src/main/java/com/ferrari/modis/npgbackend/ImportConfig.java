package com.ferrari.modis.npgbackend;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;

@Configuration
@Order(Ordered.HIGHEST_PRECEDENCE)
@PropertySources(value = {
		@PropertySource("${ConfigNPGBackendPath:file:///C:/dev/prop}/application.properties")
})
public class ImportConfig {

}
