package com.ferrari.modis.npgbackend;

import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.cloud.netflix.feign.ribbon.CachingSpringLoadBalancerFactory;
import org.springframework.cloud.netflix.feign.ribbon.ContextAwareLoadBalancerFeignClient;
import org.springframework.cloud.netflix.ribbon.SpringClientFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.annotation.Order;

import feign.Client;

@Configuration
@EnableEurekaClient
@Order(Integer.MIN_VALUE)
@EnableFeignClients(basePackages = "com.ferrari.modis.npgbackend.client")
public class EurekaConfig {

	
    @Primary
    @Bean
    public Client feignClient(CachingSpringLoadBalancerFactory cachingFactory,
                              SpringClientFactory clientFactory) {
        return new ContextAwareLoadBalancerFeignClient(new Client.Default(null, null),
                cachingFactory, clientFactory);
    }

	
}
