package com.ferrari.modis.npgbackend;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsByNameServiceWrapper;
import org.springframework.security.web.authentication.preauth.AbstractPreAuthenticatedProcessingFilter;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;
import org.springframework.security.web.authentication.preauth.RequestHeaderAuthenticationFilter;

import com.ferrari.modis.di.security.authentication.ModisPreAuthUserDetailService;

@Configuration
@ImportResource({"classpath*:META-INF/spring/applicationContext.xml"})
@EnableWebSecurity
public class ConfigXml extends WebSecurityConfigurerAdapter{

//	@Bean
//	public com.ferrari.modis.security.service.impl.ProfilerServiceSoap profileServeice(){
//		ProfilerServiceSoap ret = new com.ferrari.modis.security.service.impl.ProfilerServiceSoap();
//		
//		
//		return ret;
//	}
	
	
	private String wsApplication; 
    private String wsEndpoint;       
    private String wsUsername;  
    private String wsPassword;   
    private String ambiente;
	
	public ConfigXml(@Value("${ambiente:dev-spindox}")String ambiente,
			@Value("${ws-application}")String wsApplication,
			@Value("${ws-endpoint}")String wsEndpoint,
			@Value("${ws-username}")String wsUsername,
			@Value("${ws-password}")String wsPassword
			) {
		this.ambiente = ambiente;
		this.wsApplication = wsApplication;
		
		this.wsEndpoint = wsEndpoint;
		this.wsPassword = wsPassword;
		this.wsUsername = wsUsername;
	}
	
	@Bean
	public org.springframework.security.web.authentication.preauth.RequestHeaderAuthenticationFilter ssoFilter() throws Exception{
		RequestHeaderAuthenticationFilter ret = new org.springframework.security.web.authentication.preauth.RequestHeaderAuthenticationFilter();
		/*<property name="principalRequestHeader" value="USER" />
		<property name="authenticationManager" ref="authenticationManager" />
		<property name="checkForPrincipalChanges" value="true"/>
		<property name="invalidateSessionOnPrincipalChange" value="true"/>
		*/

		ret.setPrincipalRequestHeader("USER");
		ret.setAuthenticationManager(authenticationManager());
		ret.setCheckForPrincipalChanges(true);
		ret.setInvalidateSessionOnPrincipalChange(true);
		return ret;
	}

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
        	.authenticationProvider(ModisPreAuthProvider())
        	.addFilterBefore(ssoFilter(), AbstractPreAuthenticatedProcessingFilter.class)
            .authorizeRequests()
            .anyRequest().authenticated()
            .and()
            .csrf().disable()
            .httpBasic().disable()
            .logout().disable();
    }
    
    @Override
    public void configure(WebSecurity web) {
    	web.ignoring().antMatchers("/v2/**"
    			,"/swagger-resources/**"
    			,"/api-docs"
    			,"/webjars/**","/swagger**",
    			"/docs/**","/truciolo/**","/swagger-ui.html*");
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(ModisPreAuthProvider());
    }

    
    public com.ferrari.modis.di.security.authentication.ModisPreAuthProvider ModisPreAuthProvider(){
    	com.ferrari.modis.di.security.authentication.ModisPreAuthProvider ret = new com.ferrari.modis.di.security.authentication.ModisPreAuthProvider();
    	ret.setPreAuthenticatedUserDetailsService(
    			new UserDetailsByNameServiceWrapper<PreAuthenticatedAuthenticationToken>
    	(new ModisPreAuthUserDetailService()));
    	ret.setProfilerService(ProfilerServiceSoap());
    	ret.setAmbiente(ambiente);
    	return ret;
    }
    
    
    @Bean
    public com.ferrari.modis.security.service.impl.ProfilerServiceSoap ProfilerServiceSoap(){
    	com.ferrari.modis.security.service.impl.ProfilerServiceSoap ret = new com.ferrari.modis.security.service.impl.ProfilerServiceSoap();
    	ret .setApplication(this.wsApplication);
    	ret.setEndpoint(this.wsEndpoint);
    	ret.setPassword(this.wsPassword);
    	ret.setUsername(this.wsUsername);
    	return ret;
    }
    
    
    

	
}
