package com.ferrari.modis.npgbackend.client;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.context.annotation.Profile;

import com.ferrari.npg.media.client.PdfClient;

@Profile("local")
@FeignClient(name = "${pdf.service.name:ms-pdf}", url = "${pdf.service.url}")
public interface LocalFeignPdfClient extends PdfClient {

}