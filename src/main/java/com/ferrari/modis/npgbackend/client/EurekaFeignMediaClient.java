package com.ferrari.modis.npgbackend.client;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.context.annotation.Profile;

import com.ferrari.npg.media.client.MediaClient;

@Profile("!local")
@FeignClient(name = "${media.service.name:ms-media}")
public interface EurekaFeignMediaClient extends MediaClient {

}