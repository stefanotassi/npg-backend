package com.ferrari.modis.commons.persistence.support;

import javax.persistence.spi.PersistenceUnitTransactionType;
import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.orm.jpa.persistenceunit.MutablePersistenceUnitInfo;
import org.springframework.orm.jpa.persistenceunit.PersistenceUnitPostProcessor;


public class JtaPersistenceUnitPostProcessor implements PersistenceUnitPostProcessor{
	@SuppressWarnings("unused")
	private static final Logger logger = LoggerFactory.getLogger( PersistenceUnitPostProcessor.class );
	private DataSource dataSource;
	private String transactionType;
	private String  unitName;
	@Override
	public void postProcessPersistenceUnitInfo(MutablePersistenceUnitInfo arg0) {
		arg0.setPersistenceUnitName(unitName);
		if (transactionType.equals("JTA")){
			arg0.setJtaDataSource(dataSource);
			arg0.setTransactionType(PersistenceUnitTransactionType.JTA);
		}else{
			arg0.setNonJtaDataSource(dataSource);
			arg0.setTransactionType(PersistenceUnitTransactionType.RESOURCE_LOCAL);
		}
		
	}
	public DataSource getDataSource() {
		return dataSource;
	}
	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}
	
	public String getUnitName() {
		return unitName;
	}
	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}
	public String getTransactionType() {
		return transactionType;
	}
	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}
	
}
