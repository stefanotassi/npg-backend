package com.ferrari.modis.dpg.commons.i18n;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;
import org.springframework.web.util.WebUtils;

import com.ferrari.modis.dpg.model.ui.UiDictionaryLang;



public class LocalizationHelper {
	

	
	
	@SuppressWarnings("unused")
	private static final Logger logger = LoggerFactory.getLogger( LocalizationHelper.class );
	public static final String LOCALE_SESSION_ATTRIBUTE_NAME = SessionLocaleResolver.class.getName() + ".LOCALE";

	public static String getNormalized(){
		return getNormalized(LocaleContextHolder.getLocale()).getValue();
	}
	public static String getLangCodeNormalized(){
		return LocaleContextHolder.getLocale().getLanguage().toUpperCase();
	}
	
	public static LocalizationType getNormalized(Locale locale){
		switch (locale.getLanguage()){
		case "it":
			return LocalizationType.IT_IT;
		
//		case "de":
//			return LocalizationType.DE_DE;
//		
//		case "es":
//			return LocalizationType.ES_ES;
//		
//		case "ja":
//			return LocalizationType.JA_JP;
//			
//		case "zh":
//			return LocalizationType.ZH_CN;
//		
//		case "fr":
//			return LocalizationType.FR_FR;
		}
		
		return LocalizationType.EN_US;
	}

	
	
	@SuppressWarnings("rawtypes")
	public static String getLocalized(List ds){
		Map<String,UiDictionaryLang> descriptions=new HashMap<String,UiDictionaryLang>();
		for (Object o:ds){
			UiDictionaryLang s=(UiDictionaryLang)o;
			descriptions.put(s.getLanguage(), s);
		}
		Locale locale = LocaleContextHolder.getLocale();
		return getLocalized(descriptions,locale);
	}

	public static String getLocalized(HashMap<String,UiDictionaryLang> ds){
		Map<String,UiDictionaryLang> descriptions=new HashMap<String,UiDictionaryLang>();
		for (String s:ds.keySet()){
			descriptions.put(s, ds.get(s));
		}
		Locale locale = LocaleContextHolder.getLocale();
		return getLocalized(descriptions,locale);
	}
	public static String getLocalized(Map<String,UiDictionaryLang> descriptions){
		Locale locale = LocaleContextHolder.getLocale();
		return getLocalized(descriptions,locale);
	}
	public static String getLocalized(Map<String,UiDictionaryLang> descriptions,Locale locale){
		String lang=getNormalized(locale).getValue();
	      return getLocalized(descriptions,lang);		
	}
	
	public static String getLocalized(Map<String,UiDictionaryLang> descriptions,LocalizationType localizationType){
		String lang=localizationType.getValue();
		//String lang=getNormalized(locale).getValue();
	      return getLocalized(descriptions,lang);		
	}

	
	
	
	
	public static String getLocalized(Map<String,UiDictionaryLang> descriptions,String lang){	
		if (descriptions.containsKey(lang)) {
            return descriptions.get(lang).getDescription();
        }
		//if (descriptions.get(LocalizationType.EN_EN.getValue())!=null) {
	    //    return descriptions.get(LocalizationType.EN_EN.getValue()).getText();
        // }
		if (descriptions.get(LocalizationType.EN_US.getValue())!=null) {
	        return descriptions.get(LocalizationType.EN_US.getValue()).getDescription();
         }
		if (descriptions.get(LocalizationType.IT_IT.getValue())!=null) {
	        return descriptions.get(LocalizationType.IT_IT.getValue()).getDescription();
         }
       return null;		
	}
	
	public static void setLocale(HttpServletRequest request, HttpServletResponse response, String locale){
		WebUtils.setSessionAttribute(request, LOCALE_SESSION_ATTRIBUTE_NAME, locale);
	}
	public static String getLocalized2(Map<String, String> descriptions) {
		Locale locale = LocaleContextHolder.getLocale();
		return getLocalized2(descriptions,locale);
	}
	private static String getLocalized2(Map<String, String> descriptions,
			Locale locale) {
		   String lang=getNormalized(locale).getValue();
	      return getLocalized2(descriptions,lang);		
  }
	private static String getLocalized2(Map<String, String> descriptions,
			String lang) {
		if (descriptions.containsKey(lang)) {
            return descriptions.get(lang);
        }
		//if (descriptions.get(LocalizationType.EN_EN.getValue())!=null) {
	    ///   return descriptions.get(LocalizationType.EN_EN.getValue());
        //}
		if (descriptions.get(LocalizationType.EN_US.getValue())!=null) {
	        return descriptions.get(LocalizationType.EN_US.getValue());
        }
		if (descriptions.get(LocalizationType.IT_IT.getValue())!=null) {
	        return descriptions.get(LocalizationType.IT_IT.getValue());
        }
       return null;		
	}
	
	
	
}
