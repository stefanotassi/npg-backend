package com.ferrari.modis.dpg.commons.i18n;

import java.util.HashMap;
import java.util.Map;


public enum DocumentType {
	 F("F")
	,O("O");
	 
	 public static final Map<String, DocumentType> documentTypes = new HashMap<>();
	 static {
       for (DocumentType value : values()) {
    	   documentTypes.put(value.type, value);
       }	
    }
	 
	private final String type;
	DocumentType(String type) { 
   	this.type = type; 
   }
   public String getValue() { return type; }
}