package com.ferrari.modis.dpg.commons.i18n;

import java.util.HashMap;
import java.util.Map;

public enum LocalizationType {
	  EN_US("en-US")
	, IT_IT("it-IT");

	public static final Map<String, LocalizationType> localizations = new HashMap<>();
	static {
		for (LocalizationType value : values()) {
			localizations.put(value.locale, value);
		}
	}

	private final String locale;

	LocalizationType(String locale) {
		this.locale = locale;
	}

	public String getValue() {
		return locale;
	}
}