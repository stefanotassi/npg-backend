package com.ferrari.modis.dpg.utils;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;

public class AuthenticationDTO {

	private Collection<? extends GrantedAuthority> Authorities;
	private boolean Authenticated;
	private Object principal;
//	Object credentials;
	
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return  Authorities;
	}
	public void setAuthorities(Collection<? extends GrantedAuthority> auth) {
		Authorities = auth;
	}
	
	boolean isAuthenticated() {
		return Authenticated;
	}
	public void setAuthenticated(boolean isAuthenticated) throws IllegalArgumentException{
		Authenticated = isAuthenticated;
	}
	public boolean getAuthenticated() {
		return isAuthenticated();
	}
	
	public Object getPrincipal() {
		return principal;
	}
	public void setPrincipal(Object p) {
		principal=p;
	}
	
	
	
}
