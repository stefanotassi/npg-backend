package com.ferrari.modis.dpg.utils;



public class ProfileXMLDTO {

	private UserInfo userInfo;
	
	public UserInfo getUserInfo() {
		return userInfo;
	}

	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}

	public static class UserInfo {
		private String username;
		private String dealerCode;
		public String getDealerCode() {
			return dealerCode;
		}

		public void setDealerCode(String dealerCode) {
			this.dealerCode = dealerCode;
		}

		private String language;

	

		public String getLanguage() {
			return language;
		}

		public void setLanguage(String language) {
			this.language = language;
		}

		public String getUsername() {
			return username;
		}

		public void setUsername(String username) {
			this.username = username;
		}
	}
	
}
