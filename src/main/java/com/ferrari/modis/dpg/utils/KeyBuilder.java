package com.ferrari.modis.dpg.utils;

import com.ferrari.npg.media.client.entity.ParameterDTO;

public interface KeyBuilder {
	ParameterDTO.ParameterDTOBuilder key(String k);

}
