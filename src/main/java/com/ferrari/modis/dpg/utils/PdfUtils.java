package com.ferrari.modis.dpg.utils;

import com.ferrari.npg.media.client.entity.ParameterDTO;
import com.ferrari.npg.media.client.entity.ParameterDTO.ParameterDTOBuilder;
import com.ferrari.npg.media.client.enumerate.ParameterType;

public class PdfUtils {

	private PdfUtils() {

	}

	public static KeyBuilder paramString() {
		return new KeyBuilder() {
			@Override
			public ParameterDTOBuilder key(String k) {
				return ParameterDTO.builder().type(ParameterType.STRING).key(k);
			}
		};
	}

	public static KeyBuilder paramImage() {
		return new KeyBuilder() {
			@Override
			public ParameterDTOBuilder key(String k) {
				return ParameterDTO.builder().type(ParameterType.IMAGE).key(k);
			}
		};
	}

	public static KeyBuilder paramTemplate() {
		return new KeyBuilder() {
			@Override
			public ParameterDTOBuilder key(String k) {
				return ParameterDTO.builder().type(ParameterType.TEMPLATE).key(k);
			}
		};
	}

	public static KeyBuilder paramSvg() {
		return new KeyBuilder() {
			@Override
			public ParameterDTOBuilder key(String k) {
				return ParameterDTO.builder().type(ParameterType.SVG).key(k);
			}
		};
	}

	public static KeyBuilder paramObj() {
		return new KeyBuilder() {
			@Override
			public ParameterDTOBuilder key(String k) {
				return ParameterDTO.builder().type(ParameterType.OBJECT).key(k);
			}
		};
	}

	public static KeyBuilder paramArray() {
		return new KeyBuilder() {
			@Override
			public ParameterDTOBuilder key(String k) {
				return ParameterDTO.builder().type(ParameterType.ARRAY).key(k);
			}
		};
	}
	public static KeyBuilder paramPdf() {
		return new KeyBuilder() {
			@Override
			public ParameterDTOBuilder key(String k) {
				return ParameterDTO.builder().type(ParameterType.PDF).key(k);
			}
		};
	}
}
