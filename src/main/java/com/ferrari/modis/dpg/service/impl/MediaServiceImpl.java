package com.ferrari.modis.dpg.service.impl;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.netflix.feign.ribbon.CachingSpringLoadBalancerFactory;
import org.springframework.cloud.netflix.ribbon.SpringClientFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.ferrari.modis.dpg.commons.i18n.LocalizationType;
import com.ferrari.modis.dpg.jentity.app.JMedia;
import com.ferrari.modis.dpg.jentity.app.JMediaType;
import com.ferrari.modis.dpg.jentity.base.JEntity;
import com.ferrari.modis.dpg.jentity.base.JPage;
import com.ferrari.modis.dpg.jentity.base.JPageAuth;
import com.ferrari.modis.dpg.jentity.base.JPageData;
import com.ferrari.modis.dpg.model.app.CfgFilesWhitelist;
import com.ferrari.modis.dpg.model.app.Media;
import com.ferrari.modis.dpg.model.app.MediaType;
import com.ferrari.modis.dpg.model.app.Model;
import com.ferrari.modis.dpg.model.app.Version;
import com.ferrari.modis.dpg.model.app.VersionAttc;
import com.ferrari.modis.dpg.model.ui.UiApplicationSetting;
import com.ferrari.modis.dpg.model.ui.UiPage;
import com.ferrari.modis.dpg.repository.CfgFilesWhitelistRepository;
import com.ferrari.modis.dpg.repository.MediaRepository;
import com.ferrari.modis.dpg.repository.MediaTypeRepository;
import com.ferrari.modis.dpg.repository.UiApplicationSettingRepository;
import com.ferrari.modis.dpg.repository.UiPageRepository;
import com.ferrari.modis.dpg.repository.VersionAttcRepository;
import com.ferrari.modis.dpg.repository.VersionOptRepository;
import com.ferrari.modis.dpg.repository.VersionRepository;
import com.ferrari.modis.dpg.service.CommonService;
import com.ferrari.modis.dpg.service.DocService;
import com.ferrari.modis.dpg.service.MediaService;
import com.ferrari.modis.dpg.service.ModelService;
import com.ferrari.modis.dpg.service.UiService;
import com.ferrari.npg.media.client.MediaClient;
import com.ferrari.npg.media.client.MediaClientManual;
import com.ferrari.npg.media.client.dto.ApiException;
import com.ferrari.npg.media.client.dto.ApiResponse;
import com.ferrari.npg.media.client.entity.ThronInfoDTO;
import com.ferrari.npg.media.client.enumerate.ResponseType;
import com.ferrari.npg.media.client.enumerate.StorageType;
import com.sun.jersey.api.client.ClientHandlerException;
import com.sun.jersey.api.client.UniformInterfaceException;

import feign.Response;
import io.swagger.api.ValidationException;

@Service
public class MediaServiceImpl implements MediaService {

	private static final Logger logger = LoggerFactory.getLogger(MediaServiceImpl.class);

	private static final String BASE_USER_CDN = "https://%s-cdn.thron.com/delivery/public/image/%s/%s/%s/std/%sx0/file.jpg";

	private String pkeyThronPrivate;
	
	public Media findMediaFile(String id) {

		return null;
	}

	@PostConstruct
	public void init() {
		UiApplicationSetting param = uiApplicationSettingRepository.findByKey("pkey-thronprivate");
		if (param != null) {
			pkeyThronPrivate = param.getValue();
		}
		
	}

	@Autowired
	UiApplicationSettingRepository uiApplicationSettingRepository;

	@Autowired
	MediaRepository mediaRepository;
	@Autowired
	MediaTypeRepository mediaTypeRepository;
	@Autowired
	UiPageRepository uiPageRepository;
	@Autowired
	VersionRepository versionRepository;

	@Autowired
	UiService uiService;

	@Autowired
	SpringClientFactory clientFactory;

	@Autowired
	CachingSpringLoadBalancerFactory cachingFactory;

	@Autowired
	private MediaClient mediaClient;

	@Autowired
	private MediaClientManual mediaClientManual;

	@Autowired
	private VersionAttcRepository versionAttcRepository;

	@Autowired
	CommonService commonService;

	@Autowired
	VersionOptRepository versionOptRepository;

	@Autowired
	CfgFilesWhitelistRepository cfgFilesWhitelistRepository;

	@Value(value = "${ambiente:dev-spindox}")
	private String ambiente;

	public JPage createMediaFile(JEntity jEntity, MultipartFile uploadFile, HttpServletRequest req)
			throws IOException, NoSuchAlgorithmException {

		logger.warn("ambiente {}", ambiente);
		
		
		String z = uiService.getAuthorizationById(UiService.UPLOAD);
		
		if ( !("Y".equals(z))) {
			throw new ValidationException(-1,"ACTION_NOT_ALLOWEED");
		}

		// Tira eccezzione in caso di errore
		validateUplaodFile(uploadFile);

		Media media = new Media();

//		if (BLACKLIST.contains(uploadFile.getContentType())) {
//			throw new ValidationException(-1, "MIMETYPE_NOT_ALLOWED_FOR_UPLOAD [" + uploadFile.getContentType() + "]");
//		}

		ApiResponse<com.ferrari.npg.media.client.entity.FileDTO> resp;
		try {
			resp = mediaClientManual.upload(uploadFile.getInputStream(), uploadFile.getOriginalFilename(),
					StorageType.THRON_PRIVATE, javax.ws.rs.core.MediaType.valueOf(uploadFile.getContentType()));
		} catch (IllegalArgumentException | ApiException | IOException e) {
			logger.error("Errror uploading a file", e);
			throw new ValidationException(-1, "UPLOAD_ERROR_USING_MEDIACLIENTMANUAL");
		}

		if ((resp.getStatusCode() != 201) && (resp.getStatusCode() != 200)) {
			logger.error("Errore in upload mediaClientManual - {}", resp.getStatusCode());
			throw new ValidationException(-1, "UPLOAD_ERROR_USING_MEDIACLIENTMANUAL");
		}

		com.ferrari.npg.media.client.entity.FileDTO fileDTO = resp.getData();

		media.setContentSize(uploadFile.getSize());
		media.setFilename(uploadFile.getOriginalFilename());
		media.setFilenameOrig(uploadFile.getOriginalFilename());
		media.setHostnameOrig(req.getServerName());

		media.setContentType(uploadFile.getContentType());
		media.setTrashable("N");
		media.setId(fileDTO.getId());
		media.setUrl(fileDTO.getUrlCdn());
		media.setActive("Y");
		ThronInfoDTO thronExraInfo = null;
		if (fileDTO.getExtraInfoThron() != null
				&& (thronExraInfo = fileDTO.getExtraInfoThron().get(StorageType.THRON_PRIVATE)) != null) {
			media.setIdThron(thronExraInfo.getThronId());
			media.setClienId(thronExraInfo.getClientId());
		}

		media = mediaRepository.save(media);

		// restituisco solo l'id
		JMedia jmedia = new JMedia();
		jmedia.setId(fileDTO.getId());

		JPage jpage = new JPage();
		JPageData jPageData = new JPageData();
		jPageData.setActual(jmedia);
		jpage.setData(jPageData);
		return jpage;
	}

	public JMedia getMediaTemplate() {
		JMedia jMedia = new JMedia();
		return jMedia;
	}

	public JPage getMediaParameters(LocalizationType language, String idMediaType) {
		MediaType mediaType = mediaTypeRepository.findById(idMediaType);
		JMediaType jMediaType = new JMediaType();
		jMediaType.setId(mediaType.getId());
		jMediaType.setMinx(mediaType.getMinx());
		jMediaType.setMiny(mediaType.getMiny());
		jMediaType.setRatiox(mediaType.getRatiox());
		jMediaType.setRatioy(mediaType.getRatioy());

		List<String> types = new ArrayList<String>();

		if (mediaType.getTypes() != null) {
			String[] aExt = mediaType.getTypes().split("\\|");

			for (int i = 0; i < aExt.length; i++) {
				types.add(aExt[i]);
			}

		}
		jMediaType.setTypes(types);

		JPage jpage = new JPage();
		JPageData jPageData = new JPageData();
		jPageData.setActual(jMediaType);
		// header JPage ------------------------------------------------------------
		UiPage uiPage = uiPageRepository.findByKey(UiService.PAGE_UPLOAD);
		jpage.setId(uiPage.getId());
		jpage.setName(uiPage.getDescription());
		jpage.setLanguage(language);

		// aggiunge lista autorizzazioni -------------------------------------------
		List<JPageAuth> lJPageAuth = uiService.getAuthorizationList(UiService.PAGE_UPLOAD);
		jpage.setAuths(lJPageAuth);

		jpage.setData(jPageData);

		return jpage;

	}

	public Boolean checkThron(String id, String template) {

		ResponseEntity<com.ferrari.npg.media.client.entity.FileDTO> resp = mediaClient.getInfoFile(id);
		if (!resp.getStatusCode().is2xxSuccessful()) {
			return Boolean.FALSE;
		}
		com.ferrari.npg.media.client.entity.FileDTO fileDTO = resp.getBody();
		if (fileDTO == null)
			return Boolean.FALSE;
		return Boolean.TRUE;
	}

	public String getThronUrl(String id, String template) {
		if (id == null)
			return null;

		Media media = mediaRepository.findById(id);
		if (media == null)
			return null;

		String url = media.getUrl();
		String thronId = media.getIdThron();
		String clientId = media.getClienId();
		if (StringUtils.isEmpty(url) || StringUtils.isEmpty(thronId) || StringUtils.isEmpty(clientId)) {

			ResponseEntity<com.ferrari.npg.media.client.entity.FileDTO> resp = mediaClient.getInfoFile(id);
			if (!resp.getStatusCode().is2xxSuccessful()) {
				throw new ValidationException(-1, "ERROR_USING_GET_THRON_URL");
			}
			com.ferrari.npg.media.client.entity.FileDTO fileDTO = resp.getBody();

			if (fileDTO.getUrlCdn() != null) {
				// salvo nuovi dati in tabella media
				media.setUrl(fileDTO.getUrlCdn());
				media.setContentType(fileDTO.getContentType());
				media.setContentSize(Long.valueOf(fileDTO.getContentLength()));
				ThronInfoDTO thronExraInfo = null;
				if (fileDTO.getExtraInfoThron() != null
						&& (thronExraInfo = fileDTO.getExtraInfoThron().get(StorageType.THRON_PRIVATE)) != null) {
					media.setIdThron(thronExraInfo.getThronId());
					media.setClienId(thronExraInfo.getClientId());
				} else {
					///// Solo per le copie da test a porduzione
					
				}
				mediaRepository.save(media);
				/// base
				/// https://testferrari-cdn.thron.com/delivery/public/image/testferrari/86d9e882-5158-453e-a977-e10968700167/yfewoa/std/0x0/car.jpgù
				if (StringUtils.isEmpty(media.getIdThron()) || StringUtils.isEmpty(media.getClienId())) {
					return fileDTO.getUrlCdn();
				}
				// private static final String BASE_USER_CDN =
				// "https://%s-cdn.thron.com/delivery/public/image/%s/%s/%s/std/%sx0/file.jpg";
				// testferrari = "yfewoa"
				
				String pkey = pkeyThronPrivate;
				url = String.format(BASE_USER_CDN, media.getClienId(), media.getClienId(), media.getIdThron(), pkey ,
						"0");
				return url;
			}

			// se manca su THRON restituisce un placeholder, dipendente da tipo documento
			if ((StringUtils.isEmpty(url)) && (template != null)) {
				return getPlaceholder(template);
			}
		}
		
		String pkey = pkeyThronPrivate;
		url = String.format(BASE_USER_CDN, media.getClienId(), media.getClienId(), media.getIdThron(), pkey, "0");
		return url;

	}
	
	@Override
	public String getThronUrlJpegForPrd(String id, String template) {
		if (id == null)
			return null;

		Media media = mediaRepository.findById(id);
		if (media == null)
			return null;
		
		ResponseEntity<com.ferrari.npg.media.client.entity.FileDTO> resp = mediaClient.getInfoFile(id);
		if (!resp.getStatusCode().is2xxSuccessful()) 
			throw new ValidationException(-1, "ERROR_USING_GET_THRON_URL");
		
		com.ferrari.npg.media.client.entity.FileDTO fileDTO = resp.getBody();
		
		if (fileDTO.getUrlCdnThumbnail() != null) 
			return fileDTO.getUrlCdnThumbnail();
		
		if  (template != null)
			return getPlaceholder(template);
		
		return null;

	}
	
	
	

	private String getPlaceholder(String template) {
		String url = null;
		MediaType mediaType = mediaTypeRepository.getOne(template);
		if ((mediaType != null) && (mediaType.getIdPlaceholder() != null)) {
			Media media = mediaRepository.findById((mediaType.getIdPlaceholder()));
			if (media != null) {
				url = media.getUrl();
			}
		}
		return url;
	}

	public void downloadBatch(List<String> docs, HttpServletResponse respHttp) {

//		for (String id : docs) {
//			if (!checkImageAvailable(id)) {
//				throw new ValidationException(-1,"IMAGE_NOT_AVAILABLE");	
//			}
//		}

		logger.warn("documenti da zippare {}", docs.toString());

		Response resp = mediaClient.downloadBatch(docs);

		logger.warn("resp.status() {}", resp.status());

		try {
			if (resp.status() == 200) {

				String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
				String outFilenameZip = "Zip_" + timeStamp + ".zip";

				respHttp.setContentType(DocService.MIME_APPLICATION_ZIP);
				respHttp.setHeader("Expires:", "0");
				respHttp.setHeader("Content-Disposition", String.format("attachment; filename=\"%s\"", outFilenameZip));
				OutputStream outStream = respHttp.getOutputStream();

				IOUtils.copy(resp.body().asInputStream(), outStream);

				outStream.flush();
				outStream.close();

			}
		} catch (UniformInterfaceException | ClientHandlerException | IOException e) {
			logger.error("Error creating zip", e);
			try {
				respHttp.sendError(HttpStatus.INTERNAL_SERVER_ERROR.value());
			} catch (IOException e1) {
				// ingore
			}
		}

	}

	@Override
	public void downlaodSingle(String id, HttpServletResponse response) throws IOException {

		Media media = mediaRepository.findOne(id);
		if (media == null) {
			throw new ValidationException(-1, "IMAGE_NOT_AVAILABLE");
		}

		checkImageAvailable(id);
		checkImageAvailableOpt(id);

//		if (!checkImageAvailable(id)) {
//			throw new ValidationException(-1,"IMAGE_NOT_AVAILABLE");	
//		}

		Response respMediaClient = mediaClient.downloadFile(id, ResponseType.FILE);
		InputStream is = respMediaClient.body().asInputStream();
		Map<String, Collection<String>> header = respMediaClient.headers();
		/*
		 * HTTP/1.1 200 OK Cache-Control: no-cache, no-store, max-age=0, must-revalidate
		 * Cache-Control: max-age=86400 Date: Tue, 24 Nov 2020 14:03:11 GMT Pragma:
		 * no-cache Transfer-Encoding: chunked Content-Type: image/jpeg Expires: 0
		 * Content-Disposition: inline; filename="ferrari_02.jpg"
		 */
		Collection<String> cType = header.get("Content-Type");
		if (cType != null && !cType.isEmpty()) {
			Iterator<String> it = cType.iterator();
			if (it.hasNext()) {
				response.setContentType(it.next());
			}
		}

		Collection<String> cDisp = header.get("Content-Disposition");
		if (cDisp != null && !cDisp.isEmpty()) {
			Iterator<String> it = cDisp.iterator();
			if (it.hasNext()) {
				String[] cd = it.next().split(";");
				if (cd.length < 2) {
					if (cd[0] != null && cd[0].trim().startsWith("filename")) {
						response.setHeader("Content-Disposition", String.format("attachment; %s", cd[0].trim()));
					}
				} else {
					if (cd[1] != null && cd[1].trim().startsWith("filename")) {
						response.setHeader("Content-Disposition", String.format("attachment; %s", cd[1].trim()));
					}
				}
			}
		}
//		

		IOUtils.copy(is, response.getOutputStream());
		response.flushBuffer();
	}

	private void checkImageAvailable(String id) {



		Boolean accessVersionEnabled = Boolean.FALSE;
		Boolean accessModelEnabled = Boolean.FALSE;
		///////// accesso a versione
		List<VersionAttc> versionToCheck = versionAttcRepository.findByIdImage(id);
		versionToCheck.addAll(versionAttcRepository.findByIdImageSvg(id));
		versionToCheck.addAll(versionAttcRepository.findByIdImageThumb(id));

		if (versionToCheck.size() == 0)
			return;

		// l'immagine deve essere visibile in almeno una versione accedibile dall'utente
		for (VersionAttc versionAttc : versionToCheck) {
			String enable = uiService.getAuthorizationByVersionStatus(versionAttc.getVersion().getStatus());
			if (("Y").equals(enable)) {
				accessVersionEnabled = Boolean.TRUE;
				break;
			}
		}
		// se l'immagine non appartiene ad una versione visualizzabile...
		if (!accessVersionEnabled)
			throw new ValidationException(-1, "IMAGE_NOT_AVAILABLE");

		/////// accesso a modello
		Boolean enableAllModels = ("Y".equals(uiService.getAuthorizationById(ModelService.MODEL_DISPLAY_ENABLE)));
		if (enableAllModels) {
			// accessModelEnabled = Boolean.TRUE;
			return;
		}
//		if (!accessModelEnabled) {
		for (VersionAttc versionAttc : versionToCheck) {
			Model model = commonService.getModelFromVersionAttc(versionAttc);

			// if (("Y".equals(model.getVisibility())) || enableAllModels) {
			if ("Y".equals(model.getVisibility())) {
				// accessModelEnabled = Boolean.TRUE;
				return;
			}
		}
//		}

		throw new ValidationException(-1, "IMAGE_NOT_AVAILABLE");

//		return accessModelEnabled;

	}

	private void checkImageAvailableOpt(String id) {

		List<BigDecimal> verisionIdsBig = versionOptRepository.findByLVersionOptPics_IdImage(id);

		if (verisionIdsBig.size() == 0)
			return;
		List<Long> verisionIds = new ArrayList<Long>();
		for (BigDecimal i : verisionIdsBig) {
			verisionIds.add(i.longValue());
		}
		Boolean accessVersionEnabled = Boolean.FALSE;
		for (Long i : verisionIds) {
			Version version = versionRepository.findOne(i);
			String enable = uiService.getAuthorizationByVersionStatus(version.getStatus());
			if (("Y").equals(enable)) {
				accessVersionEnabled = Boolean.TRUE;
				break;
			}

		}

		// se l'immagine non appartiene ad una versione visualizzabile...
		if (!accessVersionEnabled)
			throw new ValidationException(-1, "IMAGE_NOT_AVAILABLE");

		Boolean enableAllModels = ("Y".equals(uiService.getAuthorizationById(ModelService.MODEL_DISPLAY_ENABLE)));

		if (enableAllModels) {
			return;
			// accessModelEnabled = Boolean.TRUE;
		}
		for (Long i : verisionIds) {
			Model model = commonService.getModelFromVersionId(i);
			if ("Y".equals(model.getVisibility())) {
				return;
			}
		}

		throw new ValidationException(-1, "IMAGE_NOT_AVAILABLE");

	}

	private void validateUplaodFile(MultipartFile uploadFile) {
		// cerca per estensione e mimetype
		List<CfgFilesWhitelist> wl = new ArrayList<CfgFilesWhitelist>();
		wl = cfgFilesWhitelistRepository.findMimetype(uploadFile.getContentType());
		logger.debug("Upload: searching...");
		if (wl.size() == 0) {
			// logger.warn("No Whitelist for mimetype, try MIME+EXT");
			wl = cfgFilesWhitelistRepository.findMimetypeAndExtension(uploadFile.getContentType(),
					getExtensionByApacheCommonLib(uploadFile.getOriginalFilename()));
			if (wl.size() == 0) {
				logger.debug("No Whitelist for mimetype {} ", uploadFile.getContentType());
				throw new ValidationException(-1, "INVALID_FILE_TYPE");
			} else {
				logger.debug("OK - Whitelist found for mimetype and extension");
			}
		} else {
			logger.debug("OK - Whitelist found for mimetype");
		}
	}

	private String getExtensionByApacheCommonLib(String filename) {
		return FilenameUtils.getExtension(filename);
	}

}
