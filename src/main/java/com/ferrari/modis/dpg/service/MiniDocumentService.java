package com.ferrari.modis.dpg.service;

import org.springframework.web.bind.annotation.RequestMethod;

import com.ferrari.modis.dpg.commons.i18n.LocalizationType;
import com.ferrari.modis.dpg.jentity.base.JEntity;
import com.ferrari.modis.dpg.jentity.base.JPage;

import io.swagger.api.ApiResponseMessage;
import io.swagger.api.NotFoundException;

public interface MiniDocumentService {
	
	public final static String TEMPLATE_MINIDOC ="OTHERDOC";

	
	public final static String MINIDOCUMENT_CREATE_ENABLE ="MX-AD";
	public final static String MINIDOCUMENT_UPDATE_ENABLE ="MX-UP";
	public final static String MINIDOCUMENT_DELETE_ENABLE ="MX-DE";
	
	public final static String MINIDOCUMENT_READ_ALL ="MX-AC";


	
	
	public JPage getMiniDocuments(LocalizationType language);
	public JPage getMiniDocument(LocalizationType language,  Long idDocument);
	public JPage postMiniDocument(JEntity jEntity, RequestMethod requestMethod ) throws NotFoundException;
	public JPage putMiniDocument(JEntity jEntity,  LocalizationType language,  Long idDpcument,   RequestMethod requestMethod) throws NotFoundException;
	public ApiResponseMessage deleteMiniDocument (Long idDocument) throws NotFoundException;
	public JPage getNewMiniDocumentForm(LocalizationType language);
}
