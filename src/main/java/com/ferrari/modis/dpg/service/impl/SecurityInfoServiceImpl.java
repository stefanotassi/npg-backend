package com.ferrari.modis.dpg.service.impl;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Collection;
import java.util.HashMap;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.stereotype.Service;

import com.ferrari.modis.dpg.model.base.AuditContext;
import com.ferrari.modis.dpg.service.SecurityInfoService;
import com.ferrari.modis.dpg.util.WebContextHolder;
import com.ferrari.modis.dpg.utils.AuthenticationDTO;
import com.ferrari.modis.dpg.utils.ProfileXMLDTO;
import com.ferrari.modis.dpg.utils.ProfileXMLDTO.UserInfo;
import com.ferrari.modis.portal.ws.ProfileXML;
import com.ferrari.modis.portal.ws.SecAdUser;

@Service
public class SecurityInfoServiceImpl implements SecurityInfoService {
	final static Logger logger = LoggerFactory.getLogger(SecurityInfoServiceImpl.class);

	@Value(value = "${ambiente:dev-spindox}")
	private String ambiente;

	@Autowired
	com.ferrari.modis.security.service.impl.ProfilerServiceSoap profilerService;

	@Override
	public ProfileXML getProfile() {

		if ("dev-spindox".equals(ambiente)) {
			System.err.println("GetProfile for:" + ambiente);
			ProfileXML profileXML = new ProfileXML();
			profileXML.setTimezone("Europe/Rome");
			SecAdUser secAdUser = new SecAdUser();

			if ("HQ-FULL".equals(AuditContext.getCurrentUserName())) {
				secAdUser.setDealerCode("999999");
				secAdUser.setEmail("hq-full-user@ferrari.it");
				secAdUser.setFax("11111111");
				secAdUser.setFirstName("Hq-user-firstname");
				secAdUser.setLastName("Hq-user-lastname");
				secAdUser.setLanguage("en_US");
				secAdUser.setUsername("dev-hq-full");

			} else if ("HQ-READ".equals(AuditContext.getCurrentUserName())) {
				secAdUser.setDealerCode("999999");
				secAdUser.setEmail("hq-read-user@ferrari.it");
				secAdUser.setFax("22222222");
				secAdUser.setFirstName("Hq-read-firstname");
				secAdUser.setLastName("Hq-read-lastname");
				secAdUser.setLanguage("it_IT");
				secAdUser.setUsername("dev-hq-read");

			} else if ("AM-ABM".equals(AuditContext.getCurrentUserName())) {
				secAdUser.setDealerCode("062917");
				secAdUser.setEmail("am-abm-user@ferrari.it");
				secAdUser.setFax("33333333");
				secAdUser.setFirstName("am-user-firstname");
				secAdUser.setLastName("am-user-lastname");
				secAdUser.setLanguage("zh_CN");
				secAdUser.setUsername("dev-am-abm");

			} else if ("DEALER".equals(AuditContext.getCurrentUserName())) {
				secAdUser.setDealerCode("052222");
				secAdUser.setEmail("dealer-user@ferrari.it");
				secAdUser.setFax("44444444");
				secAdUser.setFirstName("dealer_user_surname");
				secAdUser.setLastName("dealer_user_lastname");
				secAdUser.setLanguage("de_DE");
				secAdUser.setUsername("dev-dealer");

			}

			profileXML.setUserInfo(secAdUser);

			return profileXML;

		}

		return getProfile(AuditContext.getCurrentUserName());
	}

	@Override
	public ProfileXML getProfile(String username) {

		try {
			ProfileXML resource = null;
			if (resource == null) {
				WebContextHolder context = WebContextHolder.getInstance();
				HttpSession session = context.getSession(true);
				String signature = (String) session.getAttribute(SS_USER_SIGNATURE);
				if (AuditContext.getSignature().equals(signature)) {
					resource = (ProfileXML) session.getAttribute(SS_USER_PROFILE);
				}
			}
			if (resource == null) {
				resource = profilerService.getUserProfileByApplication(username);
				saveProfileToSession(username, resource);
			}

			return resource;
		} catch (Exception ex) {

		}
		return null;
	}

	private ProfileXMLDTO getProfileXmlDTOP(String username) {

//		
		// ProfileXML p = getProfile(username);
		ProfileXML p = getProfile();
//		

		ProfileXMLDTO ret = new ProfileXMLDTO();
		UserInfo ui = new UserInfo();
		ui.setUsername(p.getUserInfo().getUsername());
		ui.setDealerCode(p.getUserInfo().getDealerCode());
		ui.setLanguage(p.getUserInfo().getLanguage());

		ret.setUserInfo(ui);
		return ret;
	}

	@Override
	public HashMap<String, Object> getApplicationInfo(String version, String buildTime) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		HashMap<String, Object> info = new HashMap<String, Object>();

		InetAddress localhost;
		String serverName = null;

		try {
			localhost = InetAddress.getLocalHost();
			serverName = localhost.getHostName();
		} catch (UnknownHostException e) {

			e.printStackTrace();
		}

		info.put("version", version);
		info.put("buildTime", buildTime);
		info.put("profile_settings", getProfileXmlDTOP(AuditContext.getCurrentUserName()));

		
		// verificare se authV2 è ok 
		//info.put("auth",auth);
		
		AuthenticationDTO authDTO = new AuthenticationDTO();
		authDTO.setAuthorities(auth.getAuthorities());
		authDTO.setAuthenticated(auth.isAuthenticated());
		authDTO.setPrincipal(auth.getPrincipal());
		info.put("auth",authDTO);

		
		// info.put("server", serverName);
		return info;
	}

	public void saveProfileToSession(String username, ProfileXML profile) {
		try {
			WebContextHolder context = WebContextHolder.getInstance();
			HttpSession session = context.getSession(true);
			session.setAttribute(SS_USER_PROFILE, profile);
			session.setAttribute(SS_USER_NAME, username);
			session.setAttribute(SS_USER_SIGNATURE, AuditContext.getSignature());
		} catch (Exception ignore) {
		}
	}

	@Override
	public void saveProfile(String username, ProfileXML profile) {
		try {
			saveProfileToSession(username, profile);
		} catch (Exception ex) {
			ex.printStackTrace();
			logger.error(ex.getMessage());
		}
	}

}
