package com.ferrari.modis.dpg.service;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import com.ferrari.modis.dpg.commons.i18n.DocumentType;
import com.ferrari.modis.dpg.commons.i18n.LocalizationType;
import com.ferrari.modis.dpg.jentity.app.JVersionAttcForm;
import com.ferrari.modis.dpg.jentity.app.JVersionOptForm;
import com.ferrari.modis.dpg.jentity.base.JPage;

public interface DocService {
	
	public static final String EXCEL_BASIC_SHEETNAME_OPTIONALS 	= "GRID";
	
	public static final String EXCEL_FULL_SHEETNAME_SUMMARY 	= "Summary";	
	public static final String EXCEL_FULL_SHEETNAME_OPTIONALS 	= "Optionals";		
	public static final String EXCEL_FULL_SHEETNAME_GROUPS 		= "Groups";		
	
	
	public static final String TEXT_A1_OPTIONAL 			= "A1";
	public static final String TEXT_A5_VINCOLI_OPTIONALS 	= "A5";
	public static final String TEXT_B1_VINCOLI_OMOLOGAZIONE = "B1";
	public static final String TEXT_C0_NOTE_INTERNE 		= "C0";
	public static final String TEXT_C2_STATUS_SVILUPPO 		= "C2";
	public static final String TEXT_C4_CAPACITY 			= "C4";
	
	// colonna tipo operazione su XLSX per import
	public static final String UPLOAD_ROW_INSERT 	= "Insert";
	public static final String UPLOAD_ROW_UPDATE 	= "Update";
	public static final String UPLOAD_ROW_DELETE 	= "Delete";
	public static final String UPLOAD_ROW_NOCHANGE 	= "NoChange";

	
	
	public static final String MIME_APPLICATION_XLSX 	= "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
	public static final String MIME_APPLICATION_PDF 	= "application/pdf";
	public static final String MIME_APPLICATION_ZIP 	= "application/zip";
	
	
	// TIPI di PDF ////////////////////////////////////////////////////////////////////////////////////////////////
	public static final String PDF_PUBLIC = "PDF_PUBLIC";
	public static final String PDF_INTERNAL_USE_ONLY = "PDF_INTERNAL_USE_ONLY";
	public static final String PDF_VERSION = "1.0";
	
	
	// labels su PDF ///////////////////////////////////////////////////////////////////////////////////////////////
	public static final String LABELS_CONTEXT = "LBL";
	
	public static final String LBL_HEADER 					= "LBL_HEADER";
	public static final String LBL_DRAWINGS 				= "LBL_DRAWINGS";
	public static final String LBL_INDEX 					= "LBL_INDEX";
	public static final String LBL_GRID 					= "LBL_GRID";
	public static final String LBL_OPT 						= "LBL_OPT";

	public static final String LBL_PAGE 						= "LBL_PAGE";
	public static final String LBL_DATE 						= "LBL_DATE";
	
	
	public static final String LBL_OPT_RULES 				= "LBL_OPT_RULES";
	public static final String LBL_OPT_WITH 				= "LBL_OPT_WITH";
	public static final String LBL_OPT_HOMOLOGATION			= "LBL_OPT_HOMOLOGATION";
	public static final String LBL_OPT_NOTE					= "LBL_OPT_NOTE";
	public static final String LBL_OPT_STATUS				= "LBL_OPT_STATUS";
	public static final String LBL_OPT_CAPACITY				= "LBL_OPT_CAPACITY";
	
	public static final String LBL_OPT_ORDERABLE_TEXT		= "LBL_OPT_ORDERABLE_TEXT";
	public static final String LBL_OPT_LASTUPDATE			= "LBL_OPT_LASTUPDATE";
	
	
	// label colonne Excel BASIC
	public static final String LBL_XLS_MODEL				= "LBL_XLS_MODEL";
	public static final String LBL_XLS_AREA					= "LBL_XLS_AREA";
	public static final String LBL_XLS_MACROCAT				= "LBL_XLS_MACROCAT";
	public static final String LBL_XLS_SOTTOCAT				= "LBL_XLS_SOTTOCAT";
	
	public static final String LBL_XLS_CODOPT 				= "LBL_XLS_CODOPT";
	public static final String LBL_XLS_DESOPT 				= "LBL_XLS_DESOPT";
	public static final String LBL_XLS_VINCOLI_OPT 			= "LBL_XLS_VINCOLI_OPT";
	public static final String LBL_XLS_VINCOLI_OMO 			= "LBL_XLS_VINCOLI_OMO";
	public static final String LBL_XLS_ORDINABILITA_FLAG 	= "LBL_XLS_ORDINABILITA_FLAG";
	public static final String LBL_XLS_ORDINABILITA_TEXT 	= "LBL_XLS_ORDINABILITA_TEXT";
	public static final String LBL_XLS_DESCRPITION 			= "LBL_XLS_DESCRPITION";
	public static final String LBL_XLS_VERSION_MODEL 		= "LBL_XLS_VERSION_MODEL";
	public static final String LBL_XLS_UPDATE_DATE 			= "LBL_XLS_UPDATE_DATE";
	public static final String LBL_XLS_INTERNAL_NOTE 		= "LBL_XLS_INTERNAL_NOTE";
	public static final String LBL_XLS_STATUS_SVILUPPO 		= "LBL_XLS_STATUS_SVILUPPO";
	public static final String LBL_XLS_CAP_PRODUTTIVA 		= "LBL_XLS_CAP_PRODUTTIVA";
	public static final String LBL_XLS_SEQ 					= "LBL_XLS_SEQ";	
	public static final String LBL_XLS_VISIBILITY 			= "LBL_XLS_VISIBILITY";	

	public static final String UI_LOGO_FERRARI				= "pdf-logo-Ferrari";
	public static final String UI_TEMPLATE_ID = "pdf-templateId";
	
	public static final String UI_TEMPLATE_COVER_J = "pdf-Cover.jasper";
	public static final String UI_TEMPLATE_MENU_J = "pdf-Menu.jasper"; // 1-5 dipendentemente da colonne
	public static final String UI_TEMPLATE_SVG_J = "pdf-SVG.jasper";
	public static final String UI_TEMPLATE_PDF_J = "pdf-PDF.jasper";
	
	public static final String UI_TEMPLATE_DETAIL_J ="pdf-Detail.jasper";
	public static final String UI_TEMPLATE_EXCEL_J ="pdf-Excel.jasper";
	public static final String UI_TEMPLATE_DESCRIPTION_J ="pdf-Description.jasper";
	
	public void createXlsxOptionalsByVersion(JVersionOptForm jVersionOptForm, LocalizationType language, Long version, String xlsType, HttpServletResponse response) throws IOException;
	public void createPdfOptionalsByVersion(JVersionOptForm jVersionOptForm, LocalizationType language, Long version , String forceDealerMode, HttpServletResponse response) throws IOException;
	public JPage  createPdfOptionalsByVersionUI(JVersionOptForm jVersionOptForm, LocalizationType language, Long version) throws IOException;
	
	public void createZipByVersionAndType(JVersionAttcForm jVersionAttcForm, LocalizationType language, Long version , DocumentType documentType,  HttpServletResponse response) throws IOException;

	
}
