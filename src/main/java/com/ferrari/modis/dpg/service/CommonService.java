package com.ferrari.modis.dpg.service;

import com.ferrari.modis.dpg.commons.i18n.LocalizationType;
import com.ferrari.modis.dpg.model.app.Model;
import com.ferrari.modis.dpg.model.app.ModelOptionalType;
import com.ferrari.modis.dpg.model.app.Version;
import com.ferrari.modis.dpg.model.app.VersionAttc;

public interface CommonService {
	
	public static final String REQUEST_HEADER_LANG = "x-npg-language";
	
	public  LocalizationType getLanguage(String hlang);
	
	Model getModelFromVersionAttcId(Long versionAttcId);

	Model getModelFromVersionAttc(VersionAttc versionAttc);

	Model getModelFromVersion(Version version);

	Model getModelFromVersionId(Long versionId);

	Model getModelFromModelOptionalTypeId(Long modelOptionalTypeId);

	Model getModelFromModelOptionalType(ModelOptionalType modelOptionalType);


}


