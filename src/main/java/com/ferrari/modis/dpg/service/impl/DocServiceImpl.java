package com.ferrari.modis.dpg.service.impl;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.poi.hssf.record.CFRuleBase.ComparisonOperator;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.ConditionalFormattingRule;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.PatternFormatting;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.CellRangeAddressList;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFDataValidation;
import org.apache.poi.xssf.usermodel.XSSFDataValidationConstraint;
import org.apache.poi.xssf.usermodel.XSSFDataValidationHelper;
import org.apache.poi.xssf.usermodel.XSSFName;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFSheetConditionalFormatting;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ferrari.modis.dpg.commons.i18n.DocumentType;
import com.ferrari.modis.dpg.commons.i18n.LocalizationHelper;
import com.ferrari.modis.dpg.commons.i18n.LocalizationType;
import com.ferrari.modis.dpg.jentity.app.JModel;
import com.ferrari.modis.dpg.jentity.app.JModelOptionalGroup;
import com.ferrari.modis.dpg.jentity.app.JModelOptionalSubGroup;
import com.ferrari.modis.dpg.jentity.app.JPdf;
import com.ferrari.modis.dpg.jentity.app.JPdfBaseLabels;
import com.ferrari.modis.dpg.jentity.app.JPdfDrawing;
import com.ferrari.modis.dpg.jentity.app.JPdfOptional;
import com.ferrari.modis.dpg.jentity.app.JPdfType;
import com.ferrari.modis.dpg.jentity.app.JVersion;
import com.ferrari.modis.dpg.jentity.app.JVersionAttc;
import com.ferrari.modis.dpg.jentity.app.JVersionAttcForm;
import com.ferrari.modis.dpg.jentity.app.JVersionAttcSimple;
import com.ferrari.modis.dpg.jentity.app.JVersionOpt;
import com.ferrari.modis.dpg.jentity.app.JVersionOptForm;
import com.ferrari.modis.dpg.jentity.app.JXlsBaseLabels;
import com.ferrari.modis.dpg.jentity.base.JPage;
import com.ferrari.modis.dpg.jentity.base.JPageAuth;
import com.ferrari.modis.dpg.model.app.MediaConfig;
import com.ferrari.modis.dpg.model.app.Model;
import com.ferrari.modis.dpg.model.app.ModelCluster;
import com.ferrari.modis.dpg.model.app.ModelOptionalGroup;
import com.ferrari.modis.dpg.model.app.ModelOptionalSubGroup;
import com.ferrari.modis.dpg.model.app.ModelOptionalType;
import com.ferrari.modis.dpg.model.app.Version;
import com.ferrari.modis.dpg.model.app.VersionAttc;
import com.ferrari.modis.dpg.model.app.VersionOptXlsV1;
import com.ferrari.modis.dpg.model.ui.UiApplicationSetting;
import com.ferrari.modis.dpg.model.ui.UiDictionary;
import com.ferrari.modis.dpg.repository.MediaConfigRepository;
import com.ferrari.modis.dpg.repository.MediaRepository;
import com.ferrari.modis.dpg.repository.ModelClusterRepository;
import com.ferrari.modis.dpg.repository.ModelOptionalGroupRepository;
import com.ferrari.modis.dpg.repository.ModelOptionalSubGroupRepository;
import com.ferrari.modis.dpg.repository.ModelOptionalTypeRepository;
import com.ferrari.modis.dpg.repository.ModelRepository;
import com.ferrari.modis.dpg.repository.UiApplicationSettingRepository;
import com.ferrari.modis.dpg.repository.UiDictionaryRepository;
import com.ferrari.modis.dpg.repository.UiPageRepository;
import com.ferrari.modis.dpg.repository.VersionAttcRepository;
import com.ferrari.modis.dpg.repository.VersionOptRepository;
import com.ferrari.modis.dpg.repository.VersionOptTextRepository;
import com.ferrari.modis.dpg.repository.VersionOptXlsV1Repository;
import com.ferrari.modis.dpg.repository.VersionRepository;
import com.ferrari.modis.dpg.service.CommonService;
import com.ferrari.modis.dpg.service.DocService;
import com.ferrari.modis.dpg.service.MediaService;
import com.ferrari.modis.dpg.service.ModelService;
import com.ferrari.modis.dpg.service.UiService;
import com.ferrari.modis.dpg.service.VModelService;
import com.ferrari.modis.dpg.service.VersionService;
import com.ferrari.modis.dpg.utils.KeyBuilder;
import com.ferrari.modis.dpg.utils.PdfUtils;
import com.ferrari.npg.media.client.PdfClient;
import com.ferrari.npg.media.client.entity.Constant;
import com.ferrari.npg.media.client.entity.ParameterDTO;
import com.ferrari.npg.media.client.entity.ReportDTO;

import feign.Response;
import io.swagger.api.ValidationException;

@Service
public class DocServiceImpl implements DocService {
	private static final Logger logger = LoggerFactory.getLogger(DocServiceImpl.class);

	@Autowired
	ModelClusterRepository modelClusterRepository;
	@Autowired
	ModelRepository modelRepository;
	@Autowired
	ModelOptionalTypeRepository modelOptionalTypeRepository;
	@Autowired
	VersionRepository versionRepository;
	@Autowired
	VersionOptRepository versionOptRepository;
	@Autowired
	ModelOptionalGroupRepository modelOptionalGroupRepository;
	@Autowired
	ModelOptionalSubGroupRepository modelOptionalSubGroupRepository;
	@Autowired
	VersionOptTextRepository versionOptTextRepository;
	@Autowired
	VersionAttcRepository versionAttcRepository;
	@Autowired
	MediaRepository mediaRepository;
	@Autowired
	VersionOptXlsV1Repository versionOptXlsV1Repository;
	@Autowired
	UiDictionaryRepository uiDictionaryRepository;
	@Autowired
	UiPageRepository uiPageRepository;
	@Autowired
	UiApplicationSettingRepository uiApplicationSettingRepository;

	@Autowired
	UiService uiService;
	@Autowired
	VersionService versionService;
	@Autowired
	ModelService modelService;
	@Autowired
	VModelService vModelService;

	@Autowired
	MediaService mediaService;

	@Autowired
	PdfClient pdfClient;

	@Autowired
	MediaConfigRepository mediaConfigRepository;

	@Autowired
	CommonService commonService;

	public void createZipByVersionAndType(JVersionAttcForm jVersionAttcForm, LocalizationType language, Long idVersion,
			DocumentType documentType, HttpServletResponse response) throws IOException {
		if (documentType.equals(DocumentType.F)) {
		} else if (documentType.equals(DocumentType.O)) {
		} else
			throw new ValidationException(-1, "DOCUMENT_NOT_ALLOWED");

		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		List<String> docs = new ArrayList<String>();
		Integer includedDocuments = 0;
		for (JVersionAttc jVersionAttc : jVersionAttcForm.getlJVersionAttc()) {
			if (jVersionAttc.getChoice() == Boolean.FALSE)
				continue;
			VersionAttc versionAttc = versionAttcRepository.findOne(jVersionAttc.getId());
			if (versionAttc == null)
				continue;
			if (versionAttc.getIdImage() == null)
				continue;

			Model model = commonService.getModelFromVersionAttc(versionAttc);
			Boolean enableAllModels = ("Y".equals(uiService.getAuthorizationById(ModelService.MODEL_DISPLAY_ENABLE)));
			if ((!"Y".equals(model.getVisibility())) && !enableAllModels) {
				// non visibile: abilitati solo utenti meritevili
				throw new ValidationException(-1, "MODEL_NOT_FOUND"); // AVAILABLE
			}

			docs.add(versionAttc.getIdImage());
			includedDocuments++;
		}
		logger.warn("Documenti inclusi per generazione ZIP = {}", docs);
		if (includedDocuments == 0) {
			throw new ValidationException(-1, "NO_FILES_SELECTED");
		}

		mediaService.downloadBatch(docs, response);

		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//		String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
//		String outFilenameZip=fileUtils.getFilePath()+"\\"+"Zip_"+documentType+"_"+timeStamp+".zip";
//		
//		
//		File tempFile = File.createTempFile(String.format("temp-%s", timeStamp), ".zip");
//
//		
//		FileOutputStream fos = new FileOutputStream(tempFile);
//		ZipOutputStream zos = new ZipOutputStream(new BufferedOutputStream(fos));
//		
//		Integer ii=0;
//		
//		for (JVersionAttc jVersionAttc : jVersionAttcForm.getlJVersionAttc()){
//			if (jVersionAttc.getChoice()==Boolean.FALSE)
//				continue;
//			
//			VersionAttc versionAttc = versionAttcRepository.findOne(jVersionAttc.getId());
//			if (versionAttc==null)
//				continue;
//			
//			Media media = mediaRepository.findById(versionAttc.getIdImage());
//			
//			
//		
//			
//			ii++;
//			String filenameIn 		= fileUtils.getFilePath()+"\\"+media.getId();
//			String filenameInUnique = String.format("%s-%s", ii,media.getFilename());
//			
//			
//			ZipEntry entry = new ZipEntry(filenameInUnique);
//			zos.putNextEntry(entry);
//			FileInputStream is = new FileInputStream(filenameIn);
//			IOUtils.copy(is, zos);
//			zos.closeEntry();
//			is.close();
//			
//		}
//
//		zos.flush();
//		zos.close();
//		
//		
//		// se non ha incluso nemmeno un file --> errore
//		if (ii==0){
//			throw new ValidationException(-1,"NO_FILES_SELECTED");
//		}
//		
//		
//		FileInputStream is = new FileInputStream(tempFile);
//		FileOutputStream os = new FileOutputStream(outFilenameZip);
//		IOUtils.copy(is,os);
//		os.flush();
//		os.close();
//		
//		
//		
//		
//		response.setContentType("application/zip");
//		response.setHeader("Expires:", "0"); 
//		response.setHeader("Content-Disposition", String.format("attachment; filename=%s",outFilenameZip));
//		OutputStream outStream = response.getOutputStream();
//
//
//		outStream.flush();

	}

	public void createXlsxOptionalsByVersion(JVersionOptForm jVersionOptForm, LocalizationType language, Long idversion,
			String xlsType, HttpServletResponse response) throws IOException {

		if (xlsType.equals("BASIC")) {

			// verifica permission:
			if (!"Y".equals(uiService.getAuthorizationById("XLS-1")))
				throw new ValidationException(-1, "USER_PERMISSION_XLS_BASIC");

			String template = "Template_BASIC_" + language.getValue() + ".xlsx";
			createXlsxOptBASIC(jVersionOptForm, language, idversion, template, response);
		} else if (xlsType.equals("FULL")) {

			if (!"Y".equals(uiService.getAuthorizationById("XLS-2")))
				throw new ValidationException(-1, "USER_PERMISSION_XLS_FULL");

			// String template = "Template_FULL_"+language.getValue()+".xlsx";
			String template = "Template_FULL.xlsx";
			createXlsxOptFULL(jVersionOptForm, language, idversion, template, response);
		} else {
			throw new ValidationException(-1, "XLSTYPE_NOT_ALLOWEED");
		}

		return;
	}

	private void createXlsxOptBASIC(JVersionOptForm jVersionOptForm, LocalizationType language, Long idversion,
			String template, HttpServletResponse response) throws IOException {

		Version version = versionRepository.findById(idversion);
		String enable = uiService.getAuthorizationByVersionStatus(version.getStatus());
		if (!("Y").equals(enable)) {
			throw new ValidationException(-1, "VERSION_NOT_ACCESSIBLE");
		}

		System.out.println(template);

		InputStream in = DocServiceImpl.class.getResourceAsStream("/" + template);

		Model model = modelRepository.findById(jVersionOptForm.getJmodel().getId());

		Boolean enableAllModels = ("Y".equals(uiService.getAuthorizationById(ModelService.MODEL_DISPLAY_ENABLE)));
		if ((!"Y".equals(model.getVisibility())) && !enableAllModels) {
			// non visibile: abilitati solo utenti meritevili
			throw new ValidationException(-1, "MODEL_NOT_FOUND"); // AVAILABLE
		}

		XSSFWorkbook wb = new XSSFWorkbook(in);
		XSSFSheet sheet1 = wb.getSheet(EXCEL_BASIC_SHEETNAME_OPTIONALS);

		XSSFRow row;
		XSSFCell cell;

		// nascondo le colonne che non verranno popolate in base a utente

		if (!"Y".equals(uiService.getAuthorizationByText(TEXT_A5_VINCOLI_OPTIONALS)))
			sheet1.setColumnWidth(6, 0);
		if (!"Y".equals(uiService.getAuthorizationByText(TEXT_B1_VINCOLI_OMOLOGAZIONE)))
			sheet1.setColumnWidth(7, 0);
		if (!"Y".equals(uiService.getAuthorizationByText(TEXT_A1_OPTIONAL)))
			sheet1.setColumnWidth(10, 0);
		if (!"Y".equals(uiService.getAuthorizationByText(TEXT_C0_NOTE_INTERNE)))
			sheet1.setColumnWidth(13, 0);
		if (!"Y".equals(uiService.getAuthorizationByText(TEXT_C2_STATUS_SVILUPPO)))
			sheet1.setColumnWidth(14, 0);
		if (!"Y".equals(uiService.getAuthorizationByText(TEXT_C4_CAPACITY)))
			sheet1.setColumnWidth(15, 0);

		// abilitazione a Visibilit� esterna
		String enableAll = "N";
		if (!"Y".equals(uiService.getAuthorizationById("OP-VI"))) {
			sheet1.setColumnWidth(17, 0);
		} else {
			enableAll = "Y"; // enableAll viene usata nella query di caricamento
		}

		if (!"Y".equals(uiService.getAuthorizationById("OP-SQ")))
			sheet1.setColumnWidth(16, 0);

		/////////////////////////////////////////////////

		CellStyle cellStyleDate = wb.createCellStyle();
		CreationHelper createHelper = wb.getCreationHelper();
		cellStyleDate.setDataFormat(createHelper.createDataFormat().getFormat("dd/mm/yyyy"));

		Font fontRed = wb.createFont();
		fontRed.setColor(IndexedColors.RED.getIndex());
		Font fontYellow = wb.createFont();
		fontYellow.setColor(IndexedColors.YELLOW.getIndex());
		Font fontGreen = wb.createFont();
		fontGreen.setColor(IndexedColors.BRIGHT_GREEN.getIndex());

		CellStyle styleFontRed = wb.createCellStyle();
		styleFontRed.setFont(fontRed);
		CellStyle styleFontYellow = wb.createCellStyle();
		styleFontYellow.setFont(fontYellow);
		CellStyle styleFontGreen = wb.createCellStyle();
		styleFontGreen.setFont(fontGreen);

		XSSFSheetConditionalFormatting sheetCF = sheet1.getSheetConditionalFormatting();

		ConditionalFormattingRule rule1 = sheetCF.createConditionalFormattingRule(ComparisonOperator.GE, "3");
		PatternFormatting fill1 = rule1.createPatternFormatting();
		fill1.setFillBackgroundColor(IndexedColors.BRIGHT_GREEN.index);
		fill1.setFillPattern(PatternFormatting.SOLID_FOREGROUND);

		ConditionalFormattingRule rule2 = sheetCF.createConditionalFormattingRule(ComparisonOperator.GE, "2");
		PatternFormatting fill2 = rule2.createPatternFormatting();
		fill2.setFillBackgroundColor(IndexedColors.YELLOW.index);
		fill2.setFillPattern(PatternFormatting.SOLID_FOREGROUND);

		ConditionalFormattingRule rule3 = sheetCF.createConditionalFormattingRule(ComparisonOperator.GE, "1");
		PatternFormatting fill3 = rule3.createPatternFormatting();
		fill3.setFillBackgroundColor(IndexedColors.RED.index);
		fill3.setFillPattern(PatternFormatting.SOLID_FOREGROUND);

		ConditionalFormattingRule rule4 = sheetCF.createConditionalFormattingRule("R2=\"Y\""); // @@@
		PatternFormatting fill4 = rule4.createPatternFormatting();
		fill4.setFillBackgroundColor(IndexedColors.BRIGHT_GREEN.index);
		fill4.setFillPattern(PatternFormatting.SOLID_FOREGROUND);

		ConditionalFormattingRule rule5 = sheetCF.createConditionalFormattingRule("R2=\"N\""); // @@@
		// ConditionalFormattingRule rule5 =
		// sheetCF.createConditionalFormattingRule(ComparisonOperator.EQUAL, "N");
		PatternFormatting fill5 = rule5.createPatternFormatting();
		fill5.setFillBackgroundColor(IndexedColors.RED.index);
		fill5.setFillPattern(PatternFormatting.SOLID_FOREGROUND);

		CellRangeAddress[] regions = { CellRangeAddress.valueOf("I2:I1000") }; // @@@
		ConditionalFormattingRule[] cfRules = { rule1, rule2, rule3 };
		sheetCF.addConditionalFormatting(regions, cfRules);

		CellRangeAddress[] region2 = { CellRangeAddress.valueOf("R2:R1000") }; // @@@
		ConditionalFormattingRule[] cfRules2 = { rule4, rule5 };
		sheetCF.addConditionalFormatting(region2, cfRules2);

		/////////////////////////////////////////////////

		RepStatus r = new RepStatus();

		///////////////////////////////////////////////////////////////////////////

		JXlsBaseLabels jXlsBaseLabels = getHeadingsXlsBase(language);

		r.setRow(0); // @@@
		row = sheet1.getRow(r.getRow());

		r.setCol(0);
		cell = row.getCell(r.getCol());
		cell.setCellValue(jXlsBaseLabels.getLabelModel());
		r.setCol(1);
		cell = row.getCell(r.getCol());
		cell.setCellValue(jXlsBaseLabels.getLabelArea());
		r.setCol(2);
		cell = row.getCell(r.getCol());
		cell.setCellValue(jXlsBaseLabels.getLabelMacroCategoria());
		r.setCol(3);
		cell = row.getCell(r.getCol());
		cell.setCellValue(jXlsBaseLabels.getLabelSottoCategoria());

		r.setCol(4);
		cell = row.getCell(r.getCol());
		cell.setCellValue(jXlsBaseLabels.getLabelCodOpt());
		r.setCol(5);
		cell = row.getCell(r.getCol());
		cell.setCellValue(jXlsBaseLabels.getLabelShortDescOpt());
		r.setCol(6);
		cell = row.getCell(r.getCol());
		cell.setCellValue(jXlsBaseLabels.getLabelVincoliOpt());

		r.setCol(7);
		cell = row.getCell(r.getCol());
		cell.setCellValue(jXlsBaseLabels.getLabelVincoliOmologativi());

		r.setCol(8);
		cell = row.getCell(r.getCol());
		cell.setCellValue(jXlsBaseLabels.getLabelOrdFlag());
		r.setCol(9);
		cell = row.getCell(r.getCol());
		cell.setCellValue(jXlsBaseLabels.getLabelOrdText());

		r.setCol(10);
		cell = row.getCell(r.getCol());
		cell.setCellValue(jXlsBaseLabels.getLabelDescription());

		r.setCol(11);
		cell = row.getCell(r.getCol());
		cell.setCellValue(jXlsBaseLabels.getLabelVersionModel());

		r.setCol(12);
		cell = row.getCell(r.getCol());
		cell.setCellValue(jXlsBaseLabels.getLabelUpdateDate());

		r.setCol(13);
		cell = row.getCell(r.getCol());
		cell.setCellValue(jXlsBaseLabels.getLabelInternalNote());

		r.setCol(14);
		cell = row.getCell(r.getCol());
		cell.setCellValue(jXlsBaseLabels.getLabelStatusSviluppo());

		r.setCol(15);
		cell = row.getCell(r.getCol());
		cell.setCellValue(jXlsBaseLabels.getLabelCapProduttiva());

		r.setCol(16);
		cell = row.getCell(r.getCol());
		cell.setCellValue(jXlsBaseLabels.getLabelSequence());

		r.setCol(17);
		cell = row.getCell(r.getCol());
		cell.setCellValue(jXlsBaseLabels.getLabelVisibility());

		///////////////////////////////////////////////////////////////////////////
		r.setRow(0);

		List<Long> lOptChoiceYes = getOptionalsSelected(jVersionOptForm);

		if (lOptChoiceYes.isEmpty()) {
			lOptChoiceYes.add(0L);// permette di usare la in a livello di select nella chiamata getOptionalList
		}

		for (VersionOptXlsV1 versionOptXlsV1 : versionOptXlsV1Repository.getOptionalList(idversion, language.getValue(),
				lOptChoiceYes, enableAll)) {

			r.incRow();
			row = sheet1.createRow(r.getRow());

			r.setCol(0);
			cell = row.createCell(r.getCol());
			cell.setCellValue(versionOptXlsV1.getModelDescription());

			r.setCol(1);
			cell = row.createCell(r.getCol());
			cell.setCellValue(versionOptXlsV1.getArea());

			r.setCol(2);
			cell = row.createCell(r.getCol());
			cell.setCellValue(versionOptXlsV1.getGroupDescription());

			r.setCol(3);
			cell = row.createCell(r.getCol());
			cell.setCellValue(versionOptXlsV1.getSubgroupDescription());

			r.setCol(4);
			cell = row.createCell(r.getCol());
			cell.setCellValue(versionOptXlsV1.getOptCode());

			r.setCol(5);
			cell = row.createCell(r.getCol());
			cell.setCellValue(versionOptXlsV1.getOptDescription());

			r.setCol(6);
			cell = row.createCell(r.getCol());
			if ("Y".equals(uiService.getAuthorizationByText(TEXT_A5_VINCOLI_OPTIONALS)))
				cell.setCellValue(versionOptXlsV1.getTextA5VincoliOpt());

			r.setCol(7);
			cell = row.createCell(r.getCol());
			if ("Y".equals(uiService.getAuthorizationByText(TEXT_B1_VINCOLI_OMOLOGAZIONE)))
				cell.setCellValue(versionOptXlsV1.getTextB1VincoliOmologazione());

			r.setCol(8);
			cell = row.createCell(r.getCol());
			cell.setCellValue(Double.parseDouble(versionOptXlsV1.getOrderableFlag()));
			// vale 1, 2, 3; in base al valore imposta background Reg/yellow/green

			switch (versionOptXlsV1.getOrderableFlag()) {
			case "1":
				cell.setCellStyle(styleFontRed);
				break;
			case "2":
				cell.setCellStyle(styleFontYellow);
				break;
			case "3":
				cell.setCellStyle(styleFontGreen);
				break;
			default:

			}

			r.setCol(9);
			cell = row.createCell(r.getCol());
			cell.setCellValue(versionOptXlsV1.getOrderableText());

			r.setCol(10);
			cell = row.createCell(r.getCol());
			if ("Y".equals(uiService.getAuthorizationByText(TEXT_A1_OPTIONAL)))
				cell.setCellValue(versionOptXlsV1.getTextA1Long());

			r.setCol(11);
			cell = row.createCell(r.getCol());
			cell.setCellValue(versionOptXlsV1.getVersionModel());

			r.setCol(12);
			cell = row.createCell(r.getCol());
			cell.setCellValue(versionOptXlsV1.getLastUpdate());
			cell.setCellStyle(cellStyleDate);

			r.setCol(13);
			cell = row.createCell(r.getCol());
			if ("Y".equals(uiService.getAuthorizationByText(TEXT_C0_NOTE_INTERNE)))
				cell.setCellValue(versionOptXlsV1.getTextC0NoteInterne());

			r.setCol(14);
			cell = row.createCell(r.getCol());
			if ("Y".equals(uiService.getAuthorizationByText(TEXT_C2_STATUS_SVILUPPO)))
				cell.setCellValue(versionOptXlsV1.getTextC2StatusSvil());

			r.setCol(15);
			cell = row.createCell(r.getCol());
			if ("Y".equals(uiService.getAuthorizationByText(TEXT_C4_CAPACITY)))
				cell.setCellValue(versionOptXlsV1.getTextC4Capacity());

			r.setCol(16);
			cell = row.createCell(r.getCol());
			if ("Y".equals(uiService.getAuthorizationById("OP-SQ")))
				cell.setCellValue(versionOptXlsV1.getSeq());

			r.setCol(17);
			cell = row.createCell(r.getCol());
			cell.setCellValue(versionOptXlsV1.getVisibleFlag());

		}

		String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
		// sendXlsxToFileSystem(wb,"BASIC_XLS_"+model.getCode()+"_"+version.getCode()+"_"+timeStamp+".xlsx");
		// // temporaneo per TEST
		sendXlsxToBrowser(response, wb,
				"BASIC_XLS_" + model.getCode() + "_" + version.getCode() + "_" + timeStamp + ".xlsx");

		wb.close();
		return;

	}

	private void createXlsxOptFULL(JVersionOptForm jVersionOptForm, LocalizationType language, Long idversion,
			String template, HttpServletResponse response) throws IOException {

		// questo XLs è disponibile solo HQ e prevede la visualizzazione di tutte le
		// righe e colonne
		// l'unico filtro viene effettuato in fase di upload, nelle opzioni CHOICE
		// impostare il template:

		// IMPORTANTE: impostare a BLOCCATE le celle prima colonna e prima riga del
		// secondo foglio (non necessario impostare pwd)

		Version version = versionRepository.findById(idversion);
		String enable = uiService.getAuthorizationByVersionStatus(version.getStatus());
		if (!("Y").equals(enable)) {
			throw new ValidationException(-1, "VERSION_NOT_ACCESSIBLE");
		}

		System.out.println(template);

		Model model = modelRepository.findById(jVersionOptForm.getJmodel().getId());

		Boolean enableAllModels = ("Y".equals(uiService.getAuthorizationById(ModelService.MODEL_DISPLAY_ENABLE)));
		if ((!"Y".equals(model.getVisibility())) && !enableAllModels) {

			// non visibile: abilitati solo utenti meritevili
			throw new ValidationException(-1, "MODEL_NOT_FOUND"); // AVAILABLE
		}

		ModelCluster modelCluster = modelClusterRepository.findById(model.getIdMcluster());
		ModelOptionalType modelOptionalType = modelOptionalTypeRepository
				.findById(jVersionOptForm.getJversion().getIdModelOptionalType());
		// Version version = versionRepository.findById(idversion);

		InputStream in = DocServiceImpl.class.getResourceAsStream("/" + template);

		XSSFWorkbook wb = new XSSFWorkbook(in);
		XSSFSheet shSummary = wb.getSheet(EXCEL_FULL_SHEETNAME_SUMMARY);
		XSSFSheet sheet2 = wb.getSheet(EXCEL_FULL_SHEETNAME_OPTIONALS);
		XSSFSheet sheet3 = wb.getSheet(EXCEL_FULL_SHEETNAME_GROUPS);

		XSSFCellStyle lockedStyle = wb.createCellStyle();
		lockedStyle.setLocked(true);
		XSSFCellStyle unLockedStyle = wb.createCellStyle();
		unLockedStyle.setLocked(false);

		XSSFRow row;
		XSSFCell cell;

		row = shSummary.getRow(3);
		cell = row.getCell(2);
		cell.setCellValue(modelCluster.getDescription(language.getValue()));
		cell.setCellStyle(lockedStyle);

		// logger.error("Is locked {}" , cell.getCellStyle().getLocked());

		row = shSummary.getRow(4);
		cell = row.getCell(2);
		cell.setCellValue(model.getId().toString());
		cell.setCellStyle(lockedStyle);

		row = shSummary.getRow(5);
		cell = row.getCell(2);
		cell.setCellValue(model.getCode());
		cell.setCellStyle(lockedStyle);

		row = shSummary.getRow(6);
		cell = row.getCell(2);
		cell.setCellValue(model.getDescription(language.getValue()));
		cell.setCellStyle(lockedStyle);

		row = shSummary.getRow(7);
		cell = row.getCell(2);
		cell.setCellValue(modelOptionalType.getIdOptionalType());
		cell.setCellStyle(lockedStyle);

		row = shSummary.getRow(8);
		cell = row.getCell(2);
		cell.setCellValue(version.getId());
		cell.setCellStyle(lockedStyle);

		row = shSummary.getRow(9);
		cell = row.getCell(2);
		cell.setCellValue(version.getCode());
		cell.setCellStyle(lockedStyle);

		row = shSummary.getRow(10);
		cell = row.getCell(2);
		cell.setCellValue(version.getDescription(language.getValue()));
		cell.setCellStyle(lockedStyle);

		row = shSummary.getRow(11);
		cell = row.getCell(2);
		cell.setCellValue(version.getStatus());
		cell.setCellStyle(lockedStyle);

		row = shSummary.getRow(12);
		cell = row.getCell(2);
		cell.setCellValue(language.name());
		cell.setCellStyle(lockedStyle);

		RepStatus r = new RepStatus();

		////////////////////////////////////////////////////////////////////////////////////////
		// creo lista id+descrizione
		Map<Long, String> sg = new HashMap<>();
		r.setCol(0);
		for (ModelOptionalGroup modelOptionalGroup : modelOptionalGroupRepository
				.findByModelOptType(modelOptionalType.getId())) {
			List<ModelOptionalSubGroup> lModelOptionalSubGroup = modelOptionalSubGroupRepository
					.findByModelOptGroup(modelOptionalGroup.getId());
			if (lModelOptionalSubGroup.size() == 0)
				continue;
			for (ModelOptionalSubGroup modelOptionalSubGroup : lModelOptionalSubGroup) {

				r.incRow();
				row = sheet3.createRow(r.getRow());

				r.setCol(0);
				cell = row.createCell(r.getCol());
				String sgDescr = "[" + modelOptionalSubGroup.getId().toString() + "] "
						+ modelOptionalGroup.getDescription(language.getValue()) + " - "
						+ modelOptionalSubGroup.getDescription(language.getValue());
				cell.setCellValue(sgDescr);
				sg.put(modelOptionalSubGroup.getId(), sgDescr);

			}
		}

		int numRowsSubgroups = r.getRow() + 1;
		////////////////////////////////////////////////////////////////////////////////////////

		// logger.error("Is locked {}" , cell.getCellStyle().getLocked());

		// formattazione condizionale e dropdown
		// validazione : yes/no
		XSSFDataValidationHelper dvHelper = new XSSFDataValidationHelper(sheet2);

		CellRangeAddressList cellRangeAddressList1 = new CellRangeAddressList(1, 2000, 0, 0); // ID

		XSSFDataValidationConstraint dvConstraint1 = (XSSFDataValidationConstraint) dvHelper.createNumericConstraint(
				XSSFDataValidationConstraint.ValidationType.DECIMAL, XSSFDataValidationConstraint.OperatorType.BETWEEN,
				"0", "0");

		XSSFDataValidation dataValidationCol1 = (XSSFDataValidation) dvHelper.createValidation(dvConstraint1,
				cellRangeAddressList1);
		dataValidationCol1.setShowErrorBox(true);
		dataValidationCol1.setSuppressDropDownArrow(true);
		dataValidationCol1.createErrorBox("Cella non modificabile, lasciare in bianco o immettere 0", "");
		sheet2.addValidationData(dataValidationCol1);

		CellRangeAddressList cellRangeAddressList5 = new CellRangeAddressList(1, 2000, 6, 6); // sequence
		XSSFDataValidationConstraint dvConstraint5 = (XSSFDataValidationConstraint) dvHelper.createNumericConstraint(
				XSSFDataValidationConstraint.ValidationType.DECIMAL, XSSFDataValidationConstraint.OperatorType.BETWEEN,
				"1", "99999999");

		XSSFDataValidation dataValidationCol5 = (XSSFDataValidation) dvHelper.createValidation(dvConstraint5,
				cellRangeAddressList5);
		dataValidationCol5.setShowErrorBox(true);
		dataValidationCol5.setSuppressDropDownArrow(true);
		dataValidationCol5.createErrorBox("Sequence", "Please enter a number");
		sheet2.addValidationData(dataValidationCol5);

		CellRangeAddressList cellRangeAddressList = new CellRangeAddressList(1, 2000, 7, 7); // Visibility
		XSSFDataValidationConstraint dvConstraint = (XSSFDataValidationConstraint) dvHelper
				.createExplicitListConstraint(new String[] { "Y", "N" });
		XSSFDataValidation dataValidationCol8 = (XSSFDataValidation) dvHelper.createValidation(dvConstraint,
				cellRangeAddressList);
		dataValidationCol8.setShowErrorBox(true);
		dataValidationCol8.setSuppressDropDownArrow(true);
		dataValidationCol8.createErrorBox("Visibility", "Please enter Y or N");
		sheet2.addValidationData(dataValidationCol8);

		CellRangeAddressList cellRangeAddressList2 = new CellRangeAddressList(1, 2000, 10, 10); // Flag ordinabilit�
		XSSFDataValidationConstraint dvConstraint2 = (XSSFDataValidationConstraint) dvHelper
				.createExplicitListConstraint(new String[] { "1", "2", "3" });
		XSSFDataValidation dataValidationCol10 = (XSSFDataValidation) dvHelper.createValidation(dvConstraint2,
				cellRangeAddressList2);
		dataValidationCol10.setShowErrorBox(true);
		dataValidationCol10.setSuppressDropDownArrow(true);
		dataValidationCol10.createErrorBox("Ordinabilità", "Please enter 1=Red, 2=Yellow, 3=Green");
		sheet2.addValidationData(dataValidationCol10);

		CellRangeAddressList cellRangeAddressList3 = new CellRangeAddressList(1, 2000, 1, 1); // Flag Tipo operazione
																								// col B
		XSSFDataValidationConstraint dvConstraint3 = (XSSFDataValidationConstraint) dvHelper
				.createExplicitListConstraint(
						new String[] { UPLOAD_ROW_UPDATE, UPLOAD_ROW_INSERT, UPLOAD_ROW_DELETE, UPLOAD_ROW_NOCHANGE });
		XSSFDataValidation dataValidationCol3 = (XSSFDataValidation) dvHelper.createValidation(dvConstraint3,
				cellRangeAddressList3);
		dataValidationCol3.setShowErrorBox(true);
		dataValidationCol3.setSuppressDropDownArrow(true);
		dataValidationCol3.createErrorBox("Request operation", "Please enter UPDATE, INSERT, DELETE, NOCHANGE");
		sheet2.addValidationData(dataValidationCol3);

		XSSFName namedCell = wb.createName();
		namedCell.setNameName("SUBGROUPS");
		namedCell.setRefersToFormula(EXCEL_FULL_SHEETNAME_GROUPS + "!$A$2:$A$" + numRowsSubgroups);

		CellRangeAddressList cellRangeAddressList4 = new CellRangeAddressList(1, 2000, 5, 5); // lista subgroups
		XSSFDataValidationConstraint dvConstraint4 = (XSSFDataValidationConstraint) dvHelper
				.createFormulaListConstraint("SUBGROUPS");
		XSSFDataValidation dataValidationColz = (XSSFDataValidation) dvHelper.createValidation(dvConstraint4,
				cellRangeAddressList4);
		dataValidationColz.setShowErrorBox(true);
		dataValidationColz.setSuppressDropDownArrow(true);
		dataValidationColz.createErrorBox("Subgroup", "Please choice");
		sheet2.addValidationData(dataValidationColz);

		/////////////////////////////////////////////////

		XSSFSheetConditionalFormatting sheetCF = sheet2.getSheetConditionalFormatting();

		ConditionalFormattingRule rule1 = sheetCF.createConditionalFormattingRule(ComparisonOperator.GE, "3");
		PatternFormatting fill1 = rule1.createPatternFormatting();
		fill1.setFillBackgroundColor(IndexedColors.BRIGHT_GREEN.index);
		fill1.setFillPattern(PatternFormatting.SOLID_FOREGROUND);

		ConditionalFormattingRule rule2 = sheetCF.createConditionalFormattingRule(ComparisonOperator.GE, "2");
		PatternFormatting fill2 = rule2.createPatternFormatting();
		fill2.setFillBackgroundColor(IndexedColors.YELLOW.index);
		fill2.setFillPattern(PatternFormatting.SOLID_FOREGROUND);

		ConditionalFormattingRule rule3 = sheetCF.createConditionalFormattingRule(ComparisonOperator.GE, "1");
		PatternFormatting fill3 = rule3.createPatternFormatting();
		fill3.setFillBackgroundColor(IndexedColors.RED.index);
		fill3.setFillPattern(PatternFormatting.SOLID_FOREGROUND);

		ConditionalFormattingRule rule4 = sheetCF.createConditionalFormattingRule("H2=\"Y\"");
		PatternFormatting fill4 = rule4.createPatternFormatting();
		fill4.setFillBackgroundColor(IndexedColors.BRIGHT_GREEN.index);
		fill4.setFillPattern(PatternFormatting.SOLID_FOREGROUND);

		ConditionalFormattingRule rule5 = sheetCF.createConditionalFormattingRule("H2=\"N\"");
		PatternFormatting fill5 = rule5.createPatternFormatting();
		fill5.setFillBackgroundColor(IndexedColors.RED.index);
		fill5.setFillPattern(PatternFormatting.SOLID_FOREGROUND);

		CellRangeAddress[] regions = { CellRangeAddress.valueOf("K2:K1000") };
		ConditionalFormattingRule[] cfRules = { rule1, rule2, rule3 };
		sheetCF.addConditionalFormatting(regions, cfRules);

		CellRangeAddress[] region2 = { CellRangeAddress.valueOf("H2:H1000") };
		ConditionalFormattingRule[] cfRules2 = { rule4, rule5 };
		sheetCF.addConditionalFormatting(region2, cfRules2);

		// imposta protezione colonna 1

		r.setRow(0);

		String enableAll = "Y"; // ignora i criteri di visibilità per riga opt

		List<Long> lOptChoiceYes = getOptionalsSelected(jVersionOptForm);

		if (lOptChoiceYes.isEmpty()) {
			lOptChoiceYes.add(0L); // permette di usare la in a livello di select nella chiamata getOptionalList
		}

		List<VersionOptXlsV1> lVersionOptXlsV1 = versionOptXlsV1Repository.getOptionalList(idversion, "it-IT",
				lOptChoiceYes, enableAll);

		for (VersionOptXlsV1 versionOptXlsV1 : lVersionOptXlsV1) {

			// leggo riga corrispondente in inglese
			VersionOptXlsV1 versionOptXlsV1EN = versionOptXlsV1Repository.getOptional(idversion, "en-US",
					versionOptXlsV1.getId().getIdOpt());

			Boolean longText = Boolean.FALSE;
			r.incRow();
			row = sheet2.createRow(r.getRow());

			r.setCol(0);
			cell = row.createCell(r.getCol());
			cell.setCellValue(versionOptXlsV1.getId().getIdOpt());
			// logger.error("Is locked {}" , cell.getCellStyle().getLocked());
			// logger.error("Cells tule id {}" , cell.getCellStyle().getIndex());
			cell.setCellStyle(lockedStyle);

			r.setCol(1);
			cell = row.createCell(r.getCol());
			cell.setCellValue(UPLOAD_ROW_NOCHANGE);
			cell.setCellStyle(unLockedStyle);

			r.setCol(2);
			cell = row.createCell(r.getCol());
			cell.setCellValue(versionOptXlsV1.getOptCode());
			cell.setCellStyle(unLockedStyle);

			r.setCol(3);
			cell = row.createCell(r.getCol());
			cell.setCellValue(versionOptXlsV1.getOptDescription()); // IT
			cell.setCellStyle(unLockedStyle);

			r.setCol(4);
			cell = row.createCell(r.getCol());
			cell.setCellValue(versionOptXlsV1EN.getOptDescription()); // EN
			cell.setCellStyle(unLockedStyle);

			r.setCol(5);
			cell = row.createCell(r.getCol());
			cell.setCellValue(sg.get(versionOptXlsV1.getIdSubgroup()));
			cell.setCellStyle(unLockedStyle);

			r.setCol(6);
			cell = row.createCell(r.getCol());
			cell.setCellValue(versionOptXlsV1.getSeq());
			cell.setCellStyle(unLockedStyle);

			r.setCol(7);
			cell = row.createCell(r.getCol());
			cell.setCellValue(versionOptXlsV1.getVisibleFlag());
			cell.setCellStyle(unLockedStyle);

			r.setCol(8);
			cell = row.createCell(r.getCol());
			cell.setCellValue(versionOptXlsV1.getOrderableText()); // IT
			cell.setCellStyle(unLockedStyle);

			r.setCol(9);
			cell = row.createCell(r.getCol());
			cell.setCellValue(versionOptXlsV1EN.getOrderableText()); // EN
			cell.setCellStyle(unLockedStyle);

			r.setCol(10);
			cell = row.createCell(r.getCol());
			cell.setCellValue(Double.parseDouble(versionOptXlsV1.getOrderableFlag()));
			cell.setCellStyle(unLockedStyle);

			r.setCol(11);
			cell = row.createCell(r.getCol());
			cell.setCellValue(versionOptXlsV1.getVersionModel()); // IT
			cell.setCellStyle(unLockedStyle);

			r.setCol(12);
			cell = row.createCell(r.getCol());
			cell.setCellValue(versionOptXlsV1EN.getVersionModel());// EN
			cell.setCellStyle(unLockedStyle);

			r.setCol(13);
			cell = row.createCell(r.getCol());
			if (versionOptXlsV1.getTextA1Long() != null) {
				cell.setCellValue(versionOptXlsV1.getTextA1Long());
				if (versionOptXlsV1.getTextA1Long().length() > 30)
					longText = Boolean.TRUE;
			}
			cell.setCellStyle(unLockedStyle);

			r.setCol(14);
			cell = row.createCell(r.getCol());
			if (versionOptXlsV1.getTextA1Long() != null) {
				cell.setCellValue(versionOptXlsV1EN.getTextA1Long());
				if (versionOptXlsV1.getTextA1Long().length() > 30)
					longText = Boolean.TRUE;
			}
			cell.setCellStyle(unLockedStyle);

			r.setCol(15);
			cell = row.createCell(r.getCol());
			if (versionOptXlsV1.getTextA5VincoliOpt() != null) {
				cell.setCellValue(versionOptXlsV1.getTextA5VincoliOpt());
				if (versionOptXlsV1.getTextA5VincoliOpt().length() > 30)
					longText = Boolean.TRUE;

			}
			cell.setCellStyle(unLockedStyle);

			r.setCol(16);
			cell = row.createCell(r.getCol());
			if (versionOptXlsV1.getTextA5VincoliOpt() != null) {
				cell.setCellValue(versionOptXlsV1EN.getTextA5VincoliOpt());
				if (versionOptXlsV1.getTextA5VincoliOpt().length() > 30)
					longText = Boolean.TRUE;

			}
			cell.setCellStyle(unLockedStyle);

			r.setCol(17);
			cell = row.createCell(r.getCol());
			if (versionOptXlsV1.getTextB1VincoliOmologazione() != null) {
				cell.setCellValue(versionOptXlsV1.getTextB1VincoliOmologazione());
				if (versionOptXlsV1.getTextB1VincoliOmologazione().length() > 30)
					longText = Boolean.TRUE;
			}
			cell.setCellStyle(unLockedStyle);

			r.setCol(18);
			cell = row.createCell(r.getCol());
			if (versionOptXlsV1.getTextB1VincoliOmologazione() != null) {
				cell.setCellValue(versionOptXlsV1EN.getTextB1VincoliOmologazione());
				if (versionOptXlsV1.getTextB1VincoliOmologazione().length() > 30)
					longText = Boolean.TRUE;
			}
			cell.setCellStyle(unLockedStyle);

			r.setCol(19);
			cell = row.createCell(r.getCol());
			if (versionOptXlsV1.getTextC0NoteInterne() != null) {
				cell.setCellValue(versionOptXlsV1.getTextC0NoteInterne());
				if (versionOptXlsV1.getTextC0NoteInterne().length() > 30)
					longText = Boolean.TRUE;
			}
			cell.setCellStyle(unLockedStyle);

			r.setCol(20);
			cell = row.createCell(r.getCol());
			if (versionOptXlsV1.getTextC0NoteInterne() != null) {
				cell.setCellValue(versionOptXlsV1EN.getTextC0NoteInterne());
				if (versionOptXlsV1.getTextC0NoteInterne().length() > 30)
					longText = Boolean.TRUE;
			}
			cell.setCellStyle(unLockedStyle);

			r.setCol(21);
			cell = row.createCell(r.getCol());
			if (versionOptXlsV1.getTextC2StatusSvil() != null) {
				cell.setCellValue(versionOptXlsV1.getTextC2StatusSvil());
				if (versionOptXlsV1.getTextC2StatusSvil().length() > 30)
					longText = Boolean.TRUE;
			}
			cell.setCellStyle(unLockedStyle);

			r.setCol(22);
			cell = row.createCell(r.getCol());
			if (versionOptXlsV1.getTextC2StatusSvil() != null) {
				cell.setCellValue(versionOptXlsV1EN.getTextC2StatusSvil());
				if (versionOptXlsV1.getTextC2StatusSvil().length() > 30)
					longText = Boolean.TRUE;
			}
			cell.setCellStyle(unLockedStyle);

			r.setCol(23);
			cell = row.createCell(r.getCol());
			if (versionOptXlsV1.getTextC4Capacity() != null) {
				cell.setCellValue(versionOptXlsV1.getTextC4Capacity());
				if (versionOptXlsV1.getTextC4Capacity().length() > 30)
					longText = Boolean.TRUE;
			}
			cell.setCellStyle(unLockedStyle);

			r.setCol(24);
			cell = row.createCell(r.getCol());
			if (versionOptXlsV1.getTextC4Capacity() != null) {
				cell.setCellValue(versionOptXlsV1EN.getTextC4Capacity());
				if (versionOptXlsV1.getTextC4Capacity().length() > 30)
					longText = Boolean.TRUE;
			}
			cell.setCellStyle(unLockedStyle);

			float defaultrowheight = sheet2.getDefaultRowHeightInPoints();
			if (longText)
				row.setHeightInPoints(4 * defaultrowheight);

		}

		sheet2.protectSheet("passw0rd");
		shSummary.protectSheet("passw0rd");

		String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
		// sendXlsxToFileSystem(wb,"FULL_XLS_"+model.getCode()+"_"+version.getCode()+"_"+timeStamp+".xlsx");
		// // temporaneo per TEST
		sendXlsxToBrowser(response, wb,
				"FULL_XLS_" + model.getCode() + " " + version.getCode() + "_" + "_" + timeStamp + ".xlsx");
		return;

	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	public void createPdfOptionalsByVersion(JVersionOptForm jVersionOptForm
										  , LocalizationType language
										  , Long idversion
										  , String forceDealerMode
										  , HttpServletResponse response) throws IOException {

		
		Boolean flagForceDealerMode=Boolean.FALSE;
		if (forceDealerMode!=null && "DEALER".equals(forceDealerMode)) {
			logger.warn("PDF user-mode = {}", forceDealerMode);
			flagForceDealerMode=Boolean.TRUE;
		}
		
		
		
		if (!"Y".equals(uiService.getAuthorizationById("PDF-1")))
			throw new ValidationException(-1, "USER_PERMISSION_PDF");

		Version version = versionRepository.findById(idversion);
		String enable = uiService.getAuthorizationByVersionStatus(version.getStatus());
		if (!("Y").equals(enable)) {
			throw new ValidationException(-1, "VERSION_NOT_ACCESSIBLE");
		}

		List<Long> lOptChoiceYes = getOptionalsSelected(jVersionOptForm);

		if (lOptChoiceYes.isEmpty())
			throw new ValidationException(-1, "NO_OPT_SELECTED");

		JPdf jPdf = new JPdf();

		JVersion jVersion = versionService.buildFromVersion(language, version);
		jVersion.setAuthorizations(null); // rimuovo auth: in questo contesto non servono
		jPdf.setjVersion(jVersion);

		ModelOptionalType modelOptionalType = modelOptionalTypeRepository.findById(version.getIdModelOptionalType());

		Model model = modelRepository.findById(modelOptionalType.getIdModel());
		JModel jModel = modelService.buildFromModel(language, model, version);

		// forza internal use only se necessario
		if (getPdfInternalUseOnly()) {
			jModel.setConfidential(VModelService.CONFIDENTIAL);

		}

		jPdf.setjModel(jModel);

		////////////////////////////////////////////////////////////////////////////////////////////////

		JPdfType jPdfType = new JPdfType();
		jPdfType.setPdfType(getPdfTypeFromAuth()); // INTERNAL_USE_ONLY | PUBLIC
		jPdfType.setPdfVersion(PDF_VERSION);
		jPdf.setjPdfType(jPdfType);

		////////////////////////////////////////////////////////////////////////////////////////////////
		String enableAll = "N";
		if ("Y".equals(uiService.getAuthorizationById("OP-VI"))) {
			enableAll = "Y"; // enable all viene usata nella query di caricamento
		}
		
		// V2 TODO per impersonare Dealer impostare enableAll="N"; si puo' migliorare imprsonificando un ruolo DEALER
		if (flagForceDealerMode)
			enableAll = "N";


		
		
		Long countGroup = versionOptXlsV1Repository.getGroupCount(idversion, language.getValue(), lOptChoiceYes,
				enableAll);

		// System.out.println("GruppiOptional="+countGroup);

		List<JPdfOptional> lJPdfOptional = new ArrayList<JPdfOptional>();

		for (VersionOptXlsV1 versionOptXlsV1 : versionOptXlsV1Repository.getOptionalList(idversion, language.getValue(),
				lOptChoiceYes, enableAll)) {

			JPdfOptional jPdfOptional = new JPdfOptional();

			jPdfOptional.setOptId(versionOptXlsV1.getId().getIdOpt());
			jPdfOptional.setOptCode(versionOptXlsV1.getOptCode());
			jPdfOptional.setOptDescription(versionOptXlsV1.getOptDescription());
			jPdfOptional.setVisible(versionOptXlsV1.getVisibleFlag());
			jPdfOptional.setOrderableText(versionOptXlsV1.getOrderableText());
			jPdfOptional.setOrderableFlag(versionOptXlsV1.getOrderableFlag());
			jPdfOptional.setVersionModel(versionOptXlsV1.getVersionModel());
			jPdfOptional.setIdModelOptionalGroup(versionOptXlsV1.getIdGroup());
			jPdfOptional.setIdModelOptionalSubGroup(versionOptXlsV1.getIdSubgroup());
			jPdfOptional.setCodGroup(versionOptXlsV1.getCodGroup());
			jPdfOptional.setCodSubgroup(versionOptXlsV1.getCodSubgroup());
			jPdfOptional.setGroupDescription(versionOptXlsV1.getGroupDescription());
			jPdfOptional.setSubgroupDescription(versionOptXlsV1.getSubgroupDescription());

			jPdfOptional.setLastUpdate(versionOptXlsV1.getLastUpdate());
			jPdfOptional.setSeq(versionOptXlsV1.getSeq());
			jPdfOptional.setSeqGroup(versionOptXlsV1.getSeqGroup());
			jPdfOptional.setSeqSubgroup(versionOptXlsV1.getSeqSubgroup());
			jPdfOptional.setIdImageP(versionOptXlsV1.getIdImageP());
			jPdfOptional.setIdImageS1(versionOptXlsV1.getIdImageS1());
			jPdfOptional.setIdImageS2(versionOptXlsV1.getIdImageS2());
			jPdfOptional.setIdImageS3(versionOptXlsV1.getIdImageS3());
			jPdfOptional.setIdImageS4(versionOptXlsV1.getIdImageS4());

			jPdfOptional.setUrlImageP(mediaService.getThronUrl(versionOptXlsV1.getIdImageP(), null));
			jPdfOptional.setUrlImageS1(mediaService.getThronUrl(versionOptXlsV1.getIdImageS1(), null));
			jPdfOptional.setUrlImageS2(mediaService.getThronUrl(versionOptXlsV1.getIdImageS2(), null));
			jPdfOptional.setUrlImageS3(mediaService.getThronUrl(versionOptXlsV1.getIdImageS3(), null));
			jPdfOptional.setUrlImageS4(mediaService.getThronUrl(versionOptXlsV1.getIdImageS4(), null));

			jPdfOptional.setTextA1Long(versionOptXlsV1.getTextA1Long());
			jPdfOptional.setTextA5VincoliOpt(versionOptXlsV1.getTextA5VincoliOpt());
			jPdfOptional.setTextB1VincoliOmologazione(versionOptXlsV1.getTextB1VincoliOmologazione());
			jPdfOptional.setTextC0NoteInterne(versionOptXlsV1.getTextC0NoteInterne());
			jPdfOptional.setTextC2StatusSvil(versionOptXlsV1.getTextC2StatusSvil());
			jPdfOptional.setTextC4Capacity(versionOptXlsV1.getTextC4Capacity());
			lJPdfOptional.add(jPdfOptional);

		}
		jPdf.setjPdfOptionals(lJPdfOptional);
		////////////////////////////////////////////////////////////////////////////////////////////////

		// 
		// TODO inclusione Figurini collegati in PDF : da testare
		// nella jentity ricevo array JVerionAttcSimple
		List<JPdfDrawing> lJPdfDrawing = new ArrayList<JPdfDrawing>();

		if (jVersionOptForm.getlJVersionAttcSimple() != null && !jVersionOptForm.getlJVersionAttcSimple().isEmpty()) {
			logger.warn("Drawings:jVersionOptForm.getlJVersionAttcSimple() {} ", jVersionOptForm.getlJVersionAttcSimple());
			
			logger.warn("Drawings: scelti da interfaccia {}", jVersionOptForm.getlJVersionAttcSimple());
			
			
			// lista Figurini da array di scelte
			for (JVersionAttcSimple jVersionAttcSimple : jVersionOptForm.getlJVersionAttcSimple()) {
				VersionAttc versionAttc = versionAttcRepository.findById(jVersionAttcSimple.getId());
				if (versionAttc != null) {
					JPdfDrawing jPdfDrawing = new JPdfDrawing();
					jPdfDrawing.setId(versionAttc.getId());
					jPdfDrawing.setIdImage(versionAttc.getIdImage());
					jPdfDrawing.setIdImageThumb(versionAttc.getIdImageThumb());
					jPdfDrawing.setIdImageSvg(versionAttc.getIdImageSvg());

					jPdfDrawing.setDescription(versionAttc.getDescription(language.getValue()));
					jPdfDrawing.setSeq(versionAttc.getSequence());
					lJPdfDrawing.add(jPdfDrawing);
				}

			}

		} else {

			logger.warn("Drawings: funzionalità standard tutti i Figurini {}", versionAttcRepository.findByVersionTypeAll(idversion, "F"));
			
			for (VersionAttc versionAttc : versionAttcRepository.findByVersionTypeAll(idversion, "F")) {
				JPdfDrawing jPdfDrawing = new JPdfDrawing();
				jPdfDrawing.setId(versionAttc.getId());
				jPdfDrawing.setIdImage(versionAttc.getIdImage());
				jPdfDrawing.setIdImageThumb(versionAttc.getIdImageThumb());
				jPdfDrawing.setIdImageSvg(versionAttc.getIdImageSvg());

				jPdfDrawing.setDescription(versionAttc.getDescription(language.getValue()));
				jPdfDrawing.setSeq(versionAttc.getSequence());
				lJPdfDrawing.add(jPdfDrawing);
			}

		}

		jPdf.setjPdfDrawings(lJPdfDrawing);
		////////////////////////////////////////////////////////////////////////////////////////////////

		// imposta le label (column heading) per XLS Grid
		JXlsBaseLabels jXlsBaseLabels = getHeadingsXlsBase(language);
		jPdf.setjXlsBaseLabels(jXlsBaseLabels);

		// imposta le label generiche del PDF
		JPdfBaseLabels jPdfBaseLabels = getHeadingsPdfBase(language);
		jPdf.setjPdfBaseLabels(jPdfBaseLabels);

		// autorizzazioni per emissione dati
		List<JPageAuth> lJPageAuth = uiService.getAuthorizationList(UiService.PAGE_PDF);
		jPdf.setjPageAuth(lJPageAuth);

		ObjectMapper objectMapper = new ObjectMapper();
		String jsonString = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(jPdf);

		// logger.warn("json iniziale {}", jsonString);

		final KeyBuilder paramString = PdfUtils.paramString();
		final KeyBuilder paramImage = PdfUtils.paramImage();
		final KeyBuilder paramTemplate = PdfUtils.paramTemplate();
		final KeyBuilder paramSvg = PdfUtils.paramSvg();
		final KeyBuilder paramPdf = PdfUtils.paramPdf();
		final KeyBuilder paramObj = PdfUtils.paramObj();
		final KeyBuilder paramArray = PdfUtils.paramArray();

		List<List<ParameterDTO>> lDrw = new ArrayList<>();
		for (JPdfDrawing drw : lJPdfDrawing) {
			List<ParameterDTO> lVal = new ArrayList<>();
			lVal.add(paramString.key("drawingId").value(drw.getId().toString()).build());
			lVal.add(paramString.key("drawingDescription").value(drw.getDescription()).build());
			lVal.add(paramPdf.key("image").value(drw.getIdImage()).build());
			lVal.add(paramSvg.key("imageSvg").value(drw.getIdImageSvg()).build());
			lVal.add(paramString.key("seq").value(String.format("%010d", drw.getSeq())).build());

			lDrw.add(lVal);
		}

		MediaConfig mediaConfig = null;

		Map<String, Object> extraInfoP = new HashMap<String, Object>();
		Map<String, Object> extraInfoS = new HashMap<String, Object>();
		if (jModel.getQuality() == null || (mediaConfig = mediaConfigRepository.findOne(jModel.getQuality())) == null) {
			/***
			 * Default value
			 */
			extraInfoP.put(Constant.COMPRESSION_RATE, 0.9D);
			extraInfoP.put(Constant.RESIZE_FACTOR, 1.5D);

			extraInfoS.put(Constant.COMPRESSION_RATE, 0.5D);
			extraInfoS.put(Constant.RESIZE_FACTOR, 5D);
		} else {
			if (mediaConfig.getImagePCompressRate() != null) {
				extraInfoP.put(Constant.COMPRESSION_RATE, mediaConfig.getImagePCompressRate());
			}
			if (mediaConfig.getImagePResizeFactor() != null) {
				extraInfoP.put(Constant.RESIZE_FACTOR, mediaConfig.getImagePResizeFactor());
			}
			if (mediaConfig.getImageSCompressRate() != null) {
				extraInfoS.put(Constant.COMPRESSION_RATE, mediaConfig.getImageSCompressRate());
			}
			if (mediaConfig.getImagePResizeFactor() != null) {
				extraInfoS.put(Constant.RESIZE_FACTOR, mediaConfig.getImageSResizeFactor());
			}
		}

		List<List<ParameterDTO>> lOpt = new ArrayList<>();
		for (JPdfOptional opt : lJPdfOptional) {
			List<ParameterDTO> lVal = new ArrayList<>();
			lVal.add(paramString.key("optId").value(opt.getOptId().toString()).build());
			lVal.add(paramString.key("optCode").value(opt.getOptCode()).build());
			lVal.add(paramString.key("optDescription").value(opt.getOptDescription()).build());

			lVal.add(paramString.key("visible").value(opt.getVisible()).build());
			lVal.add(paramString.key("orderableText").value(opt.getOrderableText()).build());
			lVal.add(paramString.key("orderableFlag").value(opt.getOrderableFlag()).build());
			lVal.add(paramString.key("versionModel").value(opt.getVersionModel()).build());
			lVal.add(paramString.key("codGroup").value(opt.getCodGroup()).build());
			lVal.add(paramString.key("codSubgroup").value(opt.getCodSubgroup()).build());
			lVal.add(paramString.key("groupDescription").value(opt.getGroupDescription()).build());
			lVal.add(paramString.key("subgroupDescription").value(opt.getSubgroupDescription()).build());

			/*
			 * DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss"); String
			 * strDate = dateFormat.format(date); System.out.println("Converted String: " +
			 * strDate);
			 */
			DateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");

			// lVal.add(paramString.key("lastUpdate").value(opt.getLastUpdate().toString()).build());
			lVal.add(paramString.key("lastUpdate").value(dateFormat.format(opt.getLastUpdate())).build());

			lVal.add(paramString.key("seq").value(String.format("%10d", opt.getSeq())).build());
			lVal.add(paramString.key("seqGroup").value(String.format("%10d", opt.getSeqGroup())).build());
			lVal.add(paramString.key("seqSubgroup").value(String.format("%10d", opt.getSeqSubgroup())).build());

			lVal.add(paramString.key("textA1Long").value(opt.getTextA1Long()).build());
			lVal.add(paramString.key("textA5VincoliOpt").value(opt.getTextA5VincoliOpt()).build());
			lVal.add(paramString.key("textB1VincoliOmologazione").value(opt.getTextB1VincoliOmologazione()).build());
			lVal.add(paramString.key("textC0NoteInterne").value(opt.getTextC0NoteInterne()).build());
			lVal.add(paramString.key("textC2StatusSvil").value(opt.getTextC2StatusSvil()).build());
			lVal.add(paramString.key("textC4Capacity").value(opt.getTextC4Capacity()).build());

			lVal.add(paramImage.key("ImageP").value(opt.getIdImageP()).extraInfo(extraInfoP).build());
			lVal.add(paramImage.key("ImageS1").value(opt.getIdImageS1()).extraInfo(extraInfoS).build());
			lVal.add(paramImage.key("ImageS2").value(opt.getIdImageS2()).extraInfo(extraInfoS).build());
			lVal.add(paramImage.key("ImageS3").value(opt.getIdImageS3()).extraInfo(extraInfoS).build());
			lVal.add(paramImage.key("ImageS4").value(opt.getIdImageS4()).extraInfo(extraInfoS).build());
			lOpt.add(lVal);

		}

		List<ParameterDTO> pdfType = new ArrayList<>();
		pdfType.add(paramString.key("pdfType").value(jPdfType.getPdfType()).build());
		pdfType.add(paramString.key("pdfVersion").value(jPdfType.getPdfVersion()).build());

		pdfType.add(paramString.key("versionCode").value(jPdf.getjVersion().getCode()).build());
		pdfType.add(paramString.key("versionStatus").value(jPdf.getjVersion().getStatus()).build());
		pdfType.add(paramString.key("versionStatusDescr").value(jPdf.getjVersion().getStatusDescription()).build());
		pdfType.add(paramString.key("versionDescr").value(jPdf.getjVersion().getDescription()).build());
		pdfType.add(paramString.key("versionLongDescr").value(jPdf.getjVersion().getDescriptionLong()).build());
		if (jPdf.getjVersion().getPublishDate() != null) {
			pdfType.add(paramString.key("versionPublishDate").value(jPdf.getjVersion().getPublishDate().toString())
					.build());
		} else {
			pdfType.add(paramString.key("versionPublishDate").value(null).build());
		}

		List<ParameterDTO> vehicle = new ArrayList<>();
		vehicle.add(paramString.key("modelCode").value(jModel.getCode()).build());
		vehicle.add(paramString.key("modelDescription").value(jModel.getDescription()).build());
		vehicle.add(paramImage.key("modelLogo").value(jModel.getIdImageLogo()).build());
		vehicle.add(paramImage.key("modelCover").value(jModel.getIdImageCover()).build());
		vehicle.add(paramString.key("modelConfidential").value(jModel.getConfidential()).build());

		List<ParameterDTO> labels = new ArrayList<>();
		labels.add(paramString.key("labelHeader").value(jPdfBaseLabels.getLabelHeader()).build());
		labels.add(paramString.key("labelIndex").value(jPdfBaseLabels.getLabelIndex()).build());
		labels.add(paramString.key("labelOptional").value(jPdfBaseLabels.getLabelOptional()).build());
		labels.add(paramString.key("labelDrawing").value(jPdfBaseLabels.getLabelDrawing()).build());
		labels.add(paramString.key("labelGrid").value(jPdfBaseLabels.getLabelGrid()).build());
		labels.add(paramString.key("labelWatermark").value(jPdfBaseLabels.getLabelWatermark()).build());
		labels.add(paramString.key("labelUsage").value(jPdfBaseLabels.getLabelUsage()).build());
		labels.add(paramImage.key("logoFerrari").value(jPdfBaseLabels.getLogoFerrari()).build());

		labels.add(paramString.key("labelPage").value(jPdfBaseLabels.getLabelPage()).build());
		labels.add(paramString.key("labelDate").value(jPdfBaseLabels.getLabelDate()).build());

		labels.add(paramString.key("labelRules").value(jPdfBaseLabels.getLabelRules()).build());
		labels.add(paramString.key("labelWith").value(jPdfBaseLabels.getLabelWith()).build());
		labels.add(paramString.key("labelHomologation").value(jPdfBaseLabels.getLabelHomologation()).build());
		labels.add(paramString.key("labelNote").value(jPdfBaseLabels.getLabelNote()).build());
		labels.add(paramString.key("labelStatus").value(jPdfBaseLabels.getLabelStatus()).build());
		labels.add(paramString.key("labelCapacity").value(jPdfBaseLabels.getLabelCapacity()).build());
		labels.add(paramString.key("labelOrderableText").value(jPdfBaseLabels.getLabelOrderableText()).build());
		labels.add(paramString.key("labelLastUpdate").value(jPdfBaseLabels.getLabelLastUpdate()).build());

		List<ParameterDTO> labelsXls = new ArrayList<>();
		labelsXls.add(paramString.key("xlabelModel").value(jXlsBaseLabels.getLabelModel()).build());
		labelsXls.add(paramString.key("xlabelArea").value(jXlsBaseLabels.getLabelArea()).build());
		labelsXls.add(paramString.key("xlabelMacroCategoria").value(jXlsBaseLabels.getLabelMacroCategoria()).build());
		labelsXls.add(paramString.key("xlabelSottoCategoria").value(jXlsBaseLabels.getLabelSottoCategoria()).build());
		labelsXls.add(paramString.key("xlabelCodOpt").value(jXlsBaseLabels.getLabelCodOpt()).build());
		labelsXls.add(paramString.key("xlabelShortDescOpt").value(jXlsBaseLabels.getLabelShortDescOpt()).build());
		labelsXls.add(paramString.key("xlabelVincoliOpt").value(jXlsBaseLabels.getLabelVincoliOpt()).build());
		labelsXls.add(
				paramString.key("xlabelVincoliOmologativi").value(jXlsBaseLabels.getLabelVincoliOmologativi()).build());
		labelsXls.add(paramString.key("xlabelOrdFlag").value(jXlsBaseLabels.getLabelOrdFlag()).build());
		labelsXls.add(paramString.key("xlabelOrdText").value(jXlsBaseLabels.getLabelOrdText()).build());
		labelsXls.add(paramString.key("xlabelDescription").value(jXlsBaseLabels.getLabelDescription()).build());
		labelsXls.add(paramString.key("xlabelVersionModel").value(jXlsBaseLabels.getLabelVersionModel()).build());
		labelsXls.add(paramString.key("xlabelUpdateDate").value(jXlsBaseLabels.getLabelUpdateDate()).build());
		labelsXls.add(paramString.key("xlabelInternalNote").value(jXlsBaseLabels.getLabelInternalNote()).build());
		labelsXls.add(paramString.key("xlabelStatusSviluppo").value(jXlsBaseLabels.getLabelStatusSviluppo()).build());
		labelsXls.add(paramString.key("xlabelCapProduttiva").value(jXlsBaseLabels.getLabelCapProduttiva()).build());
		labelsXls.add(paramString.key("xlabelSequence").value(jXlsBaseLabels.getLabelSequence()).build());
		labelsXls.add(paramString.key("xlabelVisibility").value(jXlsBaseLabels.getLabelVisibility()).build());

		List<ParameterDTO> auth = new ArrayList<>();
		for (JPageAuth jPageAuth : lJPageAuth) {
			auth.add(paramString.key(jPageAuth.getAuthId()).value(jPageAuth.getPermission()).build());
		}

		ReportDTO z = ReportDTO.builder().templateId(getTemplate(UI_TEMPLATE_ID)).compress(Boolean.TRUE)
				.parameters(Arrays.asList(

						paramTemplate.key("Cover.jasper").value(getTemplate(UI_TEMPLATE_COVER_J)).build(),
						// paramTemplate.key("Menu.jasper").value(getTemplate(UI_TEMPLATE_MENU_J+countGroup.toString())).build(),
						paramTemplate.key("Menu.jasper").value(getTemplate(UI_TEMPLATE_MENU_J)).build(),
						paramTemplate.key("SVG.jasper").value(getTemplate(UI_TEMPLATE_SVG_J)).build(),
						paramTemplate.key("PDF.jasper").value(getTemplate(UI_TEMPLATE_PDF_J)).build(),
						paramTemplate.key("Detail.jasper").value(getTemplate(UI_TEMPLATE_DETAIL_J)).build(),
						paramTemplate.key("Excel.jasper").value(getTemplate(UI_TEMPLATE_EXCEL_J)).build(),
						paramTemplate.key("Description.jasper").value(getTemplate(UI_TEMPLATE_DESCRIPTION_J)).build(),

						paramObj.key("pdfType").list(pdfType).build(), paramObj.key("vehicle").list(vehicle).build(),
						paramObj.key("labels").list(labels).build(), paramObj.key("labelsXls").list(labelsXls).build(),
						paramArray.key("optionals").matrix(lOpt).build(),
						paramArray.key("drawings").matrix(lDrw).build(),
						paramObj.key("authorizations").list(auth).build()

				)).build();

		// ObjectMapper objectMapper2 = new ObjectMapper();
		// String jsonString2 =
		// objectMapper2.writerWithDefaultPrettyPrinter().writeValueAsString(z);
		// logger.warn(jsonString2);
		// System.out.println(jsonString2);

		Response generate = pdfClient.generate(z);

		/*
		 * 
		 * Response generate = pdfClient.generate(ReportDTO.builder()
		 * .templateId(getTemplate(UI_TEMPLATE_ID)) .compress(Boolean.TRUE)
		 * .parameters(Arrays.asList(
		 * 
		 * paramTemplate.key("Cover.jasper").value(getTemplate(UI_TEMPLATE_COVER_J)).
		 * build(),
		 * //paramTemplate.key("Menu.jasper").value(getTemplate(UI_TEMPLATE_MENU_J+
		 * countGroup.toString())).build(),
		 * paramTemplate.key("Menu.jasper").value(getTemplate(UI_TEMPLATE_MENU_J)).build
		 * (),
		 * 
		 * paramTemplate.key("SVG.jasper").value(getTemplate(UI_TEMPLATE_SVG_J)).build()
		 * ,
		 * paramTemplate.key("PDF.jasper").value(getTemplate(UI_TEMPLATE_PDF_J)).build()
		 * ,
		 * 
		 * paramTemplate.key("Detail.jasper").value(getTemplate(UI_TEMPLATE_DETAIL_J)).
		 * build(),
		 * paramTemplate.key("Excel.jasper").value(getTemplate(UI_TEMPLATE_EXCEL_J)).
		 * build(), paramTemplate.key("Description.jasper").value(getTemplate(
		 * UI_TEMPLATE_DESCRIPTION_J)).build(),
		 * 
		 * 
		 * 
		 * 
		 * paramObj.key("pdfType").list(pdfType).build(),
		 * paramObj.key("vehicle").list(vehicle).build(),
		 * paramObj.key("labels").list(labels).build(),
		 * paramObj.key("labelsXls").list(labelsXls).build(),
		 * paramArray.key("optionals").matrix(lOpt).build(),
		 * paramArray.key("drawings").matrix(lDrw).build(),
		 * paramObj.key("authorizations").list(auth).build()
		 * 
		 * )).build());
		 * 
		 */

		if (generate.status() != 200) {
			throw new ValidationException(-1, "ERROR CALLING PDF Builder Status=" + generate.status());
		}

		String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
		String outFilenamePDF = "PDF_" + model.getCode() + "_" + version.getCode() + timeStamp + ".pdf";

		response.setContentType(DocService.MIME_APPLICATION_PDF);
		response.setHeader("Expires:", "0");
		response.setHeader("Content-Disposition", String.format("attachment; filename=\"%s\"", outFilenamePDF));

		IOUtils.copy(generate.body().asInputStream(), response.getOutputStream());

	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	private String getTemplate(String key) {

		UiApplicationSetting uiApplicationSetting = uiApplicationSettingRepository.findByKey(key);

		return uiApplicationSetting.getValue();
	}

	private JPdfBaseLabels getHeadingsPdfBase(LocalizationType language) {

		JPdfBaseLabels jPdfBaseLabels = new JPdfBaseLabels();
		UiDictionary uiDictionary;

		uiDictionary = uiDictionaryRepository.findByContextAndKey(LABELS_CONTEXT, LBL_HEADER);
		jPdfBaseLabels.setLabelHeader(LocalizationHelper.getLocalized(uiDictionary.getDescriptions(), language));

		uiDictionary = uiDictionaryRepository.findByContextAndKey(LABELS_CONTEXT, LBL_DRAWINGS);
		jPdfBaseLabels.setLabelDrawing(LocalizationHelper.getLocalized(uiDictionary.getDescriptions(), language));

		uiDictionary = uiDictionaryRepository.findByContextAndKey(LABELS_CONTEXT, LBL_INDEX);
		jPdfBaseLabels.setLabelIndex(LocalizationHelper.getLocalized(uiDictionary.getDescriptions(), language));

		uiDictionary = uiDictionaryRepository.findByContextAndKey(LABELS_CONTEXT, LBL_GRID);
		jPdfBaseLabels.setLabelGrid(LocalizationHelper.getLocalized(uiDictionary.getDescriptions(), language));

		uiDictionary = uiDictionaryRepository.findByContextAndKey(LABELS_CONTEXT, LBL_OPT);
		jPdfBaseLabels.setLabelOptional(LocalizationHelper.getLocalized(uiDictionary.getDescriptions(), language));

		uiDictionary = uiDictionaryRepository.findByContextAndKey(LABELS_CONTEXT, LBL_PAGE);
		jPdfBaseLabels.setLabelPage(LocalizationHelper.getLocalized(uiDictionary.getDescriptions(), language));
		uiDictionary = uiDictionaryRepository.findByContextAndKey(LABELS_CONTEXT, LBL_DATE);
		jPdfBaseLabels.setLabelDate(LocalizationHelper.getLocalized(uiDictionary.getDescriptions(), language));

		UiApplicationSetting uiApplicationSetting = uiApplicationSettingRepository.findByKey(UI_LOGO_FERRARI);
		jPdfBaseLabels.setLogoFerrari(uiApplicationSetting.getValue());

		uiDictionary = uiDictionaryRepository.findByContextAndKey(LABELS_CONTEXT, LBL_OPT_RULES);
		jPdfBaseLabels.setLabelRules(LocalizationHelper.getLocalized(uiDictionary.getDescriptions(), language));
		uiDictionary = uiDictionaryRepository.findByContextAndKey(LABELS_CONTEXT, LBL_OPT_WITH);
		jPdfBaseLabels.setLabelWith(LocalizationHelper.getLocalized(uiDictionary.getDescriptions(), language));
		uiDictionary = uiDictionaryRepository.findByContextAndKey(LABELS_CONTEXT, LBL_OPT_HOMOLOGATION);
		jPdfBaseLabels.setLabelHomologation(LocalizationHelper.getLocalized(uiDictionary.getDescriptions(), language));

		uiDictionary = uiDictionaryRepository.findByContextAndKey(LABELS_CONTEXT, LBL_OPT_NOTE);
		jPdfBaseLabels.setLabelNote(LocalizationHelper.getLocalized(uiDictionary.getDescriptions(), language));
		uiDictionary = uiDictionaryRepository.findByContextAndKey(LABELS_CONTEXT, LBL_OPT_STATUS);
		jPdfBaseLabels.setLabelStatus(LocalizationHelper.getLocalized(uiDictionary.getDescriptions(), language));
		uiDictionary = uiDictionaryRepository.findByContextAndKey(LABELS_CONTEXT, LBL_OPT_CAPACITY);
		jPdfBaseLabels.setLabelCapacity(LocalizationHelper.getLocalized(uiDictionary.getDescriptions(), language));

		uiDictionary = uiDictionaryRepository.findByContextAndKey(LABELS_CONTEXT, LBL_OPT_ORDERABLE_TEXT);
		jPdfBaseLabels.setLabelOrderableText(LocalizationHelper.getLocalized(uiDictionary.getDescriptions(), language));

		uiDictionary = uiDictionaryRepository.findByContextAndKey(LABELS_CONTEXT, LBL_OPT_LASTUPDATE);
		jPdfBaseLabels.setLabelLastUpdate(LocalizationHelper.getLocalized(uiDictionary.getDescriptions(), language));

		return jPdfBaseLabels;
	}

	private void sendXlsxToFileSystem(XSSFWorkbook wb, String title) throws IOException {
//		String filename = fileUtils.getFilePath()+"/"+title;
//
//		
//        FileOutputStream fileOut = new FileOutputStream(filename);
//        wb.write(fileOut);
//        fileOut.close();
//        wb.close();
//        
//		System.out.println("xlsx Available in: "+filename);

	}

	private void sendXlsxToBrowser(HttpServletResponse response, XSSFWorkbook wb, String title) throws IOException {

		response.setContentType(DocService.MIME_APPLICATION_XLSX);

		response.setHeader("Expires:", "0");
		response.setHeader("Content-Disposition", String.format("attachment; filename=\"%s\"", title));

		OutputStream outStream = response.getOutputStream();
		wb.write(outStream);
		wb.close();
		outStream.flush();
		outStream.close();

	}

	private JXlsBaseLabels getHeadingsXlsBase(LocalizationType language) {
		JXlsBaseLabels jXlsBaseLabels = new JXlsBaseLabels();
		UiDictionary uiDictionary;

		uiDictionary = uiDictionaryRepository.findByContextAndKey(LABELS_CONTEXT, LBL_XLS_MODEL);
		jXlsBaseLabels.setLabelModel(LocalizationHelper.getLocalized(uiDictionary.getDescriptions(), language));
		uiDictionary = uiDictionaryRepository.findByContextAndKey(LABELS_CONTEXT, LBL_XLS_AREA);
		jXlsBaseLabels.setLabelArea(LocalizationHelper.getLocalized(uiDictionary.getDescriptions(), language));
		uiDictionary = uiDictionaryRepository.findByContextAndKey(LABELS_CONTEXT, LBL_XLS_MACROCAT);
		jXlsBaseLabels
				.setLabelMacroCategoria(LocalizationHelper.getLocalized(uiDictionary.getDescriptions(), language));
		uiDictionary = uiDictionaryRepository.findByContextAndKey(LABELS_CONTEXT, LBL_XLS_SOTTOCAT);
		jXlsBaseLabels
				.setLabelSottoCategoria(LocalizationHelper.getLocalized(uiDictionary.getDescriptions(), language));
		uiDictionary = uiDictionaryRepository.findByContextAndKey(LABELS_CONTEXT, LBL_XLS_CODOPT);
		jXlsBaseLabels.setLabelCodOpt(LocalizationHelper.getLocalized(uiDictionary.getDescriptions(), language));
		uiDictionary = uiDictionaryRepository.findByContextAndKey(LABELS_CONTEXT, LBL_XLS_DESOPT);
		jXlsBaseLabels.setLabelShortDescOpt(LocalizationHelper.getLocalized(uiDictionary.getDescriptions(), language));
		uiDictionary = uiDictionaryRepository.findByContextAndKey(LABELS_CONTEXT, LBL_XLS_VINCOLI_OPT);
		jXlsBaseLabels.setLabelVincoliOpt(LocalizationHelper.getLocalized(uiDictionary.getDescriptions(), language));
		uiDictionary = uiDictionaryRepository.findByContextAndKey(LABELS_CONTEXT, LBL_XLS_VINCOLI_OMO);
		jXlsBaseLabels
				.setLabelVincoliOmologativi(LocalizationHelper.getLocalized(uiDictionary.getDescriptions(), language));
		uiDictionary = uiDictionaryRepository.findByContextAndKey(LABELS_CONTEXT, LBL_XLS_ORDINABILITA_FLAG);
		jXlsBaseLabels.setLabelOrdFlag(LocalizationHelper.getLocalized(uiDictionary.getDescriptions(), language));
		uiDictionary = uiDictionaryRepository.findByContextAndKey(LABELS_CONTEXT, LBL_XLS_ORDINABILITA_TEXT);
		jXlsBaseLabels.setLabelOrdText(LocalizationHelper.getLocalized(uiDictionary.getDescriptions(), language));
		uiDictionary = uiDictionaryRepository.findByContextAndKey(LABELS_CONTEXT, LBL_XLS_DESCRPITION);
		jXlsBaseLabels.setLabelDescription(LocalizationHelper.getLocalized(uiDictionary.getDescriptions(), language));
		uiDictionary = uiDictionaryRepository.findByContextAndKey(LABELS_CONTEXT, LBL_XLS_VERSION_MODEL);
		jXlsBaseLabels.setLabelVersionModel(LocalizationHelper.getLocalized(uiDictionary.getDescriptions(), language));
		uiDictionary = uiDictionaryRepository.findByContextAndKey(LABELS_CONTEXT, LBL_XLS_UPDATE_DATE);
		jXlsBaseLabels.setLabelUpdateDate(LocalizationHelper.getLocalized(uiDictionary.getDescriptions(), language));
		uiDictionary = uiDictionaryRepository.findByContextAndKey(LABELS_CONTEXT, LBL_XLS_INTERNAL_NOTE);
		jXlsBaseLabels.setLabelInternalNote(LocalizationHelper.getLocalized(uiDictionary.getDescriptions(), language));
		uiDictionary = uiDictionaryRepository.findByContextAndKey(LABELS_CONTEXT, LBL_XLS_STATUS_SVILUPPO);
		jXlsBaseLabels
				.setLabelStatusSviluppo(LocalizationHelper.getLocalized(uiDictionary.getDescriptions(), language));
		uiDictionary = uiDictionaryRepository.findByContextAndKey(LABELS_CONTEXT, LBL_XLS_CAP_PRODUTTIVA);
		jXlsBaseLabels.setLabelCapProduttiva(LocalizationHelper.getLocalized(uiDictionary.getDescriptions(), language));
		uiDictionary = uiDictionaryRepository.findByContextAndKey(LABELS_CONTEXT, LBL_XLS_SEQ);
		jXlsBaseLabels.setLabelSequence(LocalizationHelper.getLocalized(uiDictionary.getDescriptions(), language));
		uiDictionary = uiDictionaryRepository.findByContextAndKey(LABELS_CONTEXT, LBL_XLS_VISIBILITY);
		jXlsBaseLabels.setLabelVisibility(LocalizationHelper.getLocalized(uiDictionary.getDescriptions(), language));

		return jXlsBaseLabels;
	}

	private List<Long> getOptionalsSelected(JVersionOptForm jVersionOptForm) {
		// prepara lista opt selezionati per la generazione xls/pdf
		List<Long> opt = new ArrayList<Long>();

		for (JModelOptionalGroup jModelOptionalGroup : jVersionOptForm.getlJModelOptionalGroup()) {
			for (JModelOptionalSubGroup jModelOptionalSubGroup : jModelOptionalGroup.getlJModelOptionalSubGroup()) {
				for (JVersionOpt jVersionOpt : jModelOptionalSubGroup.getlJVersionOptional()) {
					if (jVersionOpt.getChoice()) {
						opt.add(jVersionOpt.getId());
					}
				}
			}
		}
		return opt;
	}

	private String getPdfTypeFromAuth() {
		if ((!"Y".equals(uiService.getAuthorizationByText(TEXT_C0_NOTE_INTERNE)))
				|| (!"Y".equals(uiService.getAuthorizationByText(TEXT_C2_STATUS_SVILUPPO)))
				|| (!"Y".equals(uiService.getAuthorizationByText(TEXT_C4_CAPACITY)))) {
			return PDF_PUBLIC;

		}
		return PDF_INTERNAL_USE_ONLY;
	}

	private Boolean getPdfInternalUseOnly() {
		if ((!"Y".equals(uiService.getAuthorizationByText(TEXT_C0_NOTE_INTERNE)))
				|| (!"Y".equals(uiService.getAuthorizationByText(TEXT_C2_STATUS_SVILUPPO)))
				|| (!"Y".equals(uiService.getAuthorizationByText(TEXT_C4_CAPACITY)))) {
			return Boolean.FALSE;

		}
		return Boolean.TRUE;
	}

	class RepStatus {
		int row;
		int col;

		RepStatus() {
			this.row = 0;
			this.col = 0;
		}

		RepStatus(int row, int col) {
			this.row = row;
			this.col = col;
		}

		public int getRow() {
			return row;
		}

		public void setRow(int row) {
			this.row = row;
		}

		public int getCol() {
			return col;
		}

		public void setCol(int col) {
			this.col = col;
		}

		public int incRow() {
			row++;
			return row - 1;
		}

		public int incCol() {
			col++;
			return col - 1;
		}

	}

	@Override
	public JPage createPdfOptionalsByVersionUI(JVersionOptForm jVersionOptForm, LocalizationType language, Long version)
			throws IOException {

		return null;
	}

}
