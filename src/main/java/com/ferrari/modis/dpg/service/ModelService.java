package com.ferrari.modis.dpg.service;

import java.util.List;

import org.springframework.web.bind.annotation.RequestMethod;

import com.ferrari.modis.dpg.commons.i18n.LocalizationType;
import com.ferrari.modis.dpg.jentity.app.JModel;
import com.ferrari.modis.dpg.jentity.base.JEntity;
import com.ferrari.modis.dpg.jentity.base.JPage;
import com.ferrari.modis.dpg.jentity.base.JValueText;
import com.ferrari.modis.dpg.model.app.Model;
import com.ferrari.modis.dpg.model.app.Version;

import io.swagger.api.ApiResponseMessage;
import io.swagger.api.NotFoundException;

public interface ModelService {
	
	public static final String ID_IMAGE_NEW_CLUSTER = "ID-IMAGE-NEW-CLUSTER";
	public final static String MEDIA_MODEL_TEMPLATE_HOME = "MODEL-HOME";
	public final static String MEDIA_MODEL_TEMPLATE_LOGO = "MODEL-LOGO";
	public final static String MEDIA_MODEL_TEMPLATE_COVER = "MODEL-COVE";
	
	public final static String MODEL_CREATE_ENABLE ="MO-AD";
	public final static String MODEL_UPDATE_ENABLE ="MO-UP";
	public final static String MODEL_DISPLAY_ENABLE ="MO-VI";
	
	public final static String MODEL_DELETE_ENABLE ="MO-DE";
	
	public static final String DEFAULT_QUALITY = "MAX";
	
	public JPage getHomepage(LocalizationType language);
	
	public JPage getNewModelForm(LocalizationType language, String optionalType, String modelCluster);
	public JPage getNewModelFormV2(LocalizationType language,  String modelCluster);

	
	public JPage getModel(LocalizationType language, String optionalType, Long idmodel);
	public JPage getModelV2(LocalizationType language, Long idmodel);
	public JPage getModelTypesV2(LocalizationType language, Long idmodel);
	
	public JModel buildFromModel(LocalizationType language,Model model, Version version);
	

	public JPage postModel(JEntity jEntity, RequestMethod requestMethod ) throws NotFoundException;
	public JPage putModel(JEntity jEntity,  LocalizationType language, String optionalType, Long idmodel,   RequestMethod requestMethod) throws NotFoundException;
	
	
	public JPage postModelV2(JEntity jEntity, RequestMethod requestMethod ) throws NotFoundException;
	public JPage putModelV2(JEntity jEntity,  LocalizationType language, Long idmodel,   RequestMethod requestMethod) throws NotFoundException;
	
	
	public ApiResponseMessage deleteModel(Long idModel) throws NotFoundException;
	
	public List<JValueText> getModelCluster(LocalizationType language);
	
	public List<JValueText> getOptionalGroups(LocalizationType language, String optionalType, Long idmodel);
	public List<JValueText> getOptionalSubGroups(LocalizationType language, String optionalType, Long idmodel, Long idOptionalGroup);
	public void CheckModel(Long idModel);
	
}
