package com.ferrari.modis.dpg.service;

import java.io.IOException;
import java.util.HashMap;

import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.ferrari.modis.dpg.commons.i18n.DocumentType;
import com.ferrari.modis.dpg.commons.i18n.LocalizationType;
import com.ferrari.modis.dpg.jentity.app.JVersionAttcForm;
import com.ferrari.modis.dpg.jentity.app.JVersionOptForm;
import com.ferrari.modis.dpg.jentity.base.JEntity;
import com.ferrari.modis.dpg.jentity.base.JPage;
import com.ferrari.modis.dpg.model.app.Model;
import com.ferrari.modis.dpg.model.app.Version;

import io.swagger.api.ApiResponseMessage;

public interface VModelService {
	
	public final static String MEDIA_OPTIONAL_TEMPLATE_HIRES = "OPT-HIRES";
	public final static String MEDIA_OPTIONAL_TEMPLATE_LORES = "OPT-LORES";
	public final static String MEDIA_FIGURINO_TEMPLATE = "FIGURINO";
	public final static String MEDIA_FIGURINO_SVG_TEMPLATE = "FIG-SVG";	
	public final static String MEDIA_FIGURINO_THUMB_TEMPLATE = "FIGURINO-T";
	public final static String MEDIA_OTHERDOC_TEMPLATE = "OTHERDOC";
	public final static String MEDIA_OTHERDOC_THUMB_TEMPLATE = "OTHERDOC-T";
	
	public final static String CONFIDENTIAL ="CONFIDENTIAL - Internal Use Only";
	
	
	
	public JPage getFormByVersion(LocalizationType language, String  optionalType, Long idmodel, Long version);
	public JPage getFormByModel(LocalizationType language, String  optionalType, Long idmodel);

	public JPage getOptionalList(LocalizationType language, Long version);
	public JPage getOptional(LocalizationType language, Long version, Long idOptional);
	public JPage getOptionalTemplate(LocalizationType language, Long version);
	
	public JPage postOptional(LocalizationType language, Long idVersion, JEntity jEntity,  RequestMethod requestMethod);
	public JPage putOptional(LocalizationType language, Long idVersion, JEntity jEntity, Long idOptional , RequestMethod requestMethod);
	public ApiResponseMessage deleteOptional(Long idVersion, Long idOptional );

	
	public JVersionOptForm convertToJVersionOptForm(HashMap<String, Object> dto)throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;
	public JVersionAttcForm convertToJVersionAttcForm(HashMap<String, Object> dto)throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;
	
	public JPage getDocumentList(LocalizationType language, Long version, DocumentType documentType, Long page, Long rows);
	public JPage getDocument(LocalizationType language, Long version, DocumentType documentType, Long idDocument);
	public JPage getDocumentTemplate(LocalizationType language, Long version, DocumentType documentType);
		
	
	public JPage putDocument(LocalizationType language,Long idVersion,JEntity jEntity, Long idDocument, DocumentType documentType, RequestMethod requestMethod);
	public JPage postDocument(LocalizationType language,Long idVersion,JEntity jEntity, DocumentType documentType, RequestMethod requestMethod);
	public ApiResponseMessage deleteDocument(Long idVersion, Long idDocument, DocumentType documentType);
	
	public JPage uploadXlsOptionals(MultipartFile multipartFile) throws IOException;
	public String getConfidentialStatus(Model model);
	public String getConfidentialStatus(Version version);
}
