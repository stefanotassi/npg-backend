package com.ferrari.modis.dpg.service;

import java.io.Serializable;
import java.util.List;

public interface AdminService {

	<T> List<T> getList(Class<T> clazz);

	<T> T createOrUpdateOne(Class<T> clazz, T entity);

	<T, ID extends Serializable> T findOne(Class<T> clazz, ID id);

	<T,ID extends Serializable> void deleteOne(Class<T> clazz, ID id);

}


