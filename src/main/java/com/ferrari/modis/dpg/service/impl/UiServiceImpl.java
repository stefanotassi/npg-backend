package com.ferrari.modis.dpg.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ferrari.modis.dpg.jentity.base.JPageAuth;
import com.ferrari.modis.dpg.model.base.AuditContext;
import com.ferrari.modis.dpg.model.ui.UiPageAuth;
import com.ferrari.modis.dpg.model.ui.UiSecAction;
import com.ferrari.modis.dpg.model.ui.UiSecActionAuth;
import com.ferrari.modis.dpg.repository.UiPageAuthRepository;
import com.ferrari.modis.dpg.repository.UiPageRepository;
import com.ferrari.modis.dpg.repository.UiSecActionAuthRepository;
import com.ferrari.modis.dpg.repository.UiSecActionRepository;
import com.ferrari.modis.dpg.repository.UiSecGroupRepository;
import com.ferrari.modis.dpg.service.UiService;




@Service
public class UiServiceImpl implements UiService{

	private static final Logger logger = LoggerFactory.getLogger(UiServiceImpl.class);
	
	
	@Autowired
	UiPageAuthRepository uiPageAuthRepository;
	@Autowired
	UiPageRepository uiPageRepository;
	
	@Autowired
	UiSecActionAuthRepository uiSecActionAuthRepository;	
	@Autowired
	UiSecActionRepository uiSecActionRepository;
	
	@Autowired	
	UiSecGroupRepository uiSecGroupRepository;

	

	
	
	
	// restituisce permissions dato un form
	public List<JPageAuth> getAuthorizationList(String pageId){

		
		List<JPageAuth> ljPageAuth = new ArrayList<JPageAuth>();
		JPageAuth jPageAuth;
		for (UiPageAuth uiPageAuth: uiPageAuthRepository.findByPage(pageId)){
			jPageAuth=new JPageAuth();
			jPageAuth.setAuthId(uiPageAuth.getId().getIdAction());
			
			UiSecAction uiSecAction = uiSecActionRepository.findById(uiPageAuth.getId().getIdAction());
			if (uiSecAction==null)
				continue;
			
			jPageAuth.setAuthDescription(uiSecAction.getDescription());
			jPageAuth.setPermission(uiSecAction.getPerm()); // default da UI_SEC_ACTIONS
			
			String s = getAuthorizationById(uiPageAuth.getId().getIdAction());
			if (s!=null)
				jPageAuth.setPermission(s);
			
	
			
			
			ljPageAuth.add(jPageAuth);
		}
		return ljPageAuth;
	}
	
	public String getAuthorizationByText(String textType){
		return getAuthorizationById(AUTH_PREFIX_FOR_TEXT+textType);
	}
	public String getAuthorizationByVersionStatus(String status){
		return getAuthorizationById(AUTH_PREFIX_FOR_VERSION_STATUS+status);
	}
	
	public String getAuthorizationById(String authId){
		String auth="N";
		UiSecAction uiSecAction = uiSecActionRepository.findById(authId);
		if (uiSecAction==null)
			return auth;
		else
			auth = uiSecAction.getPerm();
		
		// List<String> gruppi = AuditContext.getCurrentGroups();
		for (String gruppo : AuditContext.getCurrentGroups()){
			UiSecActionAuth uiSecActionAuth= uiSecActionAuthRepository.findByGroupAction(gruppo, authId);
			if (uiSecActionAuth!=null){
				if (("Y").equals(uiSecActionAuth.getPerm())){
					return uiSecActionAuth.getPerm();
				}
			}
		}
		
		return auth;
	}
}
