package com.ferrari.modis.dpg.service.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ferrari.modis.dpg.commons.i18n.DocumentType;
import com.ferrari.modis.dpg.commons.i18n.LocalizationType;
import com.ferrari.modis.dpg.jentity.app.JForm;
import com.ferrari.modis.dpg.jentity.app.JGroup;
import com.ferrari.modis.dpg.jentity.app.JModel;
import com.ferrari.modis.dpg.jentity.app.JModelOptionalGroup;
import com.ferrari.modis.dpg.jentity.app.JModelOptionalSubGroup;
import com.ferrari.modis.dpg.jentity.app.JOptJump;
import com.ferrari.modis.dpg.jentity.app.JOptionalErp;
import com.ferrari.modis.dpg.jentity.app.JModelJump;
import com.ferrari.modis.dpg.jentity.app.JSubGroup;
import com.ferrari.modis.dpg.jentity.app.JVersion;
import com.ferrari.modis.dpg.jentity.app.JVersionAttc;
import com.ferrari.modis.dpg.jentity.app.JVersionAttcForm;
import com.ferrari.modis.dpg.jentity.app.JVersionAttcSimple;
import com.ferrari.modis.dpg.jentity.app.JVersionForm;
import com.ferrari.modis.dpg.jentity.app.JVersionOpt;
import com.ferrari.modis.dpg.jentity.app.JVersionOptForm;
import com.ferrari.modis.dpg.jentity.app.JVersionOptFull;
import com.ferrari.modis.dpg.jentity.app.JVersionOptImport;
import com.ferrari.modis.dpg.jentity.app.JVersionOptPics;
import com.ferrari.modis.dpg.jentity.app.JVersionOptText;
import com.ferrari.modis.dpg.jentity.base.JEntity;
import com.ferrari.modis.dpg.jentity.base.JPage;
import com.ferrari.modis.dpg.jentity.base.JPageAuth;
import com.ferrari.modis.dpg.jentity.base.JPageData;
import com.ferrari.modis.dpg.jentity.base.JPagination;
import com.ferrari.modis.dpg.model.app.Media;
import com.ferrari.modis.dpg.model.app.Model;
import com.ferrari.modis.dpg.model.app.ModelOptionalGroup;
import com.ferrari.modis.dpg.model.app.ModelOptionalSubGroup;
import com.ferrari.modis.dpg.model.app.ModelOptionalType;
import com.ferrari.modis.dpg.model.app.OptionalErp;
import com.ferrari.modis.dpg.model.app.OptionalTypeForm;
import com.ferrari.modis.dpg.model.app.TextType;
import com.ferrari.modis.dpg.model.app.Version;
import com.ferrari.modis.dpg.model.app.VersionAttc;
import com.ferrari.modis.dpg.model.app.VersionOpt;
import com.ferrari.modis.dpg.model.app.VersionOptAttc;
import com.ferrari.modis.dpg.model.app.VersionOptAttcPK;
import com.ferrari.modis.dpg.model.app.VersionOptPics;
import com.ferrari.modis.dpg.model.app.VersionOptText;
import com.ferrari.modis.dpg.model.app.VersionStatus;
import com.ferrari.modis.dpg.model.ui.UiPage;
import com.ferrari.modis.dpg.repository.MediaRepository;
import com.ferrari.modis.dpg.repository.ModelOptionalGroupRepository;
import com.ferrari.modis.dpg.repository.ModelOptionalSubGroupRepository;
import com.ferrari.modis.dpg.repository.ModelOptionalTypeRepository;
import com.ferrari.modis.dpg.repository.ModelRepository;
import com.ferrari.modis.dpg.repository.OptionalErpRepository;
import com.ferrari.modis.dpg.repository.OptionalTypeFormRepository;
import com.ferrari.modis.dpg.repository.TextTypeRepository;
import com.ferrari.modis.dpg.repository.UiPageRepository;
import com.ferrari.modis.dpg.repository.VersionAttcRepository;
import com.ferrari.modis.dpg.repository.VersionOptAttcRepository;
import com.ferrari.modis.dpg.repository.VersionOptPicsRepository;
import com.ferrari.modis.dpg.repository.VersionOptRepository;
import com.ferrari.modis.dpg.repository.VersionOptTextRepository;
import com.ferrari.modis.dpg.repository.VersionRepository;
import com.ferrari.modis.dpg.repository.VersionStatusRepository;
import com.ferrari.modis.dpg.service.DocService;
import com.ferrari.modis.dpg.service.MediaService;
import com.ferrari.modis.dpg.service.ModelService;
import com.ferrari.modis.dpg.service.UiService;
import com.ferrari.modis.dpg.service.VModelService;
import com.ferrari.modis.dpg.service.VersionService;

import io.swagger.api.ApiResponseMessage;
import io.swagger.api.NotFoundException;
import io.swagger.api.ValidationException;

@Service
public class VModelServiceImpl implements VModelService {

	private static final Logger logger = LoggerFactory.getLogger(VModelServiceImpl.class);

	// private static final String Long = null;

	@Autowired
	ModelOptionalTypeRepository modelOptionalTypeRepository;

	@Autowired
	ModelRepository modelRepository;
	@Autowired
	VersionRepository versionRepository;
	@Autowired
	OptionalTypeFormRepository optionalTypeFormRepository;
	@Autowired
	UiPageRepository uiPageRepository;

	@Autowired
	UiService uiService;
	@Autowired
	VersionService versionService;
	@Autowired
	MediaService mediaService;

	@Autowired
	ModelService modelService;

	@Autowired
	VersionOptRepository versionOptRepository;
	@Autowired
	TextTypeRepository textTypeRepository;
	@Autowired
	VersionOptTextRepository versionOptTextRepository;
	@Autowired
	VersionOptPicsRepository versionOptPicsRepository;
	@Autowired
	VersionAttcRepository versionAttcRepository;

	@Autowired
	VersionOptAttcRepository versionOptAttcRepository;
	@Autowired
	ModelOptionalSubGroupRepository modelOptionalSubGroupRepository;
	@Autowired
	ModelOptionalGroupRepository modelOptionalGroupRepository;

	@Autowired
	VersionStatusRepository versionStatusRepository;
	@Autowired
	MediaRepository mediaRepository;
	@Autowired
	OptionalErpRepository optionalErpRepository;
	
	// form con le tre colonne opt/figurini/altro
	@Override
	public JPage getFormByModel(LocalizationType language, String optionalType, Long idmodel) {

		Model model = modelRepository.findById(idmodel);
		if (model == null)
			throw new ValidationException(-1, "MODEL_NOT_FOUND");
		// la prima chiamata al form cade qui: non si conosce il numero di versione
		// e viene determinata la versione di default
		ModelOptionalType modelOptionalType = modelOptionalTypeRepository.findByModelAndOptionalType(idmodel,
				optionalType);
		// determino versione default accedibile per utente
		Version version = versionService.getDefaultVersion(modelOptionalType.getId());

		Long idVersion = null;
		if (version != null)
			idVersion = version.getId();

		return getFormByVersion(language, optionalType, idmodel, idVersion);

	}

	@Override
	public JPage getFormByVersion(LocalizationType language, String optionalType, Long idmodel, Long idVersion) {
		// prepara il form selettore con le tre colonne
		// e i dati relativi alle versioni da vedere
		// la versione corrente verrà visualizzata anche in alto a dx

		// se un utente NON ha una versione corrente il sistema non elencherà dati
		// relativi a versioni

		// determina idModelOptType
		ModelOptionalType modelOptionalType = modelOptionalTypeRepository.findByModelAndOptionalType(idmodel,
				optionalType);
		if (modelOptionalType == null)
			throw new ValidationException(-1, "MODEL_OPT_TYPE_NOT_FOUND");

		Version version = null;
		if (idVersion != null) {
			version = versionService.versionCheck(idVersion); // check accessibilità
			if (!version.getIdModelOptionalType().equals(modelOptionalType.getId()))
				throw new ValidationException(-1, "VERSION_INCORRECT");
		}

		// esistenza model
		Model model = modelRepository.findById(modelOptionalType.getIdModel());
		if (model == null)
			throw new ValidationException(-1, "MODEL_NOT_FOUND");

		Boolean enableAllModels = ("Y".equals(uiService.getAuthorizationById(ModelService.MODEL_DISPLAY_ENABLE)));
		if ((!"Y".equals(model.getVisibility())) && !enableAllModels) {
			// non visibile: abilitati solo utenti meritevili
			throw new ValidationException(-1, "MODEL_NOT_FOUND"); // AVAILABLE
		}

		JPage jpage = new JPage();
		JPageData jPageData = new JPageData();

		// data JPage ------------------------------------------------------------
		JVersionForm jVersionForm = new JVersionForm();

		// sezione form, deteremina le tre colonne standard
		jVersionForm.setlJform(getFormList(language, modelOptionalType.getIdOptionalType()));

		// sezione model
		jVersionForm.setJmodel(convertModelToJModel(language, model, version));

		// TODO aggiungere oggetto JModelType 6
		jVersionForm.setJModelJump(convertModelTypetoJModelType(modelOptionalType));

		// sezione versione, solo se presente una versione corrente
		if (version != null) {
			// versione attuale
			jVersionForm.setJversion(versionService.buildFromVersion(language, version));

			// lista versioni
			jVersionForm.setlJVersion(getVersionList(language, modelOptionalType.getId()));
		}

		// se non c'èn nessuna versione presente crea la versione Draft

		// header JPage ------------------------------------------------------------
		UiPage uiPage = uiPageRepository.findByKey(UiService.PAGE_FORM);
		jpage.setId(uiPage.getId());
		jpage.setName(uiPage.getDescription());
		jpage.setLanguage(language);

		// aggiunge lista autorizzazioni -------------------------------------------
		List<JPageAuth> lJPageAuth = uiService.getAuthorizationList(UiService.PAGE_FORM);
		jpage.setAuths(lJPageAuth);

		jPageData.setActual(jVersionForm);
		jpage.setData(jPageData);

		return jpage;
	}

	public JPage getOptionalTemplate(LocalizationType language, Long idVersion) {

		Version version = versionService.versionCheck(idVersion);

		Long idModelOptionalType = version.getIdModelOptionalType();
		ModelOptionalType modelOptionalType = modelOptionalTypeRepository.getOne(idModelOptionalType);

		JPage jpage = new JPage();
		JPageData jPageData = new JPageData();

		JVersionOptFull jVersionOptFull = new JVersionOptFull();
		jVersionOptFull.setId(null);
		jVersionOptFull.setCode(null);
		jVersionOptFull.setCodeErp(null);
		jVersionOptFull.setDescription(null);
		jVersionOptFull.setIdModelOptionalSubGroup(null);
		jVersionOptFull.setOrderable(null);

		// TODO lista optionals ERP  //////////////////////////////////
//		List<JOptionalErp> lJOptionalErp = new ArrayList<JOptionalErp>();
//		for (OptionalErp optionalErp: optionalErpRepository.findAll()) {
//			JOptionalErp jOptionalErp = new JOptionalErp();
//			jOptionalErp.setId(optionalErp.getId());
//			jOptionalErp.setDescription(optionalErp.getDescription(language.getValue()));
//			lJOptionalErp.add(jOptionalErp);
//		}
//
//		jVersionOptFull.setlJOptionalErp(lJOptionalErp);
		////////////////////////////////////////////////////////////////
		
		// TODO 1 setjOptJump OK
		jVersionOptFull.setjOptJump(null);

		// gruppi_optional per il modello scelto
		// ------------------------------------------------------
		List<JGroup> lJGroup = new ArrayList<JGroup>();
		List<JSubGroup> lJSubGroup = new ArrayList<JSubGroup>();
		for (ModelOptionalGroup mog : modelOptionalGroupRepository.findByModelOptType(idModelOptionalType)) {
			JGroup jGroup = new JGroup();
			jGroup.setId(mog.getId());
			jGroup.setCode(mog.getCode());
			jGroup.setDescription(mog.getDescription(language.getValue()));
			jGroup.setSeq(mog.getSequence());
			lJGroup.add(jGroup);
			for (ModelOptionalSubGroup mos : modelOptionalSubGroupRepository.findByModelOptGroup(mog.getId())) {
				JSubGroup jSubGroup = new JSubGroup();
				jSubGroup.setId(mos.getId());
				jSubGroup.setCode(mos.getCode());
				jSubGroup.setDescription(mos.getDescription(language.getValue()));
				jSubGroup.setSeq(mos.getSequence());
				jSubGroup.setIdGroup(mog.getId());
				lJSubGroup.add(jSubGroup);
			}
		}
		jVersionOptFull.setlJGroup(lJGroup);
		jVersionOptFull.setlJSubGroup(lJSubGroup);
		// END gruppi_optional per il modello scelto
		// --------------------------------------------------

		jVersionOptFull.setOrderableFlag(null);
		jVersionOptFull.setSequence(null);
		jVersionOptFull.setVersionModel(null);
		jVersionOptFull.setVisible("Y");

		JVersion jVersion = new JVersion();

		jVersion.setCode(version.getCode());
		jVersion.setDescription(version.getDescription(language.getValue()));
		jVersion.setDescriptionLong(version.getDescriptionLong(language.getValue()));

		jVersion.setId(version.getId());
		jVersion.setIdModelOptionalType(version.getIdModelOptionalType());
		jVersion.setPublishDate(version.getPublishDate());
		jVersion.setPublishUser(version.getPublishUser());
		jVersion.setStatus(version.getStatus());
		jVersion.setAccessible(versionService.isAccessibileByStatus(idVersion));
		jVersion.setReadonly(versionService.isReadOnly(idVersion));

		jVersionOptFull.setjVersion(jVersion);

		// ModelOptionalType modelOptionalType =
		// modelOptionalTypeRepository.getOne(version.getIdModelOptionalType());
		Model model = modelRepository.getOne(modelOptionalType.getIdModel());
		JModel jModel = modelService.buildFromModel(language, model, version);
		jVersionOptFull.setjModel(jModel);

		// TODO aggiungere oggetto JModelType 6
		jVersionOptFull.setJModelJump(convertModelTypetoJModelType(modelOptionalType));

		// visualizza la lista dei testi da caricare
		List<JVersionOptText> lJVersionOptText = new ArrayList<JVersionOptText>();
		for (TextType textType : textTypeRepository.findAll()) {
			JVersionOptText jVersionOptText = new JVersionOptText();
			jVersionOptText.setTextType(textType.getId());
			jVersionOptText.setTextTypeDescr(textType.getDescription(language.getValue()));

			if (!"Y".equals(uiService.getAuthorizationByText(textType.getId()))) {
				continue;
			}
			// VersionOptText versionOptText = versionOptTextRepository.findByIdAndTextType(
			// idOptional, textType.getId());

			lJVersionOptText.add(jVersionOptText);
		}
		jVersionOptFull.setlJVersionOptText(lJVersionOptText);

		// elenco figurini per visualizzzazione in form OPT
		List<JVersionAttc> lJVersionAttc = new ArrayList<JVersionAttc>();
		jVersionOptFull.setlJVersionAttc(lJVersionAttc);

		// header JPage ------------------------------------------------------------
		UiPage uiPage = uiPageRepository.findByKey(UiService.PAGE_DETAIL_OPT);
		jpage.setId(uiPage.getId());
		jpage.setName(uiPage.getDescription());
		jpage.setLanguage(language);

		// aggiunge lista autorizzazioni -------------------------------------------
		List<JPageAuth> lJPageAuth = uiService.getAuthorizationList(UiService.PAGE_DETAIL_OPT);

		// forzare la NON Modificabilità se <> Draft

		jpage.setAuths(lJPageAuth);

		jPageData.setActual(jVersionOptFull);
		jpage.setData(jPageData);

		return jpage;
	}

	public JPage getOptional(LocalizationType language, Long idVersion, Long idOptional) {

		VersionOpt versionOpt = versionOptRepository.findById(idOptional);
		if (versionOpt == null)
			throw new ValidationException(-1, "OPTIONAL_NOT_FOUND");

		idVersion = versionOpt.getIdVersion();
		Version version = versionService.versionCheck(idVersion);

		// visibilità Optional campo visibility e
		if (!("Y").equals(versionOpt.getVisible()) && !("Y").equals(uiService.getAuthorizationById("OP-AC"))) {
			throw new ValidationException(-1, "OPTIONAL_NOT_ALLOWED_FOR_VISIBILITY");
		}

		Long idModelOptionalType = version.getIdModelOptionalType();
		ModelOptionalType modelOptionalType = modelOptionalTypeRepository.getOne(idModelOptionalType);

		// ModelOptionalType modelOptionalType =
		// modelOptionalTypeRepository.getOne(version.getIdModelOptionalType());
		Model model = modelRepository.getOne(modelOptionalType.getIdModel());

		Boolean enableAllModels = ("Y".equals(uiService.getAuthorizationById(ModelService.MODEL_DISPLAY_ENABLE)));
		if ((!"Y".equals(model.getVisibility())) && !enableAllModels) {
			// non visibile: abilitati solo utenti meritevili
			throw new ValidationException(-1, "MODEL_NOT_FOUND"); // AVAILABLE
		}

		JPage jpage = new JPage();
		JPageData jPageData = new JPageData();

		JVersionOptFull jVersionOptFull = new JVersionOptFull();
		jVersionOptFull.setId(versionOpt.getId());
		jVersionOptFull.setCode(versionOpt.getCode());
		jVersionOptFull.setCodeErp(versionOpt.getCodeErp());
		jVersionOptFull.setDescription(versionOpt.getDescription(language.getValue()));
		jVersionOptFull.setIdModelOptionalSubGroup(versionOpt.getIdModelOptionalSubGroup());
		jVersionOptFull.setjOptJump(null);
		// TODO 1 setjOptJump
//		if (versionOpt.getCodeErp() != null) {
//			jVersionOptFull.setjOptJump(calcOtherOptionalLink(model, modelOptionalType, versionOpt.getCodeErp()));
//		}
		
		
		// TODO lista optionals ERP  //////////////////////////////////
//		List<JOptionalErp> lJOptionalErp = new ArrayList<JOptionalErp>();
//		for (OptionalErp optionalErp: optionalErpRepository.findAll()) {
//			JOptionalErp jOptionalErp = new JOptionalErp();
//			jOptionalErp.setId(optionalErp.getId());
//			jOptionalErp.setDescription(optionalErp.getDescription(language.getValue()));
//			lJOptionalErp.add(jOptionalErp);
//		}
//
//		jVersionOptFull.setlJOptionalErp(lJOptionalErp);
		////////////////////////////////////////////////////////////////

		ModelOptionalSubGroup modelOptionalSubGroup = modelOptionalSubGroupRepository
				.findOne(versionOpt.getIdModelOptionalSubGroup());
		jVersionOptFull.setIdModelOptionalGroup(modelOptionalSubGroup.getModelOptionalGroup().getId());

		// gruppi_optional per il modello scelto
		// ------------------------------------------------------
		List<JGroup> lJGroup = new ArrayList<JGroup>();
		List<JSubGroup> lJSubGroup = new ArrayList<JSubGroup>();
		for (ModelOptionalGroup mog : modelOptionalGroupRepository.findByModelOptType(idModelOptionalType)) {
			JGroup jGroup = new JGroup();
			jGroup.setId(mog.getId());
			jGroup.setCode(mog.getCode());
			jGroup.setDescription(mog.getDescription(language.getValue()));
			jGroup.setSeq(mog.getSequence());
			lJGroup.add(jGroup);
			for (ModelOptionalSubGroup mos : modelOptionalSubGroupRepository.findByModelOptGroup(mog.getId())) {
				JSubGroup jSubGroup = new JSubGroup();
				jSubGroup.setId(mos.getId());
				jSubGroup.setCode(mos.getCode());
				jSubGroup.setDescription(mos.getDescription(language.getValue()));
				jSubGroup.setSeq(mos.getSequence());
				jSubGroup.setIdGroup(mog.getId());
				lJSubGroup.add(jSubGroup);
			}
		}
		jVersionOptFull.setlJGroup(lJGroup);
		jVersionOptFull.setlJSubGroup(lJSubGroup);
		// END gruppi_optional per il modello scelto
		// --------------------------------------------------

		jVersionOptFull.setOrderable(versionOpt.getOrderable(language.getValue()));
		jVersionOptFull.setOrderableFlag(versionOpt.getOrderableFlag());
		jVersionOptFull.setSequence(versionOpt.getSequence());
		jVersionOptFull.setVersionModel(versionOpt.getVersionModel(language.getValue()));
		jVersionOptFull.setVisible(versionOpt.getVisible());
		jVersionOptFull.setLastUpdate(getLastUpdateDate(idOptional, idVersion));

		JVersion jVersion = new JVersion();
		jVersion.setCode(version.getCode());
		jVersion.setDescription(version.getDescription(language.getValue()));
		jVersion.setDescriptionLong(version.getDescriptionLong(language.getValue()));
		jVersion.setId(version.getId());
		jVersion.setIdModelOptionalType(version.getIdModelOptionalType());
		jVersion.setPublishDate(version.getPublishDate());
		jVersion.setPublishUser(version.getPublishUser());
		jVersion.setStatus(version.getStatus());
		VersionStatus versionStatus = versionStatusRepository.findById(version.getStatus());
		jVersion.setStatusDescription(versionStatus.getDescription(language.getValue()));
		jVersion.setAccessible(versionService.isAccessibileByStatus(idVersion));
		jVersion.setReadonly(versionService.isReadOnly(idVersion));

		jVersionOptFull.setjVersion(jVersion);

		// TODO aggiungere oggetto JModelType 6
		jVersionOptFull.setJModelJump(convertModelTypetoJModelType(modelOptionalType));

		JModel jModel = modelService.buildFromModel(language, model, version);

		jVersionOptFull.setjModel(jModel);

		List<JVersionOptPics> lJVersionOptPics = new ArrayList<JVersionOptPics>();
		for (VersionOptPics versionOptPics : versionOpt.getlVersionOptPics()) {
			JVersionOptPics jVersionOptPics = new JVersionOptPics();
			jVersionOptPics.setId(versionOptPics.getId());

			jVersionOptPics.setIdImage(versionOptPics.getIdImage());
			jVersionOptPics.setIdImageLow(versionOptPics.getIdImageLow());

			jVersionOptPics
					.setUrlImage(mediaService.getThronUrl(versionOptPics.getIdImage(), MEDIA_OPTIONAL_TEMPLATE_HIRES));
			jVersionOptPics.setUrlImageLow(
					mediaService.getThronUrl(versionOptPics.getIdImageLow(), MEDIA_OPTIONAL_TEMPLATE_LORES));

			jVersionOptPics.setSequence(versionOptPics.getSequence());
			jVersionOptPics.setPicType(versionOptPics.getPicType());
			jVersionOptPics.setDescription(versionOptPics.getDescription(language.getValue()));
			jVersionOptPics.setDescription_en(versionOptPics.getDescription_en());
			jVersionOptPics.setDescription_it(versionOptPics.getDescription_it());

			lJVersionOptPics.add(jVersionOptPics);

			jVersionOptPics.setMediaTemplateImageHiRes(MEDIA_OPTIONAL_TEMPLATE_HIRES);
			jVersionOptPics.setMediaTemplateImageLoRes(MEDIA_OPTIONAL_TEMPLATE_LORES);

		}
//		
//		// se non ci sono immagini collegate invio un template PICS
//		if (lJVersionOptPics.isEmpty())
//			lJVersionOptPics.add(getTemplateOptPics());
//		

		jVersionOptFull.setlJVersionOptPics(lJVersionOptPics);

		List<JVersionOptText> lJVersionOptText = new ArrayList<JVersionOptText>();
		for (TextType textType : textTypeRepository.findAll()) {
			JVersionOptText jVersionOptText = new JVersionOptText();
			jVersionOptText.setTextType(textType.getId());
			jVersionOptText.setTextTypeDescr(textType.getDescription(language.getValue()));
			// se non permesso NON restituisce testo
			if (!"Y".equals(uiService.getAuthorizationByText(textType.getId()))) {
				continue;
			}
			VersionOptText versionOptText = versionOptTextRepository.findByIdAndTextType(idOptional, textType.getId());
			if (versionOptText != null) {
				jVersionOptText.setId(versionOptText.getId());
				jVersionOptText.setTextType(versionOptText.getTextType());
				jVersionOptText.setDescription(versionOptText.getDescription(language.getValue()));
			}
			lJVersionOptText.add(jVersionOptText);
		}
		jVersionOptFull.setlJVersionOptText(lJVersionOptText);

		List<JVersionAttc> lJVersionAttc = new ArrayList<JVersionAttc>();

		for (VersionOptAttc versionOptAttc : versionOpt.getlVersionOptAttc()) {
			VersionAttc versionAttc = versionOptAttc.getVersionAttc();

			JVersionAttc jVersionAttc = new JVersionAttc();
			jVersionAttc.setId(versionAttc.getId());
			jVersionAttc.setAttachmentType(versionAttc.getAttachmentType());
			jVersionAttc.setChoice(Boolean.FALSE);
			jVersionAttc.setDescription(versionAttc.getDescription(language.getValue()));
			jVersionAttc.setIdImage(versionAttc.getIdImage());
			jVersionAttc.setIdImageThumb(versionAttc.getIdImageThumb());

			jVersionAttc.setUrlImage(versionAttc.getIdImage());
			jVersionAttc.setUrlImageThumb(versionAttc.getIdImageThumb());

			jVersionAttc.setSequence(versionAttc.getSequence());

			lJVersionAttc.add(jVersionAttc);
		}

		jVersionOptFull.setlJVersionAttc(lJVersionAttc);

		// header JPage ------------------------------------------------------------
		UiPage uiPage = uiPageRepository.findByKey(UiService.PAGE_DETAIL_OPT);
		jpage.setId(uiPage.getId());
		jpage.setName(uiPage.getDescription());
		jpage.setLanguage(language);

		// aggiunge lista autorizzazioni -------------------------------------------
		List<JPageAuth> lJPageAuth = uiService.getAuthorizationList(UiService.PAGE_DETAIL_OPT);
		// eccezione: se non è DRAFT --> fullIO = N

		for (JPageAuth jPageAuth : lJPageAuth) {
			if ((jPageAuth.getAuthId().equals("OP-IO")) && !(version.getStatus().equals("D"))) {
				jPageAuth.setPermission("N");
				jPageAuth.setAuthDescription(jPageAuth.getAuthDescription() + " [Override per DRAFT]");
			}
		}

		jpage.setAuths(lJPageAuth);

		jPageData.setActual(jVersionOptFull);
		jpage.setData(jPageData);

		return jpage;
	}

	@Override
	public JPage getOptionalList(LocalizationType language, Long idVersion) {

		Version version = versionService.versionCheck(idVersion);

		// form che elenca gli opt classificati per gruppi/sottogruppi

		// esistenza idModelOptType
		ModelOptionalType modelOptionalType = modelOptionalTypeRepository.findById(version.getIdModelOptionalType());
		if (modelOptionalType == null)
			throw new ValidationException(-1, "MODEL_OPTIONAL_TYPE_NOT_FOUND");
		// esistenza model
		Model model = modelRepository.findById(modelOptionalType.getIdModel());
		if (model == null)
			throw new ValidationException(-1, "MODEL_NOT_FOUND");

		Boolean enableAllModels = ("Y".equals(uiService.getAuthorizationById(ModelService.MODEL_DISPLAY_ENABLE)));
		if ((!"Y".equals(model.getVisibility())) && !enableAllModels) {
			// non visibile: abilitati solo utenti meritevili
			throw new ValidationException(-1, "MODEL_NOT_FOUND"); // AVAILABLE
		}

		JPage jpage = new JPage();
		JPageData jPageData = new JPageData();

		JVersionOptForm jVersionOptForm = new JVersionOptForm();
		jVersionOptForm.setJmodel(convertModelToJModel(language, model, version));

		// TODO aggiungere oggetto JModelType 6
		jVersionOptForm.setJModelJump(convertModelTypetoJModelType(modelOptionalType));

		JVersion jVersion = versionService.buildFromVersion(language, version);
		jVersion.setAuthorizations(null); // inutili in questo contesto
		jVersionOptForm.setJversion(jVersion);

		JModelOptionalGroup jModelOptionalGroup;
		for (ModelOptionalGroup modelOptionalGroup : modelOptionalType.getlModelOptionalGroup()) {
			jModelOptionalGroup = new JModelOptionalGroup();
			jModelOptionalGroup.setId(modelOptionalGroup.getId());
			jModelOptionalGroup.setCode(modelOptionalGroup.getCode());
			jModelOptionalGroup.setDescription(modelOptionalGroup.getDescription(language.getValue()));
			jModelOptionalGroup.setDescription_it(modelOptionalGroup.getDescription_it());
			jModelOptionalGroup.setDescription_en(modelOptionalGroup.getDescription_en());
			jModelOptionalGroup.setSeq(modelOptionalGroup.getSequence());

			JModelOptionalSubGroup jModelOptionalSubGroup;
			for (ModelOptionalSubGroup modelOptionalSubGroup : modelOptionalGroup.getlModelOptionalSubGroup()) {

				jModelOptionalSubGroup = new JModelOptionalSubGroup();
				jModelOptionalSubGroup.setId(modelOptionalSubGroup.getId());
				jModelOptionalSubGroup.setId(modelOptionalSubGroup.getId());
				jModelOptionalSubGroup.setCode(modelOptionalSubGroup.getCode());
				jModelOptionalSubGroup.setDescription(modelOptionalSubGroup.getDescription(language.getValue()));
				jModelOptionalSubGroup.setDescription_it(modelOptionalSubGroup.getDescription_it());
				jModelOptionalSubGroup.setDescription_en(modelOptionalSubGroup.getDescription_en());
				jModelOptionalSubGroup.setSeq(modelOptionalSubGroup.getSequence());

				List<JVersionOpt> lJVersionOptional = new ArrayList<JVersionOpt>();

				for (VersionOpt versionOpt : versionOptRepository.findByVersionAndModelOptSub(idVersion,
						modelOptionalSubGroup.getId())) {
					if ((!"Y".equals(uiService.getAuthorizationById("OP-AC")))
							&& (!"Y".equals(versionOpt.getVisible()))) {
						continue;
					}

					JVersionOpt jVersionOpt = new JVersionOpt();
					jVersionOpt.setId(versionOpt.getId());
					jVersionOpt.setCode(versionOpt.getCode());
					jVersionOpt.setSequence(versionOpt.getSequence());
					jVersionOpt.setDescription(versionOpt.getDescription(language.getValue()));
					jVersionOpt.setChoice(Boolean.FALSE);
					lJVersionOptional.add(jVersionOpt);
				}

				if (!lJVersionOptional.isEmpty()) {
					jModelOptionalSubGroup.setlJVersionOptional(lJVersionOptional);
					jModelOptionalGroup.getlJModelOptionalSubGroup().add(jModelOptionalSubGroup);
				}

			}

			jVersionOptForm.getlJModelOptionalGroup().add(jModelOptionalGroup);
		}

		// TODO V2 aggiungo lista figurini per effettuare la scelta dei figurini da
		// stampare
		List<JVersionAttcSimple> lJVersionAttcSimple = new ArrayList<JVersionAttcSimple>();
		for (VersionAttc versionAttc : versionAttcRepository.findByVersionTypeAll(idVersion, "F")) {
			JVersionAttcSimple jVersionAttcSimple = new JVersionAttcSimple();
			jVersionAttcSimple.setId(versionAttc.getId());
			jVersionAttcSimple.setAttachmentType(versionAttc.getAttachmentType());
			jVersionAttcSimple.setChoice(true);
			jVersionAttcSimple.setDescription(versionAttc.getDescription(language.getValue()));
			jVersionAttcSimple.setSequence(versionAttc.getSequence());
			lJVersionAttcSimple.add(jVersionAttcSimple);
		}
		jVersionOptForm.setlJVersionAttcSimple(lJVersionAttcSimple);

		// header JPage ------------------------------------------------------------
		UiPage uiPage = uiPageRepository.findByKey(UiService.PAGE_LIST_OPT);
		jpage.setId(uiPage.getId());
		jpage.setName(uiPage.getDescription());
		jpage.setLanguage(language);

		// aggiunge lista autorizzazioni -------------------------------------------
		List<JPageAuth> lJPageAuth = uiService.getAuthorizationList(UiService.PAGE_LIST_OPT);
		jpage.setAuths(lJPageAuth);

		jPageData.setActual(jVersionOptForm);
		jpage.setData(jPageData);

		return jpage;
	}

	public JPage postOptional(LocalizationType language, Long idVersion, JEntity jEntity, RequestMethod requestMethod) {

		Version version = versionService.versionCheck(idVersion);

		if (!"Y".equals(uiService.getAuthorizationById("OP-AD")))
			throw new ValidationException(-1, "USER_PERMISSION_OPTIONAL_ADD");

		versionService.versionCheckForUpdate(version); // per put/post/delete

		JVersionOptFull jVersionOptFull = null;

		try {
			jVersionOptFull = convertFromJEntityJVersionOptFull(jEntity.getData().getActual());
		} catch (IOException e) {
			throw new NotFoundException(-1, e.getMessage());
		}
		validateOpt(jVersionOptFull, idVersion, requestMethod);

		Long idOptional = saveOptional(jEntity.getLanguage(), idVersion, jVersionOptFull, requestMethod);

		return getOptional(language, idVersion, idOptional);

	}

	public JPage putOptional(LocalizationType language, Long idVersion, JEntity jEntity, Long idOptional,
			RequestMethod requestMethod) {

		Version version = versionService.versionCheck(idVersion);

		if (!"Y".equals(uiService.getAuthorizationById("OP-UP")))
			throw new ValidationException(-1, "USER_PERMISSION_OPTIONAL_UPDATE");

		versionService.versionCheckForUpdate(version); // per put/post/delete

		JVersionOptFull jVersionOptFull = null;
		try {
			jVersionOptFull = convertFromJEntityJVersionOptFull(jEntity.getData().getActual());
		} catch (IOException e) {
			throw new NotFoundException(-1, e.getMessage());
		}
		validateOpt(jVersionOptFull, idVersion, requestMethod);

		saveOptional(jEntity.getLanguage(), idVersion, jVersionOptFull, requestMethod);

		return getOptional(language, idVersion, idOptional);

	}

	public ApiResponseMessage deleteOptional(Long idVersion, Long idOptional) {

		Version version = versionService.versionCheck(idVersion);

		if (!"Y".equals(uiService.getAuthorizationById("OP-DE")))
			throw new ValidationException(-1, "USER_PERMISSION_OPTIONAL_DELETE");

		versionService.versionCheckForUpdate(version); // per put/post/delete

		VersionOpt versionOpt = versionOptRepository.findById(idOptional);

		if (versionOpt == null)
			throw new ValidationException(-1, "OPTIONAL_NOT_FOUND");

		versionOpt.setActive("N");

		versionOptRepository.save(versionOpt);

		ApiResponseMessage apiResponseMessage = new ApiResponseMessage();
		apiResponseMessage.setCode(0);
		apiResponseMessage.setMessage("SUCCESSFUL_DELETE");
		return apiResponseMessage;

	}

	private Date getLastUpdateDate(Long idOptional, Long idVersion) {

		Date lastUpdate;

		// se la versione è stata pubblicata usa data publish
		Version version = versionRepository.findById(idVersion);
		if ((version != null) && (version.getPublishDate() != null)) {
			lastUpdate = version.getPublishDate();
		} else
			lastUpdate = versionOptRepository.getLastUpdateDate(idOptional);

		return lastUpdate;
	}

	private List<JVersion> getVersionList(LocalizationType language, Long idModelOptType) {
		List<JVersion> lJversion = new ArrayList<JVersion>();
		lJversion = versionService.getVersions(language, idModelOptType);

		return lJversion;
	}

	private List<JForm> getFormList(LocalizationType language, String idOptType) {

		List<JForm> lJform = new ArrayList<JForm>();

		JForm jForm;
		for (OptionalTypeForm optionalTypeForm : optionalTypeFormRepository.findByOptType(idOptType)) {
			jForm = new JForm();
			jForm.setId(optionalTypeForm.getIdForm());
			jForm.setSeq(optionalTypeForm.getSequence());
			jForm.setDescription(optionalTypeForm.getDescription(language.getValue()));
			jForm.setIdImage(optionalTypeForm.getIdImage());
			jForm.setUrlImage(mediaService.getThronUrl(optionalTypeForm.getIdImage(), null));
			lJform.add(jForm);
		}
		return lJform;
	}

	// TODO V2 popola jModelType
	private JModelJump convertModelTypetoJModelType(ModelOptionalType modelOptionalType) {
		JModelJump jModelJump = new JModelJump();

		jModelJump.setId(modelOptionalType.getId());
		jModelJump.setIdModelOptionaType(modelOptionalType.getIdOptionalType());

		// se siamo P1 dobbiamo calcolare (se esiste)
		// id modeloptionalType p2
		// versione attualmente visualizzabile per P2

		ModelOptionalType m2 = modelOptionalTypeRepository.findByModelAndOptionalType(modelOptionalType.getIdModel(),
				calcOtherOptionalType(modelOptionalType.getIdOptionalType()));
		if (m2 != null) {

			jModelJump.setSwapId(m2.getId());
			jModelJump.setSwapIdModelOptionaType(m2.getIdOptionalType());

			// deteremina la versione visualizzabile per l'utente corrente
			Version v = versionService.getDefaultVersion(m2.getId());
			jModelJump.setSwapIdVersion(v.getId());

		}

		return jModelJump;
	}

	private String calcOtherOptionalType(String ot) {
		if ("P1".equals(ot)) {
			return "P2";
		} else if ("P2".equals(ot)) {
			return "P1";
		} else
			return null;

	}

	private JOptJump calcOtherOptionalLink(Model model, ModelOptionalType modelOptionalType, String codeERP) {
		JOptJump jOptJump = new JOptJump();

		if (codeERP != null) {

			// String px = calcOtherOptionalType(modelOptionalType.getIdOptionalType()); //
			// p1, p2 alternativa a attuale

			// verifico se presente un altro Px per stesso modello, usando stessa funzione
			// usata per jump modello
			JModelJump jModelJump = convertModelTypetoJModelType(modelOptionalType);

			if (jModelJump != null) {

				jOptJump.setIdModelOptionaType(jModelJump.getIdModelOptionaType());
				jOptJump.setSwapIdModelOptionaType(jModelJump.getSwapIdModelOptionaType());
				jOptJump.setSwapIdVersion(jModelJump.getSwapIdVersion());

				// cerca opt su altra versione
				// @Query("select c from VersionOpt c where c.idVersion=?1 c.codeErp = ?2 and
				// c.active='Y'")
				// List<VersionOpt> findByVersionAndCodeErp(Long idVersion, String codeErp);
				List<VersionOpt> v = versionOptRepository.findByVersionAndCodeErp(jModelJump.getSwapIdVersion(), codeERP);
				// dovrebbe esistere solo un OPT con stesso codice
				if (!v.isEmpty()) {
					jOptJump.setSwapId(v.get(0).getId());
					return jOptJump;
				}
				
			}
		}
		return null;
	}

	private JModel convertModelToJModel(LocalizationType language, Model model, Version version) {
		JModel jModel = new JModel();
		jModel.setId(model.getId());
		jModel.setCode(model.getCode());
		jModel.setDescription(model.getDescription(language.getValue()));
		jModel.setSeq(model.getSequence());
		jModel.setIdImage(model.getHomepageImage());
		jModel.setIdImageCover(model.getCoverImage());
		jModel.setIdImageLogo(model.getLogoImage());

		jModel.setUrlImage(mediaService.getThronUrl(model.getHomepageImage(), ModelService.MEDIA_MODEL_TEMPLATE_HOME));
		jModel.setUrlImageCover(
				mediaService.getThronUrl(model.getCoverImage(), ModelService.MEDIA_MODEL_TEMPLATE_COVER));
		jModel.setUrlImageLogo(mediaService.getThronUrl(model.getLogoImage(), ModelService.MEDIA_MODEL_TEMPLATE_LOGO));

		jModel.setMediaTemplateImage(ModelService.MEDIA_MODEL_TEMPLATE_HOME);
		jModel.setMediaTemplateImageLogo(ModelService.MEDIA_MODEL_TEMPLATE_LOGO);
		jModel.setMediaTemplateImageCover(ModelService.MEDIA_MODEL_TEMPLATE_COVER);
		jModel.setVisibility(model.getVisibility());
		// jModel.setConfidential(getConfidentialStatus(model, version));
		jModel.setConfidential(getConfidentialStatus(model));

		jModel.setQuality(model.getIdQuality());

		return jModel;
	}

	public JVersionOptForm convertToJVersionOptForm(HashMap<String, Object> dto)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		JVersionOptForm jVersionOptForm = objectMapper.readValue(objectMapper.writeValueAsString(dto),
				JVersionOptForm.class);
		return jVersionOptForm;
	}

	public JVersionAttcForm convertToJVersionAttcForm(HashMap<String, Object> dto)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		JVersionAttcForm jVersionAttcForm = objectMapper.readValue(objectMapper.writeValueAsString(dto),
				JVersionAttcForm.class);
		return jVersionAttcForm;
	}

	public JPage getDocumentList(LocalizationType language, Long idVersion, DocumentType documentType, Long page,
			Long rows) {

		Version version = versionService.versionCheck(idVersion);

		ModelOptionalType modelOptionalType = modelOptionalTypeRepository.findById(version.getIdModelOptionalType());
		if (modelOptionalType == null)
			throw new ValidationException(-1, "MODEL_OPTIONAL_TYPE_NOT_FOUND");
		Model model = modelRepository.findById(modelOptionalType.getIdModel());
		if (model == null)
			throw new ValidationException(-1, "MODEL_NOT_FOUND");

		Boolean enableAllModels = ("Y".equals(uiService.getAuthorizationById(ModelService.MODEL_DISPLAY_ENABLE)));
		if ((!"Y".equals(model.getVisibility())) && !enableAllModels) {
			// non visibile: abilitati solo utenti meritevili
			throw new ValidationException(-1, "MODEL_NOT_FOUND"); // AVAILABLE
		}

		JPage jpage = new JPage();
		JPageData jPageData = new JPageData();

		JVersionAttcForm jVersionAttcForm = new JVersionAttcForm();
		jVersionAttcForm.setJmodel(convertModelToJModel(language, model, version));

		// TODO aggiungere oggetto JModelType 6
		jVersionAttcForm.setJModelJump(convertModelTypetoJModelType(modelOptionalType));

		JVersion jVersion = versionService.buildFromVersion(language, version);
		jVersion.setAuthorizations(null); // in questo caso non esporto le autorizzazioni
		jVersionAttcForm.setJversion(jVersion);

		List<JVersionAttc> lJVersionAttc = new ArrayList<JVersionAttc>();
		if (page == null || page.equals(0L))
			page = 1L;

		if (rows == null || rows.equals(0L))
			rows = 10L; // default

		JPagination jPagination = new JPagination();
		jPagination.setRowsPerPage(rows);
		jPagination.setTotalRows(versionAttcRepository.countByVersionType(idVersion, documentType.getValue()));

		Sort sortBySequeceAndId = new Sort(Sort.Direction.ASC, "sequence");
		sortBySequeceAndId.and(new Sort(Sort.Direction.ASC, "id"));

		Pageable pageable = new PageRequest(page.intValue() - 1 // zero based
				, rows.intValue() // righe da visualizzare
				, sortBySequeceAndId);

		for (VersionAttc versionAttc : versionAttcRepository.findByVersionType(idVersion, documentType.getValue(),
				pageable)) {

			///////////////////////////////////////////////////////////////////////////////////
			JVersionAttc jVersionAttc = new JVersionAttc();
			jVersionAttc.setId(versionAttc.getId());
			jVersionAttc.setAttachmentType(versionAttc.getAttachmentType());
			jVersionAttc.setChoice(Boolean.FALSE);
			jVersionAttc.setDescription(versionAttc.getDescription(language.getValue()));
			jVersionAttc.setDescription_en(versionAttc.getDescription_en());
			jVersionAttc.setDescription_it(versionAttc.getDescription_it());

			jVersionAttc.setIdImage(versionAttc.getIdImage());
			jVersionAttc.setIdImageThumb(versionAttc.getIdImageThumb());
			jVersionAttc.setIdImageSvg(versionAttc.getIdImageSvg());

			jVersionAttc.setUrlImage(mediaService.getThronUrl(versionAttc.getIdImage(), null));
			
			String urlTump = mediaService.getThronUrl(versionAttc.getIdImageThumb(), null);
			if ( documentType == DocumentType.F && urlTump == null) {
				urlTump = mediaService.getThronUrlJpegForPrd(versionAttc.getIdImage(), null);
			}
			
			jVersionAttc.setUrlImageThumb(urlTump);
			jVersionAttc.setUrlImageSvg(mediaService.getThronUrl(versionAttc.getIdImageSvg(), null));

			jVersionAttc.setSequence(versionAttc.getSequence());
			jVersionAttc.setContentType(versionAttc.getContentType());

			jVersionAttc.setFilename(getFilename(versionAttc.getIdImage()));

			if (documentType.equals(DocumentType.F)) {
				jVersionAttc.setMediaTemplateImage(MEDIA_FIGURINO_TEMPLATE);
				jVersionAttc.setMediaTemplateImageThumb(MEDIA_FIGURINO_THUMB_TEMPLATE);
				jVersionAttc.setMediaTemplateImageSvg(MEDIA_FIGURINO_SVG_TEMPLATE);
			} else {
				jVersionAttc.setMediaTemplateImage(MEDIA_OTHERDOC_TEMPLATE);
				jVersionAttc.setMediaTemplateImageThumb(MEDIA_OTHERDOC_THUMB_TEMPLATE);
				jVersionAttc.setMediaTemplateImageSvg(null);
			}
			///////////////////////////////////////////////////////////////////////////////////

			List<JVersionOpt> lJVersionOpt = new ArrayList<JVersionOpt>();
			for (VersionOptAttc versionOptAttc : versionAttc.getlVersionOptAttc()) {
				VersionOpt versionOpt = versionOptAttc.getVersionOpt();
				JVersionOpt jVersionOpt = new JVersionOpt();
				jVersionOpt.setId(versionOpt.getId());
				jVersionOpt.setCode(versionOpt.getCode());
				jVersionOpt.setChoice(Boolean.FALSE);
				jVersionOpt.setDescription(versionOpt.getDescription(language.getValue()));
				jVersionOpt.setSequence(versionOpt.getSequence());

				lJVersionOpt.add(jVersionOpt);
			}
			jVersionAttc.setlJVersionOpt(lJVersionOpt);

			lJVersionAttc.add(jVersionAttc);

		}

		// TBD se richiesto creiamo link virtuali a Copertina/Logo
		if (documentType.equals(DocumentType.O)) {
//			// numeri negativi = id fittizi
//			// se richiesto possiamo attivare la lista dell virtualImages derivata da model images
//			JVersionAttc jVersionAttc = null;
//			jVersionAttc = getVirtualDocument(ModelService.MEDIA_MODEL_TEMPLATE_COVER, model); // il template serve solo a comunicare il tipo di immagine
//			jVersionAttc = getVirtualDocument(ModelService.MEDIA_MODEL_TEMPLATE_LOGO, model);
//			jVersionAttc = getVirtualDocument(ModelService.MEDIA_MODEL_TEMPLATE_HOME, model);
//			// ..... 
//			// JVersionAttc jVersionAttc = new JVersionAttc();
//			// lJVersionAttc.add(jVersionAttc);
//			

		}

		jVersionAttcForm.setlJVersionAttc(lJVersionAttc);

		jPageData.setActual(jVersionAttcForm);

		String PAGE_DOC;
		if (documentType.equals(DocumentType.F))
			PAGE_DOC = UiService.PAGE_DOC_FIGURINI;
		else if (documentType.equals(DocumentType.O))
			PAGE_DOC = UiService.PAGE_DOC_OTHERS;
		else
			throw new ValidationException(-1, "DOCUMENT_NOT_ALLOWED");

		// header JPage ------------------------------------------------------------
		UiPage uiPage = uiPageRepository.findByKey(PAGE_DOC);
		jpage.setId(uiPage.getId());
		jpage.setName(uiPage.getDescription());
		jpage.setLanguage(language);

		// paginazione

		jpage.setjPagination(jPagination);

		// aggiunge lista autorizzazioni -------------------------------------------
		List<JPageAuth> lJPageAuth = uiService.getAuthorizationList(PAGE_DOC);
		// eccezione: se non è DRAFT --> fullIO = N
		// @@@@
		for (JPageAuth jPageAuth : lJPageAuth) {
			if (!(version.getStatus().equals("D"))) {
				// permessi solo su draft, su altri stati versione non si puo' intervenire
				if ((jPageAuth.getAuthId().equals("DO-AD")) || (jPageAuth.getAuthId().equals("DO-DE"))
						|| (jPageAuth.getAuthId().equals("DO-UP"))) {
					jPageAuth.setPermission("N");
					jPageAuth.setAuthDescription(jPageAuth.getAuthDescription() + " [Override per DRAFT]");
				}
			}
		}
		jpage.setAuths(lJPageAuth);

		jpage.setData(jPageData);

		return jpage;
	}

//	private JVersionAttc getVirtualDocument(String docType, Model model) {
//		JVersionAttc jVersionAttc = new JVersionAttc();
//		jVersionAttc.setAttachmentType("O");
//		
//		if (docType.equals(ModelService.MEDIA_MODEL_TEMPLATE_COVER)) {
//			jVersionAttc.setId(-1L);
//			jVersionAttc.setSequence(999999L);
//			jVersionAttc.setIdImage(model.getCoverImage());
//			jVersionAttc.setUrlImage(mediaService.getThronUrl(model.getCoverImage(), null));
//			jVersionAttc.setDescription("TBD Cover PIC");
//			jVersionAttc.setContentType("TBD");
//		} else if (docType.equals(ModelService.MEDIA_MODEL_TEMPLATE_LOGO)) {
//			jVersionAttc.setId(-2L);
//			jVersionAttc.setSequence(999999L);
//			jVersionAttc.setIdImage(model.getLogoImage());
//			jVersionAttc.setUrlImage(mediaService.getThronUrl(model.getLogoImage(), null));
//			jVersionAttc.setDescription("TBD Logo PIC");
//			jVersionAttc.setContentType("TBD");
//		} else if (docType.equals(ModelService.MEDIA_MODEL_TEMPLATE_HOME)) {
//			jVersionAttc.setId(-3L);
//			jVersionAttc.setSequence(999999L);
//			jVersionAttc.setIdImage(model.getHomepageImage());
//			jVersionAttc.setUrlImage(mediaService.getThronUrl(model.getHomepageImage(), null));
//			jVersionAttc.setDescription("TBD Homepage PIC");
//			jVersionAttc.setContentType("TBD");
//		}
//
//		if (jVersionAttc.getIdImage()==null)
//			return null;
//		
//		System.out.println(jVersionAttc);
//		return jVersionAttc;
//	}

	@Override
	public JPage getDocument(LocalizationType language, Long idVersion, DocumentType documentType, Long idDocument) {

		JPage jpage = new JPage();
		JPageData jPageData = new JPageData();
		JVersionAttc jVersionAttc = new JVersionAttc();

		Version version = versionService.versionCheck(idVersion);

		JVersion jVersion = versionService.buildFromVersion(language, version);
		jVersion.setAuthorizations(null); // in questo caso non esporto le autorizzazioni

		Long idModelOptionalType = version.getIdModelOptionalType();
		ModelOptionalType modelOptionalType = modelOptionalTypeRepository.getOne(idModelOptionalType);
		Model model = modelRepository.getOne(modelOptionalType.getIdModel());

		Boolean enableAllModels = ("Y".equals(uiService.getAuthorizationById(ModelService.MODEL_DISPLAY_ENABLE)));
		if ((!"Y".equals(model.getVisibility())) && !enableAllModels) {
			// non visibile: abilitati solo utenti meritevili
			throw new ValidationException(-1, "MODEL_NOT_FOUND"); // AVAILABLE
		}

		JModel jModel = modelService.buildFromModel(language, model, version);

		jVersionAttc.setjModel(jModel);
		jVersionAttc.setjVersion(jVersion);

		// TODO aggiungere oggetto JModelType 6
		jVersionAttc.setJModelJump(convertModelTypetoJModelType(modelOptionalType));

		VersionAttc versionAttc = versionAttcRepository.findById(idDocument);
		if (versionAttc == null)
			throw new ValidationException(-1, "DOCUMENT_NOT_FOUND");

		jVersionAttc.setAttachmentType(versionAttc.getAttachmentType());
		jVersionAttc.setChoice(null);
		jVersionAttc.setDescription(versionAttc.getDescription(language.getValue()));
		jVersionAttc.setDescription_it(versionAttc.getDescription_it());
		jVersionAttc.setDescription_en(versionAttc.getDescription_en());
		jVersionAttc.setId(versionAttc.getId());
		jVersionAttc.setIdImage(versionAttc.getIdImage());
		jVersionAttc.setIdImageThumb(versionAttc.getIdImageThumb());
		jVersionAttc.setIdImageSvg(versionAttc.getIdImageSvg());

		if ("F".contentEquals(documentType.getValue())) {
			jVersionAttc.setUrlImage(mediaService.getThronUrl(versionAttc.getIdImage(), MEDIA_FIGURINO_TEMPLATE));
			jVersionAttc.setUrlImageThumb(
					mediaService.getThronUrl(versionAttc.getIdImageThumb(), MEDIA_FIGURINO_THUMB_TEMPLATE));
			jVersionAttc
					.setUrlImageSvg(mediaService.getThronUrl(versionAttc.getIdImageSvg(), MEDIA_FIGURINO_SVG_TEMPLATE));

			jVersionAttc.setMediaTemplateImage(MEDIA_FIGURINO_TEMPLATE);
			jVersionAttc.setMediaTemplateImageThumb(MEDIA_FIGURINO_THUMB_TEMPLATE);
			jVersionAttc.setMediaTemplateImageSvg(MEDIA_FIGURINO_SVG_TEMPLATE);

		} else {
			jVersionAttc.setUrlImage(mediaService.getThronUrl(versionAttc.getIdImage(), MEDIA_OTHERDOC_TEMPLATE));
			jVersionAttc.setUrlImageThumb(
					mediaService.getThronUrl(versionAttc.getIdImageThumb(), MEDIA_OTHERDOC_THUMB_TEMPLATE));
			jVersionAttc.setMediaTemplateImageSvg(null);

			jVersionAttc.setMediaTemplateImage(MEDIA_OTHERDOC_TEMPLATE);
			jVersionAttc.setMediaTemplateImageThumb(MEDIA_OTHERDOC_THUMB_TEMPLATE);
			jVersionAttc.setMediaTemplateImageSvg(null);

		}

		jVersionAttc.setSequence(versionAttc.getSequence());
		jVersionAttc.setContentType(versionAttc.getContentType());

		jVersionAttc.setFilename(getFilename(versionAttc.getIdImage()));

		List<JVersionOpt> lJVersionOpt = new ArrayList<JVersionOpt>();
		for (VersionOptAttc versionOptAttc : versionAttc.getlVersionOptAttc()) {

			VersionOpt versionOpt = versionOptAttc.getVersionOpt();

			JVersionOpt jVersionOpt = new JVersionOpt();
			jVersionOpt.setId(versionOpt.getId());
			jVersionOpt.setCode(versionOpt.getCode());
			jVersionOpt.setDescription(versionOpt.getDescription(language.getValue()));
			jVersionOpt.setSequence(versionOpt.getSequence());
			lJVersionOpt.add(jVersionOpt);
		}

		jVersionAttc.setlJVersionOpt(lJVersionOpt);

		jPageData.setActual(jVersionAttc);

		String PAGE_DOC;
		if (documentType.equals(DocumentType.F))
			PAGE_DOC = UiService.PAGE_DOC_FIGURINI;
		else if (documentType.equals(DocumentType.O))
			PAGE_DOC = UiService.PAGE_DOC_OTHERS;
		else
			throw new ValidationException(-1, "DOCUMENT_NOT_ALLOWED");

		// header JPage ------------------------------------------------------------
		UiPage uiPage = uiPageRepository.findByKey(PAGE_DOC);
		jpage.setId(uiPage.getId());
		jpage.setName(uiPage.getDescription());
		jpage.setLanguage(language);

		// aggiunge lista autorizzazioni -------------------------------------------
//		List<JPageAuth> lJPageAuth = uiService.getAuthorizationList(PAGE_DOC );
//		
//		jpage.setAuths(lJPageAuth);

		// aggiunge lista autorizzazioni -------------------------------------------
		List<JPageAuth> lJPageAuth = uiService.getAuthorizationList(PAGE_DOC);
		// eccezione: se non è DRAFT --> fullIO = N
		// @@@@
		for (JPageAuth jPageAuth : lJPageAuth) {
			if (!(version.getStatus().equals("D"))) {
				// permessi solo su draft, su altri stati versione non si puo' intervenire
				if ((jPageAuth.getAuthId().equals("DO-AD")) || (jPageAuth.getAuthId().equals("DO-DE"))
						|| (jPageAuth.getAuthId().equals("DO-UP"))) {
					jPageAuth.setPermission("N");
					jPageAuth.setAuthDescription(jPageAuth.getAuthDescription() + " [Override per DRAFT]");
				}
			}
		}
		jpage.setAuths(lJPageAuth);

		jpage.setData(jPageData);

		return jpage;

	}

	private String getFilename(String id) {
		Media media = mediaRepository.findById(id);
		if (media != null) {
			if (media.getFilename() != null)
				return media.getFilename();
		}
		return null;
	}

	public JPage postDocument(LocalizationType language, Long idVersion, JEntity jEntity, DocumentType documentType,
			RequestMethod requestMethod) {

		Version version = versionService.versionCheck(idVersion);

		if (!"Y".equals(uiService.getAuthorizationById("DO-AD")))
			throw new ValidationException(-1, "USER_PERMISSION_DOCUMENT_CREATE");

		versionService.versionCheckForUpdate(version); // per put/post/delete

		JVersionAttc jVersionAttc = null;
		try {
			jVersionAttc = convertFromJEntityJVersionAttc(jEntity.getData().getActual());
		} catch (IOException e) {
			throw new NotFoundException(-1, e.getMessage());
		}
		validateDocument(jVersionAttc, idVersion, requestMethod);

		Long idDocument = saveDocument(jEntity.getLanguage(), idVersion, jVersionAttc, requestMethod);

		return getDocument(jEntity.getLanguage(), idVersion, documentType, idDocument);

	}

	public JPage putDocument(LocalizationType language, Long idVersion, JEntity jEntity, Long idDocument,
			DocumentType documentType, RequestMethod requestMethod) {

		Version version = versionService.versionCheck(idVersion);

		if (!"Y".equals(uiService.getAuthorizationById("DO-UP")))
			throw new ValidationException(-1, "USER_PERMISSION_DOCUMENT_UPDATE");

		// il richiamo a version verifica la modificabilità : se draft ok else KO
		versionService.versionCheckForUpdate(version); // per put/post/delete

		JVersionAttc jVersionAttc = null;
		try {
			jVersionAttc = convertFromJEntityJVersionAttc(jEntity.getData().getActual());
		} catch (IOException e) {
			throw new NotFoundException(-1, e.getMessage());
		}
		validateDocument(jVersionAttc, idVersion, requestMethod);

		saveDocument(jEntity.getLanguage(), idVersion, jVersionAttc, requestMethod);

		return getDocument(jEntity.getLanguage(), idVersion, documentType, idDocument);

	}

	public ApiResponseMessage deleteDocument(Long idVersion, Long idDocument, DocumentType documentType) {

		Version version = versionService.versionCheck(idVersion);

		if (!"Y".equals(uiService.getAuthorizationById("DO-DE")))
			throw new ValidationException(-1, "USER_PERMISSION_DOCUMENT_DELETE");

		versionService.versionCheckForUpdate(version); // per put/post/delete

		VersionAttc versionAttc = versionAttcRepository.findById(idDocument);
		if (versionAttc == null)
			throw new ValidationException(-1, "DOCUMENT_NOT_FOUND");

		versionAttc.setActive("N");

		versionAttcRepository.save(versionAttc);

		ApiResponseMessage apiResponseMessage = new ApiResponseMessage();
		apiResponseMessage.setCode(0);
		apiResponseMessage.setMessage("SUCCESSFUL DELETE");
		return apiResponseMessage;

	}

	public JPage getDocumentTemplate(LocalizationType language, Long idVersion, DocumentType documentType) {

		versionService.versionCheck(idVersion);

		JPage jpage = new JPage();
		JPageData jPageData = new JPageData();
		JVersionAttc jVersionAttc = new JVersionAttc();

		jVersionAttc.setAttachmentType(documentType.getValue());

		List<JVersionOpt> lJVersionOpt = new ArrayList<JVersionOpt>();
		JVersionOpt jVersionOpt = new JVersionOpt(); // tutto impostato a null
		lJVersionOpt.add(jVersionOpt);
		jVersionAttc.setlJVersionOpt(lJVersionOpt);

		// dipendentemente dal tipo media
		if ("F".contentEquals(documentType.getValue())) {
			jVersionAttc.setMediaTemplateImage(MEDIA_FIGURINO_TEMPLATE);
			jVersionAttc.setMediaTemplateImageThumb(MEDIA_FIGURINO_THUMB_TEMPLATE);
			jVersionAttc.setMediaTemplateImageSvg(MEDIA_FIGURINO_SVG_TEMPLATE);
		} else {
			jVersionAttc.setMediaTemplateImage(MEDIA_OTHERDOC_TEMPLATE);
			jVersionAttc.setMediaTemplateImageThumb(MEDIA_OTHERDOC_THUMB_TEMPLATE);
			jVersionAttc.setMediaTemplateImageSvg(null);
		}

		// ... TBD altri campi da proporre
		jPageData.setActual(jVersionAttc);

		String PAGE_DOC;
		if (documentType.equals(DocumentType.F))
			PAGE_DOC = UiService.PAGE_DOC_FIGURINI;
		else if (documentType.equals(DocumentType.O))
			PAGE_DOC = UiService.PAGE_DOC_OTHERS;
		else
			throw new ValidationException(-1, "DOCUMENT_NOT_ALLOWED");

		// header JPage ------------------------------------------------------------
		UiPage uiPage = uiPageRepository.findByKey(PAGE_DOC);
		jpage.setId(uiPage.getId());
		jpage.setName(uiPage.getDescription());
		jpage.setLanguage(language);

		// aggiunge lista autorizzazioni -------------------------------------------
		List<JPageAuth> lJPageAuth = uiService.getAuthorizationList(PAGE_DOC);
		jpage.setAuths(lJPageAuth);

		jpage.setData(jPageData);

		return jpage;

	}

	private Long saveDocument(LocalizationType language, Long idVersion, JVersionAttc jVersionAttc,
			RequestMethod requestMethod) {

		VersionAttc versionAttc = null;

		if (requestMethod.equals(RequestMethod.PUT)) {

			versionAttc = versionAttcRepository.findById(jVersionAttc.getId());
			if (versionAttc == null) {
				throw new ValidationException(-1, "DOCUMENT_NOT_FOUND");
			}
			versionAttc.setId(jVersionAttc.getId());
		}

		if (requestMethod.equals(RequestMethod.POST)) {
			versionAttc = new VersionAttc();
			versionAttc.setId(null);

		}

		Version version = versionRepository.findById(idVersion);
		if (version == null)
			throw new ValidationException(-1, "VERSION_NOT_FOUND");

		versionAttc.setAttachmentType(jVersionAttc.getAttachmentType());
		versionAttc.setDescription_it(jVersionAttc.getDescription_it());
		versionAttc.setDescription_en(jVersionAttc.getDescription_en());
		versionAttc.setIdImage(jVersionAttc.getIdImage());
		versionAttc.setIdImageThumb(jVersionAttc.getIdImageThumb());
		versionAttc.setIdImageSvg(jVersionAttc.getIdImageSvg());
		versionAttc.setSequence(jVersionAttc.getSequence());
		versionAttc.setContentType(jVersionAttc.getContentType());
		versionAttc.setVersion(version);

		///////////////////////////////////////////////////////////////////////////////////////////////////////////

		versionAttc.getlVersionOptAttc().clear();
		for (JVersionOpt jVersionOpt : jVersionAttc.getlJVersionOpt()) {
			if (jVersionOpt.getId() != null) {
				VersionOpt versionOpt = versionOptRepository.findOne(jVersionOpt.getId());

				if (versionOpt != null) {
					VersionOptAttcPK versionOptAttcPK = new VersionOptAttcPK();
					versionOptAttcPK.setIdVersionOpt(jVersionOpt.getId());
					versionOptAttcPK.setIdVersionAttc(versionAttc.getId());
					VersionOptAttc versionOptAttc = versionOptAttcRepository.findOne(versionOptAttcPK);
					if (versionOptAttc == null)
						versionOptAttc = new VersionOptAttc();

					versionOptAttc.setActive("Y");
					versionOptAttc.setId(versionOptAttcPK);
					versionOptAttc.setVersionAttc(versionAttc);
					versionOptAttc.setVersionOpt(versionOpt);
					versionAttc.addVersionOptAttc(versionOptAttc);

					// System.out.println(versionOptAttc);

				}
			}
		}
		///////////////////////////////////////////////////////////////////////////////////////////////////////////

		versionAttcRepository.save(versionAttc);
		return versionAttc.getId();
	}

	private void validateDocument(JVersionAttc jVersionAttc, Long idVersion, RequestMethod requestMethod) {

		if ((requestMethod.equals(RequestMethod.PUT)) && jVersionAttc.getId() == null) {
			throw new ValidationException(-1, "DOCUMENT_UPDATE_MISSING_ID");
		}

		if ((requestMethod.equals(RequestMethod.POST)) && jVersionAttc.getId() != null) {
			throw new ValidationException(-1, "DOCUMENT_CREATE_WITH_ID");
		}

	}

	private Long saveOptional(LocalizationType language, Long idVersion, JVersionOptFull jVersionOptFull,
			RequestMethod requestMethod) {

		Version version = versionRepository.findById(idVersion);
		if (version == null)
			throw new ValidationException(-1, "VERSION_NOT_FOUND");

		VersionOpt versionOpt = null;

		if (requestMethod.equals(RequestMethod.PUT)) {
			// update
			versionOpt = versionOptRepository.findById(jVersionOptFull.getId());
			if (versionOpt == null) {
				throw new ValidationException(-1, "OPTIONAL_NOT_FOUND");
			}
			versionOpt.setId(jVersionOptFull.getId());
		}

		if (requestMethod.equals(RequestMethod.POST)) {
			// create
			versionOpt = new VersionOpt();
			versionOpt.setId(null);
		}

		versionOpt.setIdVersion(idVersion);
		versionOpt.setCode(jVersionOptFull.getCode());
		versionOpt.setCodeErp(jVersionOptFull.getCodeErp());

		versionOpt.setIdModelOptionalSubGroup(jVersionOptFull.getIdModelOptionalSubGroup());
		versionOpt.setVisible(jVersionOptFull.getVisible());
		versionOpt.setOrderableFlag(jVersionOptFull.getOrderableFlag());

		// determina sequence number se non impostato
		// se non c'è il gruppo opt: FAIL
		if (jVersionOptFull.getSequence() == null || jVersionOptFull.getSequence().equals(0L)) {
			logger.warn("Calcolo sequence in inserimento opt {}", jVersionOptFull.getCode());
			if (jVersionOptFull.getIdModelOptionalSubGroup() != null) {
				logger.warn("Calcolo sequence in inserimento opt {} da subgroup {}", jVersionOptFull.getCode(),
						jVersionOptFull.getIdModelOptionalSubGroup());
				Long seq = versionOptRepository.getNextSequenceNumber(idVersion,
						jVersionOptFull.getIdModelOptionalSubGroup());
				jVersionOptFull.setSequence(seq);
				logger.warn("Calcolo sequence: attribuito il {}", seq);
			}
		}

		versionOpt.setSequence(jVersionOptFull.getSequence());

		versionOpt.setDescription(jVersionOptFull.getDescription(), language.getValue());
		versionOpt.setOrderable(jVersionOptFull.getOrderable(), language.getValue());
		versionOpt.setVersionModel(jVersionOptFull.getVersionModel(), language.getValue());

		/////////////////////// TESTI
		/////////////////////// /////////////////////////////////////////////////////////////////

		List<VersionOptText> lVersionOptText = new ArrayList<VersionOptText>();
		versionOpt.getlVersionOptText().clear(); // workaround per orphanRemoval=true
		lVersionOptText = versionOpt.getlVersionOptText();

		for (JVersionOptText jVersionOptText : jVersionOptFull.getlJVersionOptText()) {
			if (jVersionOptText.getDescription() == null) {
				continue;
			}
			VersionOptText versionOptText = null;
			if (jVersionOptText.getId() != null) {
				versionOptText = versionOptTextRepository.findOne(jVersionOptText.getId());
			}
			if (versionOptText == null)
				versionOptText = new VersionOptText();

			versionOptText.setTextType(jVersionOptText.getTextType());
			versionOptText.setDescription(jVersionOptText.getDescription(), language.getValue());
			versionOptText.setId(jVersionOptText.getId());
			versionOptText.setVersionOpt(versionOpt);

			lVersionOptText.add(versionOptText);
		}
		versionOpt.setlVersionOptText(lVersionOptText);

		/////////////////// PICS
		/////////////////// /////////////////////////////////////////////////////////////////////

		List<VersionOptPics> lVersionOptPics = new ArrayList<VersionOptPics>();
		versionOpt.getlVersionOptPics().clear(); // workaround per orphanRemoval=true
		lVersionOptPics = versionOpt.getlVersionOptPics();

		for (JVersionOptPics jVersionOptPics : jVersionOptFull.getlJVersionOptPics()) {
			VersionOptPics versionOptPics;
			if (jVersionOptPics.getId() != null) {
				versionOptPics = versionOptPicsRepository.findOne(jVersionOptPics.getId());
			} else {
				if ((jVersionOptPics.getIdImage() == null) || (jVersionOptPics.getPicType() == null)) // se non c'è
																										// nulla da
																										// salvare...
					continue;
				versionOptPics = new VersionOptPics();

			}

			versionOptPics.setId(jVersionOptPics.getId());
			versionOptPics.setDescription(jVersionOptPics.getDescription(), language.getValue());
			versionOptPics.setDescription_it(jVersionOptPics.getDescription_it());
			versionOptPics.setDescription_en(jVersionOptPics.getDescription_en());

			versionOptPics.setIdImage(jVersionOptPics.getIdImage());
			versionOptPics.setIdImageLow(jVersionOptPics.getIdImageLow());
			versionOptPics.setPicType(jVersionOptPics.getPicType());
			versionOptPics.setSequence(jVersionOptPics.getSequence());
			versionOptPics.setVersionOpt(versionOpt);

			lVersionOptPics.add(versionOptPics);

		}

		versionOpt.setlVersionOptPics(lVersionOptPics);

		versionOptRepository.saveAndFlush(versionOpt);

//		/////////////////////////////////////////////////////////////////////////////////
//		
		List<VersionOptAttc> oldOpt = new ArrayList<VersionOptAttc>(versionOpt.getlVersionOptAttc());
		for (VersionOptAttc versionOptAttc : oldOpt) {
			versionOptAttcRepository.delete(versionOptAttc.getId());
		}
//		
//		// salva legami figuriniOpt ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
		// dal momento che devo passare l'oggetto versionOpt effettuo il save dopo aver
		// creato versionOpt

		List<VersionOptAttc> lVersionOptAttc = new ArrayList<VersionOptAttc>();
		versionOpt.getlVersionOptAttc().clear(); // workaround per orphanRemoval=true
		lVersionOptAttc = versionOpt.getlVersionOptAttc();

		for (JVersionAttc jVersionAttc : jVersionOptFull.getlJVersionAttc()) {
			VersionOptAttcPK versionOptAttcPK = new VersionOptAttcPK();
			// versionOptAttcPK.setIdVersionOpt(jVersionOptFull.getId());
			versionOptAttcPK.setIdVersionOpt(versionOpt.getId());
			versionOptAttcPK.setIdVersionAttc(jVersionAttc.getId());
			VersionOptAttc versionOptAttc = versionOptAttcRepository.findOne(versionOptAttcPK);
			if (versionOptAttc == null)
				versionOptAttc = new VersionOptAttc();
			versionOptAttc.setId(versionOptAttcPK);
			VersionAttc versionAttc = versionAttcRepository.findById(jVersionAttc.getId());
			if (versionAttc == null)
				continue;

			versionOptAttc.setVersionAttc(versionAttc);
			versionOptAttc.setVersionOpt(versionOpt);

			lVersionOptAttc.add(versionOptAttc);

		}
		versionOpt.setlVersionOptAttc(lVersionOptAttc);
		versionOptRepository.saveAndFlush(versionOpt);

		return versionOpt.getId();

	}

	private void validateOpt(JVersionOptFull jVersionOptFull, Long idVersion, RequestMethod requestMethod) {

		VersionOpt versionOpt = versionOptRepository.findByVersionAndOpt(idVersion, jVersionOptFull.getCode());
		if (requestMethod.equals(RequestMethod.POST)) {
			if (versionOpt != null) {

				if ("Y".equals(versionOpt.getActive())) {
					throw new ValidationException(-1, "OPTIONAL_DUPLICATED_NOTALLOWED");
				} else {
					throw new ValidationException(-1, "OPTIONAL_DUPLICATED_NOTALLOWED_A");
				}
			}
		}
		if (requestMethod.equals(RequestMethod.PUT)) {
			if (versionOpt != null) {
				if (!versionOpt.getId().equals(jVersionOptFull.getId())) {
					if ("Y".equals(versionOpt.getActive())) {
						throw new ValidationException(-1, "OPTIONAL_DUPLICATED_NOTALLOWED");
					} else {
						throw new ValidationException(-1, "OPTIONAL_DUPLICATED_NOTALLOWED_A");
					}

				}
			}

		}

	}

	private Long validateOpt(JVersionOptImport jVersionOptImport, Long idVersion, String operation) {
		Long retval = 0L;
		// carica versionOpt per codiceOptional;
		VersionOpt versionOpt = versionOptRepository.findByVersionAndOpt(idVersion, jVersionOptImport.getCode());

		if (operation.equals(DocService.UPLOAD_ROW_INSERT)) {
			if (versionOpt != null) {
				retval = -1L;
			}
		}

		if (!operation.equals(DocService.UPLOAD_ROW_INSERT)) {
			// siamo in richiesta update
			// se esiste un versionOpt con differente ID a parità di codice --> errore
			if ((versionOpt != null) && (!versionOpt.getId().equals(jVersionOptImport.getId()))) {
				retval = -2L;

			}
		}
		return retval;

	}

	@SuppressWarnings("deprecation")
	public JPage uploadXlsOptionals(MultipartFile multipartFile) throws IOException {

		if (!"Y".equals(uiService.getAuthorizationById("UXLS")))
			throw new ValidationException(-1, "USER_PERMISSION_UPLOAD");

		List<String> logs = new ArrayList<String>();
		ApiResponseMessage apiResponseMessage = new ApiResponseMessage();

		apiResponseMessage.setCode(0);
		apiResponseMessage.setMessage("SUCCESSFUL UPLOAD");

		XSSFWorkbook wb = new XSSFWorkbook(multipartFile.getInputStream());
		XSSFSheet shSummary = wb.getSheet(DocService.EXCEL_FULL_SHEETNAME_SUMMARY);
		if (shSummary == null) {
			apiResponseMessage.setCode(-1);
			apiResponseMessage.setMessage("INVALID_EXCEL");
			logStep(logs, "ABORTED - Invalid excel");
			throw new ValidationException(-1, "INVALID_EXCEL");

		}

		logStep(logs, "STEP 1 BEGIN - Controlli globali ");
		logStep(logs, "Import file=" + multipartFile.getOriginalFilename());

		List<JVersionOptImport> lJVersionOptImport = new ArrayList<JVersionOptImport>();

		Long idVersion = null;
		try {
			idVersion = (long) shSummary.getRow(8).getCell(2).getNumericCellValue();
		} catch (Exception e) {
			apiResponseMessage.setCode(-1);
			apiResponseMessage.setMessage("INVALID_EXCEL");
			logStep(logs, "ABORTED - Invalid excel");
			throw new ValidationException(-1, "INVALID_EXCEL");

		}

		// Version version = versionRepository.findOne(idVersion);
		Version version = versionService.versionCheck(idVersion);

		if (version == null) {

			apiResponseMessage.setCode(-1);
			apiResponseMessage.setMessage("INVALID_VERSION_ID UPLOAD");
			logStep(logs, "ABORTED - Invalid version");
			throw new ValidationException(-1, "VERSION_NOT_FOUND");
		}
		if (!"D".equals(version.getStatus())) {
			logStep(logs, "ABORTED - Version must be DRAFT");
			apiResponseMessage.setCode(-2);
			apiResponseMessage.setMessage("VERSION_MUST_BE_DRAFT UPLOAD");
			throw new ValidationException(-1, "VERSION_MUST_BE_DRAFT");

		}

		versionService.versionCheckForUpdate(version); // per put/post/delete

		if (apiResponseMessage.getCode() == 0) {

			String lang = shSummary.getRow(12).getCell(2).getStringCellValue();

			XSSFSheet shOptionals = wb.getSheet(DocService.EXCEL_FULL_SHEETNAME_OPTIONALS);

			// per ogni riga crea una entity
			Iterator<Row> itr = shOptionals.iterator();
			while (itr.hasNext()) {
				Row row = itr.next();
				if (row.getRowNum() > 0) {
					// dalla seconda riga in poi
					// System.out.println("Row=" + row.getRowNum());

					Boolean processRow = Boolean.TRUE;
					JVersionOptImport jVersionOptImport = new JVersionOptImport(); // il costruttore imposta campi a ""

					jVersionOptImport.setIdVersion(idVersion);

					String pRow = "ROW=[" + (row.getRowNum() + 1) + "]";
					jVersionOptImport.setExcelRow(pRow);

					Iterator<Cell> itrCell = row.cellIterator();
					while (itrCell.hasNext()) {

						Cell cell = itrCell.next();
						if (cell.getColumnIndex() == 0) {
							jVersionOptImport.setId((long) cell.getNumericCellValue());

						}

						// alla seconda cella effettuo controlli congruenza tra
						// id e operazione richiesta
						if (cell.getColumnIndex() == 1) {

							// System.out.println("Operation=" + cell.getStringCellValue());

							if (DocService.UPLOAD_ROW_NOCHANGE.equals(cell.getStringCellValue())) {
								// System.out.println("skip");
								logStep(logs, pRow + "-NOCHANGE: id=" + jVersionOptImport.getId());
								processRow = Boolean.FALSE;
								break;
							}

							jVersionOptImport.setOperation(cell.getStringCellValue());
							Long id = jVersionOptImport.getId();
							if (DocService.UPLOAD_ROW_INSERT.equals(jVersionOptImport.getOperation())) {
								if ((id != null) && (!id.equals(0L))) {

									logStep(logs, pRow + "-ABORTED - INSERT: l'id deve essere null");
									processRow = Boolean.FALSE;
									// throw new ValidationException(-1, "OPT_ID_NOT_NULL");
								}

							}
							if ((DocService.UPLOAD_ROW_DELETE.equals(jVersionOptImport.getOperation()))
									|| (DocService.UPLOAD_ROW_UPDATE.equals(jVersionOptImport.getOperation()))) {
								if (jVersionOptImport.getId() == null) {
									logStep(logs, pRow + "ABORTED - INSERT: l'id NON deve essere null");
									processRow = Boolean.FALSE;
									// throw new ValidationException(-1, "OPT_ID_IS_NULL");
								}
							}

						}

						if (cell.getColumnIndex() == 2) {
							jVersionOptImport.setCode(cell.getStringCellValue());
						}

						if (cell.getColumnIndex() == 3) {
							jVersionOptImport.setDescription_it(cell.getStringCellValue());
						}
						if (cell.getColumnIndex() == 4) {
							jVersionOptImport.setDescription_en(cell.getStringCellValue());
						}

						if (cell.getColumnIndex() == 5) { // subgroup
							String longDesc = cell.getStringCellValue();
							if (longDesc != null) {
								// estraggo quanto presente tra due parentesi quadre, una all'inizio ed un al
								// termine dell'id subgroup
								String idSubgroupString = longDesc.substring(longDesc.indexOf("[") + 1,
										longDesc.indexOf("]"));
								long idSubgroup = Long.parseLong(idSubgroupString);
								jVersionOptImport.setIdModelOptionalSubGroup(idSubgroup);
							} else {

							}

						}

						if (cell.getColumnIndex() == 6) {
							Long sequence = 0L;
							switch (cell.getCellType()) {
							case Cell.CELL_TYPE_NUMERIC:
								sequence = (long) cell.getNumericCellValue();
								break;
							case Cell.CELL_TYPE_STRING:

								// sequence = cell.getStringCellValue();
								break;
							default:

							}

							jVersionOptImport.setSequence(sequence);

						}
						if (cell.getColumnIndex() == 7) {
							jVersionOptImport.setVisible(cell.getStringCellValue());
						}

						if (cell.getColumnIndex() == 8) {
							jVersionOptImport.setOrderable_it(cell.getStringCellValue());
						}
						if (cell.getColumnIndex() == 9) {
							jVersionOptImport.setOrderable_en(cell.getStringCellValue());
						}

						if (cell.getColumnIndex() == 10) {
							String orderableFlag = null;

							switch (cell.getCellType()) {
							case Cell.CELL_TYPE_NUMERIC:
								orderableFlag = String.valueOf((int) cell.getNumericCellValue());
								break;
							case Cell.CELL_TYPE_STRING:
								orderableFlag = cell.getStringCellValue();
								break;
							default:

							}

							jVersionOptImport.setOrderableFlag(orderableFlag);

						}
						if (cell.getColumnIndex() == 11) {
							jVersionOptImport.setVersionModel_it(cell.getStringCellValue());
						}
						if (cell.getColumnIndex() == 12) {
							jVersionOptImport.setVersionModel_en(cell.getStringCellValue());
						}

						if (cell.getColumnIndex() == 13) {
							jVersionOptImport.setTextA1_it(cell.getStringCellValue());
						}
						if (cell.getColumnIndex() == 14) {
							jVersionOptImport.setTextA1_en(cell.getStringCellValue());
						}

						if (cell.getColumnIndex() == 15) {
							jVersionOptImport.setTextA5_it(cell.getStringCellValue());
						}
						if (cell.getColumnIndex() == 16) {
							jVersionOptImport.setTextA5_en(cell.getStringCellValue());
						}

						if (cell.getColumnIndex() == 17) {
							jVersionOptImport.setTextB1_it(cell.getStringCellValue());
						}
						if (cell.getColumnIndex() == 18) {
							jVersionOptImport.setTextB1_en(cell.getStringCellValue());
						}

						if (cell.getColumnIndex() == 19) {
							jVersionOptImport.setTextC0_it(cell.getStringCellValue());
						}
						if (cell.getColumnIndex() == 20) {
							jVersionOptImport.setTextC0_en(cell.getStringCellValue());
						}

						if (cell.getColumnIndex() == 21) {
							jVersionOptImport.setTextC2_it(cell.getStringCellValue());
						}
						if (cell.getColumnIndex() == 23) {
							jVersionOptImport.setTextC2_en(cell.getStringCellValue());
						}

						if (cell.getColumnIndex() == 23) {
							jVersionOptImport.setTextC4_it(cell.getStringCellValue());
						}
						if (cell.getColumnIndex() == 24) {
							jVersionOptImport.setTextC4_en(cell.getStringCellValue());
						}
					}

					if (processRow && jVersionOptImport.getOperation() != null) {
						// logStep(logs, jVersionOptImport.toString());
						// System.out.println(jVersionOptImport.toString());
						logStep(logs, pRow + "EXECUTE :" + jVersionOptImport.getOperation() + "-"
								+ jVersionOptImport.getCode());

						lJVersionOptImport.add(jVersionOptImport);
					}

				}

			}

			LocalizationType language = LocalizationType.valueOf(lang);
			logStep(logs, "STEP 1 END - Controlli globali ");

			logStep(logs, "STEP 2 BEGIN - Importazione ");
			uploadOptImport(lJVersionOptImport, language, idVersion, logs, apiResponseMessage);
			logStep(logs, "STEP 2 END - Importazione");

			wb.close();

		}
		logStep(logs, "FINISHED");

		JPage page = new JPage();
		((HashMap<String, Object>) page.getData().getActual()).put("logs", logs);
		((HashMap<String, Object>) page.getData().getActual()).put("apiResponseMessage", apiResponseMessage);

		return page;
	}

	private void uploadOptImport(List<JVersionOptImport> lJVersionOptImport, LocalizationType language, Long idVersion,
			List<String> logs, ApiResponseMessage apiResponseMessage) {

		for (JVersionOptImport jVersionOptImport : lJVersionOptImport) {

			Boolean processThis = Boolean.TRUE;

			String pRow = jVersionOptImport.getExcelRow();

			Long ret = validateOpt(jVersionOptImport, idVersion, jVersionOptImport.getOperation());
			if (ret.equals(-1L)) {
				logStep(logs,
						pRow + " Tentativo di inserire un Optional già esistente -" + jVersionOptImport.getCode());
				processThis = Boolean.FALSE;
			}

			else if (ret.equals(-2L)) {
				logStep(logs,
						pRow + " Tentativo di inserire un Optional già esistente -" + jVersionOptImport.getCode());
				processThis = Boolean.FALSE;

			}

			if (processThis) {

				VersionOpt versionOpt;
				if (DocService.UPLOAD_ROW_INSERT.equals(jVersionOptImport.getOperation())) {
					versionOpt = new VersionOpt();
					versionOpt.setId(null);
				} else {
					versionOpt = versionOptRepository.findOne(jVersionOptImport.getId());
					versionOpt.setId(jVersionOptImport.getId());
				}

				if (DocService.UPLOAD_ROW_DELETE.equals(jVersionOptImport.getOperation())) {
					versionOpt.setActive("N");
					// versionOpt.setCode(null);
					versionOptRepository.save(versionOpt);
					logStep(logs, pRow + " Operazione DELETE LOGICA conclusa con successo per "
							+ jVersionOptImport.getId() + "-" + jVersionOptImport.getCode());
				} else {
					//////////////////////////////////////////////

					if (jVersionOptImport.getDescription_it().isEmpty()
							&& jVersionOptImport.getDescription_en().isEmpty()) {
						logStep(logs, pRow + " Descrizione è obbligatoria");
						processThis = Boolean.FALSE;
					} else if (jVersionOptImport.getDescription_it().isEmpty()) {
						jVersionOptImport.setDescription_it(jVersionOptImport.getDescription_en());
					} else if (jVersionOptImport.getDescription_en().isEmpty()) {
						jVersionOptImport.setDescription_en(jVersionOptImport.getDescription_it());
					}
					if (jVersionOptImport.getOrderable_it().isEmpty()
							&& jVersionOptImport.getOrderable_en().isEmpty()) {
						logStep(logs, pRow + " Ordinabilità è un dato obbligatorio");
						processThis = Boolean.FALSE;
					} else if (jVersionOptImport.getOrderable_it().isEmpty()) {
						jVersionOptImport.setOrderable_it(jVersionOptImport.getOrderable_en());
					} else if (jVersionOptImport.getOrderable_en().isEmpty()) {
						jVersionOptImport.setOrderable_en(jVersionOptImport.getOrderable_it());
					}

					if (jVersionOptImport.getVersionModel_it().isEmpty()
							&& jVersionOptImport.getVersionModel_en().isEmpty()) {
						logStep(logs, pRow + " Version Model è un dato obbligatorio");
						processThis = Boolean.FALSE;
					} else if (jVersionOptImport.getVersionModel_it().isEmpty()) {
						jVersionOptImport.setVersionModel_it(jVersionOptImport.getVersionModel_en());
					} else if (jVersionOptImport.getVersionModel_en().isEmpty()) {
						jVersionOptImport.setVersionModel_en(jVersionOptImport.getVersionModel_it());
					}

					if (jVersionOptImport.getTextA1_it().isEmpty() && jVersionOptImport.getTextA1_en().isEmpty()) {
						// nop
					} else if (jVersionOptImport.getTextA1_it().isEmpty()) {
						jVersionOptImport.setTextA1_it(jVersionOptImport.getTextA1_en());
					} else if (jVersionOptImport.getTextA1_en().isEmpty()) {
						jVersionOptImport.setTextA1_en(jVersionOptImport.getTextA1_it());
					}

					if (jVersionOptImport.getTextA5_it().isEmpty() && jVersionOptImport.getTextA5_en().isEmpty()) {
						// nop
					} else if (jVersionOptImport.getTextA5_it().isEmpty()) {
						jVersionOptImport.setTextA5_it(jVersionOptImport.getTextA5_en());
					} else if (jVersionOptImport.getTextA5_en().isEmpty()) {
						jVersionOptImport.setTextA5_en(jVersionOptImport.getTextA5_it());
					}

					if (jVersionOptImport.getTextB1_it().isEmpty() && jVersionOptImport.getTextB1_en().isEmpty()) {
						// nop
					} else if (jVersionOptImport.getTextB1_it().isEmpty()) {
						jVersionOptImport.setTextB1_it(jVersionOptImport.getTextB1_en());
					} else if (jVersionOptImport.getTextA5_en().isEmpty()) {
						jVersionOptImport.setTextB1_en(jVersionOptImport.getTextB1_it());
					}

					if (jVersionOptImport.getTextC0_it().isEmpty() && jVersionOptImport.getTextC0_en().isEmpty()) {
						// nop
					} else if (jVersionOptImport.getTextC0_it().isEmpty()) {
						jVersionOptImport.setTextC0_it(jVersionOptImport.getTextC0_en());
					} else if (jVersionOptImport.getTextA5_en().isEmpty()) {
						jVersionOptImport.setTextC0_en(jVersionOptImport.getTextC0_it());
					}

					if (jVersionOptImport.getTextC2_it().isEmpty() && jVersionOptImport.getTextC2_en().isEmpty()) {
						// nop
					} else if (jVersionOptImport.getTextC2_it().isEmpty()) {
						jVersionOptImport.setTextC2_it(jVersionOptImport.getTextC2_en());
					} else if (jVersionOptImport.getTextA5_en().isEmpty()) {
						jVersionOptImport.setTextC2_en(jVersionOptImport.getTextC2_it());
					}

					if (jVersionOptImport.getTextC4_it().isEmpty() && jVersionOptImport.getTextC4_en().isEmpty()) {
						// nop
					} else if (jVersionOptImport.getTextC4_it().isEmpty()) {
						jVersionOptImport.setTextC4_it(jVersionOptImport.getTextC4_en());
					} else if (jVersionOptImport.getTextA5_en().isEmpty()) {
						jVersionOptImport.setTextC4_en(jVersionOptImport.getTextC4_it());
					}

					versionOpt.setIdVersion(jVersionOptImport.getIdVersion());
					versionOpt.setCode(jVersionOptImport.getCode());
					versionOpt.setDescription_it(jVersionOptImport.getDescription_it());
					versionOpt.setDescription_en(jVersionOptImport.getDescription_en());

					versionOpt.setIdModelOptionalSubGroup(jVersionOptImport.getIdModelOptionalSubGroup());

					versionOpt.setVisible(jVersionOptImport.getVisible());

					versionOpt.setOrderable_it(jVersionOptImport.getOrderable_it());
					versionOpt.setOrderable_en(jVersionOptImport.getOrderable_en());

					versionOpt.setOrderableFlag(jVersionOptImport.getOrderableFlag());

					if (jVersionOptImport.getIdModelOptionalSubGroup() == null) {
						logStep(logs, pRow + " Non è stato specificato il Sottogruppo OPT");
						processThis = Boolean.FALSE;
					}

					// determina sequence number se non impostato
					// se non c'è il gruppo opt: FAIL
					if ((jVersionOptImport.getSequence().equals(0L))
							&& (jVersionOptImport.getIdModelOptionalSubGroup() != null)) {
						jVersionOptImport.setSequence(versionOptRepository.getNextSequenceNumber(
								jVersionOptImport.getIdVersion(), jVersionOptImport.getIdModelOptionalSubGroup()));
					}
					versionOpt.setSequence(jVersionOptImport.getSequence());

					versionOpt.setVersionModel_it(jVersionOptImport.getVersionModel_it());
					versionOpt.setVersionModel_en(jVersionOptImport.getVersionModel_en());

					List<VersionOptText> lVersionOptText = new ArrayList<VersionOptText>();
					lVersionOptText = versionOpt.getlVersionOptText();

					setText(jVersionOptImport.getTextA1_it(), "A1", lVersionOptText, LocalizationType.IT_IT,
							versionOpt);
					setText(jVersionOptImport.getTextA5_it(), "A5", lVersionOptText, LocalizationType.IT_IT,
							versionOpt);
					setText(jVersionOptImport.getTextB1_it(), "B1", lVersionOptText, LocalizationType.IT_IT,
							versionOpt);
					setText(jVersionOptImport.getTextC0_it(), "C0", lVersionOptText, LocalizationType.IT_IT,
							versionOpt);
					setText(jVersionOptImport.getTextC2_it(), "C2", lVersionOptText, LocalizationType.IT_IT,
							versionOpt);
					setText(jVersionOptImport.getTextC4_it(), "C4", lVersionOptText, LocalizationType.IT_IT,
							versionOpt);

					setText(jVersionOptImport.getTextA1_en(), "A1", lVersionOptText, LocalizationType.EN_US,
							versionOpt);
					setText(jVersionOptImport.getTextA5_en(), "A5", lVersionOptText, LocalizationType.EN_US,
							versionOpt);
					setText(jVersionOptImport.getTextB1_en(), "B1", lVersionOptText, LocalizationType.EN_US,
							versionOpt);
					setText(jVersionOptImport.getTextC0_en(), "C0", lVersionOptText, LocalizationType.EN_US,
							versionOpt);
					setText(jVersionOptImport.getTextC2_en(), "C2", lVersionOptText, LocalizationType.EN_US,
							versionOpt);
					setText(jVersionOptImport.getTextC4_en(), "C4", lVersionOptText, LocalizationType.EN_US,
							versionOpt);

					versionOpt.setlVersionOptText(lVersionOptText);

					if (processThis) {
						versionOptRepository.save(versionOpt);
						logStep(logs, pRow + " Operazione conclusa con successo per " + jVersionOptImport.getId() + "-"
								+ jVersionOptImport.getCode());
					} else {
						logStep(logs, pRow + " Riga non importata " + jVersionOptImport.getId() + "-"
								+ jVersionOptImport.getCode());

						apiResponseMessage.setCode(-999);
						apiResponseMessage.setMessage("SOME_ERRORS_FOUND");
					}
					//////////////////////////////////////////////
				}
			}

		}
	}

	private void setText(String text, String type, List<VersionOptText> lVersionOptText, LocalizationType language,
			VersionOpt versionOpt) {
		Boolean found = Boolean.FALSE;
		for (VersionOptText versionOptText : lVersionOptText) {
			if (versionOptText.getTextType().equals(type)) {

				versionOptText.setDescription(text, language.getValue());
				found = Boolean.TRUE;
			}
		}
		if (!found) {
			if (text != null && text.trim().length() > 0) {
				// in inserimento solo se immesso testo
				VersionOptText versionOptText = new VersionOptText();
				versionOptText.setTextType(type);
				versionOptText.setDescription(text, language.getValue());
				versionOptText.setVersionOpt(versionOpt);
				lVersionOptText.add(versionOptText);

			}
		}
	}

	private JVersionAttc convertFromJEntityJVersionAttc(HashMap<String, Object> dto)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		JVersionAttc jVersionAttc = objectMapper.readValue(objectMapper.writeValueAsString(dto), JVersionAttc.class);
		return jVersionAttc;
	}

	private JVersionOptFull convertFromJEntityJVersionOptFull(HashMap<String, Object> dto)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		JVersionOptFull jVersionOptFull = objectMapper.readValue(objectMapper.writeValueAsString(dto),
				JVersionOptFull.class);
		return jVersionOptFull;
	}

	@SuppressWarnings("unused")
	private JVersionOptPics getTemplateOptPics() {
		JVersionOptPics jVersionOptPics = new JVersionOptPics();
		jVersionOptPics.setId(null);
		jVersionOptPics.setIdImage(null);
		jVersionOptPics.setIdImageLow(null);
		jVersionOptPics.setUrlImage(null);
		jVersionOptPics.setUrlImageLow(null);
		jVersionOptPics.setPicType(null);
		jVersionOptPics.setSequence(null);
		jVersionOptPics.setDescription(null);
		jVersionOptPics.setDescription_en(null);
		jVersionOptPics.setDescription_it(null);

		jVersionOptPics.setMediaTemplateImageHiRes(MEDIA_OPTIONAL_TEMPLATE_HIRES);
		jVersionOptPics.setMediaTemplateImageLoRes(MEDIA_OPTIONAL_TEMPLATE_LORES);
		return jVersionOptPics;
	};

	private void logStep(List<String> logs, String format, Object... msg) {
		try {
			if (logs != null) {
				logs.add(String.format(format, msg));
			}
		} catch (Exception ignore) {

		}

	}

	public String getConfidentialStatus(Model model) {
		if ("N".equals(model.getVisibility()))
			return CONFIDENTIAL;
		return null;
	}

	public String getConfidentialStatus(Version version) {
		if ((version != null) && ("D".contentEquals(version.getStatus())))
			return CONFIDENTIAL;

		return null;
	}
//	public String getConfidentialStatus(Model model, Version version) {
//		if ("N".equals(model.getVisibility()))
//			return CONFIDENTIAL;
//		if ((version!=null) && ("D".contentEquals(version.getStatus())))
//			return CONFIDENTIAL;
//		
//		return null;
//	}	

}
