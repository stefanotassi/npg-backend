package com.ferrari.modis.dpg.service;

import java.util.List;

import com.ferrari.modis.dpg.jentity.base.JPageAuth;

public interface UiService {
	
	public final static String PAGE_HOMEPAGE 			= "HP";
	public final static String PAGE_MODEL_MAINTENANCE 	= "MO";
	public final static String PAGE_SPLASH			 	= "SP";
	public final static String PAGE_UPLOAD 				= "UP";
	public final static String PAGE_FORM 				= "FO";	
	public final static String PAGE_LIST_OPT			= "LO";	
	public final static String PAGE_DETAIL_OPT			= "OP";		
	public final static String PAGE_DOC_FIGURINI		= "DF";
	public final static String PAGE_DOC_OTHERS			= "DO";
	public final static String PAGE_VERSIONS			= "V2"; // autorizzazioni concesse sul form versioni
	public final static String PAGE_VERSIONS_VERSION	= "VE"; // autorizzazioni concesse sulla singola versione
	public final static String PAGE_PDF					= "PD"; // autorizzazioni per costruzione PDF
	
	public final static String PAGE_MINIDOC				= "MX";

	
	public final static String AUTH_VERSION_CLONE		= "VE-CL";
	public final static String AUTH_VERSION_DELETE		= "VE-DE";
	public final static String AUTH_VERSION_PUBLISH		= "VE-PU";
	public final static String AUTH_VERSION_UPDATE		= "VE-UP";
	
	
	public final static String AUTH_PREFIX_FOR_TEXT				= "SZ-";	
	public final static String AUTH_PREFIX_FOR_VERSION_STATUS 	="STS-";
	
	
	public final static String UPLOAD					= "UPLOA"; // autorizzazioni per per l'upload dei file
	
	public List<JPageAuth> getAuthorizationList(String pageId);
	public String getAuthorizationById(String authId);
	public String getAuthorizationByText(String textType);
	public String getAuthorizationByVersionStatus(String status);
	
	
}
