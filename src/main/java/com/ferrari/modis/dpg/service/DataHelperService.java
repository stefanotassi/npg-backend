package com.ferrari.modis.dpg.service;

import java.util.List;

import com.ferrari.modis.dpg.commons.i18n.LocalizationType;
import com.ferrari.modis.dpg.jentity.base.JPage;
import com.ferrari.modis.dpg.jentity.base.JValueText;



public interface DataHelperService {

	List<JValueText> getValueTextList(LocalizationType language, String idContext);
	JPage getPermissionList(LocalizationType language, String pageId);

	List<JValueText> getOptionalsList(LocalizationType language);

}
