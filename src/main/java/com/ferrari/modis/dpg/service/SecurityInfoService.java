package com.ferrari.modis.dpg.service;


import java.util.HashMap;

import com.ferrari.modis.portal.ws.ProfileXML;



public interface SecurityInfoService {

	final public static String SS_USER_PROFILE="user_profile";
	final public static String SS_USER_NAME = "user_name";
	final public static String SS_USER_SIGNATURE= "user_signature";
	final public static String SS_OAM_COOKIE = "OAMAuthnCookie";


	
	ProfileXML getProfile();
	ProfileXML getProfile(String username);
	void saveProfile(String username,ProfileXML profile);
	HashMap<String,Object> getApplicationInfo(String version, String buildTime);

}


 