package com.ferrari.modis.dpg.service.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMethod;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ferrari.modis.dpg.commons.i18n.LocalizationType;
import com.ferrari.modis.dpg.jentity.app.JMiniDocument;
import com.ferrari.modis.dpg.jentity.base.JEntity;
import com.ferrari.modis.dpg.jentity.base.JPage;
import com.ferrari.modis.dpg.jentity.base.JPageAuth;
import com.ferrari.modis.dpg.jentity.base.JPageData;
import com.ferrari.modis.dpg.model.app.MiniDocument;
import com.ferrari.modis.dpg.model.ui.UiPage;
import com.ferrari.modis.dpg.repository.MiniDocumentRepository;
import com.ferrari.modis.dpg.repository.UiPageRepository;
import com.ferrari.modis.dpg.service.MediaService;
import com.ferrari.modis.dpg.service.MiniDocumentService;
import com.ferrari.modis.dpg.service.SecurityInfoService;
import com.ferrari.modis.dpg.service.UiService;

import io.swagger.api.ApiResponseMessage;
import io.swagger.api.NotFoundException;
import io.swagger.api.ValidationException;

@Service
public class MiniDocumentServiceImpl implements MiniDocumentService {

	@SuppressWarnings("unused")
	private static final Logger logger = LoggerFactory.getLogger(MiniDocumentServiceImpl.class);

	@Autowired
	private SecurityInfoService securityInfoService;
	@Autowired
	private MediaService mediaService;
	@Autowired
	private UiService uiService;
	
	@Autowired
	private MiniDocumentRepository miniDocumentRepository;
	@Autowired
	private UiPageRepository uiPageRepository;
	
	@Override
	public JPage getMiniDocuments(LocalizationType language) {
		
		
		if (language == null) {
			String d = securityInfoService.getProfile().getUserInfo().getLanguage();
			if (("it-IT").equals(d))
				language = LocalizationType.IT_IT;
			else
				language = LocalizationType.EN_US;
		}

		JPage jpage = new JPage();
		JPageData jPageData = new JPageData();

		Boolean enableAllDocs = ("Y".equals(uiService.getAuthorizationById(MINIDOCUMENT_READ_ALL)));
		
		List<JMiniDocument> lJMiniDocument = new ArrayList<JMiniDocument>();
		for (MiniDocument miniDocument : miniDocumentRepository.findAll()) {
			
			JMiniDocument jMiniDocument = new JMiniDocument();
			jMiniDocument.setId(miniDocument.getId());
			jMiniDocument.setSeq(miniDocument.getSequence());
			jMiniDocument.setDescription(miniDocument.getDescription(language.getValue()));
			jMiniDocument.setVisibile(miniDocument.getVisible());
			jMiniDocument.setIdDocument(miniDocument.getIdDocument());
			jMiniDocument.setUrlDocument(mediaService.getThronUrl(miniDocument.getIdDocument(), TEMPLATE_MINIDOC));
			
			// visibilità di un documento tramite permission
			if (enableAllDocs || "Y".equals(miniDocument.getVisible()));
				lJMiniDocument.add(jMiniDocument);
		}
		

		

		



		jPageData.setActual(lJMiniDocument);

		// header JPage ------------------------------------------------------------
		UiPage uiPage = uiPageRepository.findByKey(UiService.PAGE_MINIDOC);
		jpage.setId(uiPage.getId());
		jpage.setName(uiPage.getDescription());
		jpage.setLanguage(language);

		// aggiunge lista autorizzazioni -------------------------------------------
		List<JPageAuth> lJPageAuth = uiService.getAuthorizationList(UiService.PAGE_MINIDOC);
		jpage.setAuths(lJPageAuth);

		// chiusura form
		jpage.setData(jPageData);

		return jpage;
		
		


	}

	@Override
	public JPage getMiniDocument(LocalizationType language, Long idDocument) {


		JPage jpage = new JPage();

		MiniDocument miniDocument = miniDocumentRepository.findOne(idDocument);
		
		Boolean enableAllDocs = ("Y".equals(uiService.getAuthorizationById(MINIDOCUMENT_READ_ALL)));
		
		if (enableAllDocs || "Y".equals(miniDocument.getVisible())) {
			// ok
		} else {
			throw new ValidationException(-1, "DOCUMENT_NOT_ALLOWED");
		}

		
		
		JMiniDocument jMiniDocument = new JMiniDocument();
		jMiniDocument.setId(miniDocument.getId());
		jMiniDocument.setSeq(miniDocument.getSequence());
		jMiniDocument.setDescription(miniDocument.getDescription(language.getValue()));
		
		jMiniDocument.setDescription_it(miniDocument.getDescription_it());
		jMiniDocument.setDescription_en(miniDocument.getDescription_en());
		
		jMiniDocument.setVisibile(miniDocument.getVisible());
		jMiniDocument.setIdDocument(miniDocument.getIdDocument());
		jMiniDocument.setUrlDocument(mediaService.getThronUrl(miniDocument.getIdDocument(), TEMPLATE_MINIDOC));



		JPageData jPageData = new JPageData();
		jPageData.setActual(jMiniDocument);

		// header JPage ------------------------------------------------------------
		UiPage uiPage = uiPageRepository.findByKey(UiService.PAGE_MINIDOC);
		jpage.setId(uiPage.getId());
		jpage.setName(uiPage.getDescription());
		jpage.setLanguage(language);

		// aggiunge lista autorizzazioni -------------------------------------------
		List<JPageAuth> lJPageAuth = uiService.getAuthorizationList(UiService.PAGE_MINIDOC);
		jpage.setAuths(lJPageAuth);

		jpage.setData(jPageData);

		return jpage;

	}

	
	
	
		
	
	
	
	@Override
	
	public JPage postMiniDocument(JEntity jEntity, RequestMethod requestMethod) throws NotFoundException {

		if (!"Y".equals(uiService.getAuthorizationById(MINIDOCUMENT_CREATE_ENABLE)))
			throw new ValidationException(-1, "USER_PERMISSION_MINIDOCUMENT_CREATE");

		JMiniDocument jMiniDocument = null;
		try {
			jMiniDocument = convertFromJEntity(jEntity.getData().getActual());
		} catch (IOException e) {
			throw new NotFoundException(-1, e.getMessage());
		}
		validateMiniDocument(jMiniDocument, requestMethod);

		Long idDocument = saveMiniDocument(jEntity.getLanguage(), jMiniDocument, requestMethod);

		return getMiniDocument(jEntity.getLanguage(),  idDocument);

	}
	
	
	
	public JPage putMiniDocument(JEntity jEntity, LocalizationType language,  Long idDocument,
			RequestMethod requestMethod) throws NotFoundException {

		if (!"Y".equals(uiService.getAuthorizationById(MINIDOCUMENT_UPDATE_ENABLE)))
			throw new ValidationException(-1, "USER_PERMISSION_MINIDOCUMENT_UPDATE");

		JMiniDocument jMiniDocument = null;
		try {
			jMiniDocument = convertFromJEntity(jEntity.getData().getActual());
		} catch (IOException e) {
			throw new NotFoundException(-1, e.getMessage());
		}
		validateMiniDocument(jMiniDocument, requestMethod);

		saveMiniDocument(jEntity.getLanguage(), jMiniDocument, requestMethod);
		return getMiniDocument(language,  idDocument);

	}
	
	public ApiResponseMessage deleteMiniDocument(Long idDocument) throws NotFoundException {
//		CheckModel(idModel);
//
		String auth = uiService.getAuthorizationById(MINIDOCUMENT_DELETE_ENABLE);
		if (!"Y".equals(auth)) {
			throw new ValidationException(-1, "USER_PERMISSION_MINIDOCUMENT_DELETE"); // AVAILABLE
		}

		MiniDocument miniDocument = miniDocumentRepository.findOne(idDocument);
		miniDocument.setActive("N");
		miniDocumentRepository.save(miniDocument);

		return ApiResponseMessage.SUCCESS;
	}
	
	
	public JPage getNewMiniDocumentForm(LocalizationType language) {


		JPage jpage = new JPage();

		JMiniDocument jMiniDocument = new JMiniDocument();
		/////////////////////////////////////////////////////////

		jMiniDocument.setId(null);
		jMiniDocument.setSeq(null);
		jMiniDocument.setDescription(null);
		jMiniDocument.setVisibile("Y");
		jMiniDocument.setIdDocument(null);
		jMiniDocument.setUrlDocument(null);
		jMiniDocument.setMediaTemplateDocument(TEMPLATE_MINIDOC);

		/////////////////////////////////////////////////////////
		JPageData jPageData = new JPageData();
		jPageData.setActual(jMiniDocument);

		// header JPage ------------------------------------------------------------
		UiPage uiPage = uiPageRepository.findByKey(UiService.PAGE_MODEL_MAINTENANCE);
		jpage.setId(uiPage.getId());
		jpage.setName(uiPage.getDescription());
		jpage.setLanguage(language);

		// aggiunge lista autorizzazioni -------------------------------------------
		List<JPageAuth> lJPageAuth = uiService.getAuthorizationList(UiService.PAGE_MODEL_MAINTENANCE);
		jpage.setAuths(lJPageAuth);

		jpage.setData(jPageData);

		return jpage;
	}	
	
	
	
	private void validateMiniDocument(JMiniDocument jMiniDocument, RequestMethod requestMethod) {
//
//		if (requestMethod.equals(RequestMethod.POST)) {
//			CheckOptionalType(jModelFull.getIdOptionalType());
//
//		} else if (requestMethod.equals(RequestMethod.PUT)) {
//			CheckOptionalType(jModelFull.getIdOptionalType());
//			CheckModel(jModelFull.getId());
//			CheckModelAndOptType(jModelFull.getId(), jModelFull.getIdOptionalType());
//			CheckGroups(jModelFull);
//
//		} else
//			throw new ValidationException(-1, "ACTION_NOT_ALLOWEED");

	}

	
	
	
	private Long saveMiniDocument(LocalizationType language, JMiniDocument jMiniDocument, RequestMethod requestMethod) {

		MiniDocument miniDocument = null;

		if (requestMethod.equals(RequestMethod.PUT)) {
			// update
			miniDocument = miniDocumentRepository.findOne(jMiniDocument.getId());
			if (miniDocument == null) {
				throw new ValidationException(-1, "DOCUMENT_NOT_FOUND");
			}
			miniDocument.setId(jMiniDocument.getId());
		}

		if (requestMethod.equals(RequestMethod.POST)) {
			// create
			miniDocument = new MiniDocument();
			miniDocument.setId(null);

		}

		miniDocument.setId(jMiniDocument.getId());
		

		miniDocument.setDescription_it(jMiniDocument.getDescription_it());
		miniDocument.setDescription_en(jMiniDocument.getDescription_en());

		miniDocument.setSequence(jMiniDocument.getSeq());
		miniDocument.setIdDocument(jMiniDocument.getIdDocument());
		miniDocument.setVisible(jMiniDocument.getVisibile());

		miniDocumentRepository.saveAndFlush(miniDocument);
		return miniDocument.getId();

	}



	private JMiniDocument convertFromJEntity(HashMap<String, Object> dto)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		JMiniDocument jMiniDocument = objectMapper.readValue(objectMapper.writeValueAsString(dto), JMiniDocument.class);
		return jMiniDocument;
	}
	
	
}
