package com.ferrari.modis.dpg.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ferrari.modis.dpg.commons.i18n.LocalizationType;
import com.ferrari.modis.dpg.jentity.base.JPage;
import com.ferrari.modis.dpg.jentity.base.JPageData;
import com.ferrari.modis.dpg.model.app.VersionSearchV2;
import com.ferrari.modis.dpg.repository.VersionSearchRepository;
import com.ferrari.modis.dpg.repository.VersionSearchV2Repository;
import com.ferrari.modis.dpg.service.DocService;
import com.ferrari.modis.dpg.service.SearchService;
import com.ferrari.modis.dpg.service.UiService;



@Service
public class SearchServiceImpl implements SearchService{
	
	@SuppressWarnings("unused")
	private static final Logger logger = LoggerFactory.getLogger(SearchServiceImpl.class);
	
	@Autowired
	VersionSearchRepository versionSearchRepository;
	@Autowired
	VersionSearchV2Repository versionSearchV2Repository;
	
	@Autowired
	UiService uiService;

	@Override
	public JPage getListByVersion(LocalizationType language , String optionalType , Long idVersion, String searchText) {
	 	JPage jpage = new JPage();
		JPageData jPageData = new JPageData();

		List<String> pubRows=getPermissionsTexts();
		String optlVisibleAll= getOptlVisibleAll();
		
		
		
		//Long idVersion=idVersion;
		String versionAll="N";
		Long idModel=0L;
		String modelAll="Y";
		List<String> allowedStatus = getAllowedStatus();
		String lang = language.getValue();
		//String optionalType =optionalType;
		String optionalTypeAll ="N";
		String modelVisibleAll=getModelVisibleAll();
	
		//List<VersionSearch> lVersionSearch = versionSearchRepository.findBytext(idVersion, language.getValue(), pubRows, optlVisibleAll, searchText.toLowerCase());
		List<VersionSearchV2> lVersionSearchV2 = versionSearchV2Repository.findBytext(
					  idVersion
					, versionAll
					, idModel
					, modelAll
					, allowedStatus
					, lang
					, pubRows
					, optlVisibleAll
					, searchText.toLowerCase()
					, optionalType
					, optionalTypeAll
					, modelVisibleAll
					);
		jPageData.setActual(lVersionSearchV2);
		jpage.setData(jPageData);
		return jpage;
		
		
	}

	

	
	
	
	@Override
	public JPage getListFull(LocalizationType language, String searchText) {
	 	JPage jpage = new JPage();
		JPageData jPageData = new JPageData();
		
		List<String> pubRows=getPermissionsTexts();
		String optlVisibleAll= getOptlVisibleAll();	
		
		Long idVersion=0L;
		String versionAll="Y";
		Long idModel=0L;
		String modelAll="Y";
		List<String> allowedStatus = getAllowedStatus();
		String lang = language.getValue();
		String optionalType ="";
		String optionalTypeAll ="Y";
		String modelVisibleAll=getModelVisibleAll();
		
		List<VersionSearchV2> lVersionSearchV2 = versionSearchV2Repository.findBytext(
					  idVersion
					, versionAll
					, idModel
					, modelAll
					, allowedStatus
					, lang
					, pubRows
					, optlVisibleAll
					, searchText.toLowerCase()
					, optionalType
					, optionalTypeAll
					, modelVisibleAll
					);
		jPageData.setActual(lVersionSearchV2);
		jpage.setData(jPageData);
		return jpage;

	}

	@Override
	public JPage getListByModel(LocalizationType language, Long idModel, String searchText) {
		// chiamata da splash page che contiene P1 e P2, cerca in entrambi i tipi
		
	 	JPage jpage = new JPage();
		JPageData jPageData = new JPageData();
		
		List<String> pubRows=getPermissionsTexts();
		String optlVisibleAll= getOptlVisibleAll();	
		
		Long idVersion=0L;
		String versionAll="Y";
		// Long idModel=0;
		String modelAll="N";
		List<String> allowedStatus = getAllowedStatus();
		String lang = language.getValue();
		String optionalType ="";
		String optionalTypeAll ="Y";
		String modelVisibleAll=getModelVisibleAll();
		
		List<VersionSearchV2> lVersionSearchV2 = versionSearchV2Repository.findBytext(
					  idVersion
					, versionAll
					, idModel
					, modelAll
					, allowedStatus
					, lang
					, pubRows
					, optlVisibleAll
					, searchText.toLowerCase()
					, optionalType
					, optionalTypeAll
					, modelVisibleAll
					);
		jPageData.setActual(lVersionSearchV2);
		jpage.setData(jPageData);
		return jpage;
	}

	
	
	
	
	private List<String> getPermissionsTexts() {
		List<String> pubRows = new ArrayList<String>();
		pubRows.add("Y");
		// calcola livello utente per vedere o meno le sezioni riservate
		if (	(!"Y".equals(uiService.getAuthorizationByText(DocService.TEXT_C0_NOTE_INTERNE)))||
				(!"Y".equals(uiService.getAuthorizationByText(DocService.TEXT_C2_STATUS_SVILUPPO)))||
				(!"Y".equals(uiService.getAuthorizationByText(DocService.TEXT_C4_CAPACITY)))) {

		} else {
			pubRows.add("N");
		}
		return pubRows;
	}

	private List<String> getAllowedStatus(){
		List<String> allowedStatus = new ArrayList<String>();
		
		if (("Y").equals(uiService.getAuthorizationById("STS-C"))) 
			allowedStatus.add("C");
		if (("Y").equals(uiService.getAuthorizationById("STS-D"))) 
			allowedStatus.add("D");
		if (("Y").equals(uiService.getAuthorizationById("STS-K"))) 
			allowedStatus.add("K");
		if (("Y").equals(uiService.getAuthorizationById("STS-S"))) 
			allowedStatus.add("S");
		return allowedStatus;
	}
	
	private String getOptlVisibleAll() {
		String optlVisibleAll="N";
		if (("Y").equals(uiService.getAuthorizationById("OP-AC"))) {
			optlVisibleAll="Y";
		}
		return optlVisibleAll;
	}
	
	
	private String getModelVisibleAll() {
		String modelVisibleAll="N";
		if (("Y").equals(uiService.getAuthorizationById("MO-VI"))) {
			modelVisibleAll="Y";
		}
		return modelVisibleAll;
	}
}
