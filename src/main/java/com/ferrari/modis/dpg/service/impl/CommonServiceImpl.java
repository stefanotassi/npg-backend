package com.ferrari.modis.dpg.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ferrari.modis.dpg.commons.i18n.LocalizationType;
import com.ferrari.modis.dpg.model.app.Model;
import com.ferrari.modis.dpg.model.app.ModelOptionalType;
import com.ferrari.modis.dpg.model.app.Version;
import com.ferrari.modis.dpg.model.app.VersionAttc;
import com.ferrari.modis.dpg.repository.ModelOptionalTypeRepository;
import com.ferrari.modis.dpg.repository.ModelRepository;
import com.ferrari.modis.dpg.repository.VersionAttcRepository;
import com.ferrari.modis.dpg.repository.VersionRepository;
import com.ferrari.modis.dpg.service.CommonService;
import com.ferrari.modis.dpg.service.SecurityInfoService;

import io.swagger.api.ValidationException;

@Service
public class CommonServiceImpl implements CommonService {

	@Autowired
	private SecurityInfoService securityInfoService;
	
	@Autowired
	VersionAttcRepository versionAttcRepository;

	@Autowired
	VersionRepository versionRepository;

	@Autowired
	ModelOptionalTypeRepository modelOptionalTypeRepository;
	
	@Autowired
	ModelRepository modelRepository;


	public LocalizationType getLanguage(String hlang) {
		// da header http

		LocalizationType language = null;
		if (hlang == null) {

			if (securityInfoService.getProfile() == null) {
				System.err.println("Dev environment - assumed IT_IT");
				language = LocalizationType.IT_IT;
			} else {
				String d = securityInfoService.getProfile().getUserInfo().getLanguage();
				if (("it-IT").equals(d))
					language = LocalizationType.IT_IT;
				else
					language = LocalizationType.EN_US;
			}

		} else {

			try {
				language = LocalizationType.valueOf(hlang);
			} catch (IllegalArgumentException e) {

				throw new ValidationException(-1, "LANGUAGE_NOT_ALLOWED");
			}
		}
		return language;

	}
	
	
	

	@Override
	public Model getModelFromVersionAttcId(Long versionAttcId) {
		return getModelFromVersionAttc(versionAttcRepository.findOne(versionAttcId));
	}

	@Override
	public Model getModelFromVersionAttc(VersionAttc versionAttc) {
		if (versionAttc == null)
			return null;
		return getModelFromVersion(versionAttc.getVersion());
	}

	@Override
	public Model getModelFromVersionId(Long versionId) {
		return getModelFromVersion(versionRepository.findOne(versionId));
	}

	@Override
	public Model getModelFromVersion(Version version) {
		if (version == null)
			return null;

		return getModelFromModelOptionalTypeId(version.getIdModelOptionalType());

	}

	@Override
	public Model getModelFromModelOptionalTypeId(Long modelOptionalTypeId) {
		return getModelFromModelOptionalType(modelOptionalTypeRepository.findOne(modelOptionalTypeId));
	}

	@Override
	public Model getModelFromModelOptionalType(ModelOptionalType modelOptionalType) {
		if (modelOptionalType == null)
			return null;

		return  modelRepository.findById(modelOptionalType.getIdModel());

	}


}
