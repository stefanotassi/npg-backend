package com.ferrari.modis.dpg.service;

import java.util.List;

import com.ferrari.modis.dpg.commons.i18n.LocalizationType;
import com.ferrari.modis.dpg.jentity.app.JVersion;
import com.ferrari.modis.dpg.jentity.base.JEntity;
import com.ferrari.modis.dpg.jentity.base.JPage;
import com.ferrari.modis.dpg.model.app.Version;

public interface VersionService {
	
	public final static String STATUS_DRAFT 			= "D";
	public final static String STATUS_PUBLISHED 		= "C";	
	public final static String STATUS_DELETED 			= "K";
	public final static String STATUS_SUPERSEDED 		= "S";	
	
	
	
	
	public void createDefaultVersion(Long idModelOptionalType,Boolean forceCreation);
	public Version getDefaultVersion(Long idModelOptionalType);
	public List<JVersion> getVersions(LocalizationType language, Long idModelOptionalType);

	public Boolean isAccessibileByStatus(Long idVersion);
	public Boolean isReadOnly(Long idVersion);
	
	public JPage getVersion(LocalizationType language , Long idVersion);
	public JPage deleteVersion(LocalizationType language , Long idVersion);	
	public JPage updateVersion(LocalizationType language , Long idVersion, JEntity jEntity);		
	public JPage publishVersion(LocalizationType language ,Long idVersion, JEntity jEntity);
	public JPage cloneVersion(Long idVersion, JEntity jEntity);
	
	public JVersion buildFromVersion(LocalizationType language, Version version );
	
	public Version versionCheck(Long idVersion);
	public void versionCheckForUpdate(Version version);

}
