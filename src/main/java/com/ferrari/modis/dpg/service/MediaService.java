package com.ferrari.modis.dpg.service;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.multipart.MultipartFile;

import com.ferrari.modis.dpg.commons.i18n.LocalizationType;
import com.ferrari.modis.dpg.jentity.app.JMedia;
import com.ferrari.modis.dpg.jentity.base.JEntity;
import com.ferrari.modis.dpg.jentity.base.JPage;
import com.ferrari.modis.dpg.model.app.Media;

public interface MediaService {

	
	public Media findMediaFile(String id); 
	
	public  JPage createMediaFile(JEntity jEntity, MultipartFile uploadFile, HttpServletRequest req) throws IOException, NoSuchAlgorithmException ;
	
	public JPage getMediaParameters(LocalizationType language, String mediaType);
	
	public JMedia getMediaTemplate();
	
	public String getThronUrl(String id, String template);
	
	public void downloadBatch(List<String> docs , HttpServletResponse response);
	
	public void downlaodSingle(String id , HttpServletResponse response) throws IOException;

	public String getThronUrlJpegForPrd(String id, String template);
	
}
