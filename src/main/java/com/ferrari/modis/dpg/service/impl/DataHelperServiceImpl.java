package com.ferrari.modis.dpg.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ferrari.modis.dpg.commons.i18n.LocalizationHelper;
import com.ferrari.modis.dpg.commons.i18n.LocalizationType;
import com.ferrari.modis.dpg.jentity.base.JPage;
import com.ferrari.modis.dpg.jentity.base.JPageAuth;
import com.ferrari.modis.dpg.jentity.base.JPageData;
import com.ferrari.modis.dpg.jentity.base.JValueText;
import com.ferrari.modis.dpg.jentity.base.JValueTextCompareByDescription;
import com.ferrari.modis.dpg.model.app.OptionalErp;
import com.ferrari.modis.dpg.model.ui.UiDictionary;
import com.ferrari.modis.dpg.model.ui.UiPage;
import com.ferrari.modis.dpg.repository.OptionalErpRepository;
import com.ferrari.modis.dpg.repository.UiDictionaryRepository;
import com.ferrari.modis.dpg.repository.UiPageRepository;
import com.ferrari.modis.dpg.service.impl.DataHelperServiceImpl;
import com.ferrari.modis.dpg.service.DataHelperService;
import com.ferrari.modis.dpg.service.UiService;

@Service
public class DataHelperServiceImpl implements DataHelperService {
	@SuppressWarnings("unused")
	private static final Logger logger = LoggerFactory.getLogger(DataHelperServiceImpl.class);

	@Autowired
	UiDictionaryRepository uiDictionaryRepository;
	@Autowired
	UiPageRepository uiPageRepository;
	@Autowired
	UiService uiService;
	@Autowired
	OptionalErpRepository optionalErpRepository;
	
	@Override
	public List<JValueText> getValueTextList(LocalizationType language, String idContext ) {
		List<JValueText> lJValueText = new ArrayList<JValueText>();

		for (UiDictionary uiDictionary :  uiDictionaryRepository.findByContext(idContext)) {
			lJValueText.add(new JValueText(uiDictionary.getIdKey()
								, LocalizationHelper.getLocalized(uiDictionary.getDescriptions(),language )
								));
			
		}

		// lista ordinata per descrizione 3.7.2019
		//if (idContext.equals("273") || idContext.equals("274") || idContext.equals("278")) {
			Collections.sort(lJValueText, new JValueTextCompareByDescription());
		//}

		return lJValueText;
	}
	
	
	
	
	

	@Override
	public JPage getPermissionList(LocalizationType language, String pageId) {
		// elenca le autorizzazione per un id_pagina
		// ora usato solo per HH = header
		
		
		JPage jpage = new JPage();
		JPageData jPageData = new JPageData();	
		
		jPageData.setActual(null);
		// header JPage ------------------------------------------------------------
		UiPage uiPage = uiPageRepository.findByKey(pageId);
		jpage.setId(uiPage.getId());
		jpage.setName(uiPage.getDescription());
		jpage.setLanguage(language);
		
		List<JPageAuth> lJPageAuth = uiService.getAuthorizationList(pageId );
		jpage.setAuths(lJPageAuth);

		
		
		
		jpage.setData(jPageData);

		return jpage;

	}






	@Override
	public List<JValueText> getOptionalsList(LocalizationType language) {

		List<JValueText> lJValueText = new ArrayList<JValueText>();

		for (OptionalErp optionalErp :  optionalErpRepository.findAll()) {
			lJValueText.add(new JValueText(optionalErp.getId()
								, optionalErp.getDescription(language.getValue())
								));
			
		}

		return lJValueText;
		
		
	}

}
