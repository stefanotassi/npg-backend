package com.ferrari.modis.dpg.service.impl;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ferrari.modis.dpg.repository.ext.ExtJpaRepo;
import com.ferrari.modis.dpg.service.AdminService;

@Service
@SuppressWarnings({"rawtypes","unchecked"})
public class AdminServiceImpl implements AdminService {

	@Autowired
	private List<ExtJpaRepo> crudService;
	
	private Map<Class<?>, ExtJpaRepo<?,?>> mapService = new HashMap<Class<?>, ExtJpaRepo<?,?>>();
	
	@PostConstruct
	public void init() {
		for  ( ExtJpaRepo i :crudService) {
			mapService.put(i.getDomainClass(), i);
		}	
	}
	
	
	@Override
	public <T> List<T> getList(Class<T> clazz){
		ExtJpaRepo<T,? extends Serializable> service = (ExtJpaRepo<T, ? extends Serializable>) mapService.get(clazz);
		if ( service != null) {
				return service.findAll();
		}
		return null;
	}
	
	@Override
	public <T> T createOrUpdateOne(Class<T> clazz,T entity){
		ExtJpaRepo<T,? extends Serializable> service = (ExtJpaRepo<T, ? extends Serializable>) mapService.get(clazz);
		if ( service != null) {
			return (T) service.save(entity);
		}
		return null;
	}

	@Override
	public <T,ID extends Serializable> void deleteOne(Class<T> clazz,ID id){
		ExtJpaRepo<T,ID> service = (ExtJpaRepo<T, ID>) mapService.get(clazz);
		if ( service != null) {
			service.delete(id);
		}
	}


	@Override
	public <T,ID extends Serializable> T findOne(Class<T> clazz,ID id){
		ExtJpaRepo<T,ID> service = (ExtJpaRepo<T, ID>) mapService.get(clazz);
		if ( service != null) {
			return (T)service.findOne(id);
		}
		return null;
	}


	
	
	
}
