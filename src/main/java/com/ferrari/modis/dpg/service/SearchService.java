package com.ferrari.modis.dpg.service;

import com.ferrari.modis.dpg.commons.i18n.LocalizationType;
import com.ferrari.modis.dpg.jentity.base.JPage;


public interface SearchService {
	

	public JPage getListByVersion(LocalizationType language , String optionalType , Long version, String searchText);
	
	public JPage getListFull(LocalizationType language , String searchText);
	public JPage getListByModel(LocalizationType language , Long idModel, String searchText);
	
	
	
}
