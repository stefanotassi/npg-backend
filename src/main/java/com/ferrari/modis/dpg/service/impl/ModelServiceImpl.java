package com.ferrari.modis.dpg.service.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMethod;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ferrari.modis.dpg.commons.i18n.LocalizationType;
import com.ferrari.modis.dpg.jentity.app.JHomePage;
import com.ferrari.modis.dpg.jentity.app.JModel;
import com.ferrari.modis.dpg.jentity.app.JModelCluster;
import com.ferrari.modis.dpg.jentity.app.JModelFull;
import com.ferrari.modis.dpg.jentity.app.JModelFullV2;
import com.ferrari.modis.dpg.jentity.app.JModelOptionalGroup;
import com.ferrari.modis.dpg.jentity.app.JModelOptionalSubGroup;
import com.ferrari.modis.dpg.jentity.app.JModelOptionalType;
import com.ferrari.modis.dpg.jentity.app.JModelType;
import com.ferrari.modis.dpg.jentity.base.JEntity;
import com.ferrari.modis.dpg.jentity.base.JPage;
import com.ferrari.modis.dpg.jentity.base.JPageAuth;
import com.ferrari.modis.dpg.jentity.base.JPageData;
import com.ferrari.modis.dpg.jentity.base.JValueText;
import com.ferrari.modis.dpg.model.app.Model;
import com.ferrari.modis.dpg.model.app.ModelCluster;
import com.ferrari.modis.dpg.model.app.ModelOptionalGroup;
import com.ferrari.modis.dpg.model.app.ModelOptionalSubGroup;
import com.ferrari.modis.dpg.model.app.ModelOptionalType;
import com.ferrari.modis.dpg.model.app.OptionalGroup;
import com.ferrari.modis.dpg.model.app.OptionalSubGroup;
import com.ferrari.modis.dpg.model.app.OptionalType;
import com.ferrari.modis.dpg.model.app.Version;
import com.ferrari.modis.dpg.model.base.AuditContext;
import com.ferrari.modis.dpg.model.ui.UiApplicationSetting;
import com.ferrari.modis.dpg.model.ui.UiPage;
import com.ferrari.modis.dpg.repository.MediaRepository;
import com.ferrari.modis.dpg.repository.ModelClusterRepository;
import com.ferrari.modis.dpg.repository.ModelOptionalGroupRepository;
import com.ferrari.modis.dpg.repository.ModelOptionalSubGroupRepository;
import com.ferrari.modis.dpg.repository.ModelOptionalTypeRepository;
import com.ferrari.modis.dpg.repository.ModelRepository;
import com.ferrari.modis.dpg.repository.OptionalGroupRepository;
import com.ferrari.modis.dpg.repository.OptionalSubGroupRepository;
import com.ferrari.modis.dpg.repository.OptionalTypeRepository;
import com.ferrari.modis.dpg.repository.UiApplicationSettingRepository;
import com.ferrari.modis.dpg.repository.UiPageAuthRepository;
import com.ferrari.modis.dpg.repository.UiPageRepository;
import com.ferrari.modis.dpg.repository.UiSecActionAuthRepository;
import com.ferrari.modis.dpg.repository.UiSecActionRepository;
import com.ferrari.modis.dpg.repository.VersionOptRepository;
import com.ferrari.modis.dpg.repository.VersionRepository;
import com.ferrari.modis.dpg.service.MediaService;
import com.ferrari.modis.dpg.service.ModelService;
import com.ferrari.modis.dpg.service.SecurityInfoService;
import com.ferrari.modis.dpg.service.UiService;
import com.ferrari.modis.dpg.service.VModelService;
import com.ferrari.modis.dpg.service.VersionService;

import io.swagger.api.ApiResponseMessage;
import io.swagger.api.NotFoundException;
import io.swagger.api.ValidationException;

@Service
public class ModelServiceImpl implements ModelService {

	private static final Logger logger = LoggerFactory.getLogger(ModelServiceImpl.class);

	@Autowired
	private UiService uiService;
	@Autowired
	private MediaRepository mediaRepository;

	@Autowired
	private SecurityInfoService securityInfoService;
	@Autowired
	private VModelService vModelService;

	@Autowired
	VersionService versionService;
	@Autowired
	private MediaService mediaService;

	@Autowired
	private ModelClusterRepository modelClusterRepository;

	@Autowired
	private ModelRepository modelRepository;
	@Autowired
	VersionRepository versionRepository;

	@Autowired
	private OptionalTypeRepository optionalTypeRepository;
	@Autowired
	private ModelOptionalTypeRepository modelOptionalTypeRepository;

	@Autowired
	ModelOptionalGroupRepository modelOptionalGroupRepository;
	@Autowired
	OptionalGroupRepository optionalGroupRepository;
	@Autowired
	ModelOptionalSubGroupRepository modelOptionalSubGroupRepository;
	@Autowired
	OptionalSubGroupRepository optionalSubGroupRepository;

	@Autowired
	UiPageRepository uiPageRepository;
	@Autowired
	UiPageAuthRepository uiPageAuthRepository;
	@Autowired
	UiSecActionRepository uiSecActionRepository;
	@Autowired
	UiSecActionAuthRepository uiSecActionAuthRepository;
	@Autowired
	UiApplicationSettingRepository uiApplicationSettingRepository;

	@Autowired
	VersionOptRepository versionOptRepository;

	@Override
	public ApiResponseMessage deleteModel(Long idModel) throws NotFoundException {
		CheckModel(idModel);

		String auth = uiService.getAuthorizationById(MODEL_DELETE_ENABLE);
		if (!"Y".equals(auth)) {
			throw new ValidationException(-1, "MODEL_NOT_FOUND"); // AVAILABLE
		}

		Model model = modelRepository.findById(idModel);
		model.setActive("N");
		modelRepository.save(model);

		return ApiResponseMessage.SUCCESS;
	}

	public JPage getHomepage(LocalizationType language) {

		if (language == null) {
			String d = securityInfoService.getProfile().getUserInfo().getLanguage();
			if (("it-IT").equals(d))
				language = LocalizationType.IT_IT;
			else
				language = LocalizationType.EN_US;
		}

		JPage jpage = new JPage();
		JPageData jPageData = new JPageData();

		JHomePage jHomePage = new JHomePage();

		List<JModelCluster> ljModelCluster = new ArrayList<JModelCluster>();
		JModelCluster jModelCluster;

		Boolean enableAllModels = ("Y".equals(uiService.getAuthorizationById(MODEL_DISPLAY_ENABLE)));

		for (ModelCluster mcluster : modelClusterRepository.findAll()) {
			jModelCluster = new JModelCluster();
			jModelCluster.setId(mcluster.getId());
			jModelCluster.setDescription(mcluster.getDescription(language.getValue()));
			jModelCluster.setSeq(mcluster.getSequence());

			for (Model model : modelRepository.findByCluster(mcluster.getId())) {

				if ((!"Y".equals(model.getVisibility())) && !enableAllModels) {
					// non visibile: abilitati solo utenti meritevili
					continue;

				}

				JModel jModel = new JModel();
				jModel.setId(model.getId());
				jModel.setDescription(model.getDescription(language.getValue()));
				jModel.setCode(model.getCode());
				jModel.setSeq(model.getSequence());
				jModel.setIdImage(model.getHomepageImage());
				jModel.setIdImageLogo(model.getLogoImage());
				jModel.setIdImageCover(model.getCoverImage());

				System.out.println("Model=" + model.getId());
				jModel.setUrlImage(mediaService.getThronUrl(model.getHomepageImage(), MEDIA_MODEL_TEMPLATE_HOME));
				jModel.setUrlImageCover(mediaService.getThronUrl(model.getCoverImage(), MEDIA_MODEL_TEMPLATE_COVER));
				jModel.setUrlImageLogo(mediaService.getThronUrl(model.getLogoImage(), MEDIA_MODEL_TEMPLATE_LOGO));

				jModel.setMediaTemplateImage(MEDIA_MODEL_TEMPLATE_HOME);
				jModel.setMediaTemplateImageLogo(MEDIA_MODEL_TEMPLATE_LOGO);
				jModel.setMediaTemplateImageCover(MEDIA_MODEL_TEMPLATE_COVER);

				jModel.setVisibility(model.getVisibility());
				jModel.setConfidential(vModelService.getConfidentialStatus(model));

				jModel.setQuality(model.getIdQuality());

				jModelCluster.getLjmodel().add(jModel);

			}
			ljModelCluster.add(jModelCluster);
		}

		jHomePage.setlJModelCluster(ljModelCluster);

		// immagine per "nuovo modello"
		UiApplicationSetting uiApplicationSetting = uiApplicationSettingRepository.findByKey(ID_IMAGE_NEW_CLUSTER);
		if ((uiApplicationSetting != null) && (uiApplicationSetting.getValue() != null))
			jHomePage.setIdImageNew(uiApplicationSetting.getValue());
		jHomePage.setUrlImageNew(mediaService.getThronUrl(uiApplicationSetting.getValue(), null));

		jPageData.setActual(jHomePage);

		// header JPage ------------------------------------------------------------
		UiPage uiPage = uiPageRepository.findByKey(UiService.PAGE_HOMEPAGE);
		jpage.setId(uiPage.getId());
		jpage.setName(uiPage.getDescription());
		jpage.setLanguage(language);

		// aggiunge lista autorizzazioni -------------------------------------------
		List<JPageAuth> lJPageAuth = uiService.getAuthorizationList(UiService.PAGE_HOMEPAGE);
		jpage.setAuths(lJPageAuth);

		// chiusura form
		jpage.setData(jPageData);

		return jpage;

	}

	public JPage getNewModelFormV2(LocalizationType language, String modelCluster) {
		CheckOptionalType("P1");
		CheckOptionalType("P2");
		CheckModelCluster(modelCluster); // model cluster deve esistere

		JPage jpage = new JPage();

		JModelFullV2 jModelFullV2 = new JModelFullV2();
		/////////////////////////////////////////////////////////

		jModelFullV2.setId(null);
		jModelFullV2.setCode(null);
		jModelFullV2.setIdImageHome(null);
		jModelFullV2.setIdImageLogo(null);
		jModelFullV2.setIdImageCover(null);

		jModelFullV2.setUrlImageHome(null);
		jModelFullV2.setUrlImageCover(null);
		jModelFullV2.setUrlImageLogo(null);

		jModelFullV2.setIdCluster(modelCluster);

		jModelFullV2.setMediaTemplateImageHome(MEDIA_MODEL_TEMPLATE_HOME);
		jModelFullV2.setMediaTemplateImageLogo(MEDIA_MODEL_TEMPLATE_LOGO);
		jModelFullV2.setMediaTemplateImageCover(MEDIA_MODEL_TEMPLATE_COVER);

		jModelFullV2.setQuality(DEFAULT_QUALITY);

		List<JModelOptionalType> lJModelOptionalType = new ArrayList<JModelOptionalType>();

		JModelOptionalType p1 = new JModelOptionalType();
		p1.setId(null);
		p1.setIdModel(null);
		p1.setIdModelOptionalType("P1");
		p1 = buildGroups(language, p1);

		lJModelOptionalType.add(p1);

		p1 = new JModelOptionalType();
		p1.setId(null);
		p1.setIdModel(null);
		p1.setIdModelOptionalType("P2");
		p1 = buildGroups(language, p1);

		lJModelOptionalType.add(p1);

		jModelFullV2.setlJModelOptionalType(lJModelOptionalType);

		/////////////////////////////////////////////////////////
		JPageData jPageData = new JPageData();
		jPageData.setActual(jModelFullV2);

		// header JPage ------------------------------------------------------------
		UiPage uiPage = uiPageRepository.findByKey(UiService.PAGE_MODEL_MAINTENANCE);
		jpage.setId(uiPage.getId());
		jpage.setName(uiPage.getDescription());
		jpage.setLanguage(language);

		// aggiunge lista autorizzazioni -------------------------------------------
		List<JPageAuth> lJPageAuth = uiService.getAuthorizationList(UiService.PAGE_MODEL_MAINTENANCE);
		jpage.setAuths(lJPageAuth);

		jpage.setData(jPageData);

		return jpage;
	}

	private JModelOptionalType buildGroups(LocalizationType language, JModelOptionalType jModelOptionalType) {
		// carica optional Groups.subgroups dipendentemente da tipo

		JModelOptionalGroup jModelOptionalGroup;
		for (OptionalGroup optionalGroup : optionalGroupRepository
				.findByIdOptionalType(jModelOptionalType.getIdModelOptionalType())) {
			jModelOptionalGroup = new JModelOptionalGroup();
			jModelOptionalGroup.setId(null);
			jModelOptionalGroup.setCode(optionalGroup.getCode());
			jModelOptionalGroup.setDescription(optionalGroup.getDescription(language.getValue()));
			jModelOptionalGroup.setDescription_it(optionalGroup.getDescription_it());
			jModelOptionalGroup.setDescription_en(optionalGroup.getDescription_en());

			jModelOptionalGroup.setSeq(optionalGroup.getSequence());

			JModelOptionalSubGroup jModelOptionalSubGroup;
			for (OptionalSubGroup optionalSubGroup : optionalSubGroupRepository
					.findByIdOptionalGroup(optionalGroup.getId())) {
				jModelOptionalSubGroup = new JModelOptionalSubGroup();
				jModelOptionalSubGroup.setId(null);
				jModelOptionalSubGroup.setCode(optionalSubGroup.getCode());
				jModelOptionalSubGroup.setDescription(optionalSubGroup.getDescription(language.getValue()));
				jModelOptionalSubGroup.setDescription_it(optionalSubGroup.getDescription_it());
				jModelOptionalSubGroup.setDescription_en(optionalSubGroup.getDescription_en());
				jModelOptionalSubGroup.setSeq(optionalSubGroup.getSequence());
				jModelOptionalGroup.getlJModelOptionalSubGroup().add(jModelOptionalSubGroup);
			}

			jModelOptionalType.getlJModelOptionalGroup().add(jModelOptionalGroup);

		}
		return jModelOptionalType;
	}

	public JPage getNewModelForm(LocalizationType language, String optionalType, String modelCluster) {

		CheckOptionalType(optionalType); // tipo opt (P1) deve esistere ed essere autorizzato
		CheckModelCluster(modelCluster); // model cluster deve esistere

		JPage jpage = new JPage();

		JModelFull jModelFull = new JModelFull();
		jModelFull.setId(null);
		jModelFull.setCode(null);
		jModelFull.setIdImageHome(null);
		jModelFull.setIdImageLogo(null);
		jModelFull.setIdImageCover(null);

		jModelFull.setUrlImageHome(null);
		jModelFull.setUrlImageCover(null);
		jModelFull.setUrlImageLogo(null);

		jModelFull.setIdOptionalType(optionalType);
		jModelFull.setIdCluster(modelCluster);

		jModelFull.setMediaTemplateImageHome(MEDIA_MODEL_TEMPLATE_HOME);
		jModelFull.setMediaTemplateImageLogo(MEDIA_MODEL_TEMPLATE_LOGO);
		jModelFull.setMediaTemplateImageCover(MEDIA_MODEL_TEMPLATE_COVER);

		jModelFull.setQuality(DEFAULT_QUALITY);

		JModelOptionalGroup jModelOptionalGroup;
		for (OptionalGroup optionalGroup : optionalGroupRepository.findByIdOptionalType(optionalType)) {
			jModelOptionalGroup = new JModelOptionalGroup();
			jModelOptionalGroup.setId(null);
			jModelOptionalGroup.setCode(optionalGroup.getCode());
			jModelOptionalGroup.setDescription(optionalGroup.getDescription(language.getValue()));
			jModelOptionalGroup.setDescription_it(optionalGroup.getDescription_it());
			jModelOptionalGroup.setDescription_en(optionalGroup.getDescription_en());

			jModelOptionalGroup.setSeq(optionalGroup.getSequence());

			JModelOptionalSubGroup jModelOptionalSubGroup;
			for (OptionalSubGroup optionalSubGroup : optionalSubGroupRepository
					.findByIdOptionalGroup(optionalGroup.getId())) {
				jModelOptionalSubGroup = new JModelOptionalSubGroup();
				jModelOptionalSubGroup.setId(null);
				jModelOptionalSubGroup.setCode(optionalSubGroup.getCode());
				jModelOptionalSubGroup.setDescription(optionalSubGroup.getDescription(language.getValue()));
				jModelOptionalSubGroup.setDescription_it(optionalSubGroup.getDescription_it());
				jModelOptionalSubGroup.setDescription_en(optionalSubGroup.getDescription_en());
				jModelOptionalSubGroup.setSeq(optionalSubGroup.getSequence());
				jModelOptionalGroup.getlJModelOptionalSubGroup().add(jModelOptionalSubGroup);
			}

			jModelFull.getlJModelOptionalGroup().add(jModelOptionalGroup);

		}

		JPageData jPageData = new JPageData();
		jPageData.setActual(jModelFull);

		// header JPage ------------------------------------------------------------
		UiPage uiPage = uiPageRepository.findByKey(UiService.PAGE_MODEL_MAINTENANCE);
		jpage.setId(uiPage.getId());
		jpage.setName(uiPage.getDescription());
		jpage.setLanguage(language);

		// aggiunge lista autorizzazioni -------------------------------------------
		List<JPageAuth> lJPageAuth = uiService.getAuthorizationList(UiService.PAGE_MODEL_MAINTENANCE);
		jpage.setAuths(lJPageAuth);

		jpage.setData(jPageData);

		return jpage;
	}

	public JPage getModelV2(LocalizationType language, Long idmodel) {

		CheckOptionalType("P1");
		CheckOptionalType("P2");
		CheckModel(idmodel); // modello deve esistere ed essere autorizzato

		JPage jpage = new JPage();

		Model model = modelRepository.findById(idmodel);
		JModelFullV2 jModelFullV2 = new JModelFullV2();
		jModelFullV2.setId(idmodel);
//		jModelFull.setDescription(model.getDescription(language.getValue()));
		jModelFullV2.setDescription_it(model.getDescription_it());
		jModelFullV2.setDescription_en(model.getDescription_en());

		jModelFullV2.setIdImageHome(model.getHomepageImage());
		jModelFullV2.setIdImageLogo(model.getLogoImage());
		jModelFullV2.setIdImageCover(model.getCoverImage());

		jModelFullV2.setUrlImageHome(mediaService.getThronUrl(model.getHomepageImage(), MEDIA_MODEL_TEMPLATE_HOME));
		jModelFullV2.setUrlImageCover(mediaService.getThronUrl(model.getCoverImage(), MEDIA_MODEL_TEMPLATE_COVER));
		jModelFullV2.setUrlImageLogo(mediaService.getThronUrl(model.getLogoImage(), MEDIA_MODEL_TEMPLATE_LOGO));

		jModelFullV2.setMediaTemplateImageHome(MEDIA_MODEL_TEMPLATE_HOME);
		jModelFullV2.setMediaTemplateImageLogo(MEDIA_MODEL_TEMPLATE_LOGO);
		jModelFullV2.setMediaTemplateImageCover(MEDIA_MODEL_TEMPLATE_COVER);

		jModelFullV2.setCode(model.getCode());
		jModelFullV2.setVisibility(model.getVisibility());
		jModelFullV2.setConfidential(vModelService.getConfidentialStatus(model));

		jModelFullV2.setSequence(model.getSequence());
		;
		jModelFullV2.setIdCluster(model.getIdMcluster());

		jModelFullV2.setQuality(model.getIdQuality());

		Boolean existsP2 = Boolean.FALSE;

		List<JModelOptionalType> lJModelOptionalType = new ArrayList<JModelOptionalType>();

		for (ModelOptionalType modelOptionalType : modelOptionalTypeRepository.findByModel(idmodel)) {
			JModelOptionalType jModelOptionalType = new JModelOptionalType();
			jModelOptionalType.setId(modelOptionalType.getId());
			jModelOptionalType.setIdModel(modelOptionalType.getIdModel());
			jModelOptionalType.setIdModelOptionalType(modelOptionalType.getIdOptionalType());

			if ("P2".equals(modelOptionalType.getIdOptionalType())) {
				existsP2 = Boolean.TRUE;
			}

			List<JModelOptionalGroup> lJModelOptionalGroup = new ArrayList<JModelOptionalGroup>();

			JModelOptionalGroup jModelOptionalGroup;
			for (ModelOptionalGroup modelOptionalGroup : modelOptionalType.getlModelOptionalGroup()) {
				jModelOptionalGroup = new JModelOptionalGroup();
				jModelOptionalGroup.setId(modelOptionalGroup.getId());
				jModelOptionalGroup.setCode(modelOptionalGroup.getCode());
				jModelOptionalGroup.setDescription(modelOptionalGroup.getDescription(language.getValue()));
				jModelOptionalGroup.setDescription_it(modelOptionalGroup.getDescription_it());
				jModelOptionalGroup.setDescription_en(modelOptionalGroup.getDescription_en());
				jModelOptionalGroup.setSeq(modelOptionalGroup.getSequence());

				JModelOptionalSubGroup jModelOptionalSubGroup;
				for (ModelOptionalSubGroup modelOptionalSubGroup : modelOptionalGroup.getlModelOptionalSubGroup()) {
					jModelOptionalSubGroup = new JModelOptionalSubGroup();
					jModelOptionalSubGroup.setId(modelOptionalSubGroup.getId());
					jModelOptionalSubGroup.setId(modelOptionalSubGroup.getId());
					jModelOptionalSubGroup.setCode(modelOptionalSubGroup.getCode());
					jModelOptionalSubGroup.setDescription(modelOptionalSubGroup.getDescription(language.getValue()));
					jModelOptionalSubGroup.setDescription_it(modelOptionalSubGroup.getDescription_it());
					jModelOptionalSubGroup.setDescription_en(modelOptionalSubGroup.getDescription_en());
					jModelOptionalSubGroup.setSeq(modelOptionalSubGroup.getSequence());
					jModelOptionalGroup.getlJModelOptionalSubGroup().add(jModelOptionalSubGroup);
				}
				lJModelOptionalGroup.add(jModelOptionalGroup);
			}

			jModelOptionalType.setlJModelOptionalGroup(lJModelOptionalGroup);

			lJModelOptionalType.add(jModelOptionalType);

		}

		////////////////////////////////////////////////////////////////////////////
		// Se manca P2 recupero info per eventuale creazione
		if (!existsP2) {
			JModelOptionalType p1 = new JModelOptionalType();
			p1 = new JModelOptionalType();
			p1.setId(null);
			p1.setIdModel(null);
			p1.setIdModelOptionalType("P2");
			p1 = buildGroups(language, p1);

			lJModelOptionalType.add(p1);
		}

		////////////////////////////////////////////////////////////////////////////

		jModelFullV2.setlJModelOptionalType(lJModelOptionalType);

		JPageData jPageData = new JPageData();
		jPageData.setActual(jModelFullV2);

		// header JPage ------------------------------------------------------------
		UiPage uiPage = uiPageRepository.findByKey(UiService.PAGE_MODEL_MAINTENANCE);
		jpage.setId(uiPage.getId());
		jpage.setName(uiPage.getDescription());
		jpage.setLanguage(language);

		// aggiunge lista autorizzazioni -------------------------------------------
		List<JPageAuth> lJPageAuth = uiService.getAuthorizationList(UiService.PAGE_MODEL_MAINTENANCE);
		jpage.setAuths(lJPageAuth);

		jpage.setData(jPageData);

		return jpage;

	}

	@Override
	public JPage getModelTypesV2(LocalizationType language, Long idmodel) {

		JPage jpage = new JPage();
		Model model = modelRepository.findById(idmodel);

		JModelType jModelType = new JModelType();
		jModelType.setIdModel(idmodel);

		JModel jModel = new JModel();
		jModel.setId(model.getId());
		jModel.setDescription(model.getDescription(language.getValue()));
		jModel.setCode(model.getCode());
		jModel.setSeq(model.getSequence());
		jModel.setIdImage(model.getHomepageImage());
		jModel.setIdImageLogo(model.getLogoImage());
		jModel.setIdImageCover(model.getCoverImage());
		jModel.setUrlImage(mediaService.getThronUrl(model.getHomepageImage(), MEDIA_MODEL_TEMPLATE_HOME));
		jModel.setUrlImageCover(mediaService.getThronUrl(model.getCoverImage(), MEDIA_MODEL_TEMPLATE_COVER));
		jModel.setUrlImageLogo(mediaService.getThronUrl(model.getLogoImage(), MEDIA_MODEL_TEMPLATE_LOGO));
		jModel.setMediaTemplateImage(MEDIA_MODEL_TEMPLATE_HOME);
		jModel.setMediaTemplateImageLogo(MEDIA_MODEL_TEMPLATE_LOGO);
		jModel.setMediaTemplateImageCover(MEDIA_MODEL_TEMPLATE_COVER);
		jModel.setVisibility(model.getVisibility());
		jModel.setConfidential(vModelService.getConfidentialStatus(model));
		jModel.setQuality(model.getIdQuality());
		jModelType.setJmodel(jModel);

		List<JModelOptionalType> lJModelOptionalType = new ArrayList<JModelOptionalType>();
		for (ModelOptionalType modelOptionalType : modelOptionalTypeRepository.findByModel(idmodel)) {
			JModelOptionalType jModelOptionalType = new JModelOptionalType();
			jModelOptionalType.setId(modelOptionalType.getId());
			jModelOptionalType.setIdModel(modelOptionalType.getIdModel());
			jModelOptionalType.setIdModelOptionalType(modelOptionalType.getIdOptionalType());

			OptionalType optionalType = optionalTypeRepository.getOne(modelOptionalType.getIdOptionalType());

			jModelOptionalType.setDescription(optionalType.getDescription(language.getValue()));
			jModelOptionalType.setIdImage(optionalType.getIdImage());
			jModelOptionalType.setUrlImage(mediaService.getThronUrl(optionalType.getIdImage(), null));
			lJModelOptionalType.add(jModelOptionalType);
		}

		jModelType.setlJModelOptionalType(lJModelOptionalType);

		JPageData jPageData = new JPageData();
		jPageData.setActual(jModelType);

		// header JPage ------------------------------------------------------------
		UiPage uiPage = uiPageRepository.findByKey(UiService.PAGE_SPLASH);
		jpage.setId(uiPage.getId());
		jpage.setName(uiPage.getDescription());

		jpage.setLanguage(language);

		// aggiunge lista autorizzazioni -------------------------------------------
		List<JPageAuth> lJPageAuth = uiService.getAuthorizationList(UiService.PAGE_SPLASH);
		jpage.setAuths(lJPageAuth);

		jpage.setData(jPageData);

		return jpage;
	}

	//
	public JPage getModel(LocalizationType language, String optionalType, Long idmodel) {

		CheckOptionalType(optionalType); // tipo opt (P1) deve esistere ed essere autorizzato
		CheckModel(idmodel); // modello deve esistere ed essere autorizzato
		CheckModelAndOptType(idmodel, optionalType);
		// compone form per modifica

		JPage jpage = new JPage();

		Model model = modelRepository.findById(idmodel);

		ModelOptionalType modelOptionalType = modelOptionalTypeRepository.findByModelAndOptionalType(idmodel,
				optionalType);

		JModelFull jModelFull = new JModelFull();
		jModelFull.setId(idmodel);
//		jModelFull.setDescription(model.getDescription(language.getValue()));
		jModelFull.setDescription_it(model.getDescription_it());
		jModelFull.setDescription_en(model.getDescription_en());

		jModelFull.setIdImageHome(model.getHomepageImage());
		jModelFull.setIdImageLogo(model.getLogoImage());
		jModelFull.setIdImageCover(model.getCoverImage());

		jModelFull.setUrlImageHome(mediaService.getThronUrl(model.getHomepageImage(), MEDIA_MODEL_TEMPLATE_HOME));
		jModelFull.setUrlImageCover(mediaService.getThronUrl(model.getCoverImage(), MEDIA_MODEL_TEMPLATE_COVER));
		jModelFull.setUrlImageLogo(mediaService.getThronUrl(model.getLogoImage(), MEDIA_MODEL_TEMPLATE_LOGO));

		jModelFull.setMediaTemplateImageHome(MEDIA_MODEL_TEMPLATE_HOME);
		jModelFull.setMediaTemplateImageLogo(MEDIA_MODEL_TEMPLATE_LOGO);
		jModelFull.setMediaTemplateImageCover(MEDIA_MODEL_TEMPLATE_COVER);

		jModelFull.setCode(model.getCode());
		jModelFull.setVisibility(model.getVisibility());
		jModelFull.setConfidential(vModelService.getConfidentialStatus(model));

		jModelFull.setSequence(model.getSequence());
		;
		jModelFull.setIdOptionalType(optionalType);
		jModelFull.setIdCluster(model.getIdMcluster());

		jModelFull.setQuality(model.getIdQuality());

		// elenco gruppi optionals
		JModelOptionalGroup jModelOptionalGroup;
		for (ModelOptionalGroup modelOptionalGroup : modelOptionalType.getlModelOptionalGroup()) {
			jModelOptionalGroup = new JModelOptionalGroup();
			jModelOptionalGroup.setId(modelOptionalGroup.getId());
			jModelOptionalGroup.setCode(modelOptionalGroup.getCode());
			jModelOptionalGroup.setDescription(modelOptionalGroup.getDescription(language.getValue()));
			jModelOptionalGroup.setDescription_it(modelOptionalGroup.getDescription_it());
			jModelOptionalGroup.setDescription_en(modelOptionalGroup.getDescription_en());
			jModelOptionalGroup.setSeq(modelOptionalGroup.getSequence());

			JModelOptionalSubGroup jModelOptionalSubGroup;
			for (ModelOptionalSubGroup modelOptionalSubGroup : modelOptionalGroup.getlModelOptionalSubGroup()) {
				jModelOptionalSubGroup = new JModelOptionalSubGroup();
				jModelOptionalSubGroup.setId(modelOptionalSubGroup.getId());
				jModelOptionalSubGroup.setId(modelOptionalSubGroup.getId());
				jModelOptionalSubGroup.setCode(modelOptionalSubGroup.getCode());
				jModelOptionalSubGroup.setDescription(modelOptionalSubGroup.getDescription(language.getValue()));
				jModelOptionalSubGroup.setDescription_it(modelOptionalSubGroup.getDescription_it());
				jModelOptionalSubGroup.setDescription_en(modelOptionalSubGroup.getDescription_en());
				jModelOptionalSubGroup.setSeq(modelOptionalSubGroup.getSequence());
				jModelOptionalGroup.getlJModelOptionalSubGroup().add(jModelOptionalSubGroup);
			}

			jModelFull.getlJModelOptionalGroup().add(jModelOptionalGroup);
		}

		JPageData jPageData = new JPageData();
		jPageData.setActual(jModelFull);

		// header JPage ------------------------------------------------------------
		UiPage uiPage = uiPageRepository.findByKey(UiService.PAGE_MODEL_MAINTENANCE);
		jpage.setId(uiPage.getId());
		jpage.setName(uiPage.getDescription());
		jpage.setLanguage(language);

		// aggiunge lista autorizzazioni -------------------------------------------
		List<JPageAuth> lJPageAuth = uiService.getAuthorizationList(UiService.PAGE_MODEL_MAINTENANCE);
		jpage.setAuths(lJPageAuth);

		jpage.setData(jPageData);

		return jpage;

	}

	public List<JValueText> getOptionalGroups(LocalizationType language, String optionalType, Long idmodel) {
		CheckOptionalType(optionalType); // tipo opt (P1) deve esistere ed essere autorizzato
		CheckModel(idmodel); // modello deve esistere ed essere autorizzato
		CheckModelAndOptType(idmodel, optionalType);

		List<JValueText> lJValueText = new ArrayList<JValueText>();
		JValueText jValueText;

		ModelOptionalType modelOptionalType = modelOptionalTypeRepository.findByModelAndOptionalType(idmodel,
				optionalType);

		for (ModelOptionalGroup modelOptionalGroup : modelOptionalGroupRepository
				.findByModelOptType(modelOptionalType.getId())) {
			jValueText = new JValueText(modelOptionalGroup.getId().toString(),
					modelOptionalGroup.getDescription(language.getValue()));
			lJValueText.add(jValueText);
		}

		return lJValueText;
	}

	public List<JValueText> getOptionalSubGroups(LocalizationType language, String optionalType, Long idmodel,
			Long idOptionalGroup) {
		CheckOptionalType(optionalType); // tipo opt (P1) deve esistere ed essere autorizzato
		CheckModel(idmodel); // modello deve esistere ed essere autorizzato
		CheckModelAndOptType(idmodel, optionalType);

		List<JValueText> lJValueText = new ArrayList<JValueText>();
		JValueText jValueText;
		ModelOptionalType modelOptionalType = modelOptionalTypeRepository.findByModelAndOptionalType(idmodel,
				optionalType);
		// Long id = modelOptionalType.getId();
		// leggo unico ModelOptionalGroup per id_optional_type e id (passato da web): ne
		// verifica l'esistenza
		ModelOptionalGroup modelOptionalGroup = modelOptionalGroupRepository
				.findByModelOptTypeAndId(modelOptionalType.getId(), idOptionalGroup);
		if (modelOptionalGroup == null)
			throw new ValidationException(-1, "MODEL_OPT_GROUP_NOT_FOUND");

		for (ModelOptionalSubGroup modelOptionalSubGroup : modelOptionalSubGroupRepository
				.findByModelOptGroup(modelOptionalGroup.getId())) {
			jValueText = new JValueText(modelOptionalSubGroup.getId().toString(),
					modelOptionalSubGroup.getDescription(language.getValue()));
			lJValueText.add(jValueText);
		}

		return lJValueText;
	}

	private void CheckOptionalType(String ot) {
		OptionalType optionalType = optionalTypeRepository.findById(ot);
		if (optionalType == null)
			throw new ValidationException(-1, "OPT_TYPE_NOT_ALLOWED");
	}

	private void CheckModelCluster(String mc) {

		ModelCluster mcluster = modelClusterRepository.findById(mc);
		if (mcluster == null)
			throw new ValidationException(-1, "MCLUSTER_NOT_FOUND");
	}

	@Override
	public void CheckModel(Long idModel) {
		Model model = modelRepository.findById(idModel);
		if (model == null || !AuditContext.isActive(model))
			throw new ValidationException(-1, "MODEL_NOT_FOUND");

		Boolean enableAllModels = ("Y".equals(uiService.getAuthorizationById(MODEL_DISPLAY_ENABLE)));
		if ((!"Y".equals(model.getVisibility())) && !enableAllModels) {
			// non visibile: abilitati solo utenti meritevili
			throw new ValidationException(-1, "MODEL_NOT_FOUND"); // AVAILABLE
		}

	}

	private void CheckModelAndOptType(Long idModel, String optionalType) {
		ModelOptionalType modelOptionalType = modelOptionalTypeRepository.findByModelAndOptionalType(idModel,
				optionalType);
		if (modelOptionalType == null)
			throw new ValidationException(-1, "MODEL_OPTIONAL_TYPE_NOT_FOUND");
	}

	public JPage putModel(JEntity jEntity, LocalizationType language, String optionalType, Long idmodel,
			RequestMethod requestMethod) throws NotFoundException {

		if (!"Y".equals(uiService.getAuthorizationById(MODEL_UPDATE_ENABLE)))
			throw new ValidationException(-1, "USER_PERMISSION_MODEL_UPDATE");

		JModelFull jModelFull = null;
		try {
			jModelFull = convertFromJEntity(jEntity.getData().getActual());
		} catch (IOException e) {
			throw new NotFoundException(-1, e.getMessage());
		}
		validateModel(jModelFull, requestMethod);

		// Long idModel =
		saveModel(jEntity.getLanguage(), jModelFull, requestMethod);

		return getModel(language, optionalType, idmodel);

	}

	public JPage putModelV2(JEntity jEntity, LocalizationType language, Long idmodel, RequestMethod requestMethod)
			throws NotFoundException {

		if (!"Y".equals(uiService.getAuthorizationById(MODEL_UPDATE_ENABLE)))
			throw new ValidationException(-1, "USER_PERMISSION_MODEL_UPDATE");

		JModelFullV2 jModelFullV2 = null;
		try {
			jModelFullV2 = convertFromJEntityV2(jEntity.getData().getActual());
		} catch (IOException e) {
			throw new NotFoundException(-1, e.getMessage());
		}
		validateModelV2(jModelFullV2, requestMethod);

		saveModelV2(jEntity.getLanguage(), jModelFullV2, requestMethod);

		return getModelV2(language, idmodel);

	}

	public JPage postModel(JEntity jEntity, RequestMethod requestMethod) throws NotFoundException {

		if (!"Y".equals(uiService.getAuthorizationById(MODEL_CREATE_ENABLE)))
			throw new ValidationException(-1, "USER_PERMISSION_MODEL_CREATE");

		JModelFull jModelFull = null;
		try {
			jModelFull = convertFromJEntity(jEntity.getData().getActual());
		} catch (IOException e) {
			throw new NotFoundException(-1, e.getMessage());
		}
		validateModel(jModelFull, requestMethod);

		Long idModel = saveModel(jEntity.getLanguage(), jModelFull, requestMethod);

		return getModel(jEntity.getLanguage(), jModelFull.getIdOptionalType(), idModel);

	}

	public JPage postModelV2(JEntity jEntity, RequestMethod requestMethod) throws NotFoundException {

		if (!"Y".equals(uiService.getAuthorizationById(MODEL_CREATE_ENABLE)))
			throw new ValidationException(-1, "USER_PERMISSION_MODEL_CREATE");

		JModelFullV2 jModelFullV2 = null;
		try {
			jModelFullV2 = convertFromJEntityV2(jEntity.getData().getActual());
		} catch (IOException e) {
			throw new NotFoundException(-1, e.getMessage());
		}
		validateModelV2(jModelFullV2, requestMethod);

		Long idModel = saveModelV2(jEntity.getLanguage(), jModelFullV2, requestMethod);

		return getModelV2(jEntity.getLanguage(), idModel);

	}

	private Long saveModelV2(LocalizationType language, JModelFullV2 jModelFullV2, RequestMethod requestMethod) {
		Model model = null;

		if (requestMethod.equals(RequestMethod.PUT)) {
			// update
			model = modelRepository.findById(jModelFullV2.getId());
			if (model == null) {
				throw new ValidationException(-1, "MODEL_NOT_FOUND");
			}
			model.setId(jModelFullV2.getId());
		}

		if (requestMethod.equals(RequestMethod.POST)) {
			// create
			model = new Model();
			model.setId(null);

		}

		model.setCode(jModelFullV2.getCode());

		model.setDescription_it(jModelFullV2.getDescription_it());
		model.setDescription_en(jModelFullV2.getDescription_en());

		model.setSequence(jModelFullV2.getSequence());
		model.setHomepageImage(jModelFullV2.getIdImageHome());
		model.setLogoImage(jModelFullV2.getIdImageLogo());
		model.setCoverImage(jModelFullV2.getIdImageCover());

		model.setIdMcluster(jModelFullV2.getIdCluster());
		model.setVisibility(jModelFullV2.getVisibility());
		model.setIdQuality(jModelFullV2.getQuality() == null ? DEFAULT_QUALITY : jModelFullV2.getQuality());
		modelRepository.saveAndFlush(model);

		// leggo le gerarchie P1 e P2
		for (JModelOptionalType jModelOptionalType : jModelFullV2.getlJModelOptionalType()) {

			ModelOptionalType modelOptionalType = modelOptionalTypeRepository.findByModelAndOptionalType(model.getId(),
					jModelOptionalType.getIdModelOptionalType());

			if (modelOptionalType == null)
				modelOptionalType = new ModelOptionalType();

			modelOptionalType.setIdModel(model.getId());
			modelOptionalType.setIdOptionalType(jModelOptionalType.getIdModelOptionalType());

			if (modelOptionalType != null)
				modelOptionalType.getlModelOptionalGroup().clear();

			ModelOptionalGroup modelOptionalGroup;
			for (JModelOptionalGroup jModelOptionalGroup : jModelOptionalType.getlJModelOptionalGroup()) {

				List<ModelOptionalSubGroup> lModelOptionalSubGroup = null;

				modelOptionalGroup = new ModelOptionalGroup();

				modelOptionalGroup.setId(jModelOptionalGroup.getId());
				modelOptionalGroup.setCode(jModelOptionalGroup.getCode());
				modelOptionalGroup.setDescription_it(jModelOptionalGroup.getDescription_it());
				modelOptionalGroup.setDescription_en(jModelOptionalGroup.getDescription_en());
				modelOptionalGroup.setSequence(jModelOptionalGroup.getSeq());
				modelOptionalGroup.setActive("Y");

				ModelOptionalSubGroup modelOptionalSubGroup = null;
				lModelOptionalSubGroup = new ArrayList<ModelOptionalSubGroup>();
				for (JModelOptionalSubGroup jModelOptionalSubGroup : jModelOptionalGroup.getlJModelOptionalSubGroup()) {
					modelOptionalSubGroup = new ModelOptionalSubGroup();
					modelOptionalSubGroup.setId(jModelOptionalSubGroup.getId());
					modelOptionalSubGroup.setCode(jModelOptionalSubGroup.getCode());
					modelOptionalSubGroup.setDescription_it(jModelOptionalSubGroup.getDescription_it());
					modelOptionalSubGroup.setDescription_en(jModelOptionalSubGroup.getDescription_en());
					modelOptionalSubGroup.setSequence(jModelOptionalSubGroup.getSeq());
					modelOptionalSubGroup.setModelOptionalGroup(modelOptionalGroup);
					modelOptionalSubGroup.setActive("Y");
					lModelOptionalSubGroup.add(modelOptionalSubGroup);
					modelOptionalGroup.addModelOptionalSubGroup(modelOptionalSubGroup);
				}

				modelOptionalType.addlModelOptionalGroup(modelOptionalGroup);

			}
			modelOptionalTypeRepository.saveAndFlush(modelOptionalType);

			// cerca di creare versione default
			versionService.createDefaultVersion(modelOptionalType.getId(), Boolean.FALSE);
		}

		return model.getId();
	}

	private Long saveModel(LocalizationType language, JModelFull jModelFull, RequestMethod requestMethod) {

		Model model = null;

		if (requestMethod.equals(RequestMethod.PUT)) {
			// update
			model = modelRepository.findById(jModelFull.getId());
			if (model == null) {
				throw new ValidationException(-1, "MODEL_NOT_FOUND");
			}
			model.setId(jModelFull.getId());
		}

		if (requestMethod.equals(RequestMethod.POST)) {
			// create
			model = new Model();
			model.setId(null);

		}

		model.setCode(jModelFull.getCode());

		model.setDescription_it(jModelFull.getDescription_it());
		model.setDescription_en(jModelFull.getDescription_en());

		model.setSequence(jModelFull.getSequence());
		model.setHomepageImage(jModelFull.getIdImageHome());
		model.setLogoImage(jModelFull.getIdImageLogo());
		model.setCoverImage(jModelFull.getIdImageCover());

		model.setIdMcluster(jModelFull.getIdCluster());
		model.setVisibility(jModelFull.getVisibility());

		model.setIdQuality(jModelFull.getQuality() == null ? DEFAULT_QUALITY : jModelFull.getQuality());

		modelRepository.saveAndFlush(model);

		/////////////////////////////////////////////////////////////////////////////////////////
		ModelOptionalType modelOptionalType = modelOptionalTypeRepository.findByModelAndOptionalType(model.getId(),
				jModelFull.getIdOptionalType());

		if (modelOptionalType == null)
			modelOptionalType = new ModelOptionalType();

		modelOptionalType.setIdModel(model.getId());
		modelOptionalType.setIdOptionalType(jModelFull.getIdOptionalType());

		if (modelOptionalType != null)
			modelOptionalType.getlModelOptionalGroup().clear();

		ModelOptionalGroup modelOptionalGroup;
		for (JModelOptionalGroup jModelOptionalGroup : jModelFull.getlJModelOptionalGroup()) {

			List<ModelOptionalSubGroup> lModelOptionalSubGroup = null;

			modelOptionalGroup = new ModelOptionalGroup();

			modelOptionalGroup.setId(jModelOptionalGroup.getId());
			modelOptionalGroup.setCode(jModelOptionalGroup.getCode());
			modelOptionalGroup.setDescription_it(jModelOptionalGroup.getDescription_it());
			modelOptionalGroup.setDescription_en(jModelOptionalGroup.getDescription_en());
			modelOptionalGroup.setSequence(jModelOptionalGroup.getSeq());
			modelOptionalGroup.setActive("Y");

			ModelOptionalSubGroup modelOptionalSubGroup = null;
			lModelOptionalSubGroup = new ArrayList<ModelOptionalSubGroup>();
			for (JModelOptionalSubGroup jModelOptionalSubGroup : jModelOptionalGroup.getlJModelOptionalSubGroup()) {
				modelOptionalSubGroup = new ModelOptionalSubGroup();
				modelOptionalSubGroup.setId(jModelOptionalSubGroup.getId());
				modelOptionalSubGroup.setCode(jModelOptionalSubGroup.getCode());
				modelOptionalSubGroup.setDescription_it(jModelOptionalSubGroup.getDescription_it());
				modelOptionalSubGroup.setDescription_en(jModelOptionalSubGroup.getDescription_en());
				modelOptionalSubGroup.setSequence(jModelOptionalSubGroup.getSeq());
				modelOptionalSubGroup.setModelOptionalGroup(modelOptionalGroup);
				modelOptionalSubGroup.setActive("Y");
				lModelOptionalSubGroup.add(modelOptionalSubGroup);
				modelOptionalGroup.addModelOptionalSubGroup(modelOptionalSubGroup);
			}

			modelOptionalType.addlModelOptionalGroup(modelOptionalGroup);

		}
		/////////////////////////////////////////////////////////////////////////////////////////

		System.out.println(modelOptionalType.toString());

		modelOptionalTypeRepository.saveAndFlush(modelOptionalType);
		// tenta di creare la prima versione qualora non esista
		versionService.createDefaultVersion(modelOptionalType.getId(), Boolean.FALSE);

		return model.getId();

	}

	private void CheckGroupsV2(JModelFullV2 jModelFullV2) {

		for (JModelOptionalType jModelOptionalType : jModelFullV2.getlJModelOptionalType()) {

			List<Long> jsg = new ArrayList<Long>();
			for (JModelOptionalGroup jModelOptionalGroup : jModelOptionalType.getlJModelOptionalGroup()) {
				for (JModelOptionalSubGroup jModelOptionalSubGroup : jModelOptionalGroup.getlJModelOptionalSubGroup()) {
					jsg.add(jModelOptionalSubGroup.getId());
				}
			}

			logger.warn("CheckGroups - jModelOptionalSubGroup = {}", jsg.toString());

			// devo elencare i sottogruppi non presenti in JmodelFull ma presenti in Tabella
			// se presnte in tabella
			ModelOptionalType modelOptionalType = modelOptionalTypeRepository
					.findByModelAndOptionalType(jModelFullV2.getId(), jModelOptionalType.getIdModelOptionalType());

			if (modelOptionalType != null) {

				for (ModelOptionalGroup modelOptionalGroup : modelOptionalType.getlModelOptionalGroup()) {
					for (ModelOptionalSubGroup modelOptionalSubGroup : modelOptionalGroup.getlModelOptionalSubGroup()) {

						// ora verifico che il sottogruppo sia presente in jModelFull
						if (!jsg.contains(modelOptionalSubGroup.getId())) {
							// vedo se presente in optionals utilizzati
							Boolean existsOptional = versionOptRepository
									.existsOptBySubGroup(modelOptionalSubGroup.getId());
							if (existsOptional) {
								logger.warn("Tentativo di cancellazione optional {} fallita",
										modelOptionalSubGroup.getCode());
								throw new ValidationException(-1,
										"SUBGROUP_[" + modelOptionalSubGroup.getCode() + "]_NOT_DELETABLE");

							}
						}
					}
				}
			}

		}

	}

	private void CheckGroups(JModelFull jModelFull) {

		List<Long> jsg = new ArrayList<Long>();
		for (JModelOptionalGroup jModelOptionalGroup : jModelFull.getlJModelOptionalGroup()) {
			for (JModelOptionalSubGroup jModelOptionalSubGroup : jModelOptionalGroup.getlJModelOptionalSubGroup()) {
				jsg.add(jModelOptionalSubGroup.getId());
			}
		}

		logger.warn("CheckGroups - jModelOptionalSubGroup = {}", jsg.toString());

		// devo elencare i sottogruppi non presenti in JmodelFull ma presenti in Tabella
		// se presnte in tabella
		ModelOptionalType modelOptionalType = modelOptionalTypeRepository.findByModelAndOptionalType(jModelFull.getId(),
				jModelFull.getIdOptionalType());
		for (ModelOptionalGroup modelOptionalGroup : modelOptionalType.getlModelOptionalGroup()) {
			for (ModelOptionalSubGroup modelOptionalSubGroup : modelOptionalGroup.getlModelOptionalSubGroup()) {

				// ora verifico che il sottogruppo sia presente in jModelFull
				if (!jsg.contains(modelOptionalSubGroup.getId())) {
					// vedo se presente in optionals utilizzati
					Boolean existsOptional = versionOptRepository.existsOptBySubGroup(modelOptionalSubGroup.getId());
					if (existsOptional) {
						logger.warn("Tentativo di cancellazione optional {} fallita", modelOptionalSubGroup.getCode());
						throw new ValidationException(-1,
								"SUBGROUP_[" + modelOptionalSubGroup.getCode() + "]_NOT_DELETABLE");

					}
				}
			}
		}
	}

	private void validateModel(JModelFull jModelFull, RequestMethod requestMethod) {

		if (requestMethod.equals(RequestMethod.POST)) {
			CheckOptionalType(jModelFull.getIdOptionalType());

		} else if (requestMethod.equals(RequestMethod.PUT)) {
			CheckOptionalType(jModelFull.getIdOptionalType());
			CheckModel(jModelFull.getId());
			CheckModelAndOptType(jModelFull.getId(), jModelFull.getIdOptionalType());
			CheckGroups(jModelFull);

		} else
			throw new ValidationException(-1, "ACTION_NOT_ALLOWEED");

		ModelCluster modelCluster = modelClusterRepository.findById(jModelFull.getIdCluster());
		if (modelCluster == null)
			throw new ValidationException(-1, "MCLUSTER_NOT_FOUND");

		if (!("Y").equals(jModelFull.getVisibility()) && !("N").equals(jModelFull.getVisibility()))
			throw new ValidationException(-1, "MODEL_VISIBILITY_YN");

		if (jModelFull.getSequence() == null || jModelFull.getSequence().equals(0L))
			throw new ValidationException(-1, "MODEL_SEQUENCE_MISSING");

		if (jModelFull.getDescription_it() == null || jModelFull.getDescription_it().trim().equals(""))
			throw new ValidationException(-1, "MODEL_DESCRIPTION_IT");
		if (jModelFull.getDescription_en() == null || jModelFull.getDescription_en().trim().equals(""))
			throw new ValidationException(-1, "MODEL_DESCRIPTION_EN");
		if (jModelFull.getCode() == null || jModelFull.getCode().trim().equals(""))
			throw new ValidationException(-1, "MODEL_CODE_MISSING");

		if (jModelFull.getIdImageHome() != null) {
			if (!mediaRepository.exists(jModelFull.getIdImageHome()))
				throw new ValidationException(-1, "MODEL_IMAGE_MISSING");
		}

		// se immissione il codice non deve esistere
		if (requestMethod == RequestMethod.POST) {
			List<Model> models = modelRepository.findByCode(jModelFull.getCode());
			AuditContext.filterActive(models);
			if (!models.isEmpty())
				throw new ValidationException(-1, "MODEL_CODE_DUPLICATE");
		}
		// se modifica puo' esistere ma deve avere lo stesso ID
		if (requestMethod == RequestMethod.PUT) {
			List<Model> models = modelRepository.findByCode(jModelFull.getCode());
			AuditContext.filterActive(models);
			Model model = null;
			if (models.size() > 1) {
				throw new ValidationException(-1, "MODEL_CODE_DUPLICATE");
			} else if (models.isEmpty()) {
				throw new ValidationException(-1, "MODEL_CODE_MISSING");
			}
			model = models.get(0);
			if ((model != null) && (!model.getId().equals(jModelFull.getId())))
				throw new ValidationException(-1, "MODEL_CODE_DUPLICATE");
		}

	}

	private void validateModelV2(JModelFullV2 jModelFullV2, RequestMethod requestMethod) {

		if (requestMethod.equals(RequestMethod.POST)) {
			for (JModelOptionalType jModelOptionalType : jModelFullV2.getlJModelOptionalType()) {
				CheckOptionalType(jModelOptionalType.getIdModelOptionalType());
			}
		} else if (requestMethod.equals(RequestMethod.PUT)) {
			CheckModel(jModelFullV2.getId());
			for (JModelOptionalType jModelOptionalType : jModelFullV2.getlJModelOptionalType()) {
				CheckOptionalType(jModelOptionalType.getIdModelOptionalType());
				// puo' non esisterer P2
				// CheckModelAndOptType(jModelFullV2.getId(),
				// jModelOptionalType.getIdModelOptionalType());

			}
			CheckGroupsV2(jModelFullV2);

		} else
			throw new ValidationException(-1, "ACTION_NOT_ALLOWEED");

		ModelCluster modelCluster = modelClusterRepository.findById(jModelFullV2.getIdCluster());
		if (modelCluster == null)
			throw new ValidationException(-1, "MCLUSTER_NOT_FOUND");

		if (!("Y").equals(jModelFullV2.getVisibility()) && !("N").equals(jModelFullV2.getVisibility()))
			throw new ValidationException(-1, "MODEL_VISIBILITY_YN");

		if (jModelFullV2.getSequence() == null || jModelFullV2.getSequence().equals(0L))
			throw new ValidationException(-1, "MODEL_SEQUENCE_MISSING");

		if (jModelFullV2.getDescription_it() == null || jModelFullV2.getDescription_it().trim().equals(""))
			throw new ValidationException(-1, "MODEL_DESCRIPTION_IT");
		if (jModelFullV2.getDescription_en() == null || jModelFullV2.getDescription_en().trim().equals(""))
			throw new ValidationException(-1, "MODEL_DESCRIPTION_EN");
		if (jModelFullV2.getCode() == null || jModelFullV2.getCode().trim().equals(""))
			throw new ValidationException(-1, "MODEL_CODE_MISSING");

		if (jModelFullV2.getIdImageHome() != null) {
			if (!mediaRepository.exists(jModelFullV2.getIdImageHome()))
				throw new ValidationException(-1, "MODEL_IMAGE_MISSING");
		}

		// se immissione il codice non deve esistere
		if (requestMethod == RequestMethod.POST) {
			List<Model> models = modelRepository.findByCode(jModelFullV2.getCode());
			AuditContext.filterActive(models);
			if (!models.isEmpty())
				throw new ValidationException(-1, "MODEL_CODE_DUPLICATE");
		}
		// se modifica puo' esistere ma deve avere lo stesso ID
		if (requestMethod == RequestMethod.PUT) {
			List<Model> models = modelRepository.findByCode(jModelFullV2.getCode());
			AuditContext.filterActive(models);
			Model model = null;
			if (models.size() > 1) {
				throw new ValidationException(-1, "MODEL_CODE_DUPLICATE");
			} else if (models.isEmpty()) {
				throw new ValidationException(-1, "MODEL_CODE_MISSING");
			}
			model = models.get(0);
			if ((model != null) && (!model.getId().equals(jModelFullV2.getId())))
				throw new ValidationException(-1, "MODEL_CODE_DUPLICATE");
		}

	}

	private JModelFullV2 convertFromJEntityV2(HashMap<String, Object> dto)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		JModelFullV2 jmodelFullV2 = objectMapper.readValue(objectMapper.writeValueAsString(dto), JModelFullV2.class);
		return jmodelFullV2;
	}

	private JModelFull convertFromJEntity(HashMap<String, Object> dto)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		JModelFull jmodelFull = objectMapper.readValue(objectMapper.writeValueAsString(dto), JModelFull.class);
		return jmodelFull;
	}

	public List<JValueText> getModelCluster(LocalizationType language) {

		List<JValueText> lJValueText = new ArrayList<JValueText>();
		JValueText jValueText;

		for (ModelCluster modelCluster : modelClusterRepository.findAll()) {
			jValueText = new JValueText(modelCluster.getId().toString(),
					modelCluster.getDescription(language.getValue()));
			lJValueText.add(jValueText);
		}

		return lJValueText;
	}

	public JModel buildFromModel(LocalizationType language, Model model, Version version) {
		JModel jModel = new JModel();

		jModel.setId(model.getId());
		jModel.setDescription(model.getDescription(language.getValue()));
		jModel.setCode(model.getCode());
		jModel.setSeq(model.getSequence());
		jModel.setIdImage(model.getHomepageImage());
		jModel.setIdImageCover(model.getCoverImage());
		jModel.setIdImageLogo(model.getLogoImage());

		jModel.setUrlImage(mediaService.getThronUrl(model.getHomepageImage(), MEDIA_MODEL_TEMPLATE_HOME));
		jModel.setUrlImageCover(mediaService.getThronUrl(model.getCoverImage(), MEDIA_MODEL_TEMPLATE_COVER));
		jModel.setUrlImageLogo(mediaService.getThronUrl(model.getLogoImage(), MEDIA_MODEL_TEMPLATE_LOGO));

		jModel.setMediaTemplateImage(MEDIA_MODEL_TEMPLATE_HOME);
		jModel.setMediaTemplateImageLogo(MEDIA_MODEL_TEMPLATE_LOGO);
		jModel.setMediaTemplateImageCover(MEDIA_MODEL_TEMPLATE_COVER);

		jModel.setVisibility(model.getVisibility());

//		if (version!=null) {
//			jModel.setConfidential(vModelService.getConfidentialStatus(model, version));
//		} else {
		jModel.setConfidential(vModelService.getConfidentialStatus(model));
//		}

		jModel.setQuality(model.getIdQuality());

		return jModel;
	}

}
