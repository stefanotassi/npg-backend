package com.ferrari.modis.dpg.service.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ferrari.modis.dpg.commons.i18n.LocalizationType;
import com.ferrari.modis.dpg.jentity.app.JVersion;
import com.ferrari.modis.dpg.jentity.base.JEntity;
import com.ferrari.modis.dpg.jentity.base.JPage;
import com.ferrari.modis.dpg.jentity.base.JPageAuth;
import com.ferrari.modis.dpg.jentity.base.JPageData;
import com.ferrari.modis.dpg.model.app.Model;
import com.ferrari.modis.dpg.model.app.ModelOptionalType;
import com.ferrari.modis.dpg.model.app.Version;
import com.ferrari.modis.dpg.model.app.VersionStatus;
import com.ferrari.modis.dpg.model.base.AuditContext;
import com.ferrari.modis.dpg.model.ui.UiPage;
import com.ferrari.modis.dpg.repository.ModelOptionalTypeRepository;
import com.ferrari.modis.dpg.repository.ModelRepository;
import com.ferrari.modis.dpg.repository.UiPageRepository;
import com.ferrari.modis.dpg.repository.VersionOptRepository;
import com.ferrari.modis.dpg.repository.VersionRepository;
import com.ferrari.modis.dpg.repository.VersionStatusRepository;
import com.ferrari.modis.dpg.service.CommonService;
import com.ferrari.modis.dpg.service.ModelService;
import com.ferrari.modis.dpg.service.UiService;
import com.ferrari.modis.dpg.service.VModelService;
import com.ferrari.modis.dpg.service.VersionService;

import io.swagger.api.NotFoundException;
import io.swagger.api.ValidationException;

@Service

public class VersionServiceImpl implements VersionService{

	private static final Logger logger = LoggerFactory.getLogger(VersionServiceImpl.class);
	
	@Autowired
	VersionRepository versionRepository;
	@Autowired	
	VersionStatusRepository versionStatusRepository;
	@Autowired
	UiPageRepository uiPageRepository;
	@Autowired
	VersionOptRepository versionOptRepository;
	
	
	@Autowired
	ModelOptionalTypeRepository modelOptionalTypeRepository;
	
	@Autowired
	ModelRepository modelRepository;
	@Autowired
	UiService uiService;
	@Autowired
	VModelService vModelService;
	
	
	@Autowired
	private ModelService modelService;
	
	@Autowired
	CommonService commonService;


	@Override
	public void createDefaultVersion(Long idModelOptionalType,Boolean forceCreation) {
		// controlla lo stato della versione
		// se non ci sono versioni --> crea default Draft
		// se non ha una versione Current --> crea Draft
		
		List<Version> lVersion = versionRepository.findByIdModOpt(idModelOptionalType);	
		Boolean createDraft = Boolean.FALSE;
		if (lVersion.size()==0){
			createDraft=Boolean.TRUE;
		} else {
		}

		if (forceCreation || createDraft){
			// determina il codice versione V1, V2... Vn
			String kCodeNew = buildUniqueVersionCode(idModelOptionalType);
			
			if (kCodeNew!=null){
				Version version = new Version();
				version.setCode(kCodeNew);
				version.setDescription_en("Automatically created");
				version.setDescription_it("Creata automaticamente");
				version.setDescriptionLong_en("");
				version.setDescriptionLong_it("");
				
				version.setIdModelOptionalType(idModelOptionalType);
				version.setStatus(STATUS_DRAFT);
				versionRepository.save(version);
			}
		}
	}	
	
	
	public Version getDefaultVersion(Long idModelOptionalType){

		Version versionDraft = versionRepository.findVersionByStatus(idModelOptionalType, STATUS_DRAFT);
		Version versionPublished = versionRepository.findVersionByStatus(idModelOptionalType, STATUS_PUBLISHED);
		
		// se HQ* propongoi versione DRAFT, altrimenti Verions pubblicat (che puo' non esistere)
		// permission 
		
		// se accede a draft propongo Dravft version
		
		if ("Y".equals(uiService.getAuthorizationByVersionStatus(STATUS_DRAFT))) {
			if (versionDraft!=null)
				return versionDraft;
		}
		
				
		return versionPublished;
	}
	
	
	
	
	public List<JVersion> getVersions(LocalizationType language, Long idModelOptionalType){
		
		//  elenca le sole versioni accessibili per l'utente
		
		List<JVersion> lJVersion = new ArrayList<JVersion>();
		for (Version version:versionRepository.findByIdModOpt(idModelOptionalType)){

			String enable = uiService.getAuthorizationByVersionStatus(version.getStatus()); 
			
			
			if (("Y").equals(enable)){
				JVersion jVersion = buildFromVersion(language,  version );
				lJVersion.add(jVersion);
			}
		}
		return lJVersion;
		
	}
	
	
	
	

// le permission per tipo riga sono queste
//					VE-CL	Version CLONE
//					VE-DE	Version Delete
//					VE-PU	Version Publish
//					VE-UP	Version Modify
// dipendentemente dal tipo riga e dalla situazione imposta le permission

				
	public JPage getVersion(LocalizationType language , Long idVersion){
		
//		Version version = versionRepository.findById(idVersion);
//		if (version==null)
//			throw new ValidationException(-1,"VERSION_NOT_FOUND");
//		
//		if (!isAccessibileByStatus(idVersion))
//			throw new ValidationException(-1,"VERSION_NOT_ACCESSIBLE");
		
		
		Version version = versionCheck(idVersion);

		JVersion jVersion = buildFromVersion(language,  version );
		
		
		JPage jpage = new JPage();
		JPageData jPageData = new JPageData();
		jPageData.setActual(jVersion);
		// header JPage ------------------------------------------------------------
		UiPage uiPage = uiPageRepository.findByKey(UiService.PAGE_VERSIONS);
		jpage.setId(uiPage.getId());
		jpage.setName(uiPage.getDescription());
		jpage.setLanguage(language);
		// aggiunge lista autorizzazioni -------------------------------------------
		List<JPageAuth> lJPageAuth = uiService.getAuthorizationList(UiService.PAGE_VERSIONS);
		jpage.setAuths(lJPageAuth);
		jpage.setData(jPageData);
		
		return jpage;
		


	}
	
	public JPage deleteVersion(LocalizationType language , Long idVersion){

		// se richiesto crea - se necessario - anche un draft vuoto
		Version version = versionRepository.findById(idVersion);
		if (version==null)
			throw new ValidationException(-1,"VERSION_NOT_FOUND");
		
		// permetto a cancellazione alla D
		// ma anceh alla KK (per permettere, se necessario, di ricreare un D
		
		if ((!STATUS_DRAFT.equals(version.getStatus())) && 
			(!STATUS_DELETED.equals(version.getStatus()))){
			throw new ValidationException(-1,"VERSION_NOT_DELETABLE");
		}
		
		version = versionCheck(idVersion);

		
		if (!"Y".equals(retrieveAuthVersionByRequest(version, UiService.AUTH_VERSION_DELETE)))
				throw new ValidationException(-1,"USER_PERMISSION_VERSION_DELETE");
		
		
		version.setStatus(STATUS_DELETED);
		versionRepository.saveAndFlush(version);

		
		// se non esiste versione pubblicata && non esiste versione Draft
		// crea draft
		Version versionDraft = versionRepository.findVersionByStatus(version.getIdModelOptionalType(), STATUS_DRAFT);
		Version versionPublished = versionRepository.findVersionByStatus(version.getIdModelOptionalType(), STATUS_PUBLISHED);
		
		if (versionDraft==null && versionPublished==null) {
			createDefaultVersion(version.getIdModelOptionalType(),Boolean.TRUE);
		}
		
		
		return getVersion(
				language,
				version.getId());
		

	}	
	
	
	
	
	
	
	public JPage updateVersion(LocalizationType language , Long idVersion, JEntity jEntity){
		
		// jEntity � popolato con jVersion
		JVersion jVersion = new JVersion();
		try {
			jVersion = convertFromJEntity(jEntity.getData().getActual());
		} catch (IOException e) {
			throw new  NotFoundException(-1,e.getMessage());
		}
		
		Version version = versionRepository.findById(idVersion);
		if (version==null)
			throw new ValidationException(-1,"VERSION_NOT_FOUND");

		if (!"Y".equals(retrieveAuthVersionByRequest(version, UiService.AUTH_VERSION_UPDATE)))
			throw new ValidationException(-1,"USER_PERMISSION_VERSION_UPDATE");
		
		
		version = versionCheck(idVersion);
		
		
		/////////////////////////////////////////////////////////////////////////////////////////
		if ((jVersion.getCode()==null) || (jVersion.getCode().trim().length()==0))
			throw new ValidationException(-1,"VERSION_CODE_NOT_VALID");
		

		for (Version v2 : versionRepository.findByIdModOptAndCode(version.getIdModelOptionalType(), jVersion.getCode())){
			// se esiste una versione con codice = jVersion.getCode() e differente id --> errore
			
			
			if     ((jVersion.getCode().equals(v2.getCode())) 
				 && (jVersion.getIdModelOptionalType().equals(v2.getIdModelOptionalType()))  
				 && (!jVersion.getId().equals(v2.getId()))  
				 ){
				
				System.out.println(jVersion.getCode()+"-"+v2.getCode());
				System.out.println(jVersion.getIdModelOptionalType() + " "+v2.getIdModelOptionalType());
				System.out.println(jVersion.getId() + " " + v2.getId());
				throw new ValidationException(-1,"VERSION_CODE_ALREADY_EXISTENT");	
			}

		}
		
		if ((jVersion.getDescription_it()==null) || (jVersion.getDescription_it().trim().length()==0))
			throw new ValidationException(-1,"VERSION_DESCRIPTION_IT");
		if ((jVersion.getDescription_en()==null) || (jVersion.getDescription_en().trim().length()==0))
			throw new ValidationException(-1,"VERSION_DESCRIPTION_EN");		
		
		/////////////////////////////////////////////////////////////////////////////////////////		
		
		version.setId(jVersion.getId());
		version.setCode(jVersion.getCode());		
		//version.setIdModelOptionalType(jVersion.getIdModelOptionalType());
		version.setPublishDate(jVersion.getPublishDate());
		version.setPublishUser(jVersion.getPublishUser());
		//version.setStatus(jVersion.getStatus());
		version.setDescription_it(jVersion.getDescription_it());
		version.setDescription_en(jVersion.getDescription_en());
		
		version.setDescriptionLong_it(jVersion.getDescriptionLong_it());
		version.setDescriptionLong_en(jVersion.getDescriptionLong_en());
		
		version.setLastUpdateTimestamp(AuditContext.getCurrentTimestamp());
		version.setLastUpdateUsername(AuditContext.getCurrentUserName());
		
		versionRepository.save(version);
		
		
		return getVersion(
				jEntity.getLanguage(),
				version.getId());
		
		
		
	}	
		
	
	@Transactional
	public JPage publishVersion(LocalizationType language , Long idVersion, JEntity jEntity){

		JVersion jVersion = new JVersion();
		try {
			jVersion = convertFromJEntity(jEntity.getData().getActual());
		} catch (IOException e) {
			throw new  NotFoundException(-1,e.getMessage());
		}
		
		
		
		Version version = versionRepository.findById(idVersion);
		if (version==null)
			throw new ValidationException(-1,"VERSION_NOT_FOUND");
		
		if (!STATUS_DRAFT.equals(version.getStatus()))
			throw new ValidationException(-1,"VERSION_MUST_BE_DRAFT");
		
		if (!"Y".equals(retrieveAuthVersionByRequest(version, UiService.AUTH_VERSION_PUBLISH)))
			throw new ValidationException(-1,"USER_PERMISSION_VERSION_PUBLISH");
		
		// versione attualmente current (se esiste) deve diventare Superata
		Version vold = versionRepository.findVersionByStatus(version.getIdModelOptionalType(), STATUS_PUBLISHED);
		if (vold!=null){
			vold.setStatus(STATUS_SUPERSEDED);
			versionRepository.saveAndFlush(vold);
		}
			
		version.setDescription_en(jVersion.getDescription_en());
		version.setDescription_it(jVersion.getDescription_it());
		version.setDescriptionLong_en(jVersion.getDescriptionLong_en());
		version.setDescriptionLong_it(jVersion.getDescriptionLong_it());
		
		version.setStatus(STATUS_PUBLISHED);
		version.setPublishDate(AuditContext.getCurrentTimestamp());
		version.setPublishUser(AuditContext.getCurrentUserName()); 
		versionRepository.saveAndFlush(version);

		
		return getVersion(
				jEntity.getLanguage(),
				version.getId());

	}	
	
	
	
	
	
	

	public JPage cloneVersion( final Long idVersion, JEntity jEntity){
		//fixme: ora riceve jentity ma di fatto non la utilizza
		// capire se ci mettiamo in mezzo un form che popola i dati basici
		
		JVersion jVersion = new JVersion();
		try {
			jVersion = convertFromJEntity(jEntity.getData().getActual());
		} catch (IOException e) {
			throw new  NotFoundException(-1,e.getMessage());
		}
		
		
		
		// definire chi puo' clonare
		// creo un nuovo D - se non esiste - partendo da C
		Version version = versionRepository.findById(idVersion);
		if (version==null)
			throw new ValidationException(-1,"VERSION_NOT_FOUND");
		
		if (!STATUS_PUBLISHED.equals(version.getStatus())){
			throw new ValidationException(-1,"VERSION_NOT_CLONABLE");
		}
		if (!"Y".equals(retrieveAuthVersionByRequest(version, UiService.AUTH_VERSION_CLONE)))
			throw new ValidationException(-1,"USER_PERMISSION_VERSION_CLONE");
		
		version = versionCheck(idVersion);
		
		// se esiste gi� la versione Draft: impossibile continuare
		Version v2 = versionRepository.findVersionByStatus(version.getIdModelOptionalType(), STATUS_DRAFT);
		if (v2!=null)
			throw new ValidationException(-1,"VERSION_DRAFT_ALREADY_EXISTENT");



		String kCodeNew = null;

		kCodeNew = buildUniqueVersionCode(version.getIdModelOptionalType());
		
		
		Version vnew = new Version();
		vnew.setCode(kCodeNew);
		vnew.setDescription_en(jVersion.getDescription_en());
		vnew.setDescription_it(jVersion.getDescription_it());
		vnew.setDescriptionLong_en(jVersion.getDescriptionLong_en());
		vnew.setDescriptionLong_it(jVersion.getDescriptionLong_it());
		
		vnew.setIdModelOptionalType(version.getIdModelOptionalType());
		vnew.setStatus(STATUS_DRAFT);
		
		versionRepository.save(vnew);
		Long idVersionNew = vnew.getId();
		


		versionRepository.cloneVersion(AuditContext.getCurrentUserName(), idVersion, idVersionNew);			
		//System.out.println("RET="+retCode);
		

		return getVersion(
				jEntity.getLanguage(),
				idVersionNew);


	}				
	
	
	

	public JVersion buildFromVersion(LocalizationType language, Version version ){
		
		//version.getIdModelOptionalType();
		Long idModelOptionalType = version.getIdModelOptionalType();
		ModelOptionalType modelOptionalType = modelOptionalTypeRepository.getOne(idModelOptionalType);
		Model model = modelRepository.getOne(modelOptionalType.getIdModel());
		
		
		
		
		
		JVersion jVersion;
		jVersion = new JVersion();
		jVersion.setId(version.getId());
		jVersion.setCode(version.getCode());
		jVersion.setIdModelOptionalType(version.getIdModelOptionalType());
		jVersion.setPublishDate(version.getPublishDate());
		jVersion.setPublishUser(version.getPublishUser());
		jVersion.setStatus(version.getStatus());
		jVersion.setDescription(version.getDescription(language.getValue()));
		jVersion.setDescription_it(version.getDescription_it());
		jVersion.setDescription_en(version.getDescription_en());
		
		jVersion.setDescriptionLong(version.getDescriptionLong(language.getValue()));
		jVersion.setDescriptionLong_it(version.getDescriptionLong_it());
		jVersion.setDescriptionLong_en(version.getDescriptionLong_en());
		
		
		// confidenziale
		jVersion.setConfidential(vModelService.getConfidentialStatus(version));
		
		VersionStatus versionStatus = versionStatusRepository.findById(version.getStatus());		
		jVersion.setStatusDescription(versionStatus.getDescription(language.getValue()));
		jVersion.setAccessible(isAccessibileByStatus(version.getId())); 
		jVersion.setReadonly(isReadOnly(version.getId()));
		
		List<JPageAuth> authorizations = retrieveAuthVersion(version);
		jVersion.setAuthorizations(authorizations);
		
		return jVersion;
	}	
	
	private JVersion convertFromJEntity(HashMap<String, Object> dto)throws JsonParseException, JsonMappingException, JsonProcessingException, IOException{
 	    ObjectMapper objectMapper = new ObjectMapper();
	    objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
	    JVersion jVersion=objectMapper.readValue(objectMapper.writeValueAsString(dto), JVersion.class);		
	    return jVersion;
	}
	
	
	// chiamato ogni volta che si invoca un servizio per verificare l'accessibilit� alla versione
	public Version versionCheck(Long idVersion){
		Version version = versionRepository.findById(idVersion);
		if (version==null)
			throw new ValidationException(-1,"VERSION_NOT_FOUND");
		if (!isAccessibileByStatus(idVersion))
			throw new ValidationException(-1,"VERSION_NOT_ACCESSIBLE");
		
		
		Model model = commonService.getModelFromVersionId(idVersion);
		if ( model == null)
			throw new ValidationException(-1,"MODEL_NOT_FOUND");
		
		modelService.CheckModel(model.getId());
		return version;
	}
	
	
	public void versionCheckForUpdate(Version version){
		
		// se la versione non � DRAFT non � aggiornabile
		if (!STATUS_DRAFT.equals(version.getStatus())){
				throw new ValidationException(-1,"VERSION_NOT_UPDATABLE");
			}
		
		Long modelTypeId = version.getIdModelOptionalType();
		if (modelTypeId != null) {
			ModelOptionalType modelOptionalType = modelOptionalTypeRepository.findOne(modelTypeId);
			if ( modelOptionalType!= null) {
				Model model = modelRepository.findOne(modelOptionalType.getIdModel());
				Boolean enableAllModels = ("Y".equals(uiService.getAuthorizationById(ModelService.MODEL_DISPLAY_ENABLE)));
				if ((!"Y".equals(model.getVisibility()))&& !enableAllModels) {
					// non visibile: abilitati solo utenti meritevili
					throw new ValidationException(-1,"MODEL_NOT_FOUND");	//AVAILABLE
				}
			}
		}
	}
	
	
	
	private String buildUniqueVersionCode(Long idModelOptionalType){
		String kCodeNew = null;
		for (Integer ii=1; ii<1000;ii++){
			String kCode = "V"+ii.toString();
			List<Version> lv = versionRepository.findByIdModOptAndCode(idModelOptionalType, kCode);
			if (lv.size()==0){
				kCodeNew = kCode;
				break;
			}
		}
		return kCodeNew;
	}
	public Boolean isAccessibileByStatus(Long idVersion){
		// determino lo stato e lo utilizzo per verificare le autorizzazioni
		Version version = versionRepository.findById(idVersion);
		if (version==null)
			return Boolean.FALSE;

		String enable = uiService.getAuthorizationByVersionStatus(version.getStatus()); 
		
		if (("Y").equals(enable))
			return Boolean.TRUE;
		else
			return Boolean.FALSE;
		
		
	}
	
	public Boolean isReadOnly(Long idVersion){
		// determino lo stato e lo utilizzo per verificare le autorizzazioni
		Version version = versionRepository.findById(idVersion);
		if (version==null)
			return Boolean.FALSE;

		// tutti gli stati sono ReadOnly a parte il draft;
		// solo il Draft puo' essere I/O ma solo per il ruolo HQ-FULL
		if (STATUS_DRAFT.equals(version.getStatus())){
			if ("Y".equals(uiService.getAuthorizationById("VER-D"))) {
				return Boolean.FALSE; // false = not readonly = full IO
			}
		}
		return Boolean.TRUE;
	}

	
	private String retrieveAuthVersionByRequest(Version version, String action){
		
		// richiamata da ogni form per riverificare le autorizzazioni già esposte nelle response
		
		for (JPageAuth jPageAuth:retrieveAuthVersion(version)) {

			if (action.equals(jPageAuth.getAuthId())) {
				return jPageAuth.getPermission();
			}				
		}
		return "N";		
	}
	
	
	private List<JPageAuth> retrieveAuthVersion(Version version){
		// prepara la lista delle singole permission sulla singola versione
		List<JPageAuth> authorizations = uiService.getAuthorizationList(UiService.PAGE_VERSIONS_VERSION);
		
		for (JPageAuth jPageAuth :authorizations){
			// se la permission ='Y' posso abbassarla, dipendentemente dalla situazione
			
			if (UiService.AUTH_VERSION_CLONE.equals(jPageAuth.getAuthId())){

				// posso clonare solo se la riga � Consolidata e non esiste un Draft
				if (!STATUS_PUBLISHED.equals(version.getStatus())) {
					// se non consolidata
					jPageAuth.setPermission("N");
					jPageAuth.setAuthDescription(jPageAuth.getAuthDescription() + " [NoClone in NOT Consolidated Version]");
				} else {

					// se consolidata NON deve esserci una Draft
					Version vDraft = versionRepository.findVersionByStatus(version.getIdModelOptionalType(),
							STATUS_DRAFT);
					if (vDraft != null) {
						jPageAuth.setAuthDescription(jPageAuth.getAuthDescription() + " [NoClone: already Draft Version present]");
						jPageAuth.setPermission("N");
					}

				}	
				
			}
			if (UiService.AUTH_VERSION_DELETE.equals(jPageAuth.getAuthId())){
					if (!STATUS_DRAFT.equals(version.getStatus())){
						//VE-DE	Version Delete
						// posso cancellare solo se la riga � Draft
						jPageAuth.setAuthDescription(jPageAuth.getAuthDescription()+" [NoDelete: Version is not Draft]");
						jPageAuth.setPermission("N");
					}
				
			}					
			if (UiService.AUTH_VERSION_PUBLISH.equals(jPageAuth.getAuthId())){
					//VE-PU	Version publish
				// posso pubblicare solo se la riga � Draft
					if (!STATUS_DRAFT.equals(version.getStatus())){
						jPageAuth.setAuthDescription(jPageAuth.getAuthDescription()	+" [NoPublish: Version is not Draft]");
						jPageAuth.setPermission("N");
					}
			}					
			if (UiService.AUTH_VERSION_UPDATE.equals(jPageAuth.getAuthId())){
				// Update, solo per Draft
					if (!STATUS_DRAFT.equals(version.getStatus())){
						jPageAuth.setAuthDescription(jPageAuth.getAuthDescription()	+" [Update: Version is not Draft]");
						jPageAuth.setPermission("N");
					}
			}
			
			
		}

		return authorizations;
	}
	
	
	
}
