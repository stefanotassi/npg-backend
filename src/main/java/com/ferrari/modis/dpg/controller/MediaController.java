package com.ferrari.modis.dpg.controller;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.ferrari.modis.dpg.jentity.app.JMedia;
import com.ferrari.modis.dpg.jentity.base.JPage;
import com.ferrari.modis.dpg.service.CommonService;
import com.ferrari.modis.dpg.service.MediaService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.api.NotFoundException;

 
@Controller
@RequestMapping(value = "/media/V1", produces = { APPLICATION_JSON_VALUE })
@Api(value = "/media/V1", description = "Media files API-V1")

public class MediaController {
	@SuppressWarnings("unused")
	private static final Logger logger = LoggerFactory.getLogger( MediaController.class );

	@Autowired
	private MediaService mediaService;		
	@Autowired
	private CommonService commonService;		
	
	
	
	// UPLOAD
	@ApiOperation(value = "Media - upload File", notes = "Media - upload File")
  	@RequestMapping(value="/", method=RequestMethod.POST,consumes = org.springframework.http.MediaType.MULTIPART_FORM_DATA_VALUE)
 
 	public @ResponseBody ResponseEntity<JPage> 
  		uploadMediaFile(@RequestParam("files") MultipartFile files

  						, HttpServletRequest req) throws IOException, NoSuchAlgorithmException{	
				
  		MultipartFile uploadFile=files;
  		JPage jPage=mediaService.createMediaFile(null, uploadFile ,req);
		
		return new ResponseEntity<JPage>(jPage, HttpStatus.OK);
 	 } 	 	
  	
	
	// UPLOAD
	@ApiOperation(value = "Media - downloadFiel File", notes = "Media - upload File")
  	@RequestMapping(value="/download/{idMedia}", method=RequestMethod.GET)
 
 	public void  
  		downloadMediaFile(@ApiParam(value = "idMedia"			,required=true ) @PathVariable("idMedia") String idMedia

  						, HttpServletRequest req, HttpServletResponse res) throws IOException, NoSuchAlgorithmException{	
		
		mediaService.downlaodSingle(idMedia, res);
		
 	 } 	 	
  	
  
	
	
	
	
	
	
	// funzioni di servizio ///////////////////////////////////////////////////////////////////////////////////////////
	@ApiOperation(value = "Media - template for File upload", notes = "Media - temnplate for File upload", response = JMedia.class)
	@ApiResponses(value = { 
			@ApiResponse(code = 200, message = "Ok"),
			@ApiResponse(code = 500, message = "Server Error") })
	@RequestMapping(
		  value = {"/template/"} 
		 ,produces = { "application/json" }
		 ,method = RequestMethod.GET)

	public ResponseEntity<JMedia> getMediaTemplate()
			 throws NotFoundException {
		return new ResponseEntity<JMedia>(mediaService.getMediaTemplate(),HttpStatus.OK);
	}
	
	
	@ApiOperation(value = "Media - parameters for upload by MediaType", notes = "Media - parameters for upload by MediaType", response = JPage.class)
	@ApiResponses(value = { 
			@ApiResponse(code = 200, message = "Ok"),
			@ApiResponse(code = 500, message = "Server Error") })
	@RequestMapping(
		  value = {"/{mediaType}"} 
		 ,produces = { "application/json" }
		 ,method = RequestMethod.GET)

	public ResponseEntity<JPage> getMediaParameters( 
			 		@ApiParam(value = "Language IT_IT|EN_US"	)  			 @RequestHeader(value=CommonService.REQUEST_HEADER_LANG ,required=false )  String hlang
					,@ApiParam(value = "Media type"			,required=true ) @PathVariable("mediaType") String mediaType 

			)
			 throws NotFoundException {
		return new ResponseEntity<JPage>(mediaService.getMediaParameters(commonService.getLanguage(hlang), mediaType),HttpStatus.OK);
	}



	@ApiOperation(value = "Media - refresh and get URL by ID (for test and debug)", notes = "Media - refresh and get URL by ID (for test and debug)", response = String.class)
	@ApiResponses(value = { 
			@ApiResponse(code = 200, message = "Ok"),
			@ApiResponse(code = 500, message = "Server Error") })
	@RequestMapping(
		  value = {"/url/{id}/{mediaType}"} 
		 ,produces = { MediaType.TEXT_PLAIN }
		 ,method = RequestMethod.GET)

	public ResponseEntity<String> getUrlById( 
			@ApiParam(value = "id"			,required=true ) @PathVariable("id") String id 
		   ,@ApiParam(value = "Media type"			,required=false ) @PathVariable("mediaType") String mediaType )
			 throws NotFoundException {
		return new ResponseEntity<String>(mediaService.getThronUrl(id, mediaType),HttpStatus.OK);
	} 
	

	
	

}
