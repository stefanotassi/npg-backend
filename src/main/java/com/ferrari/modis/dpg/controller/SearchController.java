package com.ferrari.modis.dpg.controller;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.ferrari.modis.dpg.jentity.base.JPage;
import com.ferrari.modis.dpg.service.CommonService;
import com.ferrari.modis.dpg.service.SearchService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.api.NotFoundException;




@Controller
@RequestMapping(value = "/search/V1", produces = { APPLICATION_JSON_VALUE })
@Api(value = "/search/V1", description = "Version Model API-V1")
public class SearchController {

	@SuppressWarnings("unused")
	private static final Logger logger = LoggerFactory.getLogger( SearchController.class );
	
	
	@Autowired
	SearchService searchService;
	@Autowired
	private CommonService commonService;
	
	

		
	@ApiOperation(value = "Ricerca (ristretta su Versione Corrente, OptType Corrente)", notes = "Ricerca per Versione", response = JPage.class)
	@ApiResponses(value = { 
			@ApiResponse(code = 200, message = "Ok"),
			@ApiResponse(code = 500, message = "Server Error") })
	@RequestMapping(
		  value = {"/V/{optionalType}/{version}"} 
		 ,produces = { APPLICATION_JSON_VALUE }
		 ,method = RequestMethod.GET)
	
	public ResponseEntity<JPage> getSearchMV( 
		  @ApiParam(value = "Language IT_IT|EN_US"	)  			 	 @RequestHeader(value=CommonService.REQUEST_HEADER_LANG ,required=false )  String hlang
		 ,@ApiParam(value = "P1|P2"					,required=true ) @PathVariable("optionalType") 		String optionalType 
		 ,@ApiParam(value = "Versione"				,required=true ) @PathVariable("version") 			Long version
		 ,@ApiParam(value = "Search Text"			,required=true ) @RequestParam("searchText") 				String searchText
		 
		 
			) throws NotFoundException {
		
		return new ResponseEntity<JPage>(searchService.getListByVersion(commonService.getLanguage(hlang), optionalType,  version, searchText),HttpStatus.OK);
	}
	
	
	
	
	@ApiOperation(value = "Ricerca (FULL da Homepage (restituisce mod, vers, Px))", notes = "Ricerca Full", response = JPage.class)
	@ApiResponses(value = { 
			@ApiResponse(code = 200, message = "Ok"),
			@ApiResponse(code = 500, message = "Server Error") })
	@RequestMapping(
		  value = {"/VHome"} 
		 ,produces = { APPLICATION_JSON_VALUE }
		 ,method = RequestMethod.GET)
	
	public ResponseEntity<JPage> getSearchMVHome( 
		  @ApiParam(value = "Language IT_IT|EN_US"	)  			 	 @RequestHeader(value=CommonService.REQUEST_HEADER_LANG ,required=false )  String hlang
		 ,@ApiParam(value = "Search Text"			,required=true ) @RequestParam("searchText") 				String searchText
	
			) throws NotFoundException {
		return new ResponseEntity<JPage>(searchService.getListFull(commonService.getLanguage(hlang), searchText),HttpStatus.OK);
	}
	
	
	@ApiOperation(value = "Ricerca (Sul modello da Splash page (restituisce versioni e Px))", notes = "Ricerca Modello", response = JPage.class)
	@ApiResponses(value = { 
			@ApiResponse(code = 200, message = "Ok"),
			@ApiResponse(code = 500, message = "Server Error") })
	@RequestMapping(
		  value = {"/VSplash/{idModel}"} 
		 ,produces = { APPLICATION_JSON_VALUE }
		 ,method = RequestMethod.GET)
	
	public ResponseEntity<JPage> getSearchMVSplah( 
		  @ApiParam(value = "Language IT_IT|EN_US"	)  			 	 @RequestHeader(value=CommonService.REQUEST_HEADER_LANG ,required=false )  String hlang
		 ,@ApiParam(value = "IdModel"				,required=true ) @PathVariable("idModel") 			Long idModel
		 ,@ApiParam(value = "Search Text"			,required=true ) @RequestParam("searchText") 				String searchText
		 
		 
			) throws NotFoundException {
		
		return new ResponseEntity<JPage>(searchService.getListByModel(commonService.getLanguage(hlang), idModel, searchText),HttpStatus.OK);
	}
	
		
	
	
	
	
	
	
	
	
}
