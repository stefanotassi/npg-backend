package com.ferrari.modis.dpg.controller;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.ferrari.modis.dpg.jentity.base.JPage;
import com.ferrari.modis.dpg.jentity.base.JValueText;
import com.ferrari.modis.dpg.service.CommonService;
import com.ferrari.modis.dpg.service.DataHelperService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.api.NotFoundException;

// /api/list/{language}/{contextId}





@Controller
@RequestMapping(value = "/api/V1", produces = { APPLICATION_JSON_VALUE })
@Api(value = "/api/V1", description = "Basic Api API-V1")
public class DataHelperController {
	
	@SuppressWarnings("unused")
	private static final Logger logger = LoggerFactory.getLogger(DataHelperController.class);
	
	@Autowired
	DataHelperService dataHelperService;
	@Autowired
	private CommonService commonService;	
	
	
	@ApiOperation(value = "Returns base list values", notes = "", response = JValueText.class)
	  @io.swagger.annotations.ApiResponses(value = { 
	    @io.swagger.annotations.ApiResponse(code = 200, message = "Form validation rules"),
	    @io.swagger.annotations.ApiResponse(code = 500, message = "Server Error") })
	  @RequestMapping(value = {"/list/{contextId}"},
	  produces = { "application/json" },method = RequestMethod.GET)
	  public ResponseEntity<List<JValueText>> listGet(
			   @ApiParam(value = "Language IT_IT|EN_US"	)  			 	 @RequestHeader(value=CommonService.REQUEST_HEADER_LANG ,required=false )  String hlang
			 , @ApiParam(value = "contextId",required=true ) @PathVariable("contextId") String contextId

			  
			  )
	      throws NotFoundException {
	      return new ResponseEntity<List<JValueText>>(dataHelperService.getValueTextList(commonService.getLanguage(hlang), contextId),HttpStatus.OK);
	  } 

	  // servizio che restituisce le sole permission dato un codice form
	  // viene utilizzato per ora solo su form HH per restituire le abilitazioni PDF/XLS download
	
	@ApiOperation(value = "Returns permissions for page id", notes = "", response = JPage.class)
	  @io.swagger.annotations.ApiResponses(value = { 
	    @io.swagger.annotations.ApiResponse(code = 200, message = "Form validation rules"),
	    @io.swagger.annotations.ApiResponse(code = 500, message = "Server Error") })
	  @RequestMapping(value = {"/permissions/{pageId}"},
	  produces = { "application/json" },method = RequestMethod.GET)
	  public ResponseEntity<JPage> listPermissions(
			   @ApiParam(value = "Language IT_IT|EN_US"	)  	 @RequestHeader(value=CommonService.REQUEST_HEADER_LANG ,required=false )  String hlang
			 , @ApiParam(value = "pageId",required=true ) @PathVariable("pageId") String pageId

			  
			  )
	      throws NotFoundException {
	      return new ResponseEntity<JPage>(dataHelperService.getPermissionList(commonService.getLanguage(hlang), pageId),HttpStatus.OK);
	  } 	
	
	
	// lista opt da ERP
	@ApiOperation(value = "Returns OPTIONALS List", notes = "", response = JValueText.class)
	  @io.swagger.annotations.ApiResponses(value = { 
	    @io.swagger.annotations.ApiResponse(code = 200, message = "Form validation rules"),
	    @io.swagger.annotations.ApiResponse(code = 500, message = "Server Error") })
	  @RequestMapping(value = {"/optionals"},
	  produces = { "application/json" },method = RequestMethod.GET)
		public ResponseEntity<List<JValueText>>  listOptionalsErp(
			   @ApiParam(value = "Language IT_IT|EN_US"	)  	 @RequestHeader(value=CommonService.REQUEST_HEADER_LANG ,required=false )  String hlang

			  )
	      throws NotFoundException {
		
	      return new ResponseEntity<List<JValueText>>(dataHelperService.getOptionalsList(commonService.getLanguage(hlang)),HttpStatus.OK);
	  } 	
	
	
	
	
}
