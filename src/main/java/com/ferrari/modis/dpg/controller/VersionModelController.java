package com.ferrari.modis.dpg.controller;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.ferrari.modis.dpg.commons.i18n.DocumentType;
import com.ferrari.modis.dpg.jentity.app.JVersionAttcForm;
import com.ferrari.modis.dpg.jentity.app.JVersionOptForm;
import com.ferrari.modis.dpg.jentity.base.JEntity;
import com.ferrari.modis.dpg.jentity.base.JPage;
import com.ferrari.modis.dpg.service.CommonService;
import com.ferrari.modis.dpg.service.DocService;
import com.ferrari.modis.dpg.service.VModelService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.api.ApiResponseMessage;
import io.swagger.api.NotFoundException;
import io.swagger.api.ValidationException;


@Controller
@RequestMapping(value = "/vmodel/V1", produces = { APPLICATION_JSON_VALUE })
@Api(value = "/vmodel/V1", description = "Version Model API-V1")
public class VersionModelController {

	@SuppressWarnings("unused")
	private static final Logger logger = LoggerFactory.getLogger( VersionModelController.class );
	
	//private static final String MIME_APPLICATION_EXCEL 	= "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

	
	
	
	@Autowired
	private VModelService vModelService;

	@Autowired
	private DocService docService;
	
	@Autowired
	private CommonService commonService;
	
	
	@ApiOperation(value = "Form scelta percorso/versione", notes = "Form scelta percorso/versione", response = JPage.class)
	@ApiResponses(value = { 
			@ApiResponse(code = 200, message = "Ok"),
			@ApiResponse(code = 500, message = "Server Error") })
	@RequestMapping(
		  value = {"/version/{optionalType}/{idmodel}/{version}"} 
		 ,produces = { APPLICATION_JSON_VALUE }
		 ,method = RequestMethod.GET)
	
	public ResponseEntity<JPage> getFormByModelAndVersion( 
		  @ApiParam(value = "Language IT_IT|EN_US"	)  			 @RequestHeader(value=CommonService.REQUEST_HEADER_LANG ,required=false )  String hlang
		 ,@ApiParam(value = "P1|P2"							,required=true ) @PathVariable("optionalType") 		String optionalType 
		 ,@ApiParam(value = "id Modello"					,required=true ) @PathVariable("idmodel") 			Long idmodel 
		 ,@ApiParam(value = "Versione"						,required=true ) @PathVariable("version") 			Long version 
			) throws NotFoundException {

		return new ResponseEntity<JPage>(vModelService.getFormByVersion(commonService.getLanguage(hlang), optionalType, idmodel, version),HttpStatus.OK);
	}
		
	

	@ApiOperation(value = "Form scelta percorso/versione vers=calc", notes = "Form scelta percorso/versione vers=calc", response = JPage.class)
	@ApiResponses(value = { 
			@ApiResponse(code = 200, message = "Ok"),
			@ApiResponse(code = 500, message = "Server Error") })
	@RequestMapping(
		  value = {"/version/{optionalType}/{idmodel}"} 
		 ,produces = { APPLICATION_JSON_VALUE }
		 ,method = RequestMethod.GET)
	
	public ResponseEntity<JPage> getFormByModel( 
			@ApiParam(value = "Language IT_IT|EN_US"	)  			 @RequestHeader(value=CommonService.REQUEST_HEADER_LANG ,required=false )  String hlang
		 ,@ApiParam(value = "P1|P2"							,required=true ) @PathVariable("optionalType") 		String optionalType 
		 ,@ApiParam(value = "id Modello"					,required=true ) @PathVariable("idmodel") 			Long idmodel 

			) throws NotFoundException {
		
		return new ResponseEntity<JPage>(vModelService.getFormByModel(commonService.getLanguage(hlang), optionalType, idmodel),HttpStatus.OK);
		// restituisce la versione corrente, calcolata con algoritmo
	}

	
	
	@ApiOperation(value = "Lista optionals", notes = "Lista optionals", response = JPage.class)
	@ApiResponses(value = { 
			@ApiResponse(code = 200, message = "Ok"),
			@ApiResponse(code = 500, message = "Server Error") })
	@RequestMapping(
		  value = {"/version/Optional/{version}"} 
		 ,produces = {APPLICATION_JSON_VALUE}
		 ,method = RequestMethod.GET)
	
	public ResponseEntity<JPage> getOptionalsByVersion( 
			@ApiParam(value = "Language IT_IT|EN_US"	)  			 @RequestHeader(value=CommonService.REQUEST_HEADER_LANG ,required=false )  String hlang
		,@ApiParam(value = "Versione"						,required=true ) @PathVariable("version") 			Long version 
			) throws NotFoundException {
		return new ResponseEntity<JPage>(vModelService.getOptionalList(commonService.getLanguage(hlang), version),HttpStatus.OK);
	}
	
	
	
	
	
	@ApiOperation(value = "Build XLSX optionals", notes = "Build XLSX optionals")
	@ApiResponses(value = { 
			@ApiResponse(code = 200, message = "Ok"),
			@ApiResponse(code = 500, message = "Server Error") })
	@RequestMapping(
		  value = {"/version/download/OptionalXLS/{version}/{xlsType}"} 
		 ,produces = {DocService.MIME_APPLICATION_XLSX }
		 ,method = RequestMethod.POST)
	
	// JEntity � il json scaricato da getOptionalsByVersion, compilato nelle righe "choice"
	public void CreateXSLXOptionalsByVersion( 
 		 @ApiParam(value = "",required=true ) @RequestBody  JEntity jEntity
		,@ApiParam(value = "Language IT_IT|EN_US"	)  			 			 @RequestHeader(value=CommonService.REQUEST_HEADER_LANG ,required=false )  String hlang
		,@ApiParam(value = "Versione"						,required=true ) @PathVariable("version") 			Long version
		,@ApiParam(value = "BASIC|FULL"						,required=true ) @PathVariable("xlsType") 			String xlsType
		
		,HttpServletResponse response
			) throws NotFoundException, IOException {

		JVersionOptForm jVersionOptForm=null; // da entity
		try {
			jVersionOptForm = vModelService.convertToJVersionOptForm(jEntity.getData().getActual());
		} catch (IOException e) {
			throw new  NotFoundException(-1,e.getMessage());
		}
				
		docService.createXlsxOptionalsByVersion(jVersionOptForm, commonService.getLanguage(hlang), version, xlsType, response);
		return;
	}	
	
	
	

	
	
	@ApiOperation(value = "Build PDF optionals", notes = "Build PDF optionals")
	@ApiResponses(value = { 
			@ApiResponse(code = 200, message = "Ok"),
			@ApiResponse(code = 500, message = "Server Error") })
	@RequestMapping(
		  value = {"/version/download/OptionalPDF/{version}"} 
		 ,produces = {DocService.MIME_APPLICATION_PDF,MediaType.APPLICATION_JSON }
		 ,method = RequestMethod.POST)
	
	// JEntity è il json scaricato da getOptionalsByVersion, compilato nelle righe "choice"
	public void CreatePDFptionalsByVersion( 
 		 @ApiParam(value = "",required=true ) @RequestBody  	JEntity jEntity
		,@ApiParam(value = "Language IT_IT|EN_US"	)  			 @RequestHeader(value=CommonService.REQUEST_HEADER_LANG ,required=false )  String hlang
		,@ApiParam(value = "Versione"			,required=true ) @PathVariable("version") 			Long version
		,HttpServletResponse response)
			 throws NotFoundException, IOException {

		JVersionOptForm jVersionOptForm=null; // da entity
		try {
			jVersionOptForm = vModelService.convertToJVersionOptForm(jEntity.getData().getActual());
		} catch (IOException e) {
			throw new  NotFoundException(-1,e.getMessage());
		}
		

		docService.createPdfOptionalsByVersion(jVersionOptForm, commonService.getLanguage(hlang), version, null,  response);

	}	
	

	@ApiOperation(value = "Build PDF optionals V2", notes = "Build PDF optionals V2")
	@ApiResponses(value = { 
			@ApiResponse(code = 200, message = "Ok"),
			@ApiResponse(code = 500, message = "Server Error") })
	@RequestMapping(
		  value = {"/version/download/OptionalPDF/{version}/{forceDealerMode}"} 
		 ,produces = {DocService.MIME_APPLICATION_PDF,MediaType.APPLICATION_JSON }
		 ,method = RequestMethod.POST)
	
	// JEntity è il json scaricato da getOptionalsByVersion, compilato nelle righe "choice"
	public void CreatePDFptionalsByVersionAndMode( 
 		 @ApiParam(value = "",required=true ) @RequestBody  	JEntity jEntity
		,@ApiParam(value = "Language IT_IT|EN_US"	)  			 @RequestHeader(value=CommonService.REQUEST_HEADER_LANG ,required=false )  String hlang
		,@ApiParam(value = "Versione"			,required=true ) @PathVariable("version") 	Long version
		,@ApiParam(value = "Force Dealer Mode blank|DEALER"	,required=true ) @RequestParam("forceDealerMode") 		String forceDealerMode
		,HttpServletResponse response)
			 throws NotFoundException, IOException {

		JVersionOptForm jVersionOptForm=null; // da entity
		try {
			jVersionOptForm = vModelService.convertToJVersionOptForm(jEntity.getData().getActual());
		} catch (IOException e) {
			throw new  NotFoundException(-1,e.getMessage());
		}
		

		docService.createPdfOptionalsByVersion(jVersionOptForm, commonService.getLanguage(hlang), version, forceDealerMode, response);

	}	
		
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	@ApiOperation(value = "Build PDF optionals da UI", notes = "Build PDF optionals da UI" , response = JPage.class)
	@ApiResponses(value = { 
			@ApiResponse(code = 200, message = "Ok"),
			@ApiResponse(code = 500, message = "Server Error") })
	@RequestMapping(
		  value = {"/version/download/OptionalPDFUI/{version}"} 
		 ,produces = {MediaType.APPLICATION_JSON }
		 ,method = RequestMethod.POST)
	
	// JEntity � il json scaricato da getOptionalsByVersion, compilato nelle righe "choice"
	public ResponseEntity<JPage> CreatePDFptionalsByVersionUI( 
 		 @ApiParam(value = "",required=true ) @RequestBody  JEntity jEntity
		,@ApiParam(value = "Language IT_IT|EN_US"	)  			 @RequestHeader(value=CommonService.REQUEST_HEADER_LANG ,required=false )  String hlang
		,@ApiParam(value = "Versione"						,required=true ) @PathVariable("version") 			Long version
		,HttpServletResponse response
			)
			 throws NotFoundException, IOException {

		JVersionOptForm jVersionOptForm=null; // da entity
		try {
			jVersionOptForm = vModelService.convertToJVersionOptForm(jEntity.getData().getActual());
		} catch (IOException e) {
			throw new  NotFoundException(-1,e.getMessage());
		}
		
		return new ResponseEntity<JPage>(
				docService.createPdfOptionalsByVersionUI(jVersionOptForm, commonService.getLanguage(hlang), version)
				,HttpStatus.OK);

	}	
	

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	@ApiOperation(value = "Get Optional", notes = "Get Optional", response = JPage.class)
	@ApiResponses(value = { 
			@ApiResponse(code = 200, message = "Ok"),
			@ApiResponse(code = 500, message = "Server Error") })
	@RequestMapping(
		  value = {"/version/Optional/{version}/{optional}"} 
		 ,produces = { APPLICATION_JSON_VALUE }
		 ,method = RequestMethod.GET)
	
	public ResponseEntity<JPage> getOptional( 
			@ApiParam(value = "Language IT_IT|EN_US"	)  			@RequestHeader(value=CommonService.REQUEST_HEADER_LANG ,required=false )  String hlang
		   ,@ApiParam(value = "Versione"		,required=true ) 	@PathVariable("version") 			Long version 
		   ,@ApiParam(value = "Optional"		,required=true ) 	@PathVariable("optional") 			Long idOptional 
		)
			 throws NotFoundException {
		return new ResponseEntity<JPage>(vModelService.getOptional(commonService.getLanguage(hlang), version, idOptional),HttpStatus.OK);
	}

	
	
	
	@ApiOperation(value = "Update Optional", notes = "Update Optional", response = JPage.class)
	@ApiResponses(value = { 
	    @ApiResponse(code = 200, message = "ok"),
	    @ApiResponse(code = 500, message = "Server Error") })
	
	@RequestMapping(  value = {"/version/Optional/{version}/{optional}"} 
					, produces = { APPLICATION_JSON_VALUE }
					, method = RequestMethod.PUT)
	
	public ResponseEntity<JPage> optionalUpdate(
	     @ApiParam(value = "",required=true ) @RequestBody  JEntity jEntity
		,@ApiParam(value = "Language IT_IT|EN_US"	)  			 @RequestHeader(value=CommonService.REQUEST_HEADER_LANG ,required=false )  String hlang
		,@ApiParam(value = "Versione"				,required=true ) @PathVariable("version") 				Long idVersion 
		,@ApiParam(value = "optional"				,required=true ) @PathVariable("optional") 				Long idOptional 
			) throws NotFoundException, ValidationException {
		return new ResponseEntity<JPage>(vModelService.putOptional(commonService.getLanguage(hlang),idVersion, jEntity, idOptional, RequestMethod.PUT),HttpStatus.OK);
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	@ApiOperation(value = "Delete Optional", notes = "Delete Optional", response = ApiResponseMessage.class)
	@ApiResponses(value = { 
		@ApiResponse(code = 200, message = "ok"),
	    @ApiResponse(code = 500, message = "Server Error") })
	@RequestMapping(  value = {"/version/Optional/{version}/{optional}"} 
					, produces = { APPLICATION_JSON_VALUE }
					, method = RequestMethod.DELETE)
	
	public ResponseEntity<ApiResponseMessage> optionalDelete(
		 @ApiParam(value = "Versione"				,required=true ) @PathVariable("version") 				Long idVersion 	
		,@ApiParam(value = "optional"				,required=true ) @PathVariable("optional") 				Long idOptional 
			) throws NotFoundException, ValidationException {
		return new ResponseEntity<ApiResponseMessage>(vModelService.deleteOptional(idVersion, idOptional),HttpStatus.OK);
	}
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	@ApiOperation(value = "Create Optional", notes = "Create Optional", response = JPage.class)
	@ApiResponses(value = { 
	    @ApiResponse(code = 201, message = "Resource Created"),
	    @ApiResponse(code = 500, message = "Server Error") })
	
	@RequestMapping(  value = {"/version/Optional/{version}"} 
					, produces = { APPLICATION_JSON_VALUE }
					, method = RequestMethod.POST)
	
	public ResponseEntity<JPage> optionalCreate(
	     @ApiParam(value = "",required=true ) @RequestBody  JEntity jEntity
		,@ApiParam(value = "Language IT_IT|EN_US"	)  			 @RequestHeader(value=CommonService.REQUEST_HEADER_LANG ,required=false )  String hlang
		,@ApiParam(value = "Versione"				,required=true ) @PathVariable("version") 				Long idVersion 
		
			) throws NotFoundException, ValidationException {
		return new ResponseEntity<JPage>(vModelService.postOptional(commonService.getLanguage(hlang), idVersion, jEntity,  RequestMethod.POST),HttpStatus.CREATED);
	   
	}
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	
	
	@ApiOperation(value = "Template Optional", notes = "Get form vuoto Optional", response = JPage.class)
	@ApiResponses(value = { 
			@ApiResponse(code = 200, message = "Ok"),
			@ApiResponse(code = 500, message = "Server Error") })
	@RequestMapping(
		  value = {"/version/Optional/template/{version}"} 
		 ,produces = { APPLICATION_JSON_VALUE }
		 ,method = RequestMethod.GET)
	
	public ResponseEntity<JPage> getOptionalForm( 
			@ApiParam(value = "Language IT_IT|EN_US"	)  			 @RequestHeader(value=CommonService.REQUEST_HEADER_LANG ,required=false )  String hlang
		,@ApiParam(value = "Versione"						,required=true ) @PathVariable("version") 			Long version
		) throws NotFoundException {

		return new ResponseEntity<JPage>(vModelService.getOptionalTemplate(commonService.getLanguage(hlang), version),HttpStatus.OK);
		
	}
		
	
	
	
	

	
	@ApiOperation(value = "Get Document list", notes = "Get Document list", response = JPage.class)
	@ApiResponses(value = { 
			@ApiResponse(code = 200, message = "Ok"),
			@ApiResponse(code = 500, message = "Server Error") })
	@RequestMapping(
		  value = {"/version/document/{docType}/{version}"} 
		 ,produces = { APPLICATION_JSON_VALUE }
		 ,method = RequestMethod.GET)
	
	public ResponseEntity<JPage> getDocFiguriniByVersion( 
			@ApiParam(value = "Language IT_IT|EN_US"	)  @RequestHeader(value=CommonService.REQUEST_HEADER_LANG ,required=false )  String hlang
 		   ,@ApiParam(value = "Document Type   F|O"	,required=true  ) @PathVariable("docType") 			DocumentType documentType 
 		   ,@ApiParam(value = "Versione"			,required=true  ) @PathVariable("version") 			Long version 
 		   ,@ApiParam(value = "Page"				,required=false ) @RequestParam(required=false)		Long page
 		   ,@ApiParam(value = "Rows"				,required=false ) @RequestParam(required=false) 	Long rows

			)
			 throws NotFoundException {
		return new ResponseEntity<JPage>(vModelService.getDocumentList(commonService.getLanguage(hlang), version, documentType, page, rows),HttpStatus.OK);
	}	
	
	
	
	
	
	
	@ApiOperation(value = "Get Figurino", notes = "Get Figurino", response = JPage.class)
	@ApiResponses(value = { 
			@ApiResponse(code = 200, message = "Ok"),
			@ApiResponse(code = 500, message = "Server Error") })
	@RequestMapping(
		  value = {"/version/document/{docType}/{version}/{idDocument}"} 
		 ,produces = { APPLICATION_JSON_VALUE }
		 ,method = RequestMethod.GET)
	
	public ResponseEntity<JPage> getDocFigurinoByVersion( 
			@ApiParam(value = "Language IT_IT|EN_US"	)  			 @RequestHeader(value=CommonService.REQUEST_HEADER_LANG ,required=false )  String hlang
		 ,@ApiParam(value = "Document Type   F|O"					,required=true ) @PathVariable("docType") 			DocumentType documentType 
		 
		,@ApiParam(value = "Versione"								,required=true ) @PathVariable("version") 			Long version
		,@ApiParam(value = "Id Figurino"							,required=true ) @PathVariable("idDocument") 		Long idDocument 
			)
			 throws NotFoundException {
		return new ResponseEntity<JPage>(vModelService.getDocument(commonService.getLanguage(hlang), version, documentType, idDocument),HttpStatus.OK);
	}
		
	
	@ApiOperation(value = "Update Figurino", notes = "Update Figurino", response = JPage.class)
	@ApiResponses(value = { 
		@ApiResponse(code = 200, message = "ok"),
	    @ApiResponse(code = 500, message = "Server Error") })
	
	@RequestMapping(  value = {"/version/document/{docType}/{version}/{idDocument}"} 
					, produces = { APPLICATION_JSON_VALUE }
					, method = RequestMethod.PUT)
	
	public ResponseEntity<JPage> figurinoUpdate(
	     @ApiParam(value = "",required=true ) @RequestBody  JEntity jEntity
	     ,@ApiParam(value = "Language IT_IT|EN_US"	)  			 @RequestHeader(value=CommonService.REQUEST_HEADER_LANG ,required=false )  String hlang
	    ,@ApiParam(value = "Document Type   F|O"					,required=true ) @PathVariable("docType") 		DocumentType documentType 
		,@ApiParam(value = "Versione"								,required=true ) @PathVariable("version") 		Long idVersion 
		,@ApiParam(value = "Id Figurino"							,required=true ) @PathVariable("idDocument") 	Long idDocument 
			) throws NotFoundException, ValidationException {

		return new ResponseEntity<JPage>(vModelService.putDocument(commonService.getLanguage(hlang),idVersion, jEntity, idDocument, documentType, RequestMethod.PUT),HttpStatus.OK);
	}
	
	
	@ApiOperation(value = "Delete Figurino", notes = "Delete Figurino", response = ApiResponseMessage.class)
	@ApiResponses(value = { 
		@ApiResponse(code = 200, message = "ok"),
	    @ApiResponse(code = 500, message = "Server Error") })
	
	@RequestMapping(  value = {"/version/document/{docType}/{version}/{idDocument}"} 
					, produces = { APPLICATION_JSON_VALUE }
					, method = RequestMethod.DELETE)
	
	public ResponseEntity<ApiResponseMessage> figurinoDelete(
	     @ApiParam(value = "Document Type   F|O"					,required=true ) @PathVariable("docType") 			DocumentType documentType 
		,@ApiParam(value = "Versione"				,required=true ) @PathVariable("version") 				Long idVersion 	
		,@ApiParam(value = "Id Other docdument"		,required=true ) @PathVariable("idDocument") 			Long idDocument 
			) throws NotFoundException, ValidationException {

		return new ResponseEntity<ApiResponseMessage>(vModelService.deleteDocument(idVersion, idDocument, documentType),HttpStatus.OK);
	}
	
	
	
	@ApiOperation(value = "Create Figurino", notes = "Create Figurino", response = JPage.class)
	@ApiResponses(value = { 
	    @ApiResponse(code = 201, message = "Resource Created"),
	    @ApiResponse(code = 500, message = "Server Error") })
	
	@RequestMapping(  value = {"/version/document/{docType}/{version}"} 
					, produces = { APPLICATION_JSON_VALUE }
					, method = RequestMethod.POST)
	
	public ResponseEntity<JPage> figurinoCreate(
	     @ApiParam(value = "",required=true ) @RequestBody  JEntity jEntity
	     ,@ApiParam(value = "Language IT_IT|EN_US"	)  			 @RequestHeader(value=CommonService.REQUEST_HEADER_LANG ,required=false )  String hlang
	    ,@ApiParam(value = "Document Type   F|O"					,required=true ) @PathVariable("docType") 			DocumentType documentType 
		,@ApiParam(value = "Versione"				,required=true ) @PathVariable("version") 				Long idVersion 
 
			) throws NotFoundException, ValidationException {

		return new ResponseEntity<JPage>(vModelService.postDocument(commonService.getLanguage(hlang),idVersion, jEntity, documentType,  RequestMethod.POST),HttpStatus.CREATED);
	   
	}

	

	
	@ApiOperation(value = "Get Template Figurino", notes = "Get Template Figurino", response = JPage.class)
	@ApiResponses(value = { 
			@ApiResponse(code = 200, message = "Ok"),
			@ApiResponse(code = 500, message = "Server Error") })
	@RequestMapping(
		  value = {"/version/document/template/{docType}/{version}"} 
		 ,produces = { APPLICATION_JSON_VALUE }
		 ,method = RequestMethod.GET)
	
	public ResponseEntity<JPage> getTemplateFigurino( 
		 @ApiParam(value = "Language IT_IT|EN_US"	)  			 @RequestHeader(value=CommonService.REQUEST_HEADER_LANG ,required=false )  String hlang
 		,@ApiParam(value = "Document Type   F|O"			,required=true ) @PathVariable("docType") 			DocumentType documentType 
		,@ApiParam(value = "Versione"						,required=true ) @PathVariable("version") 			Long versionId

			)
			 throws NotFoundException {
		return new ResponseEntity<JPage>(vModelService.getDocumentTemplate(commonService.getLanguage(hlang), versionId, documentType),HttpStatus.OK);
	}
	
	
		

	@ApiOperation(value = "Build ZIP Figurini", notes = "Build ZIP Figurini")
	@ApiResponses(value = { 
			@ApiResponse(code = 200, message = "Ok"),
			@ApiResponse(code = 500, message = "Server Error") })
	@RequestMapping(
		  value = {"/version/download/DocumentZIP/{docType}/{version}"} 
		 ,produces = { DocService.MIME_APPLICATION_ZIP ,MediaType.APPLICATION_JSON }
		 ,method = RequestMethod.POST)
	
	// JEntity � il json scaricato da JVersionAttcForm, compilato nelle righe "choice"
	public void CreateZIPFiguriniByVersion( 
		 @ApiParam(value = ""						,required=true ) @RequestBody  				JEntity jEntity
		,@ApiParam(value = "Language IT_IT|EN_US"	)  			 	 @RequestHeader(value=CommonService.REQUEST_HEADER_LANG ,required=false )  String hlang
		,@ApiParam(value = "Document Type   F|O"	,required=true ) @PathVariable("docType") 	DocumentType documentType
		,@ApiParam(value = "Versione"				,required=true ) @PathVariable("version") 	Long versionId
		,HttpServletResponse response
			)
			 throws NotFoundException, IOException {
		
		
		JVersionAttcForm jVersionAttcForm=null; // da entity
		try {
			jVersionAttcForm = vModelService.convertToJVersionAttcForm(jEntity.getData().getActual());
		} catch (IOException e) {
			throw new  NotFoundException(-1,e.getMessage());
		}
				
		docService.createZipByVersionAndType(jVersionAttcForm, commonService.getLanguage(hlang), versionId, documentType, response);
		 return;
	}	
		
	
	

	
	
	/////////////////////////////////////////////////////////////////////
	@ApiOperation(value = "Upload xls Optionals for update", notes = "Upload xls Optionals for update")
	@ApiResponses(value = { 
			@ApiResponse(code = 200, message = "Ok"),
			@ApiResponse(code = 500, message = "Server Error") })

  	@RequestMapping(
  			value = {"/version/Optional/upload"}
  			,produces = {  APPLICATION_JSON_VALUE }
  			,method = RequestMethod.POST)
	
	
 	public @ResponseBody ResponseEntity<JPage> 
		uploadXlsOptionals(@RequestParam("files") MultipartFile files
  						, HttpServletRequest req) throws IOException, NoSuchAlgorithmException{	
						MultipartFile multipartFile=files;
						
						//List<String> logs=new ArrayList<String>();
						
						JPage jPage = vModelService.uploadXlsOptionals(multipartFile);
						return new ResponseEntity<JPage>(jPage,HttpStatus.OK);
	
	} 	 	
	
	
	
}
