package com.ferrari.modis.dpg.controller;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.ferrari.modis.dpg.jentity.base.JEntity;
import com.ferrari.modis.dpg.jentity.base.JPage;
import com.ferrari.modis.dpg.service.CommonService;
import com.ferrari.modis.dpg.service.VersionService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.api.NotFoundException;
import io.swagger.api.ValidationException;


@Controller
@RequestMapping(value = "/version/V1", produces = { APPLICATION_JSON_VALUE })
@Api(value = "/version/V1", description = "Version API-V1")
public class VersionController {

	@SuppressWarnings("unused")
	private static final Logger logger = LoggerFactory.getLogger( VersionController.class );
	
	@Autowired
	VersionService versionService;
	@Autowired
	private CommonService commonService;
	
	


	
	@ApiOperation(value = "Get Version", notes = "Get Version", response = JPage.class)
	@ApiResponses(value = { 
			@ApiResponse(code = 200, message = "Ok"),
			@ApiResponse(code = 500, message = "Server Error") })
	@RequestMapping(
		  value = {"/{version}"} 
		 ,produces = { "application/json" }
		 ,method = RequestMethod.GET)
	
	public ResponseEntity<JPage> getVersion( 
			 	@ApiParam(value = "Language IT_IT|EN_US"	)  	 @RequestHeader(value=CommonService.REQUEST_HEADER_LANG ,required=false )  String hlang
			   ,@ApiParam(value = "Versione"	,required=true ) @PathVariable("version") 			Long idVersion 
			) throws NotFoundException {

		return new ResponseEntity<JPage>(versionService.getVersion(commonService.getLanguage(hlang),idVersion),HttpStatus.OK);
		
		

	}
	
	@ApiOperation(value = "Delete Version", notes = "Delete Version", response = JPage.class)
	@ApiResponses(value = { 
			@ApiResponse(code = 200, message = "Ok"),
			@ApiResponse(code = 500, message = "Server Error") })
	@RequestMapping(
		  value = {"/{version}"} 
		 ,produces = { "application/json" }
		 ,method = RequestMethod.DELETE)
	
	public ResponseEntity<JPage> deleteVersion( 
			 		 @ApiParam(value = "Language IT_IT|EN_US"	)  			 @RequestHeader(value=CommonService.REQUEST_HEADER_LANG ,required=false )  String hlang
					,@ApiParam(value = "Versione"						,required=true ) @PathVariable("version") 			Long idVersion 
//					,@ApiParam(value = "Create Draft if missing"		,required=true ) @PathVariable("createDraft") 		Boolean createDraft 
			) throws NotFoundException {
		return new ResponseEntity<JPage>(versionService.deleteVersion(commonService.getLanguage(hlang),idVersion),HttpStatus.OK);
	}	


	
	
	@ApiOperation(value = "Update Version", notes = "Update Version", response = JPage.class)
	@ApiResponses(value = { 
		@ApiResponse(code = 200, message = "Ok"),
	    @ApiResponse(code = 500, message = "Server Error") })
	
	@RequestMapping(  value = "/{version}"
					, produces = { "application/json" }
					, method = RequestMethod.PUT)
	
	public ResponseEntity<JPage> versionUpdate(
	      @ApiParam(value = "",required=true ) @RequestBody  JEntity jEntity
		 ,@ApiParam(value = "Language IT_IT|EN_US"	)  			 @RequestHeader(value=CommonService.REQUEST_HEADER_LANG ,required=false )  String hlang
		 ,@ApiParam(value = "Versione"				,required=true ) @PathVariable("version") 			Long idVersion 
			) throws NotFoundException, ValidationException {

		return new ResponseEntity<JPage>(versionService.updateVersion(commonService.getLanguage(hlang),idVersion,jEntity),HttpStatus.OK);
	   
	}
	

	
	
	
	@ApiOperation(value = "Publish Version", notes = "Publish Version: form Draft Version", response = JPage.class)
	@ApiResponses(value = { 
		@ApiResponse(code = 200, message = "Ok"),
	    @ApiResponse(code = 500, message = "Server Error") })
	
	@RequestMapping(  value = "publish/{version}"
					, produces = { "application/json" }
					, method = RequestMethod.PUT)
	
	public ResponseEntity<JPage> versionPublish(
	     @ApiParam(value = "",required=true ) @RequestBody  JEntity jEntity
		 ,@ApiParam(value = "Language IT_IT|EN_US"	)  			 @RequestHeader(value=CommonService.REQUEST_HEADER_LANG ,required=false )  String hlang
		,@ApiParam(value = "Versione"				,required=true ) @PathVariable("version") 			Long idVersion 
	    )
	    throws NotFoundException, ValidationException {

		return new ResponseEntity<JPage>(versionService.publishVersion(commonService.getLanguage(hlang),idVersion, jEntity),HttpStatus.OK);
	   
	}	


	
	
	@ApiOperation(value = "Clone Version", notes = "Clone Version, from Published (C) to Draft (D)", response = JPage.class)
	@ApiResponses(value = { 
		@ApiResponse(code = 201, message = "Resource created"),
	    @ApiResponse(code = 500, message = "Server Error") })
	
	@RequestMapping(  value = "clone/{version}"
					, produces = { "application/json" }
					, method = RequestMethod.POST)
	
	public ResponseEntity<JPage> versionClone(
	     @ApiParam(value = "",required=true ) @RequestBody  JEntity jEntity
		,@ApiParam(value = "Versione"				,required=true ) @PathVariable("version") 			Long idVersion 
	    )
	    throws NotFoundException, ValidationException {

		return new ResponseEntity<JPage>(versionService.cloneVersion(idVersion, jEntity),HttpStatus.CREATED);
	   
	}	
	
	
	
}
