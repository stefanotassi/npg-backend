package com.ferrari.modis.dpg.controller;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.util.UriComponentsBuilder;

import com.ferrari.modis.dpg.jentity.base.JEntity;
import com.ferrari.modis.dpg.jentity.base.JPage;
import com.ferrari.modis.dpg.service.CommonService;
import com.ferrari.modis.dpg.service.MiniDocumentService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.api.ApiResponseMessage;
import io.swagger.api.NotFoundException;
import io.swagger.api.ValidationException;


@Controller
@RequestMapping(value = "/miniDocument/V1", produces = { APPLICATION_JSON_VALUE })
@Api(value = "/miniDocument/V1", description = "miniDocument API-V1")
public class MiniDocumentController {

	@SuppressWarnings("unused")
	private static final Logger logger = LoggerFactory.getLogger( MiniDocumentController.class );
	
	@Autowired
	private MiniDocumentService miniDocumentService;
	@Autowired
	private CommonService commonService;
	


	
	
	
	
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	@ApiOperation(value = "Document List", notes = "Document List", response = JPage.class)
	@ApiResponses(value = { 
			@ApiResponse(code = 200, message = "Ok"),
			@ApiResponse(code = 500, message = "Server Error") })
	
	@RequestMapping(
		  value = {"/"}
		 ,produces = { "application/json" }
		 ,method = RequestMethod.GET)
	
	public ResponseEntity<JPage> getDocumentList(

			  @ApiParam(value = "Language IT_IT|EN_US"	)  @RequestHeader(value=CommonService.REQUEST_HEADER_LANG ,required=false )  String hlang
			) throws NotFoundException {


			return new ResponseEntity<JPage>(miniDocumentService.getMiniDocuments(commonService.getLanguage(hlang)),HttpStatus.OK);
			
	}
	
	
	
	@ApiOperation(value = "Document", notes = "Document", response = JPage.class)
	@ApiResponses(value = { 
			@ApiResponse(code = 200, message = "Ok"),
			@ApiResponse(code = 500, message = "Server Error") })
	
	@RequestMapping(
		  value = {"/{idDocument}"}
		 ,produces = { "application/json" }
		 ,method = RequestMethod.GET)
	
	public ResponseEntity<JPage> getDocument(

			  @ApiParam(value = "Language IT_IT|EN_US"	)  @RequestHeader(value=CommonService.REQUEST_HEADER_LANG ,required=false )  String hlang
			 ,@ApiParam(value = "id Document"				,required=true ) @PathVariable("idDocument") Long idDocument 

			) throws NotFoundException {


			return new ResponseEntity<JPage>(miniDocumentService.getMiniDocument(commonService.getLanguage(hlang), idDocument),HttpStatus.OK);
			
	}
	
	
	
	@ApiOperation(value = "POST Document: CREATE", notes = "Put Document - CREATE", response = JPage.class)
	@ApiResponses(value = { 
	    @ApiResponse(code = 201, message = "Resource Created"),
	    @ApiResponse(code = 500, message = "Server Error") })
	
	@RequestMapping(  value = "/"
					, produces = { "application/json" }
					, method = RequestMethod.POST)
	public ResponseEntity<JPage> createDocument(
	    @ApiParam(value = "",required=true ) @RequestBody  JEntity jEntity
	    ,UriComponentsBuilder uriComponentsBuilder
	    )
	    throws NotFoundException, ValidationException {
	    return new ResponseEntity<JPage>(miniDocumentService.postMiniDocument(jEntity, RequestMethod.POST),HttpStatus.CREATED);

	}
	
	@ApiOperation(value = "PUT Document: UPDATE", notes = "POST Document: UPDATE", response = JPage.class)
	@ApiResponses(value = { 
		@ApiResponse(code = 200, message = "Ok"),
	    @ApiResponse(code = 500, message = "Server Error") })
	
	@RequestMapping(  value = "/{idDocument}"
					, produces = { "application/json" }
					, method = RequestMethod.PUT)
	
	public ResponseEntity<JPage> updateDocument(
	     @ApiParam(value = "",required=true ) 					 @RequestBody  JEntity jEntity
		,@ApiParam(value = "Language IT_IT|EN_US"	)  			 @RequestHeader(value=CommonService.REQUEST_HEADER_LANG ,required=false )  String hlang
		,@ApiParam(value = "id Document"				,required=true ) @PathVariable("idDocument") Long idDocument 
	    )
	    throws NotFoundException, ValidationException {
	    return new ResponseEntity<JPage>(miniDocumentService.putMiniDocument(jEntity, commonService.getLanguage(hlang),idDocument, RequestMethod.PUT),HttpStatus.OK);
	}

	

	@ApiOperation(value = "Delete Document: Delete", notes = "Delete Document: Delete", response = JPage.class)
	@ApiResponses(value = { 
		@ApiResponse(code = 200, message = "Ok"),
	    @ApiResponse(code = 500, message = "Server Error") })
	
	@RequestMapping(  value = "/{idDocument}"
					, produces = { "application/json" }
					, method = RequestMethod.DELETE)
	
	public ResponseEntity<ApiResponseMessage> deleteDocument(
		 @ApiParam(value = "Language IT_IT|EN_US"	)  			 	 @RequestHeader(value=CommonService.REQUEST_HEADER_LANG ,required=false )  String hlang
		,@ApiParam(value = "id Document"				,required=true ) @PathVariable("idDocument") Long idDocument 
	    )
	    throws NotFoundException, ValidationException {
	    return new ResponseEntity<ApiResponseMessage>(miniDocumentService.deleteMiniDocument( idDocument),HttpStatus.OK);
	}

	
	
	
	
	
	
	@ApiOperation(value = "Document - new form - template per inserimento", notes = "Document - new form - template per inserimento", response = JPage.class)
	@ApiResponses(value = { 
			@ApiResponse(code = 200, message = "Ok"),
			@ApiResponse(code = 500, message = "Server Error") })
	@RequestMapping(
		  value = {"/template/{idDocument}"} // _form/en/P1/1
		 ,produces = { "application/json" }
		 ,method = RequestMethod.GET)
	
	
	// richiesta informazioni per precaricamenteo form nuovo modello
	public ResponseEntity<JPage> getDocumentTemplate( 

					 @ApiParam(value = "Language IT_IT|EN_US"	)  			 @RequestHeader(value=CommonService.REQUEST_HEADER_LANG ,required=false )  String hlang


			)
			
			 throws NotFoundException {
		return new ResponseEntity<JPage>(miniDocumentService.getNewMiniDocumentForm(commonService.getLanguage(hlang)),HttpStatus.OK);
	}
	
	
}
