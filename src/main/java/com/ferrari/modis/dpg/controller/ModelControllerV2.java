package com.ferrari.modis.dpg.controller;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.util.UriComponentsBuilder;

import com.ferrari.modis.dpg.jentity.base.JEntity;
import com.ferrari.modis.dpg.jentity.base.JPage;
import com.ferrari.modis.dpg.service.CommonService;
import com.ferrari.modis.dpg.service.ModelService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.api.NotFoundException;
import io.swagger.api.ValidationException;


@Controller
@RequestMapping(value = "/model/V2", produces = { APPLICATION_JSON_VALUE })
@Api(value = "/model/V2", description = "Model API-V2")
public class ModelControllerV2 {

	@SuppressWarnings("unused")
	private static final Logger logger = LoggerFactory.getLogger( ModelControllerV2.class );
	
	@Autowired
	private ModelService modelService;
	@Autowired
	private CommonService commonService;
	

	
	@ApiOperation(value = "Model - new form - template per inserimento", notes = "Model - new form - template per inserimento", response = JPage.class)
	@ApiResponses(value = { 
			@ApiResponse(code = 200, message = "Ok"),
			@ApiResponse(code = 500, message = "Server Error") })
	@RequestMapping(
		  value = {"/template/{modelCluster}"} // _form/en/P1/1
		 ,produces = { "application/json" }
		 ,method = RequestMethod.GET)
	
	
	// richiesta informazioni per precaricamento form nuovo modello
	public ResponseEntity<JPage> getModelTemplateV2( 

					 @ApiParam(value = "Language IT_IT|EN_US"	)  			 @RequestHeader(value=CommonService.REQUEST_HEADER_LANG ,required=false )  String hlang
					,@ApiParam(value = "id Cluster modelli"	,required=true ) @PathVariable("modelCluster") String modelCluster 

			)
			
			 throws NotFoundException {
		return new ResponseEntity<JPage>(modelService.getNewModelFormV2(commonService.getLanguage(hlang),  modelCluster),HttpStatus.OK);
	}
	
	
	
	
	
	// per caricare il form Splash
	@ApiOperation(value = "Model - Lista tipi opt", notes = "Model - Lista tipi opt", response = JPage.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Ok"),
			@ApiResponse(code = 500, message = "Server Error") })
	@RequestMapping(
		  value = {"/types/{idmodel}"} // en/P1/23
		 ,produces = { "application/json" }
		 ,method = RequestMethod.GET)
	
	public ResponseEntity<JPage> getModelTypesV2( 
				@ApiParam(value = "Language IT_IT|EN_US"	)  			 @RequestHeader(value=CommonService.REQUEST_HEADER_LANG ,required=false )  String hlang
			   ,@ApiParam(value = "id Modello"				,required=true ) @PathVariable("idmodel") Long idmodel 
			   )

			 throws NotFoundException {
		 		return new ResponseEntity<JPage>(modelService.getModelTypesV2(commonService.getLanguage(hlang), idmodel),HttpStatus.OK);
	}

	
	
	
	
	
	
	
	
	
// TODO V2 	GET Model	- testare
	@ApiOperation(value = "Model - read", notes = "Model - read", response = JPage.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Ok"),
			@ApiResponse(code = 500, message = "Server Error") })
	@RequestMapping(
		  value = {"/{idmodel}"} // en/P1/23
		 ,produces = { "application/json" }
		 ,method = RequestMethod.GET)
	
	public ResponseEntity<JPage> getModelV2( 
				@ApiParam(value = "Language IT_IT|EN_US"	)  			 @RequestHeader(value=CommonService.REQUEST_HEADER_LANG ,required=false )  String hlang
			   ,@ApiParam(value = "id Modello"				,required=true ) @PathVariable("idmodel") Long idmodel 
			   )
					
			
			 throws NotFoundException {
		 		return new ResponseEntity<JPage>(modelService.getModelV2(commonService.getLanguage(hlang), idmodel),HttpStatus.OK);
	}

	

// TODO V2 	POST Model: CREATE - testare
	@ApiOperation(value = "POST Model: CREATE", notes = "Put Model - CREATE", response = JPage.class)
	@ApiResponses(value = { 
	    @ApiResponse(code = 201, message = "Resource Created"),
	    @ApiResponse(code = 500, message = "Server Error") })
	
	@RequestMapping(  value = "/"
					, produces = { "application/json" }
					, method = RequestMethod.POST)
	public ResponseEntity<JPage> createModelV2(
	    @ApiParam(value = "",required=true ) @RequestBody  JEntity jEntity
	    ,UriComponentsBuilder uriComponentsBuilder
	    )
	    throws NotFoundException, ValidationException {
	       return new ResponseEntity<JPage>(modelService.postModelV2(jEntity, RequestMethod.POST),HttpStatus.CREATED);

	}

	
	// TODO V2 PUT Model: UPDATE - testare
	@ApiOperation(value = "PUT Model: UPDATE", notes = "POST Model: UPDATE", response = JPage.class)
	@ApiResponses(value = { 
		@ApiResponse(code = 200, message = "Ok"),
	    @ApiResponse(code = 500, message = "Server Error") })
	
	@RequestMapping(  value = "/{idmodel}"
					, produces = { "application/json" }
					, method = RequestMethod.PUT)
	
	public ResponseEntity<JPage> updateModelV2(
	     @ApiParam(value = "",required=true ) 					 @RequestBody  JEntity jEntity
		,@ApiParam(value = "Language IT_IT|EN_US"	)  			 @RequestHeader(value=CommonService.REQUEST_HEADER_LANG ,required=false )  String hlang
		,@ApiParam(value = "id Modello"				,required=true ) @PathVariable("idmodel") Long idmodel 
	    )
	    throws NotFoundException, ValidationException {
		   return new ResponseEntity<JPage>(modelService.putModelV2(jEntity, commonService.getLanguage(hlang), idmodel, RequestMethod.PUT),HttpStatus.OK);
	}

	
	
//	unused V2 DELETE Model
//	@ApiOperation(value = "Delete Model: Delete", notes = "Delete Model: Delete", response = JPage.class)
//	@ApiResponses(value = { 
//		@ApiResponse(code = 200, message = "Ok"),
//	    @ApiResponse(code = 500, message = "Server Error") })
//	
//	@RequestMapping(  value = "/{idmodel}"
//					, produces = { "application/json" }
//					, method = RequestMethod.DELETE)
//	
//	public ResponseEntity<ApiResponseMessage> deleteModel(
//		 @ApiParam(value = "Language IT_IT|EN_US"	)  			 	 @RequestHeader(value=CommonService.REQUEST_HEADER_LANG ,required=false )  String hlang
//		,@ApiParam(value = "id Modello"				,required=true ) @PathVariable("idmodel") Long idmodel 
//	    )
//	    throws NotFoundException, ValidationException {
//				throw new NotImplementedException("non implemntata");
//	    // return new ResponseEntity<ApiResponseMessage>(modelService.deleteModel( idmodel),HttpStatus.OK);
//	}

	
	
	
	
//	
//	@ApiOperation(value = "Optional Groups", notes = "Optional Groups", response = JValueText.class)
//	@ApiResponses(value = { 
//			@ApiResponse(code = 200, message = "Ok"),
//			@ApiResponse(code = 500, message = "Server Error") })
//	@RequestMapping(
//		  value = {"/groups/{optionalType}/{idmodel}"} // en/P1/23
//		 ,produces = { "application/json" }
//		 ,method = RequestMethod.GET)
//	
//	public ResponseEntity<List<JValueText>> getListOptGroup( 
//			    @ApiParam(value = "Language IT_IT|EN_US"	)  			 @RequestHeader(value=CommonService.REQUEST_HEADER_LANG ,required=false )  String hlang
//			   ,@ApiParam(value = "P1|P2"					,required=true ) @PathVariable("optionalType") String optionalType 
//			   ,@ApiParam(value = "id Modello"				,required=true ) @PathVariable("idmodel") Long idmodel 
//			   )
//			 throws NotFoundException {
//		return new ResponseEntity<List<JValueText>>(modelService.getOptionalGroups(commonService.getLanguage(hlang),optionalType, idmodel),HttpStatus.OK);
//		
//	}
//	
//	
//	
	
//	@ApiOperation(value = "Optional subgroups", notes = "Optional subgroups", response = JValueText.class)
//	@ApiResponses(value = { 
//			@ApiResponse(code = 200, message = "Ok"),
//			@ApiResponse(code = 500, message = "Server Error") })
//	@RequestMapping(
//		  value = {"/groups/{optionalType}/{idmodel}/{idgroup}"} // en/P1/23
//		 ,produces = { "application/json" }
//		 ,method = RequestMethod.GET)
//	
//	public ResponseEntity<List<JValueText>> getListOptSubGroup( 
//			    @ApiParam(value = "Language IT_IT|EN_US"	)  			 @RequestHeader(value=CommonService.REQUEST_HEADER_LANG ,required=false )  String hlang
//			   ,@ApiParam(value = "P1|P2"					,required=true ) @PathVariable("optionalType") String optionalType 
//			   ,@ApiParam(value = "id Modello"				,required=true ) @PathVariable("idmodel") Long idmodel 
//			   ,@ApiParam(value = "id Optional group"		,required=true ) @PathVariable("idgroup") Long idgroup 
//			   )
//			 throws NotFoundException {
//		return new ResponseEntity<List<JValueText>>(modelService.getOptionalSubGroups(commonService.getLanguage(hlang),optionalType, idmodel, idgroup),HttpStatus.OK);
//		
//	}	
//
//	
	
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//	@ApiOperation(value = "Homepage", notes = "Homepage", response = JPage.class)
//	@ApiResponses(value = { 
//			@ApiResponse(code = 200, message = "Ok"),
//			@ApiResponse(code = 500, message = "Server Error") })
//	
//	@RequestMapping(
//		  value = {"/homepage"}
//		 ,produces = { "application/json" }
//		 ,method = RequestMethod.GET)
//	
//	public ResponseEntity<JPage> getHomePageModelList(
//
//			  @ApiParam(value = "Language IT_IT|EN_US"	)  @RequestHeader(value=CommonService.REQUEST_HEADER_LANG ,required=false )  String hlang
//			) throws NotFoundException {
//
//
//			return new ResponseEntity<JPage>(modelService.getHomepage(commonService.getLanguage(hlang)),HttpStatus.OK);
//			
//	}
//	
	

	
}
