package com.ferrari.modis.dpg.controller;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.ferrari.modis.dpg.jentity.base.JPage;
import com.ferrari.modis.dpg.model.app.MediaConfig;
import com.ferrari.modis.dpg.service.AdminService;
import com.ferrari.modis.dpg.service.CommonService;
import com.ferrari.modis.dpg.service.SearchService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.api.NotFoundException;




@Controller
@RequestMapping(value = "/admin/V1", produces = { APPLICATION_JSON_VALUE })
@Api(value = "/admin/V1", description = "Version Model API-V1")
public class AdminController {

	@SuppressWarnings("unused")
	private static final Logger logger = LoggerFactory.getLogger( AdminController.class );
	
	@Autowired
	private AdminService adminService;

		
	@ApiOperation(value = "Ricerca (ristretta su Versione Corrente, OptType Corrente)", notes = "Ricerca per Versione", response = JPage.class)
	@ApiResponses(value = { 
			@ApiResponse(code = 200, message = "Ok"),
			@ApiResponse(code = 500, message = "Server Error") })
	@RequestMapping(
		  value = {"/media_config"} 
		 ,produces = { APPLICATION_JSON_VALUE }
		 ,method = RequestMethod.GET)
	
	public ResponseEntity<List<MediaConfig>> getSearchMV() throws NotFoundException {
		
		return ResponseEntity.ok(adminService.getList(MediaConfig.class));
	}
	
	
	
	@ApiOperation(value = "Ricerca (ristretta su Versione Corrente, OptType Corrente)", notes = "Ricerca per Versione", response = JPage.class)
	@ApiResponses(value = { 
			@ApiResponse(code = 200, message = "Ok"),
			@ApiResponse(code = 500, message = "Server Error") })
	@RequestMapping(
		  value = {"/media_config"} 
		 ,produces = { APPLICATION_JSON_VALUE }
		 ,method = RequestMethod.POST)
	
	public ResponseEntity<MediaConfig> getSearchMVs(@RequestBody MediaConfig media) throws NotFoundException {
		
		return ResponseEntity.ok(adminService.createOrUpdateOne(MediaConfig.class,media));
	}
	
	
	@ApiOperation(value = "Ricerca (ristretta su Versione Corrente, OptType Corrente)", notes = "Ricerca per Versione", response = JPage.class)
	@ApiResponses(value = { 
			@ApiResponse(code = 200, message = "Ok"),
			@ApiResponse(code = 500, message = "Server Error") })
	@RequestMapping(
		  value = {"/media_config/{ID}"} 
		 ,produces = { APPLICATION_JSON_VALUE }
		 ,method = RequestMethod.DELETE)
	
	public ResponseEntity<Void> deleteMediaConfig( @ApiParam(value = "ID"				,required=true ) @PathVariable("ID") 			String id) throws NotFoundException {
		adminService.deleteOne(MediaConfig.class, id);
		return ResponseEntity.ok().build();
	}

	
	
	@ApiOperation(value = "Ricerca (ristretta su Versione Corrente, OptType Corrente)", notes = "Ricerca per Versione", response = JPage.class)
	@ApiResponses(value = { 
			@ApiResponse(code = 200, message = "Ok"),
			@ApiResponse(code = 500, message = "Server Error") })
	@RequestMapping(
		  value = {"/media_config/{ID}"} 
		 ,produces = { APPLICATION_JSON_VALUE }
		 ,method = RequestMethod.GET)
	
	public ResponseEntity<MediaConfig> getOneMediaConfig( @ApiParam(value = "ID"				,required=true ) @PathVariable("ID") 			String id) throws NotFoundException {
		return ResponseEntity.ok(adminService.findOne(MediaConfig.class, id));
	}
}
