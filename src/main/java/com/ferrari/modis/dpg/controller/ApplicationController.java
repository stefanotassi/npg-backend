package com.ferrari.modis.dpg.controller;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.ferrari.modis.dpg.jentity.base.JNotification;
import com.ferrari.modis.dpg.model.base.AuditContext;
import com.ferrari.modis.dpg.service.SecurityInfoService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.api.NotFoundException;

@Controller
@RequestMapping(value = "/Application/V1", produces = { APPLICATION_JSON_VALUE })
@Api(value = "/Application/V1", description = "Application API-V1")
public class ApplicationController {

	private static final Logger logger = LoggerFactory.getLogger(ApplicationController.class);
	
	@Autowired
	private SecurityInfoService securityInfoService;


	@Value(value = "${version}")
	String version;

	@Value(value = "${build.date}")
	String buildTime;

	@ApiOperation(value = "Application information", notes = "Application information", response = JNotification.class)
	@ApiResponses(value = { 
			@ApiResponse(code = 200, message = "Ok"),
			@ApiResponse(code = 500, message = "Server Error") })
	
	@RequestMapping(
		  value = {"/info"}
		 ,produces = { "application/json" }
		 ,method = RequestMethod.GET)
		
	public ResponseEntity<Object> applicationInfoGet() throws NotFoundException {

		logger.info("log get application info user=[{}] ",AuditContext.getCurrentUserName());
		return new ResponseEntity<Object>(securityInfoService.getApplicationInfo(version, buildTime), HttpStatus.OK);
	}
	

}
