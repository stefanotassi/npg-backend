package com.ferrari.modis.dpg.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.HttpMediaTypeNotAcceptableException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.WebRequest;

import com.ferrari.modis.dpg.commons.i18n.LocalizationHelper;
import com.ferrari.modis.dpg.model.ui.UiDictionary;
import com.ferrari.modis.dpg.repository.UiDictionaryRepository;
import com.ferrari.modis.security.authentication.ModisAuthenticationException;

import io.swagger.api.ApiResponseMessage;
import io.swagger.api.ValidationException;

@ControllerAdvice
@Controller
public class NpgExceptionHandler {

	private final Logger logger=LoggerFactory.getLogger(NpgExceptionHandler.class);
	
	@Autowired
	UiDictionaryRepository uiDictionaryRepository;
	
	public static String getStackTrace (Throwable t)
	{
	    StringWriter stringWriter = new StringWriter();
	    PrintWriter  printWriter  = new PrintWriter(stringWriter);
	    t.printStackTrace(printWriter);
	    printWriter.close();    //surprise no IO exception here
	    try {
	        stringWriter.close();
	    }
	    catch (IOException e) {
	    }
	    return stringWriter.toString();
	}

	@ExceptionHandler(java.lang.NullPointerException.class)
	public ResponseEntity<ApiResponseMessage> handleNullPointerException(Exception ex){
		ex.printStackTrace();
		logger.error(getStackTrace(ex));
		logger.error("",ex);
		return new ResponseEntity<ApiResponseMessage>(new ApiResponseMessage(ApiResponseMessage.ERROR,"INTERNAL ERROR"),HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@RequestMapping(value = "/500", method = RequestMethod.GET)
	public ResponseEntity<ApiResponseMessage> internalServerError(Exception ex){
		
		return new ResponseEntity<ApiResponseMessage>(new ApiResponseMessage(ApiResponseMessage.ERROR,"Access denied"),HttpStatus.FORBIDDEN);
	}
	
	
	
	@RequestMapping(value = "/403", method = RequestMethod.GET)
	public ResponseEntity<ApiResponseMessage> accessDenied(Exception ex){
		
		return new ResponseEntity<ApiResponseMessage>(HttpStatus.FORBIDDEN);
	}

	@ExceptionHandler(org.springframework.web.HttpMediaTypeNotAcceptableException.class)
	public void handeMediTypeExcpetion(HttpMediaTypeNotAcceptableException e,HttpServletResponse resp) {
		logger.error("Strana eccezione MediType",e);
		try {
			resp.sendError(HttpStatus.INTERNAL_SERVER_ERROR.value(), "KO");
		} catch (IOException ex) {
			logger.error("Error handeMediTypeExcpetion",ex);
		}
	}
	
	@ExceptionHandler(Exception.class)
	public ResponseEntity<ApiResponseMessage> handleAllException(Exception ex,WebRequest webRequest){
		//logger.error("",ex);
		if (!(ex instanceof ValidationException)){
//			ex.printStackTrace();
			logger.error("Error presentation",ex);
//			logger.error(getStackTrace(ex));
		}
		
		//request.get
		/////////////////////////////////////////////////////////////////////
		// 776: throw new ValidationException(-1,"blah blah blah");
		// 777: trigger message
		/////////////////////////////////////////////////////////////////////		
	
		if (ex instanceof ModisAuthenticationException){
			return new ResponseEntity<ApiResponseMessage>(new ApiResponseMessage(ApiResponseMessage.ERROR,ex.getMessage()),HttpStatus.FORBIDDEN);
		}
		

		if (ex instanceof ValidationException){
			if (((ValidationException)ex).getCode()==-1){
				try{				
					UiDictionary e = uiDictionaryRepository.findByContextAndKey("776", ex.getMessage());
					if (e!=null){
						if (ex.getMessage().indexOf(e.getDescription())>=0){
							return new ResponseEntity<ApiResponseMessage>(new ApiResponseMessage(ApiResponseMessage.ERROR,LocalizationHelper.getLocalized(e.getDescriptions())),HttpStatus.INTERNAL_SERVER_ERROR);										
						}
					}
				} catch (Exception ignore){}			
			}
		}
		
		
		
		if (ex.getCause()!=null){
			try{
				
				for (UiDictionary e:uiDictionaryRepository.findByContext("777")){
					if (ex.getCause().getMessage().indexOf(e.getDescription())>=0){
						return new ResponseEntity<ApiResponseMessage>(new ApiResponseMessage(ApiResponseMessage.ERROR,LocalizationHelper.getLocalized(e.getDescriptions())),HttpStatus.INTERNAL_SERVER_ERROR);										
					}
				}
			} catch (Exception ignore){}
		}
		//return new ResponseEntity<ApiResponseMessage>(new ApiResponseMessage(ApiResponseMessage.ERROR,ex.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		return new ResponseEntity<ApiResponseMessage>(new ApiResponseMessage(ApiResponseMessage.ERROR,"Error"),HttpStatus.INTERNAL_SERVER_ERROR);
	}

}
