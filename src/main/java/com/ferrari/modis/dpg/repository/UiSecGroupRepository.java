package com.ferrari.modis.dpg.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import org.springframework.stereotype.Repository;
import com.ferrari.modis.dpg.model.ui.UiSecGroup;

@Repository
public interface UiSecGroupRepository extends JpaRepository<UiSecGroup, String> {
	@Query("select d from UiSecGroup d where d.active='Y'")
	List<UiSecGroup> findAll();
	

}
