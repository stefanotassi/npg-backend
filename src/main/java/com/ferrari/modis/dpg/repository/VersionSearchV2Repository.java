package com.ferrari.modis.dpg.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.ferrari.modis.dpg.model.app.VersionSearchV2;

@Repository
public interface VersionSearchV2Repository extends JpaRepository<VersionSearchV2, String> {


	@Query("select c from VersionSearchV2 c "
			+ "where (c.idVersion=:idVersion  or :versionAll='Y') "
			+ "and (c.idModel=:idModel  or :modelAll='Y') "
			+ "and c.versionStatus  in (:allowedStatus) "
			+ "and c.language=:lang "
			+ "and (c.visible='Y' or :optlVisibleAll='Y') "
			+ "and c.pub  in (:pubRow) "
			+ "and (optionalType= :optionalType or :optionalTypeAll='Y') "
			+ "and (c.modelVisible='Y' or :modelVisibleAll='Y') "
			+ "and lower(c.description) like  %:searchText% " )
	List<VersionSearchV2> findBytext( 	@Param("idVersion") 		Long idVersion,
										@Param("versionAll")		String versionAll,  // include tutte le versioni
										
										@Param("idModel") 			Long idModel,
										@Param("modelAll")			String modelAll,  // include tutte le versioni
										
										@Param("allowedStatus")		List<String> allowedStatus,
										@Param("lang")	    		String lang,
										@Param("pubRow")			List<String> pubRow,
										@Param("optlVisibleAll")	String optlVisibleAll,
										@Param("searchText") 		String searchText ,
										@Param("optionalType") 		String optionalType ,  // P1 o P2
										@Param("optionalTypeAll") 	String optionalTypeAll, // se Y --> sia P1 che P2
										@Param("modelVisibleAll")	String modelVisibleAll  // vede anche i modelli non visibili
										
									
	);	
	
	
	;	 

}

