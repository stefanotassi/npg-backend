package com.ferrari.modis.dpg.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.ferrari.modis.dpg.model.app.CfgFilesWhitelist;
import com.ferrari.modis.dpg.model.app.Media;
import com.ferrari.modis.dpg.model.app.MediaConfig;
import com.ferrari.modis.dpg.repository.ext.ExtJpaRepo;

@Repository
public interface CfgFilesWhitelistRepository extends ExtJpaRepo<CfgFilesWhitelist, String> {

	
	@Query("select d from CfgFilesWhitelist d where lower(d.mimeType)=lower(?1) and d.extension is null")
	List<CfgFilesWhitelist> findMimetype(String mimetype);

	@Query("select d from CfgFilesWhitelist d where lower(d.mimeType)=lower(?1) and lower(d.extension)=lower(?2)")
	List<CfgFilesWhitelist> findMimetypeAndExtension(String mimetype, String extension);
	

}
