package com.ferrari.modis.dpg.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.ferrari.modis.dpg.model.app.OptionalErp;

@Repository
public interface OptionalErpRepository extends JpaRepository<OptionalErp, String> {
	
//	@Query("select c from OptionalErp c where c.id=?1 c.active='Y'")
//	OptionalType findById(String id);
	
	
	@Query("select c from OptionalErp c where length(c.id)>=4 and c.active='Y' order by c.id")
	List<OptionalErp> findAll();
	
	
}
