package com.ferrari.modis.dpg.repository;

import java.util.List;

import javax.persistence.Column;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.ferrari.modis.dpg.model.app.VersionAttc;

@Repository
//public interface VersionAttcRepository extends JpaRepository<VersionAttc, Long> {
public interface VersionAttcRepository extends PagingAndSortingRepository<VersionAttc, Long>, JpaRepository<VersionAttc, Long> {
	
	
	@Query("select c from VersionAttc c where c.version.id=?1 and c.attachmentType=?2 and c.active='Y' order by c.sequence, c.id")
	List<VersionAttc> findByVersionType(Long idVersion, String attachmentType, Pageable pageable);
	
	@Query("select c from VersionAttc c where c.version.id=?1 and c.attachmentType=?2  and c.active='Y' order by c.sequence, c.id")
	List<VersionAttc> findByVersionTypeAll(Long idVersion, String attachmentType);
	
	
	@Query("select count(c) from VersionAttc c where c.version.id=?1  and c.active='Y' and c.attachmentType=?2")
	Long countByVersionType(Long idVersion, String attachmentType);
	
	
	@Query("select c from VersionAttc c where c.id=?1  and c.active='Y'")
	VersionAttc findById(Long id);
	/*
	 * @Column(name="ID_IMAGE")		
	private String idImage;
	
	@Column(name="ID_IMAGE_THUMB")		
	private String idImageThumb;

	@Column(name="ID_IMAGE_SVG")		
	private String idImageSvg;
	 */
	
	List<VersionAttc> findByIdImage ( String idImage);
	List<VersionAttc> findByIdImageThumb ( String idImageThumb);
	List<VersionAttc> findByIdImageSvg ( String idImageSvg);
	
}
