package com.ferrari.modis.dpg.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import org.springframework.stereotype.Repository;
import com.ferrari.modis.dpg.model.app.MediaType;

@Repository
public interface MediaTypeRepository extends JpaRepository<MediaType, String> {


	@Query("select c from MediaType c where c.id=?1 and c.active='Y'")
	MediaType findById(String id);
	 
}
