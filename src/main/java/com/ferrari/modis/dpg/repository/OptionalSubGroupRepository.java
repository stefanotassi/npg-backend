package com.ferrari.modis.dpg.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import org.springframework.stereotype.Repository;
import com.ferrari.modis.dpg.model.app.OptionalSubGroup;

@Repository
public interface OptionalSubGroupRepository extends JpaRepository<OptionalSubGroup, Long> {
	@Query("select c from OptionalSubGroup c where c.idOptionalGroup=?1 and c.active='Y'")
	List<OptionalSubGroup> findByIdOptionalGroup(Long idOptionalGroup);
}
