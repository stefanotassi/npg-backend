package com.ferrari.modis.dpg.repository;

import org.springframework.stereotype.Repository;

import com.ferrari.modis.dpg.model.app.MediaConfig;
import com.ferrari.modis.dpg.repository.ext.ExtJpaRepo;

@Repository
public interface MediaConfigRepository extends ExtJpaRepo<MediaConfig, String> {

}
