package com.ferrari.modis.dpg.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.ferrari.modis.dpg.model.ui.UiSecActionAuth;
import com.ferrari.modis.dpg.model.ui.UiSecActionAuthPK;

@Repository
public interface UiSecActionAuthRepository extends JpaRepository<UiSecActionAuth, UiSecActionAuthPK> {
	
	@Query("select d from UiSecActionAuth d where d.active='Y' and d.id.groupId=?1 and d.id.actionId=?2")
	UiSecActionAuth findByGroupAction(String groupId, String actionId);
	

}
