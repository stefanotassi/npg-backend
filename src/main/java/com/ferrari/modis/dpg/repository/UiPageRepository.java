package com.ferrari.modis.dpg.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import org.springframework.stereotype.Repository;
import com.ferrari.modis.dpg.model.ui.UiPage;

@Repository
public interface UiPageRepository extends JpaRepository<UiPage, String> {

	@Query("select d from UiPage d where d.active='Y' and d.id=?1")
	public UiPage findByKey(String key);

}
