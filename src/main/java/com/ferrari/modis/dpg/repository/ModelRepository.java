package com.ferrari.modis.dpg.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import org.springframework.stereotype.Repository;
import com.ferrari.modis.dpg.model.app.Model;

@Repository
public interface ModelRepository extends JpaRepository<Model, Long> {

	@Query("select c from Model c where c.idMcluster=?1 and  c.active='Y'")
	List<Model> findByCluster(String idMcluster);
	
	
	@Query("select c from Model c where c.id=?1")
	Model findById(Long id);
	
	
	@Query("select c from Model c where c.code=?1")
	List<Model> findByCode(String code);
	
	@Query("select c from Model c where c.code=?1 and c.active = ?2")
	Model findByCodeAndActive(String code,String active);
	
	
	
}
