package com.ferrari.modis.dpg.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import org.springframework.stereotype.Repository;
import com.ferrari.modis.dpg.model.app.ModelOptionalGroup;

@Repository
public interface ModelOptionalGroupRepository extends JpaRepository<ModelOptionalGroup, Long> {

	
	@Query("select c from ModelOptionalGroup c where c.modelOptionalType.id=?1 and  c.active='Y' order by c.sequence")
	List<ModelOptionalGroup> findByModelOptType(Long idModelOptType);

	@Query("select c from ModelOptionalGroup c where c.modelOptionalType.id=?1 and c.id=?2 and  c.active='Y'")
	ModelOptionalGroup findByModelOptTypeAndId(Long idModelOptType, Long id);
	
}
 