package com.ferrari.modis.dpg.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.ferrari.modis.dpg.model.ui.UiDictionary;



@Repository
public interface UiDictionaryRepository  
		extends JpaRepository<UiDictionary, Long>,JpaSpecificationExecutor<UiDictionary>{


	@Query("select d from UiDictionary d where d.idContext=?1")
	List<UiDictionary> findByContext(String context);
	
	@Query("select d from UiDictionary d where d.idContext=?1 and d.idKey=?2")
	UiDictionary  findByContextAndKey(String context,String key);
	
}

