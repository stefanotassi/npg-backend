package com.ferrari.modis.dpg.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.ferrari.modis.dpg.model.app.MiniDocument;

@Repository
public interface MiniDocumentRepository extends JpaRepository<MiniDocument, Long> {

	@Query("select c from MiniDocument c where c.active='Y' ")
	List<MiniDocument> findAll();
	
	
	
}
