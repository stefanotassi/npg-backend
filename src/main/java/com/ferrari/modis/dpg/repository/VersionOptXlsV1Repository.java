package com.ferrari.modis.dpg.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.ferrari.modis.dpg.model.app.VersionOptXlsV1;
import com.ferrari.modis.dpg.model.app.VersionOptXlsV1PK;

@Repository
public interface VersionOptXlsV1Repository extends JpaRepository<VersionOptXlsV1, VersionOptXlsV1PK> {

	// enableAll per includere in base all'autorizzazione utente sulla visibilit�
	@Query("select c from  VersionOptXlsV1 c where c.id.idVersion=:idVersion "
			+ "and c.id.lang=:language "
			+ "and c.id.idOpt in :optId "
			+ "and (c.visibleFlag='Y' or :enableAll='Y') "
			+ "order by c.seqGroup, c.seqSubgroup, c.seq")
	List<VersionOptXlsV1> getOptionalList(@Param ("idVersion") Long idVersion
			 							, @Param ("language") String language
			 							, @Param ("optId") List<Long> optId
			 							, @Param ("enableAll") String enableAll
			 );
	
	
	// per determinare il numero di colonne in stampa PDF
	@Query("select count(distinct c.idGroup) from  VersionOptXlsV1 c where c.id.idVersion=:idVersion "
			+ "and c.id.lang=:language "
			+ "and c.id.idOpt in :optId "
			+ "and (c.visibleFlag='Y' or :enableAll='Y') ")
			//+ "group by c.idGroup")
	Long getGroupCount(@Param ("idVersion") Long idVersion
						, @Param ("language") String language
						, @Param ("optId") List<Long> optId
						, @Param ("enableAll") String enableAll
			 );
	
	
	
	@Query("select c from  VersionOptXlsV1 c where c.id.idVersion=:idVersion "
			+ "and c.id.lang=:language "
			+ "and c.id.idOpt=:optId ")
	VersionOptXlsV1 getOptional(@Param ("idVersion") Long idVersion
			 							, @Param ("language") String language
			 							, @Param ("optId") Long optId

			 );
	
	
}
