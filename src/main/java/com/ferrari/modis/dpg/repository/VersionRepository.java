package com.ferrari.modis.dpg.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.data.repository.query.Param;

import org.springframework.stereotype.Repository;
import com.ferrari.modis.dpg.model.app.Version;

@Repository
public interface VersionRepository extends JpaRepository<Version, Long> {

	@Query("select c from Version c where c.id=?1") 
	Version findById(Long id);

	@Query("select c from Version c where c.idModelOptionalType=?1 and c.code=?2 order by c.id") 
	List<Version> findByIdModOptAndCode(Long idModelOptionalType, String code);
	
	@Query("select c from Version c where c.idModelOptionalType=?1 order by c.id")
	List<Version> findByIdModOpt(Long idModelOptionalType);	
	
	@Query("select c from Version c where c.idModelOptionalType=?1 and c.status=?2") 
	Version findVersionByStatus(Long idModelOptionalType, String status);	
	// attenzione, usare solo per Draft e C=Published in quanto possono essercene al max 1
	
	
	@Procedure(procedureName = "SP_CLONE_VERSION_V1")
	void cloneVersion(@Param("p_user") String userId
			,@Param("p_VFrom") Long fromVersion
			,@Param("p_VTo") Long toVersion
	
			);
	 
}
