package com.ferrari.modis.dpg.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import org.springframework.stereotype.Repository;
import com.ferrari.modis.dpg.model.app.TextType;

@Repository
public interface TextTypeRepository extends JpaRepository<TextType, Long> {

	@Query("select c from TextType c where c.active='Y'")
	List<TextType> findAll();
	
	
}
