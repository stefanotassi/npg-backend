package com.ferrari.modis.dpg.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import org.springframework.stereotype.Repository;
import com.ferrari.modis.dpg.model.app.VersionOptText;

@Repository
public interface VersionOptTextRepository extends JpaRepository<VersionOptText, Long> {

	
	@Query("select c from VersionOptText c where  c.versionOpt.id=?1 and c.textType=?2")
	VersionOptText findByIdAndTextType(Long idOptional, String textType);	
	

	
}
