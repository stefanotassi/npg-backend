package com.ferrari.modis.dpg.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import org.springframework.stereotype.Repository;
import com.ferrari.modis.dpg.model.app.OptionalGroup;

@Repository
public interface OptionalGroupRepository extends JpaRepository<OptionalGroup, Long> {
	@Query("select c from OptionalGroup c where c.idOptionalType=?1 and c.active='Y'")
	List<OptionalGroup> findByIdOptionalType(String optionalType);
}
