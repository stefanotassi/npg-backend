package com.ferrari.modis.dpg.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import org.springframework.stereotype.Repository;
import com.ferrari.modis.dpg.model.ui.UiApplicationSetting;

@Repository
public interface UiApplicationSettingRepository extends JpaRepository<UiApplicationSetting, String> {

	@Query("select c from UiApplicationSetting c where c.key=?1 and c.active='Y'")
	public UiApplicationSetting findByKey(String key);

}
