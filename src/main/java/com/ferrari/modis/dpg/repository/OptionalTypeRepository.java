package com.ferrari.modis.dpg.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.ferrari.modis.dpg.model.app.OptionalType;

@Repository
public interface OptionalTypeRepository extends JpaRepository<OptionalType, String> {
	@Query("select c from OptionalType c where c.id=?1 and  c.visible='Y' and c.active='Y'")
	OptionalType findById(String id);
}
