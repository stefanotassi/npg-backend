package com.ferrari.modis.dpg.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import org.springframework.stereotype.Repository;
import com.ferrari.modis.dpg.model.ui.UiPageAuth;

@Repository
public interface UiPageAuthRepository extends JpaRepository<UiPageAuth, String> {

	@Query("select d from UiPageAuth d where d.active='Y' and d.id.idPage=?1")
	List<UiPageAuth> findByPage(String idPage);
	

}
