package com.ferrari.modis.dpg.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.ferrari.modis.dpg.model.app.VersionOptAttc;
import com.ferrari.modis.dpg.model.app.VersionOptAttcPK;

@Repository
public interface VersionOptAttcRepository extends JpaRepository<VersionOptAttc, VersionOptAttcPK> {


  
}
