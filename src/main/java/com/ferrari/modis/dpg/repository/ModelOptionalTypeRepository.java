package com.ferrari.modis.dpg.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import org.springframework.stereotype.Repository;
import com.ferrari.modis.dpg.model.app.ModelOptionalType;

@Repository
public interface ModelOptionalTypeRepository extends JpaRepository<ModelOptionalType, Long> {

	
	@Query("select c from ModelOptionalType c where c.id=?1 and  c.active='Y'")
	ModelOptionalType findById(Long id);

	@Query("select c from ModelOptionalType c where c.idModel=?1 and c.idOptionalType=?2 and  c.active='Y'")
	ModelOptionalType findByModelAndOptionalType(Long idModel, String optionalType);


	@Query("select c from ModelOptionalType c where c.idModel=?1  and  c.active='Y'")
	List<ModelOptionalType> findByModel(Long idModel);

	
}
 