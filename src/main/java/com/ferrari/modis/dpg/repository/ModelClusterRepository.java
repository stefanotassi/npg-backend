package com.ferrari.modis.dpg.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import org.springframework.stereotype.Repository;
import com.ferrari.modis.dpg.model.app.ModelCluster;

@Repository
public interface ModelClusterRepository extends JpaRepository<ModelCluster, String> {

	@Query("select c from ModelCluster c where c.active='Y'")
	List<ModelCluster> findAll();
	@Query("select c from ModelCluster c where c.id=?1 and c.active='Y'")
	ModelCluster findById(String id);
}
