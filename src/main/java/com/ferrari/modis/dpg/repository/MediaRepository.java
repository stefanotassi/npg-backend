package com.ferrari.modis.dpg.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.ferrari.modis.dpg.model.app.Media;

@Repository
public interface MediaRepository extends JpaRepository<Media, String> {

	@Query(value ="SELECT lower(RAWTOHEX(SYS_GUID())) FROM DUAL", nativeQuery = true)
	String guid();
	
	@Query("select c from Media c where c.id=?1 and c.active='Y'")
	Media findById(String id);
	 
}
