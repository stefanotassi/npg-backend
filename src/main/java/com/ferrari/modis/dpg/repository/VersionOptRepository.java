package com.ferrari.modis.dpg.repository;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import org.springframework.stereotype.Repository;
import com.ferrari.modis.dpg.model.app.VersionOpt;

@Repository
public interface VersionOptRepository extends JpaRepository<VersionOpt, Long> {

	@Query("select c from VersionOpt c where c.idVersion=?1 and  c.IdModelOptionalSubGroup=?2 and c.active='Y' order by c.sequence")
	List<VersionOpt> findByVersionAndModelOptSub(Long idVersion, Long idModeloptSubgroup);
	
	@Query("select c from VersionOpt c where c.id=?1  and c.active='Y'")
	VersionOpt findById(Long idOptional);
	
	
	@Query("select c from VersionOpt c where c.idVersion=?1  and c.active='Y'")
	List<VersionOpt> findByVersion(Long idVersion);

	@Query("select c from VersionOpt c where c.idVersion=?1 and c.code=?2 ") // includo gli annullati: usata per testare in insert/update
	VersionOpt findByVersionAndOpt(@Param ("idVersion") Long idVersion
								 , @Param ("code") String code);

	@Query("select case when count(c)>0 then true else false end  from VersionOpt c where c.idVersion=?1 and c.code=?2  and c.active='Y'")
	boolean existsVersionAndOpt(@Param ("idVersion") Long idVersion
			 				  , @Param ("code") String code);
	
	// query nativa per determinare data ultima modifica
	@Query(value = "select last_update from VERSION_OPT_LASTUPD_VW where id=?1", nativeQuery = true)
	Date  getLastUpdateDate(Long idVersionOpt);
	;
	
	
	// determina primo sequence number
	@Query(value = "select coalesce(max(seq), 0)+10 as SEQ from version_opt where id_version=?1 and id_model_opt_subgroup=?2", nativeQuery = true)
	Long  getNextSequenceNumber(Long idVersion, Long idModeloptSubgroup);
	;

	// per determinare la cancellabilità di un sottogruppoOpt
	@Query("select case when count(c)>0 then true else false end  from VersionOpt c where c.IdModelOptionalSubGroup=?1")
	boolean existsOptBySubGroup(@Param ("idSubgroup") Long idSubgroup);

	@Query(value = "select distinct c.id_version from ( " + 
			"select b.id_version from version_opt_pics a join version_opt b on a.id_version_opt = b.id where a.id_image = ?1 " + 
			"union  " + 
			"select b.id_version from version_opt_pics a join version_opt b on a.id_version_opt = b.id where a.id_image_low = ?1 " + 
			") c ",nativeQuery = true)
	List<BigDecimal> findByLVersionOptPics_IdImage(String idImage);

	
	@Query("select c from VersionOpt c where c.idVersion=?1 and c.codeErp = ?2 and c.active='Y'")
	List<VersionOpt> findByVersionAndCodeErp(Long idVersion, String codeErp);
	
	
}
