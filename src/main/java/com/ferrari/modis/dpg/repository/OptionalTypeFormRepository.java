package com.ferrari.modis.dpg.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import org.springframework.stereotype.Repository;
import com.ferrari.modis.dpg.model.app.OptionalTypeForm;

@Repository
public interface OptionalTypeFormRepository extends JpaRepository<OptionalTypeForm, Long> {

	@Query("select c from OptionalTypeForm c where c.idOptionalType=?1 and  c.active='Y'")
	List<OptionalTypeForm> findByOptType(String optionalType);
	
	 
}
