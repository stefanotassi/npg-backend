package com.ferrari.modis.dpg.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import org.springframework.stereotype.Repository;
import com.ferrari.modis.dpg.model.app.ModelOptionalSubGroup;

@Repository
public interface ModelOptionalSubGroupRepository extends JpaRepository<ModelOptionalSubGroup, Long> {

	
	@Query("select c from ModelOptionalSubGroup c where c.modelOptionalGroup.id=?1 and  c.active='Y' order by c.sequence")
	List<ModelOptionalSubGroup> findByModelOptGroup(Long idModelOptGroup);


}
 