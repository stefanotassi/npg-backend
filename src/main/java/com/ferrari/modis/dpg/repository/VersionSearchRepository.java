package com.ferrari.modis.dpg.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import org.springframework.stereotype.Repository;
import com.ferrari.modis.dpg.model.app.VersionSearch;

@Repository
public interface VersionSearchRepository extends JpaRepository<VersionSearch, String> {


	@Query("select c from VersionSearch c "
			+ "where c.idVersion=:idVersion "
			+ "and c.language=:lang "
			+ "and (c.visible='Y' or :optlVisibleAll='Y') "
			+ "and c.pub  in (:pubRow) "
			+ "and lower(c.description) like  %:searchText%")
	List<VersionSearch> findBytext( @Param("idVersion") 		Long idVersion,
									@Param("lang")	    		String lang,
									@Param("pubRow")			List<String> pubRow,
									@Param("optlVisibleAll")	String optlVisibleAll,
									@Param("searchText") 		String searchText );	 

}

