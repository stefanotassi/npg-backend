package com.ferrari.modis.dpg.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import org.springframework.stereotype.Repository;
import com.ferrari.modis.dpg.model.app.VersionStatus;

@Repository
public interface VersionStatusRepository extends JpaRepository<VersionStatus, String> {

	@Query("select c from VersionStatus c where c.id=?1")
	VersionStatus findById(String id);
	
	
	 
}
