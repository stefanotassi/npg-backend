package com.ferrari.modis.dpg.repository.ext;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface ExtJpaRepo<T,ID extends Serializable> extends JpaRepository<T, ID >{

	Class<T> getDomainClass() ;

	
}
