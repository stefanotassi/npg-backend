package com.ferrari.modis.dpg.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import org.springframework.stereotype.Repository;
import com.ferrari.modis.dpg.model.ui.UiSecAction;

@Repository
public interface UiSecActionRepository extends JpaRepository<UiSecAction, String> {
	@Query("select d from UiSecAction d where d.active='Y' and d.id=?1")
	UiSecAction findById(String id);
	

}
