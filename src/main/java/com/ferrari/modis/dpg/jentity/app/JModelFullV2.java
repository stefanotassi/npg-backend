package com.ferrari.modis.dpg.jentity.app;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class JModelFullV2 {
	
	
	Long id;
	String description_it;
	String description_en;
	String code;
	String idImageHome;
	String idImageLogo;
	String idImageCover;

	String urlImageHome;
	String urlImageLogo;
	String urlImageCover;
	

	String idCluster;
	Long sequence;
	String visibility;
	String confidential;
	

	String mediaTemplateImageHome;
	String mediaTemplateImageLogo;
	String mediaTemplateImageCover;
	
	String quality;
	
	
	

	@JsonProperty("types")	
	List<JModelOptionalType> lJModelOptionalType;
	
	
	public JModelFullV2() {
		lJModelOptionalType = new ArrayList<JModelOptionalType>();
	}
	
	 
	public Long getId() {
		return id;
	}


	public String getCode() {
		return code;
	}

	public String getIdImageHome() {
		return idImageHome;
	}
	public void setId(Long id) {
		this.id = id;
	}

	public void setCode(String code) {
		this.code = code;
	}
	public void setIdImageHome(String idImage) {
		this.idImageHome = idImage;
	}
	public String getIdCluster() {
		return idCluster;
	}
	
	public void setIdCluster(String idCluster) {
		this.idCluster = idCluster;
	}


	public List<JModelOptionalType> getlJModelOptionalType() {
		return lJModelOptionalType;
	}


	public void setlJModelOptionalType(List<JModelOptionalType> lJModelOptionalType) {
		this.lJModelOptionalType = lJModelOptionalType;
	}


	public Long getSequence() {
		return sequence;
	}


	public void setSequence(Long sequence) {
		this.sequence = sequence;
	}


	public String getVisibility() {
		return visibility;
	}


	public void setVisibility(String visibility) {
		this.visibility = visibility;
	}


	public String getDescription_it() {
		return description_it;
	}


	public void setDescription_it(String description_it) {
		this.description_it = description_it;
	}


	public String getDescription_en() {
		return description_en;
	}


	public void setDescription_en(String description_en) {
		this.description_en = description_en;
	}


	public String getIdImageLogo() {
		return idImageLogo;
	}


	public void setIdImageLogo(String idImageLogo) {
		this.idImageLogo = idImageLogo;
	}


	public String getIdImageCover() {
		return idImageCover;
	}


	public void setIdImageCover(String idImageCover) {
		this.idImageCover = idImageCover;
	}


	public String getMediaTemplateImageHome() {
		return mediaTemplateImageHome;
	}


	public void setMediaTemplateImageHome(String mediaTemplateImage) {
		this.mediaTemplateImageHome = mediaTemplateImage;
	}


	public String getMediaTemplateImageLogo() {
		return mediaTemplateImageLogo;
	}


	public void setMediaTemplateImageLogo(String mediaTemplateImageLogo) {
		this.mediaTemplateImageLogo = mediaTemplateImageLogo;
	}


	public String getMediaTemplateImageCover() {
		return mediaTemplateImageCover;
	}


	public void setMediaTemplateImageCover(String mediaTemplateImageCover) {
		this.mediaTemplateImageCover = mediaTemplateImageCover;
	}


	public String getUrlImageHome() {
		return urlImageHome;
	}


	public void setUrlImageHome(String urlImageHome) {
		this.urlImageHome = urlImageHome;
	}


	public String getUrlImageLogo() {
		return urlImageLogo;
	}


	public void setUrlImageLogo(String urlImageLogo) {
		this.urlImageLogo = urlImageLogo;
	}


	public String getUrlImageCover() {
		return urlImageCover;
	}


	public void setUrlImageCover(String urlImageCover) {
		this.urlImageCover = urlImageCover;
	}


	public String getConfidential() {
		return confidential;
	}


	public void setConfidential(String confidential) {
		this.confidential = confidential;
	}
	
	
	public String getQuality() {
		return quality;
	}


	public void setQuality(String quality) {
		this.quality = quality;
	}


	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("JModelFullV2 [id=");
		builder.append(id);
		builder.append(", description_it=");
		builder.append(description_it);
		builder.append(", description_en=");
		builder.append(description_en);
		builder.append(", code=");
		builder.append(code);
		builder.append(", idImageHome=");
		builder.append(idImageHome);
		builder.append(", idImageLogo=");
		builder.append(idImageLogo);
		builder.append(", idImageCover=");
		builder.append(idImageCover);
		builder.append(", urlImageHome=");
		builder.append(urlImageHome);
		builder.append(", urlImageLogo=");
		builder.append(urlImageLogo);
		builder.append(", urlImageCover=");
		builder.append(urlImageCover);
		builder.append(", idCluster=");
		builder.append(idCluster);
		builder.append(", sequence=");
		builder.append(sequence);
		builder.append(", visibility=");
		builder.append(visibility);
		builder.append(", confidential=");
		builder.append(confidential);
		builder.append(", mediaTemplateImageHome=");
		builder.append(mediaTemplateImageHome);
		builder.append(", mediaTemplateImageLogo=");
		builder.append(mediaTemplateImageLogo);
		builder.append(", mediaTemplateImageCover=");
		builder.append(mediaTemplateImageCover);
		builder.append(", quality=");
		builder.append(quality);
		builder.append(", lJModelOptionalType=");
		builder.append(lJModelOptionalType);
		builder.append("]");
		return builder.toString();
	}


	
	
	
}
