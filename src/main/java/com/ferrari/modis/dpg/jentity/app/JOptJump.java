package com.ferrari.modis.dpg.jentity.app;

// per presentazione del id P1/P2 corrente + sistema di switch

public class JOptJump {

	String idModelOptionaType; // P1, P2 : Current type


	// se esiste Px (es P2)
	String swapIdModelOptionaType; // P1, P2 target Type
	Long swapIdVersion;
	Long swapId;			// opt con stesso CODE_ERP su Px alternativo
	
	
	
	
	
	
	
	
	
	public String getIdModelOptionaType() {
		return idModelOptionaType;
	}
	public void setIdModelOptionaType(String idModelOptionaType) {
		this.idModelOptionaType = idModelOptionaType;
	}
	public String getSwapIdModelOptionaType() {
		return swapIdModelOptionaType;
	}
	public void setSwapIdModelOptionaType(String swapIdModelOptionaType) {
		this.swapIdModelOptionaType = swapIdModelOptionaType;
	}
	public Long getSwapIdVersion() {
		return swapIdVersion;
	}
	public void setSwapIdVersion(Long swapIdVersion) {
		this.swapIdVersion = swapIdVersion;
	}
	public Long getSwapId() {
		return swapId;
	}
	public void setSwapId(Long swapId) {
		this.swapId = swapId;
	}
	
	
	
	

}
