package com.ferrari.modis.dpg.jentity.base;

import java.util.Comparator;

public class JValueTextCompareByDescription implements Comparator<JValueText>{


	public int compare(JValueText o1, JValueText o2) {
		return o1.getText().compareToIgnoreCase(o2.getText());
	}
}
