package com.ferrari.modis.dpg.jentity.app;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import io.swagger.api.utils.UnixTimestampDeserializer;
import io.swagger.api.utils.UnixTimestampSerializer;

public class JVersionOptFull {
	
	
	private Long id;
	private String code;
	private String codeErp;
	
	private String description;
	private Long sequence;
	
	private Long idModelOptionalSubGroup;
	private Long idModelOptionalGroup;

	
	
	@JsonProperty("groups")
	private List<JGroup> lJGroup;
	@JsonProperty("subGroups")
	private List<JSubGroup> lJSubGroup;
	
//	@JsonProperty("optionalErp")
//	private List<JOptionalErp> lJOptionalErp;
	
	private String visible;
	private String orderable;
	private String orderableFlag;
	private String versionModel;

	
	@JsonProperty("lastUpdate")
	@JsonSerialize(using=UnixTimestampSerializer.class)
	@JsonDeserialize(using=UnixTimestampDeserializer.class)
	private Date   lastUpdate;
	
	@JsonProperty("model")
	private JModel	jModel;
	
	@JsonProperty("version")
	private JVersion jVersion;


	@JsonProperty("type")
	JModelJump	jModelJump;
	
	@JsonProperty("switchToOptional")
	JOptJump	jOptJump;
	

	
	@JsonProperty("pics")
	private List<JVersionOptPics> lJVersionOptPics;
	@JsonProperty("text")
	private List<JVersionOptText> lJVersionOptText;	
	@JsonProperty("figurini")
	private List<JVersionAttc> lJVersionAttc;		
	
	
	public JVersionOptFull() {
	lJVersionOptPics = new ArrayList<JVersionOptPics>();
	lJVersionOptText = new ArrayList<JVersionOptText>();
	lJVersionAttc = new ArrayList<JVersionAttc>();
	lJGroup = new ArrayList<JGroup>();
	lJSubGroup = new ArrayList<JSubGroup>();
//	lJOptionalErp = new ArrayList<JOptionalErp>();
}
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Long getSequence() {
		return sequence;
	}
	public void setSequence(Long sequence) {
		this.sequence = sequence;
	}
	public Long getIdModelOptionalSubGroup() {
		return idModelOptionalSubGroup;
	}
	public void setIdModelOptionalSubGroup(Long idModelOptionalSubGroup) {
		this.idModelOptionalSubGroup = idModelOptionalSubGroup;
	}
	public String getVisible() {
		return visible;
	}
	public void setVisible(String visible) {
		this.visible = visible;
	}
	public String getOrderable() {
		return orderable;
	}
	public void setOrderable(String orderable) {
		this.orderable = orderable;
	}
	public String getOrderableFlag() {
		return orderableFlag;
	}
	public void setOrderableFlag(String orderableFlag) {
		this.orderableFlag = orderableFlag;
	}

	public String getVersionModel() {
		return versionModel;
	}
	public void setVersionModel(String versionModel) {
		this.versionModel = versionModel;
	}
	public List<JVersionOptPics> getlJVersionOptPics() {
		return lJVersionOptPics;
	}
	public void setlJVersionOptPics(List<JVersionOptPics> lJVersionOptPics) {
		this.lJVersionOptPics = lJVersionOptPics;
	}
	public List<JVersionOptText> getlJVersionOptText() {
		return lJVersionOptText;
	}
	public void setlJVersionOptText(List<JVersionOptText> lJVersionOptText) {
		this.lJVersionOptText = lJVersionOptText;
	}
	public JVersion getjVersion() {
		return jVersion;
	}
	public void setjVersion(JVersion jVersion) {
		this.jVersion = jVersion;
	}
	public List<JVersionAttc> getlJVersionAttc() {
		return lJVersionAttc;
	}
	public void setlJVersionAttc(List<JVersionAttc> lJVersionAttc) {
		this.lJVersionAttc = lJVersionAttc;
	}
	public Date getLastUpdate() {
		return lastUpdate;
	}
	public void setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}


	public Long getIdModelOptionalGroup() {
		return idModelOptionalGroup;
	}


	public void setIdModelOptionalGroup(Long idModelOptionalGroup) {
		this.idModelOptionalGroup = idModelOptionalGroup;
	}


	public JModel getjModel() {
		return jModel;
	}


	public void setjModel(JModel jModel) {
		this.jModel = jModel;
	}


	public List<JGroup> getlJGroup() {
		return lJGroup;
	}


	public void setlJGroup(List<JGroup> lJGroup) {
		this.lJGroup = lJGroup;
	}


	public List<JSubGroup> getlJSubGroup() {
		return lJSubGroup;
	}


	public void setlJSubGroup(List<JSubGroup> lJSubGroup) {
		this.lJSubGroup = lJSubGroup;
	}


	public JModelJump getJModelJump() {
		return jModelJump;
	}


	public void setJModelJump(JModelJump jModelJump) {
		this.jModelJump = jModelJump;
	}


	public String getCodeErp() {
		return codeErp;
	}


	public void setCodeErp(String codeErp) {
		this.codeErp = codeErp;
	}








	public JOptJump getjOptJump() {
		return jOptJump;
	}


	public void setjOptJump(JOptJump jOptJump) {
		this.jOptJump = jOptJump;
	}


//	public List<JOptionalErp> getlJOptionalErp() {
//		return lJOptionalErp;
//	}
//
//
//	public void setlJOptionalErp(List<JOptionalErp> lJOptionalErp) {
//		this.lJOptionalErp = lJOptionalErp;
//	}


	public JModelJump getjModelJump() {
		return jModelJump;
	}


	public void setjModelJump(JModelJump jModelJump) {
		this.jModelJump = jModelJump;
	}





	
	
	
}
