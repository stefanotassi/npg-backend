package com.ferrari.modis.dpg.jentity.app;

//usate in homepage
public class JModel {
	
	
	Long id;
	String description;
	String code;
	Long seq;
	String idImage;
	String idImageLogo;
	String idImageCover;

	String urlImage;
	String urlImageLogo;
	String urlImageCover;
	
	String mediaTemplateImage;
	String mediaTemplateImageLogo;
	String mediaTemplateImageCover;
	
	String visibility;
	String confidential;
	
	String quality;
	
	public String getQuality() {
		return quality;
	}
	public void setQuality(String quality) {
		this.quality = quality;
	}
	public Long getId() {
		return id;
	}
	public String getDescription() {
		return description;
	}
	public String getCode() {
		return code;
	}
	public Long getSeq() {
		return seq;
	}
	public String getIdImage() {
		return idImage;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public void setSeq(Long seq) {
		this.seq = seq;
	}
	public void setIdImage(String idImage) {
		this.idImage = idImage;
	}
	public String getIdImageLogo() {
		return idImageLogo;
	}
	public void setIdImageLogo(String idImageLogo) {
		this.idImageLogo = idImageLogo;
	}
	public String getIdImageCover() {
		return idImageCover;
	}
	public void setIdImageCover(String idImageCover) {
		this.idImageCover = idImageCover;
	}
	public String getMediaTemplateImage() {
		return mediaTemplateImage;
	}
	public void setMediaTemplateImage(String mediaTemplateImage) {
		this.mediaTemplateImage = mediaTemplateImage;
	}
	public String getMediaTemplateImageLogo() {
		return mediaTemplateImageLogo;
	}
	public void setMediaTemplateImageLogo(String mediaTemplateImageLogo) {
		this.mediaTemplateImageLogo = mediaTemplateImageLogo;
	}
	public String getMediaTemplateImageCover() {
		return mediaTemplateImageCover;
	}
	public void setMediaTemplateImageCover(String mediaTemplateImageCover) {
		this.mediaTemplateImageCover = mediaTemplateImageCover;
	}
	
	
	public String getUrlImage() {
		return urlImage;
	}
	public void setUrlImage(String urlImage) {
		this.urlImage = urlImage;
	}
	public String getUrlImageLogo() {
		return urlImageLogo;
	}
	public void setUrlImageLogo(String urlImageLogo) {
		this.urlImageLogo = urlImageLogo;
	}
	public String getUrlImageCover() {
		return urlImageCover;
	}
	public void setUrlImageCover(String urlImageCover) {
		this.urlImageCover = urlImageCover;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("JModel [id=");
		builder.append(id);
		builder.append(", description=");
		builder.append(description);
		builder.append(", code=");
		builder.append(code);
		builder.append(", seq=");
		builder.append(seq);
		builder.append(", idImage=");
		builder.append(idImage);
		builder.append(", idImageLogo=");
		builder.append(idImageLogo);
		builder.append(", idImageCover=");
		builder.append(idImageCover);
		builder.append(", urlImage=");
		builder.append(urlImage);
		builder.append(", urlImageLogo=");
		builder.append(urlImageLogo);
		builder.append(", urlImageCover=");
		builder.append(urlImageCover);
		builder.append(", mediaTemplateImage=");
		builder.append(mediaTemplateImage);
		builder.append(", mediaTemplateImageLogo=");
		builder.append(mediaTemplateImageLogo);
		builder.append(", mediaTemplateImageCover=");
		builder.append(mediaTemplateImageCover);
		builder.append(", visibility=");
		builder.append(visibility);
		builder.append(", confidential=");
		builder.append(confidential);
		builder.append("]");
		return builder.toString();
	}
	public String getConfidential() {
		return confidential;
	}
	public void setConfidential(String confidential) {
		this.confidential = confidential;
	}
	public String getVisibility() {
		return visibility;
	}
	public void setVisibility(String visibility) {
		this.visibility = visibility;
	}

	
	
	
}
