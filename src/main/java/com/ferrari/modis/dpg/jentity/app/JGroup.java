package com.ferrari.modis.dpg.jentity.app;

// esportare lista gruppi in dettaglio optional

public class JGroup {
	 
	
	Long id;
	String code;
	String description;
	Long seq;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Long getSeq() {
		return seq;
	}
	public void setSeq(Long seq) {
		this.seq = seq;
	}
	

	
}
