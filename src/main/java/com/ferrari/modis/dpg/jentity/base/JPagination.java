package com.ferrari.modis.dpg.jentity.base;

public class JPagination {
	
	Long 	rowsPerPage;
	Long 	totalRows;
	public Long getRowsPerPage() {
		return rowsPerPage;
	}
	public void setRowsPerPage(Long rowsPerPage) {
		this.rowsPerPage = rowsPerPage;
	}
	public Long getTotalRows() {
		return totalRows;
	}
	public void setTotalRows(Long totalRows) {
		this.totalRows = totalRows;
	}

}
