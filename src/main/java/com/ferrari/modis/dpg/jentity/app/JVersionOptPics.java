package com.ferrari.modis.dpg.jentity.app;

public class JVersionOptPics {
	
	private Long id;
	//private Long idVersionOpt;
	private String idImage;
	private String idImageLow;
	
	private String urlImage;
	private String urlImageLow;
	
	
	private String picType;
	private String description;
	
	private Long sequence;
	
	private String mediaTemplateImageHiRes;
	private String mediaTemplateImageLoRes;

	private String description_en;	
	private String description_it;	
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}


	public String getIdImage() {
		return idImage;
	}
	public void setIdImage(String idImage) {
		this.idImage = idImage;
	}
	public String getPicType() {
		return picType;
	}
	public void setPicType(String picType) {
		this.picType = picType;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
//	public List<JVersionOpt> getlJVersionOpt() {
//		return lJVersionOpt;
//	}
//	public void setlJVersionOpt(List<JVersionOpt> lJVersionOpt) {
//		this.lJVersionOpt = lJVersionOpt;
//	}
	public Long getSequence() {
		return sequence;
	}
	public void setSequence(Long sequence) {
		this.sequence = sequence;
	}
	public String getIdImageLow() {
		return idImageLow;
	}
	public void setIdImageLow(String idImageLow) {
		this.idImageLow = idImageLow;
	}
	public String getMediaTemplateImageHiRes() {
		return mediaTemplateImageHiRes;
	}
	public void setMediaTemplateImageHiRes(String mediaTemplateImageHiRes) {
		this.mediaTemplateImageHiRes = mediaTemplateImageHiRes;
	}
	public String getMediaTemplateImageLoRes() {
		return mediaTemplateImageLoRes;
	}
	public void setMediaTemplateImageLoRes(String mediaTemplateImageLoRes) {
		this.mediaTemplateImageLoRes = mediaTemplateImageLoRes;
	}
	public String getUrlImage() {
		return urlImage;
	}
	public void setUrlImage(String urlImage) {
		this.urlImage = urlImage;
	}
	public String getUrlImageLow() {
		return urlImageLow;
	}
	public void setUrlImageLow(String urlImageLow) {
		this.urlImageLow = urlImageLow;
	}
	public String getDescription_en() {
		return description_en;
	}
	public void setDescription_en(String description_en) {
		this.description_en = description_en;
	}
	public String getDescription_it() {
		return description_it;
	}
	public void setDescription_it(String description_it) {
		this.description_it = description_it;
	}
}
	
	
	
