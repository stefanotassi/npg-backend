package com.ferrari.modis.dpg.jentity.base;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.ferrari.modis.dpg.commons.i18n.LocalizationType;

import io.swagger.annotations.ApiModelProperty;


public class JEntity {
	
	String id;
	String name;
	LocalizationType language;
	JEntityData data=null; 			// payload data del json
	
	public JEntity()
	{
		this.data=new JEntityData();
		
	}

	
	@ApiModelProperty(value = "")
	@JsonProperty("id")
	public String getId() {
		return id;
	}
	@ApiModelProperty(value = "")
	@JsonProperty("name")
	public String getName() {
		return name;
	}
	@ApiModelProperty(value = "")
	@JsonProperty("data")
	public JEntityData getData() {
		return data;
	}	

	
	@JsonProperty("lang")	
	public LocalizationType getLanguage() {
		return language;
	}

	

	public void setId(String id) {
		this.id = id;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setData(JEntityData data) {
		this.data = data;
	}




	public void setLanguage(LocalizationType language) {
		this.language = language;
	}
	



	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	



}
