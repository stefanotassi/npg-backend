package com.ferrari.modis.dpg.jentity.app;


public class JForm {
	 
	
	String id;
	String description;
	Long seq;
	String idImage;
	String urlImage;
	
	
	
	public String getUrlImage() {
		return urlImage;
	}
	public void setUrlImage(String urlImage) {
		this.urlImage = urlImage;
	}
	public String getId() {
		return id;
	}
	public String getDescription() {
		return description;
	}
	public Long getSeq() {
		return seq;
	}
	public String getIdImage() {
		return idImage;
	}
	public void setId(String id) {
		this.id = id;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public void setSeq(Long seq) {
		this.seq = seq;
	}
	public void setIdImage(String idImage) {
		this.idImage = idImage;
	}
	
	
	 

	
}
