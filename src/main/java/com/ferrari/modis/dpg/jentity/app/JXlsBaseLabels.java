package com.ferrari.modis.dpg.jentity.app;

public class JXlsBaseLabels {
	
	private String labelModel;
	private String labelArea; // es P1
	private String labelMacroCategoria;
	private String labelSottoCategoria;
	
	private String labelCodOpt;
	private String labelShortDescOpt;
	private String labelVincoliOpt;
	private String labelVincoliOmologativi;
	private String labelOrdFlag;
	private String labelOrdText;
	private String labelDescription;
	private String labelVersionModel;
	private String labelUpdateDate;
	private String labelInternalNote;
	private String labelStatusSviluppo;
	private String labelCapProduttiva;
	private String labelSequence;	
	private String labelVisibility;
	public String getLabelModel() {
		return labelModel;
	}
	public void setLabelModel(String labelModel) {
		this.labelModel = labelModel;
	}
	public String getLabelArea() {
		return labelArea;
	}
	public void setLabelArea(String labelArea) {
		this.labelArea = labelArea;
	}
	public String getLabelMacroCategoria() {
		return labelMacroCategoria;
	}
	public void setLabelMacroCategoria(String labelMacroCategoria) {
		this.labelMacroCategoria = labelMacroCategoria;
	}
	public String getLabelSottoCategoria() {
		return labelSottoCategoria;
	}
	public void setLabelSottoCategoria(String labelSottoCategoria) {
		this.labelSottoCategoria = labelSottoCategoria;
	}
	public String getLabelCodOpt() {
		return labelCodOpt;
	}
	public void setLabelCodOpt(String labelCodOpt) {
		this.labelCodOpt = labelCodOpt;
	}
	public String getLabelShortDescOpt() {
		return labelShortDescOpt;
	}
	public void setLabelShortDescOpt(String labelShortDescOpt) {
		this.labelShortDescOpt = labelShortDescOpt;
	}
	public String getLabelVincoliOpt() {
		return labelVincoliOpt;
	}
	public void setLabelVincoliOpt(String labelVincoliOpt) {
		this.labelVincoliOpt = labelVincoliOpt;
	}
	public String getLabelVincoliOmologativi() {
		return labelVincoliOmologativi;
	}
	public void setLabelVincoliOmologativi(String labelVincoliOmologativi) {
		this.labelVincoliOmologativi = labelVincoliOmologativi;
	}
	public String getLabelOrdFlag() {
		return labelOrdFlag;
	}
	public void setLabelOrdFlag(String labelOrdFlag) {
		this.labelOrdFlag = labelOrdFlag;
	}
	public String getLabelOrdText() {
		return labelOrdText;
	}
	public void setLabelOrdText(String labelOrdText) {
		this.labelOrdText = labelOrdText;
	}
	public String getLabelDescription() {
		return labelDescription;
	}
	public void setLabelDescription(String labelDescription) {
		this.labelDescription = labelDescription;
	}
	public String getLabelVersionModel() {
		return labelVersionModel;
	}
	public void setLabelVersionModel(String labelVersionModel) {
		this.labelVersionModel = labelVersionModel;
	}
	public String getLabelUpdateDate() {
		return labelUpdateDate;
	}
	public void setLabelUpdateDate(String labelUpdateDate) {
		this.labelUpdateDate = labelUpdateDate;
	}
	public String getLabelInternalNote() {
		return labelInternalNote;
	}
	public void setLabelInternalNote(String labelInternalNote) {
		this.labelInternalNote = labelInternalNote;
	}
	public String getLabelStatusSviluppo() {
		return labelStatusSviluppo;
	}
	public void setLabelStatusSviluppo(String labelStatusSviluppo) {
		this.labelStatusSviluppo = labelStatusSviluppo;
	}
	public String getLabelCapProduttiva() {
		return labelCapProduttiva;
	}
	public void setLabelCapProduttiva(String labelCapProduttiva) {
		this.labelCapProduttiva = labelCapProduttiva;
	}
	public String getLabelSequence() {
		return labelSequence;
	}
	public void setLabelSequence(String labelSequence) {
		this.labelSequence = labelSequence;
	}
	public String getLabelVisibility() {
		return labelVisibility;
	}
	public void setLabelVisibility(String labelVisibility) {
		this.labelVisibility = labelVisibility;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("JXlsBaseLabels [labelModel=");
		builder.append(labelModel);
		builder.append(", labelArea=");
		builder.append(labelArea);
		builder.append(", labelMacroCategoria=");
		builder.append(labelMacroCategoria);
		builder.append(", labelSottoCategoria=");
		builder.append(labelSottoCategoria);
		builder.append(", labelCodOpt=");
		builder.append(labelCodOpt);
		builder.append(", labelShortDescOpt=");
		builder.append(labelShortDescOpt);
		builder.append(", labelVincoliOpt=");
		builder.append(labelVincoliOpt);
		builder.append(", labelVincoliOmologativi=");
		builder.append(labelVincoliOmologativi);
		builder.append(", labelOrdFlag=");
		builder.append(labelOrdFlag);
		builder.append(", labelOrdText=");
		builder.append(labelOrdText);
		builder.append(", labelDescription=");
		builder.append(labelDescription);
		builder.append(", labelVersionModel=");
		builder.append(labelVersionModel);
		builder.append(", labelUpdateDate=");
		builder.append(labelUpdateDate);
		builder.append(", labelInternalNote=");
		builder.append(labelInternalNote);
		builder.append(", labelStatusSviluppo=");
		builder.append(labelStatusSviluppo);
		builder.append(", labelCapProduttiva=");
		builder.append(labelCapProduttiva);
		builder.append(", labelSequence=");
		builder.append(labelSequence);
		builder.append(", labelVisibility=");
		builder.append(labelVisibility);
		builder.append("]");
		return builder.toString();
	}
}
