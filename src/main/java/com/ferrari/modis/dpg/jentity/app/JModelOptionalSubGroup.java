package com.ferrari.modis.dpg.jentity.app;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class JModelOptionalSubGroup {
	
	Long id;
	String description;
	String description_it;	
	String description_en;
	String code;	
	Long seq;
	@JsonProperty("optionals")	
	List<JVersionOpt> lJVersionOptional;
	
	
	public Long getId() {
		return id;
	}
	public String getDescription() {
		return description;
	}
	public String getCode() {
		return code;
	}
	public Long getSeq() {
		return seq;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public void setSeq(Long seq) {
		this.seq = seq;
	}
	public String getDescription_it() {
		return description_it;
	}
	public String getDescription_en() {
		return description_en;
	}
	public void setDescription_it(String description_it) {
		this.description_it = description_it;
	}
	public void setDescription_en(String description_en) {
		this.description_en = description_en;
	}
	public List<JVersionOpt> getlJVersionOptional() {
		return lJVersionOptional;
	}
	public void setlJVersionOptional(List<JVersionOpt> lJVersionOptional) {
		this.lJVersionOptional = lJVersionOptional;
	}
	
	
}
