package com.ferrari.modis.dpg.jentity.app;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
//usate in homepage

public class JModelCluster {
	
	
	String id;
	String description;
	Long seq;
	List<JModel> ljmodel;
	
	public JModelCluster() {
		ljmodel = new ArrayList<JModel>();
	}

	@JsonProperty("id")
	public String getId() {
		return id;
	}
	@JsonProperty("description")
	public String getDescription() {
		return description;
	}

	
	@JsonProperty("sequence")
	public Long getSeq() {
		return seq;
	}
	@JsonProperty("models")
	public List<JModel> getLjmodel() {
		return ljmodel;
	}

	
	
	public void setId(String id) {
		this.id = id;
	}

	public void setDescription(String description) {
		this.description = description;
	}


	public void setSeq(Long seq) {
		this.seq = seq;
	}

	public void setLjmodel(List<JModel> ljmodel) {
		this.ljmodel = ljmodel;
	}
	
	
	
	
}
