package com.ferrari.modis.dpg.jentity.app;


public class JMiniDocument {
	
	
	Long id;
	String description;
	
	String description_it;
	String description_en;
	
	Long seq;

	String idDocument;
	String urlDocument;
	String mediaTemplateDocument;
	
	String visibile;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getSeq() {
		return seq;
	}

	public void setSeq(Long seq) {
		this.seq = seq;
	}

	public String getIdDocument() {
		return idDocument;
	}

	public void setIdDocument(String idDocument) {
		this.idDocument = idDocument;
	}

	public String getUrlDocument() {
		return urlDocument;
	}

	public void setUrlDocument(String urlDocument) {
		this.urlDocument = urlDocument;
	}

	public String getMediaTemplateDocument() {
		return mediaTemplateDocument;
	}

	public void setMediaTemplateDocument(String mediaTemplateDocument) {
		this.mediaTemplateDocument = mediaTemplateDocument;
	}

	public String getVisibile() {
		return visibile;
	}

	public void setVisibile(String visibile) {
		this.visibile = visibile;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("JMiniDocument [id=");
		builder.append(id);
		builder.append(", description=");
		builder.append(description);
		builder.append(", seq=");
		builder.append(seq);
		builder.append(", idDocument=");
		builder.append(idDocument);
		builder.append(", urlDocument=");
		builder.append(urlDocument);
		builder.append(", mediaTemplateDocument=");
		builder.append(mediaTemplateDocument);
		builder.append(", visibile=");
		builder.append(visibile);
		builder.append("]");
		return builder.toString();
	}

	public String getDescription_it() {
		return description_it;
	}

	public void setDescription_it(String description_it) {
		this.description_it = description_it;
	}

	public String getDescription_en() {
		return description_en;
	}

	public void setDescription_en(String description_en) {
		this.description_en = description_en;
	}

	
	
	
	
}
