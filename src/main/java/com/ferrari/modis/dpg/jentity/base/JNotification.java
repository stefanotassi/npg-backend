package com.ferrari.modis.dpg.jentity.base;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Objects;

import javax.xml.bind.annotation.XmlAttribute;


@ApiModel(description = "")
public class JNotification  {
  
  private Long    id = null;
 
  private String  appName = null;
  private Long    applicationId = null;
  private String  userName = null;
  private Long    userId = null; 
  private String  title = null;
  private Integer priority = null;
  private String  description = null;
  private String  href = null;

  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("id")
  public Long getId() {
    return id;
  }
  public void setId(Long id) {
    this.id = id;
  }
  
  public Long getApplicationId() {
	return applicationId;
  }
  public void setApplicationId(Long applicationId) {
	this.applicationId = applicationId;
  }
  
  public Long getUserId() {
	return userId;
  }
  public void setUserId(Long userId) {
	this.userId = userId;
  }
  
  @XmlAttribute
  @ApiModelProperty(value = "")
  @JsonProperty("appName")
  public String getAppName() {
	return appName;
  }
  public void setAppName(String appName) {
	this.appName = appName;
 }
  @XmlAttribute
  @ApiModelProperty(value = "")
  @JsonProperty("userName")
  public String getUserName() {
	return userName;
  }
  public void setUserName(String userName) {
	this.userName = userName;
 }
  /****/
  @XmlAttribute
  @ApiModelProperty(value = "")
  @JsonProperty("title")
  public String getTitle() {
    return title;
  }
  public void setTitle(String title) {
    this.title = title;
  }

  /**
   **/
  @XmlAttribute
  @ApiModelProperty(value = "")
  @JsonProperty("priority")
  public Integer getPriority() {
    return priority;
  }
  public void setPriority(Integer priority) {
    this.priority = priority;
  }

  /**
   **/
  @XmlAttribute
  @ApiModelProperty(value = "")
  @JsonProperty("description")
  public String getDescription() {
    return description;
  }
  public void setDescription(String description) {
    this.description = description;
  }

  /**
   **/
  @XmlAttribute
  @ApiModelProperty(value = "")
  @JsonProperty("href")
  public String getHref() {
    return href;
  }
  public void setHref(String href) {
    this.href = href;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    JNotification notification = (JNotification) o;
    return Objects.equals(id, notification.id) &&
        Objects.equals(title, notification.title) &&
        Objects.equals(priority, notification.priority) &&
        Objects.equals(description, notification.description) &&
        Objects.equals(href, notification.href);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, title, priority, description, href);
  }

  @Override
  public String toString()  {
    StringBuilder sb = new StringBuilder();
    sb.append("class Notification {\n");
    
    sb.append("  id: ").append(id).append("\n");
    sb.append("  title: ").append(title).append("\n");
    sb.append("  priority: ").append(priority).append("\n");
    sb.append("  description: ").append(description).append("\n");
    sb.append("  href: ").append(href).append("\n");
    sb.append("}\n");
    return sb.toString(); 
  }
}
