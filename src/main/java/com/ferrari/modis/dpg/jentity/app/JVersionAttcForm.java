package com.ferrari.modis.dpg.jentity.app;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class JVersionAttcForm {

	
	
	
	@JsonProperty("model")
	JModel	jmodel;

	@JsonProperty("version")
	JVersion	jversion;
	
	@JsonProperty("type")
	JModelJump	jmodelJump;
	
	@JsonProperty("documents")	
	List<JVersionAttc> lJVersionAttc;
	

	public JVersionAttcForm() {
		lJVersionAttc = new ArrayList<JVersionAttc>();

	}




	public JModel getJmodel() {
		return jmodel;
	}
	public void setJmodel(JModel jmodel) {
		this.jmodel = jmodel;
	}

	public JVersion getJversion() {
		return jversion;
	}

	public void setJversion(JVersion jversion) {
		this.jversion = jversion;
	}




	public List<JVersionAttc> getlJVersionAttc() {
		return lJVersionAttc;
	}




	public void setlJVersionAttc(List<JVersionAttc> lJVersionAttc) {
		this.lJVersionAttc = lJVersionAttc;
	}




	public JModelJump getJModelJump() {
		return jmodelJump;
	}




	public void setJModelJump(JModelJump jmodelJump) {
		this.jmodelJump = jmodelJump;
	}


}
