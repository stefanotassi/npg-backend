package com.ferrari.modis.dpg.jentity.app;

public class JPdfType {

	private String pdfType;
	private String pdfVersion;
	
	public String getPdfType() {
		return pdfType;
	}
	public void setPdfType(String pdfType) {
		this.pdfType = pdfType;
	}
	public String getPdfVersion() {
		return pdfVersion;
	}
	public void setPdfVersion(String pdfVersion) {
		this.pdfVersion = pdfVersion;
	}
	
	
}
