package com.ferrari.modis.dpg.jentity.app;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class JVersionOptForm {
// form elenco optionals: gerarchia Gruppo+sottogruppo+optional
	
	
	
	@JsonProperty("model")
	JModel	jmodel;
	@JsonProperty("version")
	JVersion	jversion;
	
	@JsonProperty("type")
	JModelJump	jmodelJump;
	
	@JsonProperty("groups")	
	List<JModelOptionalGroup> lJModelOptionalGroup;

	// TODO V2 serve solo per passare lista figurini da includere in stampa PDF
	@JsonProperty("documents")	
	List<JVersionAttcSimple> lJVersionAttcSimple;
	
	
	
	
	
	
	public JVersionOptForm() {
		lJModelOptionalGroup = new ArrayList<JModelOptionalGroup>();
		lJVersionAttcSimple = new ArrayList<JVersionAttcSimple>();
	}
	public JModel getJmodel() {
		return jmodel;
	}
	public void setJmodel(JModel jmodel) {
		this.jmodel = jmodel;
	}
	public JVersion getJversion() {
		return jversion;
	}
	public void setJversion(JVersion jversion) {
		this.jversion = jversion;
	}
	public List<JModelOptionalGroup> getlJModelOptionalGroup() {
		return lJModelOptionalGroup;
	}
	public void setlJModelOptionalGroup(List<JModelOptionalGroup> lJModelOptionalGroup) {
		this.lJModelOptionalGroup = lJModelOptionalGroup;
	}
	public JModelJump getJModelJump() {
		return jmodelJump;
	}
	public void setJModelJump(JModelJump jmodelJump) {
		this.jmodelJump = jmodelJump;
	}
	public List<JVersionAttcSimple> getlJVersionAttcSimple() {
		return lJVersionAttcSimple;
	}
	public void setlJVersionAttcSimple(List<JVersionAttcSimple> lJVersionAttcSimple) {
		this.lJVersionAttcSimple = lJVersionAttcSimple;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("JVersionOptForm [jmodel=");
		builder.append(jmodel);
		builder.append(", jversion=");
		builder.append(jversion);
		builder.append(", jmodelJump=");
		builder.append(jmodelJump);
		builder.append(", lJModelOptionalGroup=");
		builder.append(lJModelOptionalGroup);
		builder.append(", lJVersionAttcSimple=");
		builder.append(lJVersionAttcSimple);
		builder.append("]");
		return builder.toString();
	}
	
}
