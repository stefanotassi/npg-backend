package com.ferrari.modis.dpg.jentity.app;

import java.util.Date;

public class JPdfOptional {
	
	private Long optId;
	private String optCode;
	private String optDescription;
	
	private String visible;
	private String orderableText;
	private String orderableFlag;
	private String versionModel;
	
	private Long idModelOptionalSubGroup;
	private Long idModelOptionalGroup;
	
	private String codGroup;
	private String codSubgroup;
	private String groupDescription;
	private String subgroupDescription;
	
	private Date  lastUpdate;
	
	private Long seq;
	private Long seqGroup;	
	private Long seqSubgroup;
	
	private String idImageP;
	private String idImageS1;
	private String idImageS2;
	private String idImageS3;
	private String idImageS4;

	private String urlImageP;
	private String urlImageS1;
	private String urlImageS2;
	private String urlImageS3;
	private String urlImageS4;
	
	private String textA1Long;
	private String textA5VincoliOpt;
	private String textB1VincoliOmologazione;
	private String textC0NoteInterne;
	private String textC2StatusSvil;	
	private String textC4Capacity;
	
	public Long getOptId() {
		return optId;
	}
	public void setOptId(Long optId) {
		this.optId = optId;
	}
	public String getOptCode() {
		return optCode;
	}
	public void setOptCode(String optCode) {
		this.optCode = optCode;
	}
	public String getOptDescription() {
		return optDescription;
	}
	public void setOptDescription(String optDescription) {
		this.optDescription = optDescription;
	}
	public String getVisible() {
		return visible;
	}
	public void setVisible(String visible) {
		this.visible = visible;
	}
	public String getOrderableText() {
		return orderableText;
	}
	public void setOrderableText(String orderableText) {
		this.orderableText = orderableText;
	}
	public String getOrderableFlag() {
		return orderableFlag;
	}
	public void setOrderableFlag(String orderableFlag) {
		this.orderableFlag = orderableFlag;
	}
	public String getVersionModel() {
		return versionModel;
	}
	public void setVersionModel(String versionModel) {
		this.versionModel = versionModel;
	}
	public Long getIdModelOptionalSubGroup() {
		return idModelOptionalSubGroup;
	}
	public void setIdModelOptionalSubGroup(Long idModelOptionalSubGroup) {
		this.idModelOptionalSubGroup = idModelOptionalSubGroup;
	}
	public Long getIdModelOptionalGroup() {
		return idModelOptionalGroup;
	}
	public void setIdModelOptionalGroup(Long idModelOptionalGroup) {
		this.idModelOptionalGroup = idModelOptionalGroup;
	}
	public String getCodGroup() {
		return codGroup;
	}
	public void setCodGroup(String codGroup) {
		this.codGroup = codGroup;
	}
	public String getCodSubgroup() {
		return codSubgroup;
	}
	public void setCodSubgroup(String codSubgroup) {
		this.codSubgroup = codSubgroup;
	}
	public String getGroupDescription() {
		return groupDescription;
	}
	public void setGroupDescription(String groupDescription) {
		this.groupDescription = groupDescription;
	}
	public String getSubgroupDescription() {
		return subgroupDescription;
	}
	public void setSubgroupDescription(String subgroupDescription) {
		this.subgroupDescription = subgroupDescription;
	}
	public Date getLastUpdate() {
		return lastUpdate;
	}
	public void setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}
	public Long getSeq() {
		return seq;
	}
	public void setSeq(Long seq) {
		this.seq = seq;
	}
	public Long getSeqGroup() {
		return seqGroup;
	}
	public void setSeqGroup(Long seqGroup) {
		this.seqGroup = seqGroup;
	}
	public Long getSeqSubgroup() {
		return seqSubgroup;
	}
	public void setSeqSubgroup(Long seqSubgroup) {
		this.seqSubgroup = seqSubgroup;
	}
	public String getIdImageP() {
		return idImageP;
	}
	public void setIdImageP(String idImageP) {
		this.idImageP = idImageP;
	}
	public String getIdImageS1() {
		return idImageS1;
	}
	public void setIdImageS1(String idImageS1) {
		this.idImageS1 = idImageS1;
	}
	public String getIdImageS2() {
		return idImageS2;
	}
	public void setIdImageS2(String idImageS2) {
		this.idImageS2 = idImageS2;
	}
	public String getIdImageS3() {
		return idImageS3;
	}
	public void setIdImageS3(String idImageS3) {
		this.idImageS3 = idImageS3;
	}
	public String getIdImageS4() {
		return idImageS4;
	}
	public void setIdImageS4(String idImageS4) {
		this.idImageS4 = idImageS4;
	}
	public String getTextA1Long() {
		return textA1Long;
	}
	public void setTextA1Long(String textA1Long) {
		this.textA1Long = textA1Long;
	}
	public String getTextA5VincoliOpt() {
		return textA5VincoliOpt;
	}
	public void setTextA5VincoliOpt(String textA5VincoliOpt) {
		this.textA5VincoliOpt = textA5VincoliOpt;
	}
	public String getTextB1VincoliOmologazione() {
		return textB1VincoliOmologazione;
	}
	public void setTextB1VincoliOmologazione(String textB1VincoliOmologazione) {
		this.textB1VincoliOmologazione = textB1VincoliOmologazione;
	}
	public String getTextC0NoteInterne() {
		return textC0NoteInterne;
	}
	public void setTextC0NoteInterne(String textC0NoteInterne) {
		this.textC0NoteInterne = textC0NoteInterne;
	}
	public String getTextC2StatusSvil() {
		return textC2StatusSvil;
	}
	public void setTextC2StatusSvil(String textC2StatusSvil) {
		this.textC2StatusSvil = textC2StatusSvil;
	}
	public String getTextC4Capacity() {
		return textC4Capacity;
	}
	public void setTextC4Capacity(String textC4Capacity) {
		this.textC4Capacity = textC4Capacity;
	}
	public String getUrlImageP() {
		return urlImageP;
	}
	public void setUrlImageP(String urlImageP) {
		this.urlImageP = urlImageP;
	}
	public String getUrlImageS1() {
		return urlImageS1;
	}
	public void setUrlImageS1(String urlImageS1) {
		this.urlImageS1 = urlImageS1;
	}
	public String getUrlImageS2() {
		return urlImageS2;
	}
	public void setUrlImageS2(String urlImageS2) {
		this.urlImageS2 = urlImageS2;
	}
	public String getUrlImageS3() {
		return urlImageS3;
	}
	public void setUrlImageS3(String urlImageS3) {
		this.urlImageS3 = urlImageS3;
	}
	public String getUrlImageS4() {
		return urlImageS4;
	}
	public void setUrlImageS4(String urlImageS4) {
		this.urlImageS4 = urlImageS4;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("JPdfOptional [optId=");
		builder.append(optId);
		builder.append(", optCode=");
		builder.append(optCode);
		builder.append(", optDescription=");
		builder.append(optDescription);
		builder.append(", visible=");
		builder.append(visible);
		builder.append(", orderableText=");
		builder.append(orderableText);
		builder.append(", orderableFlag=");
		builder.append(orderableFlag);
		builder.append(", versionModel=");
		builder.append(versionModel);
		builder.append(", idModelOptionalSubGroup=");
		builder.append(idModelOptionalSubGroup);
		builder.append(", idModelOptionalGroup=");
		builder.append(idModelOptionalGroup);
		builder.append(", codGroup=");
		builder.append(codGroup);
		builder.append(", codSubgroup=");
		builder.append(codSubgroup);
		builder.append(", groupDescription=");
		builder.append(groupDescription);
		builder.append(", subgroupDescription=");
		builder.append(subgroupDescription);
		builder.append(", lastUpdate=");
		builder.append(lastUpdate);
		builder.append(", seq=");
		builder.append(seq);
		builder.append(", seqGroup=");
		builder.append(seqGroup);
		builder.append(", seqSubgroup=");
		builder.append(seqSubgroup);
		builder.append(", idImageP=");
		builder.append(idImageP);
		builder.append(", idImageS1=");
		builder.append(idImageS1);
		builder.append(", idImageS2=");
		builder.append(idImageS2);
		builder.append(", idImageS3=");
		builder.append(idImageS3);
		builder.append(", idImageS4=");
		builder.append(idImageS4);
		builder.append(", urlImageP=");
		builder.append(urlImageP);
		builder.append(", urlImageS1=");
		builder.append(urlImageS1);
		builder.append(", urlImageS2=");
		builder.append(urlImageS2);
		builder.append(", urlImageS3=");
		builder.append(urlImageS3);
		builder.append(", urlImageS4=");
		builder.append(urlImageS4);
		builder.append(", textA1Long=");
		builder.append(textA1Long);
		builder.append(", textA5VincoliOpt=");
		builder.append(textA5VincoliOpt);
		builder.append(", textB1VincoliOmologazione=");
		builder.append(textB1VincoliOmologazione);
		builder.append(", textC0NoteInterne=");
		builder.append(textC0NoteInterne);
		builder.append(", textC2StatusSvil=");
		builder.append(textC2StatusSvil);
		builder.append(", textC4Capacity=");
		builder.append(textC4Capacity);
		builder.append(", getOptId()=");
		builder.append(getOptId());
		builder.append(", getOptCode()=");
		builder.append(getOptCode());
		builder.append(", getOptDescription()=");
		builder.append(getOptDescription());
		builder.append(", getVisible()=");
		builder.append(getVisible());
		builder.append(", getOrderableText()=");
		builder.append(getOrderableText());
		builder.append(", getOrderableFlag()=");
		builder.append(getOrderableFlag());
		builder.append(", getVersionModel()=");
		builder.append(getVersionModel());
		builder.append(", getIdModelOptionalSubGroup()=");
		builder.append(getIdModelOptionalSubGroup());
		builder.append(", getIdModelOptionalGroup()=");
		builder.append(getIdModelOptionalGroup());
		builder.append(", getCodGroup()=");
		builder.append(getCodGroup());
		builder.append(", getCodSubgroup()=");
		builder.append(getCodSubgroup());
		builder.append(", getGroupDescription()=");
		builder.append(getGroupDescription());
		builder.append(", getSubgroupDescription()=");
		builder.append(getSubgroupDescription());
		builder.append(", getLastUpdate()=");
		builder.append(getLastUpdate());
		builder.append(", getSeq()=");
		builder.append(getSeq());
		builder.append(", getSeqGroup()=");
		builder.append(getSeqGroup());
		builder.append(", getSeqSubgroup()=");
		builder.append(getSeqSubgroup());
		builder.append(", getIdImageP()=");
		builder.append(getIdImageP());
		builder.append(", getIdImageS1()=");
		builder.append(getIdImageS1());
		builder.append(", getIdImageS2()=");
		builder.append(getIdImageS2());
		builder.append(", getIdImageS3()=");
		builder.append(getIdImageS3());
		builder.append(", getIdImageS4()=");
		builder.append(getIdImageS4());
		builder.append(", getTextA1Long()=");
		builder.append(getTextA1Long());
		builder.append(", getTextA5VincoliOpt()=");
		builder.append(getTextA5VincoliOpt());
		builder.append(", getTextB1VincoliOmologazione()=");
		builder.append(getTextB1VincoliOmologazione());
		builder.append(", getTextC0NoteInterne()=");
		builder.append(getTextC0NoteInterne());
		builder.append(", getTextC2StatusSvil()=");
		builder.append(getTextC2StatusSvil());
		builder.append(", getTextC4Capacity()=");
		builder.append(getTextC4Capacity());
		builder.append(", getUrlImageP()=");
		builder.append(getUrlImageP());
		builder.append(", getUrlImageS1()=");
		builder.append(getUrlImageS1());
		builder.append(", getUrlImageS2()=");
		builder.append(getUrlImageS2());
		builder.append(", getUrlImageS3()=");
		builder.append(getUrlImageS3());
		builder.append(", getUrlImageS4()=");
		builder.append(getUrlImageS4());
		builder.append(", getClass()=");
		builder.append(getClass());
		builder.append(", hashCode()=");
		builder.append(hashCode());
		builder.append(", toString()=");
		builder.append(super.toString());
		builder.append("]");
		return builder.toString();
	}

}
