package com.ferrari.modis.dpg.jentity.app;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.ferrari.modis.dpg.jentity.base.JPageAuth;

public class JVersion {
	private Long id;
	private String code;
	private Long idModelOptionalType;
	private String status;
	private String statusDescription;
	private Date publishDate;
	private String publishUser;
	private String description;
	private String descriptionLong;
	private Boolean accessible;
	private Boolean readonly;

	// ho anche le descrizioni in lingua in quanto in certe condizioni servono
	private String description_en;	
	private String description_it;	
	private String descriptionLong_en;	
	private String descriptionLong_it;	
	
	private String confidential;
	
	
	@JsonProperty("authorizations")
    @JsonInclude(Include.NON_NULL)
	List<JPageAuth> authorizations=null;	
	
	
	public JVersion(){
		this.authorizations=new ArrayList<JPageAuth>();
	}
	
	
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public Long getIdModelOptionalType() {
		return idModelOptionalType;
	}
	public void setIdModelOptionalType(Long idModelOptionalType) {
		this.idModelOptionalType = idModelOptionalType;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Date getPublishDate() {
		return publishDate;
	}
	public void setPublishDate(Date publishDate) {
		this.publishDate = publishDate;
	}
	public String getPublishUser() {
		return publishUser;
	}
	public void setPublishUser(String publishUser) {
		this.publishUser = publishUser;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Boolean getAccessible() {
		return accessible;
	}
	public void setAccessible(Boolean accessible) {
		this.accessible = accessible;
	}
	public String getStatusDescription() {
		return statusDescription;
	}
	public void setStatusDescription(String statusDescription) {
		this.statusDescription = statusDescription;
	}
	public Boolean getReadonly() {
		return readonly;
	}
	public void setReadonly(Boolean readonly) {
		this.readonly = readonly;
	}
	public String getDescription_en() {
		return description_en;
	}
	public void setDescription_en(String description_en) {
		this.description_en = description_en;
	}
	public String getDescription_it() {
		return description_it;
	}
	public void setDescription_it(String description_it) {
		this.description_it = description_it;
	}






	public List<JPageAuth> getAuthorizations() {
		return authorizations;
	}




	public void setAuthorizations(List<JPageAuth> authorizations) {
		this.authorizations = authorizations;
	}




	public String getDescriptionLong() {
		return descriptionLong;
	}
	public void setDescriptionLong(String descriptionLong) {
		this.descriptionLong = descriptionLong;
	}
	public String getDescriptionLong_en() {
		return descriptionLong_en;
	}
	public void setDescriptionLong_en(String descriptionLong_en) {
		this.descriptionLong_en = descriptionLong_en;
	}
	public String getDescriptionLong_it() {
		return descriptionLong_it;
	}
	public void setDescriptionLong_it(String descriptionLong_it) {
		this.descriptionLong_it = descriptionLong_it;
	}




	public String getConfidential() {
		return confidential;
	}




	public void setConfidential(String confidential) {
		this.confidential = confidential;
	}

}
