package com.ferrari.modis.dpg.jentity.base;

import java.util.HashMap;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;


public class JEntityData {

	
	
	
	private  HashMap<String, Object> actual  = null;
	
	public JEntityData(){
		this.actual=new HashMap<String, Object>();
	}
	
	@ApiModelProperty(value = "")
	@JsonProperty("actual")
	public HashMap<String, Object> getActual() {
		return actual;
	}
	public void setActual(HashMap<String, Object> actual) {
		this.actual = actual;
	}

	
	
	
}
