package com.ferrari.modis.dpg.jentity.app;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

// response per creazione del form SPLASHù

public class JModelType {
	

	Long idModel;
	
	@JsonProperty("model")
	JModel	jmodel;
	
	@JsonProperty("types")	
	List<JModelOptionalType> lJModelOptionalType;

	
	
	


	public List<JModelOptionalType> getlJModelOptionalType() {
		return lJModelOptionalType;
	}

	public void setlJModelOptionalType(List<JModelOptionalType> lJModelOptionalType) {
		this.lJModelOptionalType = lJModelOptionalType;
	}

	public Long getIdModel() {
		return idModel;
	}

	public void setIdModel(Long idModel) {
		this.idModel = idModel;
	}

	public JModel getJmodel() {
		return jmodel;
	}

	public void setJmodel(JModel jmodel) {
		this.jmodel = jmodel;
	}
	
	
	
}
