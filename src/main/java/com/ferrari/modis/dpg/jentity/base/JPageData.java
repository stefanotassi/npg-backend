package com.ferrari.modis.dpg.jentity.base;

import java.util.HashMap;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;


public class JPageData {

	
	
	private  Object actual = null;
	
	

	
	public JPageData(){
		this.actual=new HashMap<String, Object>();
	}
	
	@ApiModelProperty(value = "")
	@JsonProperty("actual")
	public Object getActual() {
		return actual;
	}
	public void setActual(Object actual) {
		this.actual = actual;
	}

	
	
	
}
