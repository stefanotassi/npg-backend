package com.ferrari.modis.dpg.jentity.app;

public class JVersionOpt {
	// dettaglio opt per versione, versione minimale per lista optional
	
	private Long id;
	private String code;
	private String description;
	private Long sequence;
	private Boolean choice; // serve per creazione albero per download
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Long getSequence() {
		return sequence;
	}
	public void setSequence(Long sequence) {
		this.sequence = sequence;
	}
	public Boolean getChoice() {
		return choice;
	}
	public void setChoice(Boolean choice) {
		this.choice = choice;
	}
	
	
	
}
