package com.ferrari.modis.dpg.jentity.app;

public class JVersionOptText {
	
	private Long id;
//	private Long idVersionOpt;
	private String textType;
	private String description;
	 
	private String textTypeDescr;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
//	public Long getIdVersionOpt() {
//		return idVersionOpt;
//	}
//	public void setIdVersionOpt(Long idVersionOpt) {
//		this.idVersionOpt = idVersionOpt;
//	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getTextType() {
		return textType;
	}
	public void setTextType(String textType) {
		this.textType = textType;
	}
	public String getTextTypeDescr() {
		return textTypeDescr;
	}
	public void setTextTypeDescr(String textTypeDescr) {
		this.textTypeDescr = textTypeDescr;
	}
	
}
	
	
	
