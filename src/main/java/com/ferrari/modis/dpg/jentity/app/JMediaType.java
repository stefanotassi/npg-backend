package com.ferrari.modis.dpg.jentity.app;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class JMediaType {
	 
	

	private String 	id;
	//private String types;
	private Long minx;
	private Long miny;
	private Double ratiox;
	private Double ratioy;
	
	@JsonProperty("types")	
	private List<String> types;
	
	
	public JMediaType() {
		types = new ArrayList<String>();
	}
	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	public List<String> getTypes() {
		return types;
	}
	public void setTypes(List<String> types) {
		this.types = types;
	}
	public Long getMinx() {
		return minx;
	}
	public void setMinx(Long minx) {
		this.minx = minx;
	}
	public Long getMiny() {
		return miny;
	}
	public void setMiny(Long miny) {
		this.miny = miny;
	}
	public Double getRatiox() {
		return ratiox;
	}
	public void setRatiox(Double ratiox) {
		this.ratiox = ratiox;
	}
	public Double getRatioy() {
		return ratioy;
	}
	public void setRatioy(Double ratioy) {
		this.ratioy = ratioy;
	}
}
