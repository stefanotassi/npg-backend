package com.ferrari.modis.dpg.jentity.app;

public class JPdfBaseLabels {
	
	// labels per generazione PDF
	private String labelHeader;
	private String labelIndex;
	private String labelOptional;
	private String labelDrawing;
	private String labelGrid;
	private String labelWatermark;  	// es DRAFT
	private String labelUsage;  		// es Internal Use ONLY (se user HQ*)
	
	private String labelRules;
	private String labelWith;
	private String labelHomologation;
	private String labelNote;
	private String labelStatus;
	private String labelCapacity;
	private String labelOrderableText;
	private String labelLastUpdate;
	
	private String labelDate;
	private String labelPage;
	
	
	
	
	public String getLabelDate() {
		return labelDate;
	}
	public void setLabelDate(String labelDate) {
		this.labelDate = labelDate;
	}
	public String getLabelPage() {
		return labelPage;
	}
	public void setLabelPage(String labelPage) {
		this.labelPage = labelPage;
	}
	private String logoFerrari;
	private String urlLogoFerrari;	
	
	public String getLabelHeader() {
		return labelHeader;
	}
	public void setLabelHeader(String labelHeader) {
		this.labelHeader = labelHeader;
	}
	public String getLabelIndex() {
		return labelIndex;
	}
	public void setLabelIndex(String labelIndex) {
		this.labelIndex = labelIndex;
	}
	public String getLabelOptional() {
		return labelOptional;
	}
	public void setLabelOptional(String labelOptional) {
		this.labelOptional = labelOptional;
	}
	public String getLabelDrawing() {
		return labelDrawing;
	}
	public void setLabelDrawing(String labelDrawing) {
		this.labelDrawing = labelDrawing;
	}
	public String getLabelGrid() {
		return labelGrid;
	}
	public void setLabelGrid(String labelGrid) {
		this.labelGrid = labelGrid;
	}
	public String getLabelWatermark() {
		return labelWatermark;
	}
	public void setLabelWatermark(String labelWatermark) {
		this.labelWatermark = labelWatermark;
	}
	public String getLabelUsage() {
		return labelUsage;
	}
	public void setLabelUsage(String labelUsage) {
		this.labelUsage = labelUsage;
	}
	
	public String getLogoFerrari() {
		return logoFerrari;
	}
	public void setLogoFerrari(String logoFerrari) {
		this.logoFerrari = logoFerrari;
	}
	
	public String getUrlLogoFerrari() {
		return urlLogoFerrari;
	}
	public void setUrlLogoFerrari(String urlLogoFerrari) {
		this.urlLogoFerrari = urlLogoFerrari;
	}
	
	
	
	public String getLabelRules() {
		return labelRules;
	}
	public void setLabelRules(String labelRules) {
		this.labelRules = labelRules;
	}
	public String getLabelWith() {
		return labelWith;
	}
	public void setLabelWith(String labelWith) {
		this.labelWith = labelWith;
	}
	public String getLabelHomologation() {
		return labelHomologation;
	}
	public void setLabelHomologation(String labelHomologation) {
		this.labelHomologation = labelHomologation;
	}
	public String getLabelNote() {
		return labelNote;
	}
	public void setLabelNote(String labelNote) {
		this.labelNote = labelNote;
	}
	public String getLabelStatus() {
		return labelStatus;
	}
	public void setLabelStatus(String labelStatus) {
		this.labelStatus = labelStatus;
	}
	public String getLabelCapacity() {
		return labelCapacity;
	}
	public void setLabelCapacity(String labelCapacity) {
		this.labelCapacity = labelCapacity;
	}
	
	public String getLabelOrderableText() {
		return labelOrderableText;
	}
	public void setLabelOrderableText(String labelOrderableText) {
		this.labelOrderableText = labelOrderableText;
	}
	public String getLabelLastUpdate() {
		return labelLastUpdate;
	}
	public void setLabelLastUpdate(String labelLastUpdate) {
		this.labelLastUpdate = labelLastUpdate;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("JPdfBaseLabels [labelHeader=");
		builder.append(labelHeader);
		builder.append(", labelIndex=");
		builder.append(labelIndex);
		builder.append(", labelOptional=");
		builder.append(labelOptional);
		builder.append(", labelDrawing=");
		builder.append(labelDrawing);
		builder.append(", labelGrid=");
		builder.append(labelGrid);
		builder.append(", labelWatermark=");
		builder.append(labelWatermark);
		builder.append(", labelUsage=");
		builder.append(labelUsage);
		builder.append(", labelRules=");
		builder.append(labelRules);
		builder.append(", labelWith=");
		builder.append(labelWith);
		builder.append(", labelHomologation=");
		builder.append(labelHomologation);
		builder.append(", labelNote=");
		builder.append(labelNote);
		builder.append(", labelStatus=");
		builder.append(labelStatus);
		builder.append(", labelCapacity=");
		builder.append(labelCapacity);
		builder.append(", labelOrderableText=");
		builder.append(labelOrderableText);
		builder.append(", labelLastUpdate=");
		builder.append(labelLastUpdate);
		builder.append(", logoFerrari=");
		builder.append(logoFerrari);
		builder.append(", urlLogoFerrari=");
		builder.append(urlLogoFerrari);
		builder.append("]");
		return builder.toString();
	}



}
