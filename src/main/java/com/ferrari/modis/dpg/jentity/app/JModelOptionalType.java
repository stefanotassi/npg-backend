package com.ferrari.modis.dpg.jentity.app;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class JModelOptionalType {
	
	Long id;
	Long idModel;
	String idModelOptionalType;
	String description;
	String idImage;
	String urlImage;

		 
	@JsonProperty("groups")	
	List<JModelOptionalGroup> lJModelOptionalGroup;

	
	
	
	public JModelOptionalType() {
		lJModelOptionalGroup = new ArrayList<JModelOptionalGroup>();
	}

	
	
	
	
	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public Long getIdModel() {
		return idModel;
	}


	public void setIdModel(Long idModel) {
		this.idModel = idModel;
	}


	public String getIdModelOptionalType() {
		return idModelOptionalType;
	}


	public void setIdModelOptionalType(String idModelOptionalType) {
		this.idModelOptionalType = idModelOptionalType;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public String getIdImage() {
		return idImage;
	}


	public void setIdImage(String idImage) {
		this.idImage = idImage;
	}


	public String getUrlImage() {
		return urlImage;
	}


	public void setUrlImage(String urlImage) {
		this.urlImage = urlImage;
	}


	public List<JModelOptionalGroup> getlJModelOptionalGroup() {
		return lJModelOptionalGroup;
	}


	public void setlJModelOptionalGroup(List<JModelOptionalGroup> lJModelOptionalGroup) {
		this.lJModelOptionalGroup = lJModelOptionalGroup;
	}
	
	
	
	

	
	
	
	
	
}
