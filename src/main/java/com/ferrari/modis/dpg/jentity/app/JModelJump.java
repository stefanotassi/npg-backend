package com.ferrari.modis.dpg.jentity.app;

// per presentazione del id P1/P2 corrente + sistema di switch

public class JModelJump {

	Long id;
	String idModelOptionaType; // P1, P2

	// se esiste Px (es P2)
	Long swapId;
	String swapIdModelOptionaType; // P1, P2
	Long swapIdVersion;
	
	
	
	
	public Long getId() {
		return id;
	}

	public String getIdModelOptionaType() {
		return idModelOptionaType;
	}

	public void setIdModelOptionaType(String idModelOptionaType) {
		this.idModelOptionaType = idModelOptionaType;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("JModelType [id=");
		builder.append(id);
		builder.append(", idModelOptionaType=");
		builder.append(idModelOptionaType);
		builder.append(", swapId=");
		builder.append(swapId);
		builder.append(", swapIdModelOptionaType=");
		builder.append(swapIdModelOptionaType);
		builder.append(", swapIdVersion=");
		builder.append(swapIdVersion);
		builder.append("]");
		return builder.toString();
	}

	public Long getSwapId() {
		return swapId;
	}

	public void setSwapId(Long swapId) {
		this.swapId = swapId;
	}

	public String getSwapIdModelOptionaType() {
		return swapIdModelOptionaType;
	}

	public void setSwapIdModelOptionaType(String swapIdModelOptionaType) {
		this.swapIdModelOptionaType = swapIdModelOptionaType;
	}

	public Long getSwapIdVersion() {
		return swapIdVersion;
	}

	public void setSwapIdVersion(Long swapIdVersion) {
		this.swapIdVersion = swapIdVersion;
	}

}
