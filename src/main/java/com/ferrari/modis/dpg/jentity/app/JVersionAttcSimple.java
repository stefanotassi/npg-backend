package com.ferrari.modis.dpg.jentity.app;

public class JVersionAttcSimple {
	// serve per trasferire informazioni per scegliere Figurini in fase di download/generazione PDF
	
	private Long id;
	private String description;
	private String attachmentType; // F/O
	private Long sequence;
	private Boolean choice; // serve per creazione albero per download
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getAttachmentType() {
		return attachmentType;
	}
	public void setAttachmentType(String attachmentType) {
		this.attachmentType = attachmentType;
	}
	public Long getSequence() {
		return sequence;
	}
	public void setSequence(Long sequence) {
		this.sequence = sequence;
	}
	public Boolean getChoice() {
		return choice;
	}
	public void setChoice(Boolean choice) {
		this.choice = choice;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("JVersionAttcSimple [id=");
		builder.append(id);
		builder.append(", description=");
		builder.append(description);
		builder.append(", attachmentType=");
		builder.append(attachmentType);
		builder.append(", sequence=");
		builder.append(sequence);
		builder.append(", choice=");
		builder.append(choice);
		builder.append("]");
		return builder.toString();
	}



	
}
