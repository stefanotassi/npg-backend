package com.ferrari.modis.dpg.jentity.base;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.ferrari.modis.dpg.commons.i18n.LocalizationType;

import io.swagger.annotations.ApiModelProperty;


	
@JsonPropertyOrder({ "id", "name", "language", "data", "authorizations" })
public class JPage {
	String id;
	String name;
	LocalizationType language;
	JPageData data=null; 			// payload data del json
	List<JPageAuth> authorizations=null;	
	JPagination jPagination=null;
	
	
	public JPage(){
		this.data=new JPageData();
		this.authorizations=new ArrayList<JPageAuth>();
	}

	
	@ApiModelProperty(value = "")
	@JsonProperty("id")
	public String getId() {
		return id;
	}
	@ApiModelProperty(value = "")
	@JsonProperty("name")
	public String getName() {
		return name;
	}
	@ApiModelProperty(value = "")
	@JsonProperty("data")
	public JPageData getData() {
		return data;
	}	
	@ApiModelProperty(value = "")
	@JsonProperty("authorizations")
	public List<JPageAuth> getAuths() {
		return authorizations;
	}
	@JsonProperty("lang")	
	public LocalizationType getLanguage() {
		return language;
	}

	
	

	public void setId(String id) {
		this.id = id;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setData(JPageData data) {
		this.data = data;
	}
	public void setAuths(List<JPageAuth> auths) {
		this.authorizations = auths;
	}




	public void setLanguage(LocalizationType language) {
		this.language = language;
	}

	@JsonProperty("pagination")	
	public JPagination getjPagination() {
		return jPagination;
	}


	public void setjPagination(JPagination jPagination) {
		this.jPagination = jPagination;
	}
	







	
}
