package com.ferrari.modis.dpg.jentity.app;

public class JMediaConfig {
	private String id;
	
	private Double imagePCompressRate;
	private Double imagePResizeFactor;
	private Double imageSCompressRate;
	private Double imageSResizeFactor;
	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Double getImagePCompressRate() {
		return imagePCompressRate;
	}
	public void setImagePCompressRate(Double imagePCompressRate) {
		this.imagePCompressRate = imagePCompressRate;
	}
	public Double getImagePResizeFactor() {
		return imagePResizeFactor;
	}
	public void setImagePResizeFactor(Double imagePResizeFactor) {
		this.imagePResizeFactor = imagePResizeFactor;
	}
	public Double getImageSCompressRate() {
		return imageSCompressRate;
	}
	public void setImageSCompressRate(Double imageSCompressRate) {
		this.imageSCompressRate = imageSCompressRate;
	}
	public Double getImageSResizeFactor() {
		return imageSResizeFactor;
	}
	public void setImageSResizeFactor(Double imageSResizeFactor) {
		this.imageSResizeFactor = imageSResizeFactor;
	}
	
	
}
