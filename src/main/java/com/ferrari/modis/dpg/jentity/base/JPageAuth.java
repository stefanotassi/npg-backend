package com.ferrari.modis.dpg.jentity.base;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;


@JsonPropertyOrder({ "authId", "authDescription", "permission" })
public class JPageAuth {
	String authId;
	String authDescription;
	String permission;
	
	public String getAuthId() {
		return authId;
	}
	public String getPermission() {
		return permission;
	}
	public void setAuthId(String authId) {
		this.authId = authId;
	}
	public void setPermission(String permission) {
		this.permission = permission;
	}
	public String getAuthDescription() {
		return authDescription;
	}
	public void setAuthDescription(String authDescription) {
		this.authDescription = authDescription;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("JPageAuth [authId=");
		builder.append(authId);
		builder.append(", authDescription=");
		builder.append(authDescription);
		builder.append(", permission=");
		builder.append(permission);
		builder.append("]");
		return builder.toString();
	}
	
}
