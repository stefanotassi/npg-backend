package com.ferrari.modis.dpg.jentity.app;
// usata in fase di importazione da excel

public class JVersionOptImport {
	
	private String  excelRow;
	private Long idVersion;
	private Long id;
	private String operation;
	private String code;
	private String description_it;
	private String description_en;
	private Long sequence;
	
	private Long idModelOptionalSubGroup;
	private String visible;
	private String orderable_it;
	private String orderable_en;
	private String orderableFlag;
	private String versionModel_it;
	private String versionModel_en;

	private String textA1_it;
	private String textA5_it;
	private String textB1_it;
	private String textC0_it;
	private String textC2_it;
	private String textC4_it;
	
	private String textA1_en;
	private String textA5_en;
	private String textB1_en;
	private String textC0_en;
	private String textC2_en;
	private String textC4_en;
	
	
	
	
	public JVersionOptImport() {
		
		description_it="";
		description_en="";
		sequence=0L;

		visible="";
		orderable_it="";
		orderable_en="";
		orderableFlag="";
		versionModel_it="";
		versionModel_en="";

		textA1_it="";
		textA5_it="";
		textB1_it="";
		textC0_it="";
		textC2_it="";
		textC4_it="";
		
		textA1_en="";
		textA5_en="";
		textB1_en="";
		textC0_en="";
		textC2_en="";
		textC4_en="";
	}
	
	public Long getIdVersion() {
		return idVersion;
	}
	public void setIdVersion(Long idVersion) {
		this.idVersion = idVersion;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getOperation() {
		return operation;
	}
	public void setOperation(String operation) {
		this.operation = operation;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getDescription_it() {
		return description_it;
	}
	public void setDescription_it(String description_it) {
		this.description_it = description_it;
	}
	public String getDescription_en() {
		return description_en;
	}
	public void setDescription_en(String description_en) {
		this.description_en = description_en;
	}
	public Long getSequence() {
		return sequence;
	}
	public void setSequence(Long sequence) {
		this.sequence = sequence;
	}
	public Long getIdModelOptionalSubGroup() {
		return idModelOptionalSubGroup;
	}
	public void setIdModelOptionalSubGroup(Long idModelOptionalSubGroup) {
		this.idModelOptionalSubGroup = idModelOptionalSubGroup;
	}
	public String getVisible() {
		return visible;
	}
	public void setVisible(String visible) {
		this.visible = visible;
	}
	public String getOrderable_it() {
		return orderable_it;
	}
	public void setOrderable_it(String orderable_it) {
		this.orderable_it = orderable_it;
	}
	public String getOrderable_en() {
		return orderable_en;
	}
	public void setOrderable_en(String orderable_en) {
		this.orderable_en = orderable_en;
	}
	public String getOrderableFlag() {
		return orderableFlag;
	}
	public void setOrderableFlag(String orderableFlag) {
		this.orderableFlag = orderableFlag;
	}
	public String getVersionModel_it() {
		return versionModel_it;
	}
	public void setVersionModel_it(String versionModel_it) {
		this.versionModel_it = versionModel_it;
	}
	public String getVersionModel_en() {
		return versionModel_en;
	}
	public void setVersionModel_en(String versionModel_en) {
		this.versionModel_en = versionModel_en;
	}
	public String getTextA1_it() {
		return textA1_it;
	}
	public void setTextA1_it(String textA1_it) {
		this.textA1_it = textA1_it;
	}
	public String getTextA5_it() {
		return textA5_it;
	}
	public void setTextA5_it(String textA5_it) {
		this.textA5_it = textA5_it;
	}
	public String getTextB1_it() {
		return textB1_it;
	}
	public void setTextB1_it(String textB1_it) {
		this.textB1_it = textB1_it;
	}
	public String getTextC0_it() {
		return textC0_it;
	}
	public void setTextC0_it(String textC0_it) {
		this.textC0_it = textC0_it;
	}
	public String getTextC2_it() {
		return textC2_it;
	}
	public void setTextC2_it(String textC2_it) {
		this.textC2_it = textC2_it;
	}
	public String getTextC4_it() {
		return textC4_it;
	}
	public void setTextC4_it(String textC4_it) {
		this.textC4_it = textC4_it;
	}
	public String getTextA1_en() {
		return textA1_en;
	}
	public void setTextA1_en(String textA1_en) {
		this.textA1_en = textA1_en;
	}
	public String getTextA5_en() {
		return textA5_en;
	}
	public void setTextA5_en(String textA5_en) {
		this.textA5_en = textA5_en;
	}
	public String getTextB1_en() {
		return textB1_en;
	}
	public void setTextB1_en(String textB1_en) {
		this.textB1_en = textB1_en;
	}
	public String getTextC0_en() {
		return textC0_en;
	}
	public void setTextC0_en(String textC0_en) {
		this.textC0_en = textC0_en;
	}
	public String getTextC2_en() {
		return textC2_en;
	}
	public void setTextC2_en(String textC2_en) {
		this.textC2_en = textC2_en;
	}
	public String getTextC4_en() {
		return textC4_en;
	}
	public void setTextC4_en(String textC4_en) {
		this.textC4_en = textC4_en;
	}

	public String getExcelRow() {
		return excelRow;
	}

	public void setExcelRow(String excelRow) {
		this.excelRow = excelRow;
	}




	

}
