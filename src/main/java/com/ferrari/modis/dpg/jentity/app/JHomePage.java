package com.ferrari.modis.dpg.jentity.app;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
//usata in homepage

public class JHomePage {
	
	


	List<JModelCluster> lJModelCluster;
	String idImageNew;
	String urlImageNew;
	
	
	public JHomePage() {
		lJModelCluster = new ArrayList<JModelCluster>();
	}


	
	@JsonProperty("modelCluster")
	public List<JModelCluster> getlJModelCluster() {
		return lJModelCluster;
	}

	public void setlJModelCluster(List<JModelCluster> lJModelCluster) {
		this.lJModelCluster = lJModelCluster;
	}
	
	@JsonProperty("idImageNew")
	public String getIdImageNew() {
		return idImageNew;
	}

	public void setIdImageNew(String idImageNew) {
		this.idImageNew = idImageNew;
	}


	@JsonProperty("urlImageNew")
	public String getUrlImageNew() {
		return urlImageNew;
	}



	public void setUrlImageNew(String urlImageNew) {
		this.urlImageNew = urlImageNew;
	}

	
	

	
}
