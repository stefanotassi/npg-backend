package com.ferrari.modis.dpg.jentity.app;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class JVersionAttc {
	// dettaglio doc per versione, versione minimale per lista documenti
	
	private Long id;
	private String description;
	private String attachmentType; // F/O
	
	private String idImage;
	private String idImageThumb;
	private String idImageSvg;
	
	private String urlImage;
	private String urlImageThumb;
	private String urlImageSvg;
	
	private Long sequence;
	private Boolean choice; // serve per creazione albero per download

	// per FORM maint descrizioni
	private String description_it;
	private String description_en;
	
	private String contentType; 
	
	private String mediaTemplateImage;
	private String mediaTemplateImageThumb;	
	private String mediaTemplateImageSvg;	
	
	private String filename;
	
	@JsonProperty("optionals")
	private List<JVersionOpt> lJVersionOpt; 
	
	
	@JsonProperty("model")
	private JModel	jModel;
	
	// TODO JModelType add @JsonProperty("type") type JModelType
	@JsonProperty("type")
	JModelJump	jmodelJump;
	
	@JsonProperty("version")
	private JVersion jVersion;

	
	
	public JVersionAttc() {
		lJVersionOpt = new ArrayList<JVersionOpt>();
	}
	
	  
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Long getSequence() {
		return sequence;
	}
	public void setSequence(Long sequence) {
		this.sequence = sequence;
	}
	public Boolean getChoice() {
		return choice;
	}
	public void setChoice(Boolean choice) {
		this.choice = choice;
	}
	public String getAttachmentType() {
		return attachmentType;
	}
	public void setAttachmentType(String attachmentType) {
		this.attachmentType = attachmentType;
	}
	public String getIdImage() {
		return idImage;
	}
	public void setIdImage(String idImage) {
		this.idImage = idImage;
	}
	public String getIdImageThumb() {
		return idImageThumb;
	}
	public void setIdImageThumb(String idImageThumb) {
		this.idImageThumb = idImageThumb;
	}
	public String getDescription_it() {
		return description_it;
	}
	public void setDescription_it(String description_it) {
		this.description_it = description_it;
	}
	public String getDescription_en() {
		return description_en;
	}
	public void setDescription_en(String description_en) {
		this.description_en = description_en;
	}
	public List<JVersionOpt> getlJVersionOpt() {
		return lJVersionOpt;
	}
	public void setlJVersionOpt(List<JVersionOpt> lJVersionOpt) {
		this.lJVersionOpt = lJVersionOpt;
	}


	public String getContentType() {
		return contentType;
	}


	public void setContentType(String contentType) {
		this.contentType = contentType;
	}


	public String getMediaTemplateImage() {
		return mediaTemplateImage;
	}


	public void setMediaTemplateImage(String mediaTemplateImage) {
		this.mediaTemplateImage = mediaTemplateImage;
	}


	public String getMediaTemplateImageThumb() {
		return mediaTemplateImageThumb;
	}


	public void setMediaTemplateImageThumb(String mediaTemplateImageThumb) {
		this.mediaTemplateImageThumb = mediaTemplateImageThumb;
	}


	public String getUrlImage() {
		return urlImage;
	}


	public void setUrlImage(String urlImage) {
		this.urlImage = urlImage;
	}


	public String getUrlImageThumb() {
		return urlImageThumb;
	}


	public void setUrlImageThumb(String urlImageThumb) {
		this.urlImageThumb = urlImageThumb;
	}


	public String getFilename() {
		return filename;
	}


	public void setFilename(String filename) {
		this.filename = filename;
	}


	public JModel getjModel() {
		return jModel;
	}


	public void setjModel(JModel jModel) {
		this.jModel = jModel;
	}


	public JVersion getjVersion() {
		return jVersion;
	}


	public void setjVersion(JVersion jVersion) {
		this.jVersion = jVersion;
	}


	public String getIdImageSvg() {
		return idImageSvg;
	}


	public void setIdImageSvg(String idImageSvg) {
		this.idImageSvg = idImageSvg;
	}


	public String getUrlImageSvg() {
		return urlImageSvg;
	}


	public void setUrlImageSvg(String urlImageSvg) {
		this.urlImageSvg = urlImageSvg;
	}


	public String getMediaTemplateImageSvg() {
		return mediaTemplateImageSvg;
	}


	public void setMediaTemplateImageSvg(String mediaTemplateImageSvg) {
		this.mediaTemplateImageSvg = mediaTemplateImageSvg;
	}


	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("JVersionAttc [id=");
		builder.append(id);
		builder.append(", description=");
		builder.append(description);
		builder.append(", attachmentType=");
		builder.append(attachmentType);
		builder.append(", idImage=");
		builder.append(idImage);
		builder.append(", idImageThumb=");
		builder.append(idImageThumb);
		builder.append(", idImageSvg=");
		builder.append(idImageSvg);
		
		builder.append(", urlImage=");
		builder.append(urlImage);
		builder.append(", urlImageThumb=");
		builder.append(urlImageThumb);
		builder.append(", urlImageSvg=");
		builder.append(urlImageSvg);
		
		
		builder.append(", sequence=");
		builder.append(sequence);
		builder.append(", choice=");
		builder.append(choice);
		builder.append(", description_it=");
		builder.append(description_it);
		builder.append(", description_en=");
		builder.append(description_en);
		builder.append(", contentType=");
		builder.append(contentType);
		builder.append(", mediaTemplateImage=");
		builder.append(mediaTemplateImage);
		builder.append(", mediaTemplateImageThumb=");
		builder.append(mediaTemplateImageThumb);
		builder.append(", filename=");
		builder.append(filename);
		builder.append(", lJVersionOpt=");
		builder.append(lJVersionOpt);
		builder.append(", jModel=");
		builder.append(jModel);
		builder.append(", jVersion=");
		builder.append(jVersion);
		builder.append("]");
		return builder.toString();
	}


	public JModelJump getJModelJump() {
		return jmodelJump;
	}


	public void setJModelJump(JModelJump jmodelJump) {
		this.jmodelJump = jmodelJump;
	}


	
}
