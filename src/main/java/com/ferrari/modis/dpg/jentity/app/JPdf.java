package com.ferrari.modis.dpg.jentity.app;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.ferrari.modis.dpg.jentity.base.JPageAuth;

public class JPdf {
	
	@JsonProperty("version")
	private JVersion jVersion;
	@JsonProperty("model")
	private JModel jModel;
	
	@JsonProperty("pdfType")
	private JPdfType jPdfType;
	
	@JsonProperty("optionals")
	private List<JPdfOptional> jPdfOptionals;	
	
	@JsonProperty("drawings")
	private List<JPdfDrawing> jPdfDrawings;	
	
	@JsonProperty("pdfLabels")
	private JPdfBaseLabels jPdfBaseLabels;
	@JsonProperty("gridLabels")
	private JXlsBaseLabels jXlsBaseLabels;
	
	@JsonProperty("authorizations")
	private List<JPageAuth> jPageAuth;

	
	public JPdf(){
		this.jPdfOptionals=new ArrayList<JPdfOptional>();
		this.jPdfDrawings=new ArrayList<JPdfDrawing>();		
		this.jPageAuth=new ArrayList<JPageAuth>();		
	}

	public JVersion getjVersion() {
		return jVersion;
	}

	public void setjVersion(JVersion jVersion) {
		this.jVersion = jVersion;
	}

	public JModel getjModel() {
		return jModel;
	}

	public void setjModel(JModel jModel) {
		this.jModel = jModel;
	}

	


	public List<JPdfOptional> getjPdfOptionals() {
		return jPdfOptionals;
	}

	public void setjPdfOptionals(List<JPdfOptional> jPdfOptionals) {
		this.jPdfOptionals = jPdfOptionals;
	}

	public List<JPdfDrawing> getjPdfDrawings() {
		return jPdfDrawings;
	}

	public void setjPdfDrawings(List<JPdfDrawing> jPdfDrawings) {
		this.jPdfDrawings = jPdfDrawings;
	}

	
	


	public JXlsBaseLabels getjXlsBaseLabels() {
		return jXlsBaseLabels;
	}

	public void setjXlsBaseLabels(JXlsBaseLabels jXlsBaseLabels) {
		this.jXlsBaseLabels = jXlsBaseLabels;
	}

	public JPdfBaseLabels getjPdfBaseLabels() {
		return jPdfBaseLabels;
	}

	public void setjPdfBaseLabels(JPdfBaseLabels jPdfBaseLabels) {
		this.jPdfBaseLabels = jPdfBaseLabels;
	}

	public List<JPageAuth> getjPageAuth() {
		return jPageAuth;
	}

	public void setjPageAuth(List<JPageAuth> jPageAuth) {
		this.jPageAuth = jPageAuth;
	}

	public JPdfType getjPdfType() {
		return jPdfType;
	}

	public void setjPdfType(JPdfType jPdfType) {
		this.jPdfType = jPdfType;
	}






}
