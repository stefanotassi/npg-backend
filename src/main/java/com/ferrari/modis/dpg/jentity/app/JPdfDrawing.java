package com.ferrari.modis.dpg.jentity.app;

public class JPdfDrawing {


	private Long id;
	private String description;
	private String idImage;
	private String idImageThumb;
	private String idImageSvg;
	
	private String urlImage;
	private String urlImageThumb;
	private String urlImageSvg;
	
	private Long seq;	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getIdImage() {
		return idImage;
	}
	public void setIdImage(String idImage) {
		this.idImage = idImage;
	}
	public String getIdImageThumb() {
		return idImageThumb;
	}
	public void setIdImageThumb(String idImageThumb) {
		this.idImageThumb = idImageThumb;
	}
	public Long getSeq() {
		return seq;
	}
	public void setSeq(Long seq) {
		this.seq = seq;
	}
	public String getUrlImage() {
		return urlImage;
	}
	public void setUrlImage(String urlImage) {
		this.urlImage = urlImage;
	}
	public String getUrlImageThumb() {
		return urlImageThumb;
	}
	public void setUrlImageThumb(String urlImageThumb) {
		this.urlImageThumb = urlImageThumb;
	}
	public String getIdImageSvg() {
		return idImageSvg;
	}
	public void setIdImageSvg(String idImageSvg) {
		this.idImageSvg = idImageSvg;
	}
	public String getUrlImageSvg() {
		return urlImageSvg;
	}
	public void setUrlImageSvg(String urlImageSvg) {
		this.urlImageSvg = urlImageSvg;
	}
	
}
