package com.ferrari.modis.dpg.jentity.app;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class JVersionForm {
// form di scelta opt/figurini/altro
	
	@JsonProperty("forms")
	List<JForm> lJform;	
	
	@JsonProperty("model")
	JModel	jmodel;

	// TODO JModelType add @JsonProperty("type") type JModelType
	@JsonProperty("type")
	JModelJump	jmodelJump;
	
	@JsonProperty("versionActual")
	JVersion	jversion;

	@JsonProperty("versions")
	List<JVersion> lJVersion;
	

	
	
	public JVersionForm() {
		lJform = new ArrayList<JForm>();
		lJVersion = new ArrayList<JVersion>();
	}

	public List<JForm> getlJform() {
		return lJform;
	}
	public void setlJform(List<JForm> lJform) {
		this.lJform = lJform;
	}



	public JModel getJmodel() {
		return jmodel;
	}
	public void setJmodel(JModel jmodel) {
		this.jmodel = jmodel;
	}

	public JVersion getJversion() {
		return jversion;
	}

	public void setJversion(JVersion jversion) {
		this.jversion = jversion;
	}

	public List<JVersion> getlJVersion() {
		return lJVersion;
	}

	public void setlJVersion(List<JVersion> lJVersion) {
		this.lJVersion = lJVersion;
	}

	public JModelJump getJModelJump() {
		return jmodelJump;
	}

	public void setJModelJump(JModelJump jmodelJump) {
		this.jmodelJump = jmodelJump;
	}
	
	 

}
